<?php
/**
 * @author walter
 */

namespace frontend\helpers;


use yii\helpers\Html;

/**
 * Class ExtendedHtml
 * @package frontend\helpers
 */
class BodyTagClassHelper extends Html
{

    public static function getBodyClass($controllerId)
    {
        switch ($controllerId) {
            case 'about/index':
                $result = 'about-page';
                break;
            case 'home/index':
                $result = 'home-page preload-progress preload-cover';
                break;
            case 'services/index':
                $result = 'services-page';
                break;
            case 'media/index':
                $result = 'media-page';
                break;
            case 'content-pages/index':
                $result = 'serv-page';
                break;
            case 'contacts/index':
                $result = 'contacts-page';
                break;
            default:
                $result = 'f0f-page';
                break;
        }
        return $result;
    }

}