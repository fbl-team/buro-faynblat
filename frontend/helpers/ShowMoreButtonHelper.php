<?php
namespace frontend\helpers;

use common\components\model\Helper;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


/**
 * Site controller
 */
class ShowMoreButtonHelper
{
    const TYPE_MEDIA_WRITE_BLOCK = 1;
    const TYPE_MEDIA_SPEEK_BLOCK = 2;
    const TYPE_ABOUT_DIPLOMS_BLOCK = 3;
    const TYPE_ONE_PROJECT_ANOTHER_PROJECT = 4;
    const TYPE_ONE_PROJECT_CONTENT = 5;
    const TYPE_ONE_BLOG_ANOTHER_BLOG = 6;
    const TYPE_ONE_BLOG_CONTENT = 7;

    public static function getStepByType($type)
    {
        $stepArray = [
            self::TYPE_MEDIA_WRITE_BLOCK => 3,
            self::TYPE_MEDIA_SPEEK_BLOCK => 2,
            self::TYPE_ABOUT_DIPLOMS_BLOCK => 3,
            self::TYPE_ONE_PROJECT_ANOTHER_PROJECT => 3,
            self::TYPE_ONE_PROJECT_CONTENT => 3,
            self::TYPE_ONE_BLOG_ANOTHER_BLOG => 3,
            self::TYPE_ONE_BLOG_CONTENT => 3,
        ];
        return $stepArray[$type];
    }

    public static function getAjaxReplaceClass($type)
    {
        $classArray = [
            self::TYPE_MEDIA_WRITE_BLOCK => 'load-more-ajax',
            self::TYPE_MEDIA_SPEEK_BLOCK => 'load-more-ajax-second-buttton-on-page',
            self::TYPE_ABOUT_DIPLOMS_BLOCK => 'load-more-ajax',
            self::TYPE_ONE_PROJECT_ANOTHER_PROJECT => 'load-more-ajax-second-buttton-on-page',
            self::TYPE_ONE_PROJECT_CONTENT => 'load-more-ajax',
            self::TYPE_ONE_BLOG_ANOTHER_BLOG => 'load-more-ajax-second-buttton-on-page',
            self::TYPE_ONE_BLOG_CONTENT => 'load-more-ajax',
        ];
        return $classArray[$type];
    }
}
