<?php
namespace frontend\helpers;

use common\components\model\Helper;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


/**
 * Site controller
 */
class SiteUrlHelper
{
    /**
     * @param $route
     * @param $params
     *
     * @return string
     */
    public static function createUrl($route, $params)
    {
        return Url::to(
            ArrayHelper::merge(
                [$route],
                $params
            )
        );
    }

    //Ссылки на страници

    public static function createHomeUrl($params = [])
    {
        return self::createUrl('/home/index', $params);
    }

    //Ссылки на страници проектов

    public static function createAllProjectsUrl($params = [])
    {
        return self::createUrl('/project/all-projects', $params);
    }

    public static function createOneProjectUrl($params = [])
    {
        return self::createUrl('/project/one-project', $params);
    }

    //Ссылки на страници блога

    public static function createAllBlogsUrl($params = [])
    {
        return self::createUrl('/blog/all-blogs', $params);
    }

    public static function createOneBlogUrl($params = [])
    {
        return self::createUrl('/blog/one-blog', $params);
    }

    //Страници контактов, о нас и т.п.

    public static function createAboutUrl($params = [])
    {
        return self::createUrl('/about/index', $params);
    }

    public static function createServicesUrl($params = [])
    {
        return self::createUrl('/services/index', $params);
    }

    public static function createMediaUrl($params = [])
    {
        return self::createUrl('/media/index', $params);
    }

    public static function createContactsUrl($params = [])
    {
        return self::createUrl('/contacts/index', $params);
    }

    //Ссылки на контентные страници

    public static function createContentPageUrl($params = [])
    {
        return self::createUrl('/content-pages/index', $params);
    }

    //Ссылка на заявку

    public static function createRequestUrl($params = [])
    {
        return self::createUrl('/request/request/faynblat-call-you', $params);
    }

    //Ссылка на запрос вакансии

    public static function createCareersUrl($params = [])
    {
        return self::createUrl('/careersRequest/careers-request/send', $params);
    }

    //Ссылка на запрос вакансии

    public static function createCareersPagesUrl($params = [])
    {
        return self::createUrl('/careers', $params);
    }

    //Ссылка на подгрузить еще

    public static function createLoadMoreElementsUrl($params = [])
    {
        return self::createUrl('/site/load-more-elements', $params);
    }
}
