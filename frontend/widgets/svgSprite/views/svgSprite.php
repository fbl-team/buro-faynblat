<svg style="position: absolute; width: 0; height: 0;" width="0" height="0" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <defs>
        <symbol id="arrow-drop" viewBox="0 0 11 8.8">
            <polygon points="5.5,8.8 0,1.3 1.6,0 5.5,5.6 9.4,0 11,1.3 	" />
        </symbol>
        <symbol id="tw" viewBox="0 0 14 11">
            <path id="tw_icon" d="M13.6.2c-.6.3-1.2.6-1.8.7-.6-.6-1.3-.9-2.1-.9-1.6 0-2.9 1.2-2.9 2.8 0 .2 0 .4.1.6C4.6 3.3 2.5 2.3 1 .5.7.9.6 1.4.6 1.9c0 .9.5 1.8 1.3 2.3-.5 0-.9-.1-1.3-.3 0 1.3 1 2.5 2.3 2.7-.5.1-.9.1-1.3.1C2 7.8 3 8.6 4.2 8.6 3.2 9.4 2 9.8.7 9.8H0c1.3.8 2.8 1.2 4.4 1.2 4.4.1 8.1-3.4 8.2-7.8v-.5c.6-.4 1.1-.9 1.4-1.4-.5.2-1.1.4-1.7.4.6-.3 1.1-.8 1.3-1.5z"
            />
        </symbol>
        <symbol id="facebook" viewBox="0 0 7.1 13">
            <path id="f_icon" d="M6.7 0H5.1C3.4-.1 2 1.2 1.9 2.8v1.9H.3c-.2 0-.3.1-.3.2V7c0 .1.1.3.3.3H2v5.4c0 .1.1.3.3.3h2.2c.1 0 .3-.1.3-.3V7.3h2c.1 0 .3-.1.3-.3V4.9c0-.2-.1-.3-.3-.3h-2V3.4c-.2-.4 0-.8.4-.9h1.5c.2 0 .3-.1.3-.3v-2c0-.1-.1-.2-.3-.2z" />
        </symbol>
        <symbol id="vk" viewBox="0 0 17 10">
            <path id="vk_icon" d="M14.6 6.4l1.7 1.7.6.9c.2.3.1.7-.2.9-.1 0-.1.1-.2.1H14c-.6 0-1.2-.2-1.6-.7-.3-.4-.7-.8-1-1.1-.1-.2-.2-.3-.4-.4-.2-.2-.5-.2-.7 0 0 0-.1.1-.1.2-.2.4-.3.8-.3 1.2.1.4-.1.7-.5.8h-.3c-1.2.1-2.5-.2-3.5-.8-.9-.6-1.8-1.4-2.4-2.3C2 5.1.9 3.1.1 1.1-.1.6 0 .4.5.3h2.4c.3 0 .6.2.7.5C4 2 4.6 3 5.3 4c.2.3.4.5.6.7.1.1.4.2.5 0 .1-.1.1-.1.1-.2.1-.2.1-.4.1-.7.1-.8.1-1.6 0-2.3 0-.4-.4-.8-.8-.9-.2 0-.2-.1-.1-.2.2-.3.5-.4.8-.4h2.8c.4 0 .6.4.6.8V4c0 .3.1.6.4.8.2 0 .5-.1.6-.3.6-.7 1.1-1.6 1.5-2.5.2-.4.3-.8.5-1.2.1-.3.3-.5.6-.4h2.9c.3 0 .5.2.5.5.1 0 .1.1.1.2-.3.7-.6 1.3-1.1 1.9-.4.6-.9 1.3-1.4 1.9-.4.3-.4.9 0 1.3 0 .1.1.1.1.2z"
            />
        </symbol>
        <symbol id="logo-footer" viewBox="0 0 32.2 26">
            <style>
                .st0{fill-rule:evenodd;clip-rule:evenodd;fill:#A86280;}
            </style>
            <path d="M25 .1L12.5 26h7.1L32.2.1zM14.2 15l-2-4h4l2-4h-8l-1-3h10l2-4H0l10.6 22.3z" class="st0" />
        </symbol>
        <symbol id="vintage" viewBox="0 0 84.1 115.5">
            <path d="M21.7 0H0l5.1 19.8h21.5zm41.5 0L33.7 115.5h21L84.1 0z" />
        </symbol>
        <symbol id="play" viewBox="0 0 41.999 41.999">
            <path d="M36.068,20.176l-29-20C6.761-0.035,6.363-0.057,6.035,0.114C5.706,0.287,5.5,0.627,5.5,0.999v40
	c0,0.372,0.206,0.713,0.535,0.886c0.146,0.076,0.306,0.114,0.465,0.114c0.199,0,0.397-0.06,0.568-0.177l29-20
	c0.271-0.187,0.432-0.494,0.432-0.823S36.338,20.363,36.068,20.176z" />
        </symbol>
        <symbol id="check_icon" viewBox="0 0 20.6 17.2">
            <path d="M19.8,0.6l-11.9,15L0.8,6.4" />
        </symbol>

    </defs>
</svg>
