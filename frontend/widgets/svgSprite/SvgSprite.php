<?php
/**
 * Author: Pavel Naumenko
 */

namespace app\widgets\svgSprite;

/**
 * Class Widget
 *
 * @package app\modules\banner\widgets\headBannerSlider
 */
class SvgSprite extends \yii\base\Widget
{

    public function run()
    {
        return $this->render('svgSprite');
    }
}
