<?php

use frontend\components\TypicalFunction;

$workPrincipleElements = $model->pageAboutAboutOurWorks;

?>

<?php if (!empty($workPrincipleElements)) { ?>
    <section class="info-in-digits js-ancolor">
        <div class="container__inner">
            <div class="heading heading_inv">
                <?php if ($model->about_our_work_label) { ?>
                <span class="heading__title">
                    <?= $model->about_our_work_label ?>
                </span>
                <?php } ?>
            </div>
            <div class="info-in-digits__iw js-indigit-slider">
                <?php foreach ($workPrincipleElements as $i => $element) { ?>
                <div class="info-in-digits__item" data-num="<?= $i ?>">
                    <?php if ($element->label_under_description) { ?>
                        <div class="info-in-digits__pre"><?= $element->label_under_description ?></div>
                    <?php } ?>
                    <div class="info-in-digits__value" data-count="<?= $element->label ?>"><?= $element->label ?></div>
                    <div class="info-in-digits__key"><?= $element->description ?></div>
                    <div class="info-in-digits__column"></div>
                </div>
                <?php } ?>
            </div>
            <div class="pagin pagin_slider"></div>
        </div>
    </section>
<?php } ?>
