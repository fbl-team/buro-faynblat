<?php
/**
 * Author: Pavel Naumenko
 */

namespace app\widgets\header;
use frontend\modules\pages\models\about\PageAbout;
use common\helpers\MenusHelper;
use frontend\modules\menus\models\Menus;
use Yii;

/**
 * Class Widget
 *
 * @package app\modules\banner\widgets\headBannerSlider
 */
class Header extends \yii\base\Widget
{
    public $staticModel;


    /**
     * @inheritdoc
     */
    public function run()
    {
        $headerElements = Menus::find()
            ->where(['type_id' => MenusHelper::HEADER_ELEMENTS])
            ->isPublished()
            ->orderBy('position ASC')
            ->all();

        $aboutModel = PageAbout::find()->one();

        $menuItems = [];

        foreach ($headerElements as $element) {
            $menuItems[] = [
                'label' => $element->label,
                'url' => MenusHelper::getMenusItemUrl($element->url),
            ];
        }

        return $this->render('header', [
                'menuItems' => $menuItems,
                'staticModel' => $this->staticModel,
                'aboutModel' => $aboutModel,
            ]
        );
    }

}
