<?php
/**
 * Author: Pavel Naumenko
 */

namespace frontend\widgets\blogsBlock;

use common\helpers\MenusHelper;
use common\helpers\BlogsHelper;
use frontend\modules\menus\models\Menus;
use frontend\modules\blogs\models\Blogs;

/**
 * Class Widget
 *
 * @package app\modules\banner\widgets\headBannerSlider
 */
class BlogsBlock extends \yii\base\Widget
{
    public $type;
    public $staticModel;

    public function run()
    {
        $models = Blogs::find()
            ->with(['blogSmallImage', 'blogBigImage'])
            ->orderBy('position DESC')
            ->isPublished();
        $staticModel = $this->staticModel;
        $view = '';
        $blockLabel = '';
        $blockDescription = '';
        $blockButtonLabel = '';

        switch ($this->type) {
            case (BlogsHelper::SHOW_ON_MAIN):
                $models->andWhere(['show_on_main' => 1]);
                $models->limit(3);
                $view = 'home';
                $blockLabel = $staticModel->blog_block_label;
                $blockDescription = $staticModel->blog_block_short_description;
                $blockButtonLabel = $staticModel->blog_block_button_label;
                break;
            case (BlogsHelper::SHOW_ON_ABOUT):
                $models->andWhere(['show_on_about' => 1]);
                $models->limit(1);
                $view = 'oneSection';
                $blockLabel = $staticModel->blogs_block_label;
                $blockDescription = $staticModel->blogs_block_description;
                $blockButtonLabel = $staticModel->blogs_block_button_label;
                break;
            case (BlogsHelper::SHOW_ON_SERVICES):
                $models->andWhere(['show_on_services' => 1]);
                $models->limit(1);
                $view = 'oneSection';
                $blockLabel = $staticModel->blog_block_label;
                $blockDescription = $staticModel->blog_block_description;
                $blockButtonLabel = $staticModel->blog_block_button_label;
                break;
            case (BlogsHelper::SHOW_LAST):
                $models->limit(4);
                $view = 'twoSection';
                $blockLabel = $staticModel->blog_block_label;
                $blockDescription = $staticModel->blog_block_short_description;
                $blockButtonLabel = $staticModel->blog_block_button_label;
                break;
            default:
                $view = '';
                $blockLabel = '';
                $blockDescription = '';
                $blockButtonLabel = '';
                break;

        }

        $models = $models->orderBy('position DESC')->all();

        if ($view && !empty($models)) {
            return $this->render($view, [
                    'models' => $models,
                    'staticModel' => $staticModel,
                    'blockLabel' => $blockLabel,
                    'blockDescription' => $blockDescription,
                    'blockButtonLabel' => $blockButtonLabel,
                ]
            );
        }
    }

}
