<?php
/**
 * @author myha
 */
use frontend\components\TypicalFunction;
use frontend\components\TypicalFunctionFaynblat;
use frontend\helpers\SiteUrlHelper;
use frontend\widgets\lang\Lang;
use yii\widgets\Menu;

?>

<section class="projects f-blog">
    <div class="container__inner">
        <div class="heading">
            <?php if ($blockLabel) { ?>
                <span class="heading__caption"><?= $blockLabel ?></span>
            <?php } ?>
            <?php if ($blockDescription) { ?>
                <div class="heading__title">

                    <?= TypicalFunctionFaynblat::generateAnimatedString($blockDescription) ?>

                </div>
            <?php } ?>
        </div>
    </div>
    <div class="container">
        <div class="projects__block-line">
            <?php foreach ($models as $key => $model) { ?>
                <div class="projects__row line blog">
                    <div class="projects__col projects__col_smaller">
                        <a class="projects__img-wrap projects__img-wrap_right" href="<?= SiteUrlHelper::createOneBlogUrl(['alias' => $model->alias]) ?>">
                            <?= TypicalFunction::getImage($model, 'blogSmallImage' , ['blog', 'smallimage'], ['class' => 'projects__img']) ?>
                        </a>
                        <div class="projects__info">
                            <a class="projects__info_iw" href="<?= SiteUrlHelper::createOneBlogUrl(['alias' => $model->alias]) ?>">
                                <?php if ($model->label) { ?>
                                    <span class="projects__info-caption"><?= $model->label ?></span>
                                <?php } ?>
                                <?php if ($model->created_at) { ?>
                                    <span class="projects__info-line"><?= Yii::$app->formatter->asDatetime($model->created_at,'short') ?></span>
                                <?php } ?>
                            </a>
                        </div>
                    </div>
                </div>
            <?php } ?>

        </div>
        <div style="clear: both"></div>
        <div class="container">
            <div class="projects__block">
                <div class="projects__row blog">
                    <div class="projects__col projects__col_bigger">
                        <a class="link-to" style="margin-top: 20px" href="<?= SiteUrlHelper::createAllBlogsUrl() ?>">
                            <span class='link-to__text'><?= $blockButtonLabel ?></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
