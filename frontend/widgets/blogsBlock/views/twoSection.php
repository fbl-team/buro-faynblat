<?php
/**
 * @author myha
 */
use frontend\components\TypicalFunction;
use frontend\components\TypicalFunctionFaynblat;
use frontend\helpers\SiteUrlHelper;
use frontend\widgets\lang\Lang;
use yii\widgets\Menu;

?>

<section class="projects f-blog">
    <div class="container__inner">
        <div class="heading">
            <?php if ($blockLabel) { ?>
                <span class="heading__caption"><?= $blockLabel ?></span>
            <?php } ?>
            <?php if ($blockDescription) { ?>
                <div class="heading__title">

                    <?= TypicalFunctionFaynblat::generateAnimatedString($blockDescription) ?>

                </div>
            <?php } ?>
        </div>
    </div>
    <div class="container">
        <div class="projects__iw">

            <?php foreach ($models as $key => $model) { ?>
                <?php if ($key%2) { ?>
                    <div class="projects__row blog">
                        <div class="projects__col projects__col_smaller">
                            <div class="projects__info">
                                <a class="projects__info_iw" href="<?= SiteUrlHelper::createOneBlogUrl(['alias' => $model->alias]) ?>">
                                    <?php if ($model->label) { ?>
                                        <span class="projects__info-caption"><?= $model->label ?></span>
                                    <?php } ?>
                                    <?php if ($model->created_at) { ?>
                                        <span class="projects__info-line"><?= Yii::$app->formatter->asDatetime($model->created_at,'short') ?></span>
                                    <?php } ?>
                            </div>
                            <a class="projects__img-wrap projects__img-wrap_left" href="<?= SiteUrlHelper::createOneBlogUrl(['alias' => $model->alias]) ?>">
                                <?= TypicalFunction::getImage($model, 'blogSmallImage', ['blog', 'smallimage'], ['class' => 'projects__img']) ?>
                            </a>
                        </div>
                        <div class="projects__col projects__col_bigger">
                            <div class="projects__info announcement">
                                <a class="projects__img-wrap projects__img-wrap_left f-blog-announcement" href="<?= SiteUrlHelper::createOneBlogUrl(['alias' => $model->alias]) ?>">
                                    <span class="projects__info-caption"><?= $model->announcement ?></span>
                                </a>
                            </div>
                            <?php if($key == count($models)-1) { ?>
                                <a class="link-to" href="<?= SiteUrlHelper::createAllProjectsUrl() ?>">
                                    <span class='link-to__text'><?= $blockButtonLabel ?></span>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="projects__row blog">
                        <div class="projects__col projects__col_bigger">
                            <div class="projects__info announcement">
                                <a class="projects__img-wrap projects__img-wrap_left f-blog-announcement" href="<?= SiteUrlHelper::createOneBlogUrl(['alias' => $model->alias]) ?>">
                                    <span class="projects__info-caption"><?= $model->announcement ?></span>
                                </a>
                            </div>
                        </div>
                        <div class="projects__col projects__col_smaller">
                            <a class="projects__img-wrap projects__img-wrap_right" href="<?= SiteUrlHelper::createOneBlogUrl(['alias' => $model->alias]) ?>">
                                <?= TypicalFunction::getImage($model, 'blogSmallImage' , ['blog', 'smallimage'], ['class' => 'projects__img']) ?>
                            </a>
                            <div class="projects__info">
                                <a class="projects__info_iw" href="<?= SiteUrlHelper::createOneBlogUrl(['alias' => $model->alias]) ?>">
                                    <?php if ($model->label) { ?>
                                        <span class="projects__info-caption"><?= $model->label ?></span>
                                    <?php } ?>
                                    <?php if ($model->created_at) { ?>
                                        <span class="projects__info-line"><?= Yii::$app->formatter->asDatetime($model->created_at,'short') ?></span>
                                    <?php } ?>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>

        </div>
    </div>
</section>
