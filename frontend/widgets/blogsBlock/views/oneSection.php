<?php
/**
 * @author myha
 */

use frontend\components\TypicalFunction;
use frontend\components\TypicalFunctionFaynblat;
use frontend\helpers\SiteUrlHelper;
use frontend\widgets\lang\Lang;
use yii\widgets\Menu;

?>

<section class="blogs blogs_due">
    <div class="container__inner">
        <div class="heading">
            <?php if ($blockLabel) { ?>
                <span class="heading__caption"><?= $blockLabel ?></span>
            <?php } ?>
            <?php if ($blockDescription) { ?>
            <div class="heading__title">

                <?= TypicalFunctionFaynblat::generateAnimatedString($blockDescription) ?>

            </div>
            <?php } ?>
        </div>
    </div>
    <div class="container">

        <div class="blogs__iw">
            <?php foreach ($models as $model) { ?>
                <div class="blogs__row">
                <div class="blogs__col blogs__col_bigger">
                    <div class="blogs__info">
                        <a class="blogs__info_iw" href="<?= SiteUrlHelper::createOneBlogUrl(['alias' => $model->alias]) ?>">
                            <?php if ($model->label) { ?>
                                <span class="blogs__info-caption"><?= $model->label; ?></span>
                            <?php } ?>
                            <?php if ($model->city_and_square) { ?>
                                <span class="blogs__info-line"><?= $model->city_and_square; ?></span>
                            <?php } ?>
                        </a>
                    </div>
                    <a class="blogs__img-wrap blogs__img-wrap_left" href="<?= SiteUrlHelper::createOneBlogUrl(['alias' => $model->alias]) ?>">
                        
                        <?= TypicalFunction::getImage($model, 'blogSmallImage', ['blog', 'smallimage'], ['class' => 'blogs__img']) ?>
                    
                    </a>
                </div>
                <div class="blogs__col blogs__col_smaller">
                    <a class="blogs__img-wrap blogs__img-wrap_right" href="<?= SiteUrlHelper::createOneBlogUrl(['alias' => $model->alias]) ?>">
                        
                        <?= TypicalFunction::getImage($model, 'blogBigImage', ['blog', 'bigimage'], ['class' => 'blogs__img']) ?>
                    
                    </a>
                    <div class="blogs__info">
                        <a class="blogs__info_iw" href="<?= SiteUrlHelper::createOneBlogUrl(['alias' => $model->alias]) ?>">
                            <?php if ($model->label) { ?>
                                <span class="blogs__info-caption"><?= $model->label ?></span>
                            <?php } ?>
                            <?php if ($model->city_and_square) { ?>
                                <span class="blogs__info-line"><?= $model->city_and_square ?></span>
                            <?php } ?>
                        </a>
                    </div>
                    <a class="link-to" href="<?= SiteUrlHelper::createAllBlogsUrl() ?>">

                        <span class='link-to__text'><?= $blockButtonLabel ?></span>
                        
                    </a>
                </div>
            </div>
            <?php break; } ?>
        </div>

    </div>
</section>