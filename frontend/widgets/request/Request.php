<?php
/**
 * Author: Pavel Naumenko
 */

namespace app\widgets\request;
use common\models\RestVariant;
use common\models\Country;
use frontend\modules\request\models\form\RequestFormModel;

/**
 * Class Widget
 *
 * @package app\modules\banner\widgets\headBannerSlider
 */
class Request extends \yii\base\Widget
{
    public $buttonLabel;

    public $model = false;

    public function run()
    {
        $model = $this->model ? $this->model : new RequestFormModel();

        return $this->render('request', [
            'model' => $model,
            'buttonLabel' => $this->buttonLabel
        ]);
    }
}
