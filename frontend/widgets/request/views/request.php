<?php

use frontend\helpers\SiteUrlHelper;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
        'action' => SiteUrlHelper::createRequestUrl(),
        'options' => [
            'class' => 'ajax-form form-wrap faynblat-call-you',
			'onsubmit' => "gtag('event', 'sendemail', { 'event_category': 'mail', 'event_action': 'send', });return true;",
        ],
        'enableAjaxValidation' => false,
        'errorCssClass' => 'has-error',
        'fieldConfig' => [
            'template' => "{input}\n{label}\n<span class=\"error-label\">error</span>\n{error}",
            'errorOptions' => ['class' => 'help-text'],
            'inputOptions' => ['class' => 'form-control'],
            'labelOptions' => ['class' => 'form-label']
        ]
    ]); ?>

    <?= $form->field($model, 'name'); ?>
    <?= $form->field($model, 'phone'); ?>
    <?= $form->field($model, 'email'); ?>

    <div class="main-form__btn">
        <!--после нажатия добавляем кнопке класс "action-btn_done"-->
        <?/*<button class='action-btn js-action-btn form-submit ' id="meet-form-submit" onclick="ga('send', 'event', 'Кнопка отправить заявку', 'Отправка', '');"> */ ?>
        <button class='action-btn js-action-btn form-submit ' id="meet-form-submit">
            <?= $buttonLabel ?>
            <svg class="check-icon">
                <use xlink:href="#check_icon"></use>
            </svg>
        </button>
    </div>

<?php ActiveForm::end(); ?>