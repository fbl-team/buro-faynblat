<?php
/**
 * Author: Pavel Naumenko
 */

namespace frontend\widgets\rightMenu;
use common\helpers\MenusHelper;
use frontend\modules\menus\models\Menus;

/**
 * Class Widget
 *
 * @package app\modules\banner\widgets\headBannerSlider
 */
class RightMenu extends \yii\base\Widget
{
    public $allActive = false;
    public $alias = false;

    public function run()
    {
        $headerElements = Menus::find()
            ->where(['type_id' => MenusHelper::RIGHT_ELEMENTS])
            ->isPublished()
            ->orderBy('position ASC')
            ->all();

        $menuItems = [];

        foreach ($headerElements as $key => $element) {
            $menuItems[] = [
                'class' => ($this->alias == ltrim($element->url, '/')) ? 'active' : '',
                'label' => $element->label,
                'url' => MenusHelper::getMenusItemUrl($element->url),
            ];
        }

        if (!empty($menuItems)) {
            return $this->render('rightMenu', [
                    'menuItems' => $menuItems,
                    'allActive' => $this->allActive,
                ]
            );
        }

    }

}
