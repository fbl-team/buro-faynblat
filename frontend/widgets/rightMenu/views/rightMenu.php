<?php
use frontend\widgets\CircularReference;
?>
<!--на странцие about никакой из пунктов не будет с классом active-->
<div class="a-menu a-menu_open <?= $allActive ? 'a-menu_base' : '' ?>">
    <ul>
<!--        <li class="a-menu__item show-mob active"><a class="a-menu__link" href="04_services_faynblat.html">Услуги</a>-->
<!--        </li>-->
    <?php foreach ($menuItems as $key => $item): ?>
        <li class="a-menu__item <?= $item['class'] ?>">
            <?php $linkWidget = CircularReference::begin([
                'href' => $item['url'],
                'linkOptions' => ['class' => 'a-menu__link']
            ]) ?>
            <?= $linkWidget->start() ?>
            <?php if ($linkWidget->isCurrentRoute()): ?>
                <span class="a-menu__link">
                    <?= $item['label'] ?>
                </span>
            <?php else: ?>
                <?= $item['label'] ?>
            <?php endif; ?>
            <?php $linkWidget->end() ?>
        </li>
    <?php endforeach; ?>
    </ul>
</div>