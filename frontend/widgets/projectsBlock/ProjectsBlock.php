<?php
/**
 * Author: Pavel Naumenko
 */

namespace frontend\widgets\projectsBlock;

use common\helpers\MenusHelper;
use common\helpers\ProjectsHelper;
use frontend\modules\menus\models\Menus;
use frontend\modules\projects\models\Projects;
use yii\db\Expression;

/**
 * Class Widget
 *
 * @package app\modules\banner\widgets\headBannerSlider
 */
class ProjectsBlock extends \yii\base\Widget
{
    public $type;
    public $staticModel;

    public function run()
    {
        $models = Projects::find()
            ->with(['projectSmallImage', 'projectBigImage'])
            ->isPublished();
        $staticModel = $this->staticModel;
        $view = '';
        $blockLabel = '';
        $blockDescription = '';
        $blockButtonLabel = '';

        switch ($this->type) {
            case (ProjectsHelper::SHOW_ON_MAIN):
                $models->andWhere(['show_on_main' => 1]);
                $models->orderBy(new Expression('rand()'));
                $models->limit(4);
                $view = 'twoSection';
                $blockLabel = $staticModel->project_block_label;
                $blockDescription = $staticModel->project_block_short_description;
                $blockButtonLabel = $staticModel->project_block_button_label;
                break;
            case (ProjectsHelper::SHOW_ON_ABOUT):
                $models->andWhere(['show_on_about' => 1]);
                $models->orderBy('position DESC');
                $models->limit(1);
                $view = 'oneSection';
                $blockLabel = $staticModel->projects_block_label;
                $blockDescription = $staticModel->projects_block_description;
                $blockButtonLabel = $staticModel->projects_block_button_label;
                break;
            case (ProjectsHelper::SHOW_ON_SERVICES):
                $models->andWhere(['show_on_services' => 1]);
                $models->orderBy('position DESC');
                $models->limit(1);
                $view = 'oneSection';
                $blockLabel = $staticModel->project_block_label;
                $blockDescription = $staticModel->project_block_description;
                $blockButtonLabel = $staticModel->project_block_button_label;
                break;
            default:
                $view = '';
                $blockLabel = '';
                $blockDescription = '';
                $blockButtonLabel = '';
                break;

        }

        $models = $models->all();

        if ($view && !empty($models)) {
            return $this->render($view, [
                    'models' => $models,
                    'staticModel' => $staticModel,
                    'blockLabel' => $blockLabel,
                    'blockDescription' => $blockDescription,
                    'blockButtonLabel' => $blockButtonLabel,
                ]
            );
        }
    }

}
