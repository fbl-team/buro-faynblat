<?php
/**
 * @author myha
 */
use frontend\components\TypicalFunction;
use frontend\components\TypicalFunctionFaynblat;
use frontend\helpers\SiteUrlHelper;
use frontend\widgets\lang\Lang;
use yii\widgets\Menu;
?>

<section class="projects">
    <div class="container__inner">
        <div class="heading">
            <?php if ($blockLabel) { ?>
                <span class="heading__caption"><?= $blockLabel ?></span>
            <?php } ?>
            <?php if ($blockDescription) { ?>
                <div class="heading__title">

                    <?= TypicalFunctionFaynblat::generateAnimatedString($blockDescription) ?>

                </div>
            <?php } ?>
        </div>
    </div>
    <div class="container">
        <div class="projects__iw">

            <?php foreach ($models as $key => $model) { ?>
                <?php if ($key%2) { ?>
                    <div class="projects__row">
                        <div class="projects__col projects__col_smaller">
                            <div class="projects__info">
                                <a class="projects__info_iw" href="<?= SiteUrlHelper::createOneProjectUrl(['alias' => $model->alias]) ?>">
                                    <?php if ($model->label) { ?>
                                        <span class="projects__info-caption"><?= $model->label ?></span>
                                    <?php } ?>
                                    <?php if ($model->city_and_square) { ?>
                                        <span class="projects__info-line"><?= $model->city_and_square ?></span>
                                    <?php } ?>
                                </a>
                            </div>
                            <a class="projects__img-wrap projects__img-wrap_left" href="<?= SiteUrlHelper::createOneProjectUrl(['alias' => $model->alias]) ?>">
                                <?= TypicalFunction::getImage($model, 'projectSmallImage', ['project', 'smallimage'], ['class' => 'projects__img']) ?>
                            </a>
                        </div>
                        <div class="projects__col projects__col_bigger">
                            <a class="projects__img-wrap projects__img-wrap_right" href="<?= SiteUrlHelper::createOneProjectUrl(['alias' => $model->alias]) ?>">
                                <?= TypicalFunction::getImage($model, 'projectBigImage', ['project', 'bigimage'], ['class' => 'projects__img']) ?>
                            </a>
                            <?php if($key == count($models)-1) { ?>
                                <a class="link-to" href="<?= SiteUrlHelper::createAllProjectsUrl() ?>">
                                    <span class='link-to__text'><?= $blockButtonLabel ?></span>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="projects__row">
                        <div class="projects__col projects__col_bigger">
                            <a class="projects__img-wrap projects__img-wrap_left" href="<?= SiteUrlHelper::createOneProjectUrl(['alias' => $model->alias]) ?>">
                                <?= TypicalFunction::getImage($model, 'projectBigImage', ['project', 'bigimage'], ['class' => 'projects__img']) ?>
                            </a>
                        </div>
                        <div class="projects__col projects__col_smaller">
                            <a class="projects__img-wrap projects__img-wrap_right" href="<?= SiteUrlHelper::createOneProjectUrl(['alias' => $model->alias]) ?>">
                                <?= TypicalFunction::getImage($model, 'projectSmallImage' , ['project', 'smallimage'], ['class' => 'projects__img']) ?>
                            </a>
                            <div class="projects__info">
                                <a class="projects__info_iw" href="<?= SiteUrlHelper::createOneProjectUrl(['alias' => $model->alias]) ?>">
                                    <?php if ($model->label) { ?>
                                        <span class="projects__info-caption"><?= $model->label ?></span>
                                    <?php } ?>
                                    <?php if ($model->city_and_square) { ?>
                                        <span class="projects__info-line"><?= $model->city_and_square ?></span>
                                    <?php } ?>
                                </a>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>

        </div>
    </div>
</section>
