<?php
/**
 * Author: Pavel Naumenko
 */

namespace app\widgets\footer;
use common\helpers\MenusHelper;
use common\models\RestVariant;
use common\models\Country;
use frontend\modules\menus\models\Menus;

/**
 * Class Widget
 *
 * @package app\modules\banner\widgets\headBannerSlider
 */
class Footer extends \yii\base\Widget
{

    public $staticModel;
    public $staticModelOnePage = false;
    public $career = false;

    public function run()
    {
        $footerElements = Menus::find()
            ->where(['type_id' => MenusHelper::FOOTER_ELEMENTS])
            ->isPublished()
            ->orderBy('position ASC')
            ->all();

        $menuItems = [];
        foreach ($footerElements as $element) {
            $menuItems[] = [
                'label' => $element->label,
                'url' => MenusHelper::getMenusItemUrl($element->url),
            ];
        }

        return $this->render('footer', [
            'menuItems' => $menuItems,
            'staticModel' => $this->staticModel,
            'staticModelOnePage' => $this->staticModelOnePage,
            'career' => $this->career,
        ]);
    }
}
