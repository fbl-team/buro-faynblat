<?php


use frontend\components\TypicalFunction;
use frontend\helpers\SiteUrlHelper;
use frontend\modules\request\models\form\RequestFormModel;
use frontend\widgets\CircularReference;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Menu;
use app\widgets\request\Request;
use app\widgets\career\Career;

?>

<!--main-form-->
<div class="main-form">
    <?php if ($staticModelOnePage) { ?>
        <div class="container__inner">
            <div class="heading heading_lelan">
                <?php if ($staticModelOnePage->footer_request_block_label) { ?>
                    <div class="heading__title">
                        <?= $staticModelOnePage->footer_request_block_label ?>
                    </div>
                <?php } ?>
            </div>
            <div class="main-form__data">

                <?= Request::widget(['buttonLabel' => $staticModelOnePage->footer_request_block_button_label]); ?>

            </div>

            <?= $this->render('svgLetter') ?>

        </div>
    <?php } elseif($career) { ?>
    <div class="container__inner">
        <div class="heading heading_lelan">
            <?php if ($staticModel->footer_request_block_label) { ?>
            <span class="heading__title">
                <?= $staticModel->footer_request_block_label ?>
            </span>
            <?php } ?>
        </div>
        <div class="main-form__data">
            <?= Career::widget(['buttonLabel' => $staticModel->footer_request_block_button_label]); ?>
        </div>

        <?= $this->render('svgLetter') ?>

    </div>
    <?php } else { ?>
    <div class="container__inner">
        <div class="heading heading_lelan">
            <?php if ($staticModel->footer_request_block_label) { ?>
            <span class="heading__title">
                <?= $staticModel->footer_request_block_label ?>
            </span>
            <?php } ?>
        </div>
        <div class="main-form__data">

            <?= Request::widget(['buttonLabel' => $staticModel->footer_request_block_button_label]); ?>

        </div>

        <?= $this->render('svgLetter') ?>

    </div>
    <?php } ?>
</div>
<!--footer-->
<footer class="main-footer">
    <div class="container">

        <div class="footer__row posi clearfix">
            <!--logo-->
            <div class="main-footer__logo main-footer__item">
                <div class="logo-small">
                    <?php $linkWidget = CircularReference::begin([
                        'href' => SiteUrlHelper::createHomeUrl(),
                        'homePage' => true
                    ]) ?>
                    <?= $linkWidget->start() ?>
                        <svg width="26" height="21">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#logo-footer"></use>
                        </svg>
                        <svg class="show-o-tab" viewBox="0 0 95 26" height="21" width="95">
                            <path fill="#010202" d="M0 19h3v-4h6v-3h-6v-2h7V7l-10 .1V19zm16-12l-5 12h3l1-2h5l1 2h3l-5-12h-3zm0 7l1-3h1l1 3h-3zm66-7l-5 12h3l1-2h5l1 2h3l-5-12h-3zm0 7l1-3h1l1 3h-3zm-51.5-2l-3.5-5h-3l5 7v5h3v-5l5-7h-3l-3.5 5zm16.5 1l-5-6h-3v12h3v-7l5.3 7h2.7V7h-3v6zm13.6-.3c1.2-.5 1.8-1.4 1.8-2.7 0-.8-.3-1.4-.8-1.9-.7-.7-1.8-1.1-3.2-1.1h-5.5v12h5.5c1.4 0 2.5-.3 3.3-.9.8-.6 1.2-1.4 1.2-2.4 0-.8-.2-1.4-.6-1.9-.3-.4-.9-.8-1.7-1.1zm-5-3h2.1c.5 0 .9.1 1.1.3.3.2.4.4.4.8 0 .7-.5 1.1-1.6 1.1h-2.1V9.7zm3.7 6.4c-.3.2-.7.3-1.2.3h-2.5v-2.2h2.5c.5 0 .9.1 1.2.3.3.2.4.5.4.8 0 .3-.1.6-.4.8zm9.7-9.1h-3v12h9v-3h-6V7zm21 0v3h4v9h3v-9h4V7h-11z"></path>
                        </svg>
                        <!--copy-->
                        <div class="main-footer__copy show-mob">
                            <span>Faynblat 2016</span>
                        </div>
                    <?php $linkWidget->end() ?>
                </div>
            </div>
            <!--nav-->
            <div class="main-footer__nav main-footer__item">
                <?php foreach ($menuItems as $item): ?>
                    <li>
                        <?= CircularReference::widget([
                            'href' => $item['url'],
                            'content' => $item['label']
                        ]) ?>
                    </li>
                <?php endforeach; ?>
            </div>
            <!--social-->
            <div class="main-footer__social">
                <div class="social ">
                    <? /*<i>
                        <a href="<?= TypicalFunction::isKeyExist('Social web: Twitter group link', '#') ?>" target="_blank" rel="nofollow">
                            <svg width="14" height="11">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tw"></use>
                            </svg>
                        </a>

                    </i>
					*/?>
                    <i>
                        <a href="<?= TypicalFunction::isKeyExist('Social web: Facebook group link', '#') ?>" target="_blank" rel="nofollow">
                            <svg width="7" height="13">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#facebook"></use>
                            </svg>
                        </a>

                    </i>
                    <i>
                        <a href="<?= TypicalFunction::isKeyExist('Social web: Vk group link', '#') ?>" target="_blank" rel="nofollow">
                            <svg width="17" height="10">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#vk"></use>
                            </svg>
                        </a>

                    </i>
										<!-- -->
					 <i class="fayn-newss">
                        <a class="fayn-instagram" href="https://www.instagram.com/design_faynblat/" target="_blank" rel="nofollow">
                            <span height="13" width="13" alt="instagram profile">
                        </a>

                    </i>

					 <i>
                        <a class="fayn-youtube" href="https://www.youtube.com/channel/UCHKEYv9_y8rpd-5CDbeIgNw/" target="_blank" rel="nofollow">
                           <span width="13" height="13" alt="youtube profile">
                        </a>

                    </i>

                </div>
            </div>
        </div>
		<!-- first footer adress end -->
        <div class="footer-adress-temp">
            <p style="text-align:center;"><?= Yii::$app->get('config')->get('Footer address') ?>
        &#9742; <a href="tel:+38 067 3136579">+38 067 3136579</a>, &#128231; <a href="mailto:assistant@faynblat.com.ua">assistant@faynblat.com.ua</a> </p>
        </div>
		<!-- second footer adress end -->
        <div class="footer__row posi clearfix">
            <!--copy-->
            <div class="main-footer__copy">
                <span>Faynblat 2016</span>
            </div>
            <!--by-->
            <div class="main-footer__by">
                <span>by</span>
                <a href="//vintage.com.ua/" target="_blank" rel=nofollow>
                    <svg width="13" height="15">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#vintage"></use>
                    </svg>
                </a>
            </div>
        </div>
    </div>


					<!-- temp styles (till css asset refresh)-->
					<style type="text/css">
					.fayn-newss {width:16px;height:16px;}
					.fayn-newss a {width:16px;height:16px;display: inline-block;vertical-align: middle;}
					.fayn-instagram span {background-image:url('/uploads/a/403-instagram.png');background-size:cover;height:13px;width:13px;display:block;transition:0.5s;}
					.fayn-youtube span {background-image:url('/uploads/a/414-youtube.png');background-size:cover;height:13px;width:13px;display:block;transition:0.5s;}
					.fayn-instagram span:hover {background-image:url('/uploads/a/403-instagram-hover.png');;transition:0.8s;}
					.fayn-youtube span:hover {background-image:url('/uploads/a/414-youtube-hover.png');transition:0.8s;}
					.main-text h2 {font-size: 16px;margin-top: 20px;font-weight: 700;}
					.main-text strong {font-weight:700} 
					.fscreen__title h1 {
						display: block;
						font-weight: normal;
						font-size: 38px;
						letter-spacing: .09em;
						}
	
					</style>
					<!-- -->
</footer>