<?php
/**
 * Author: Pavel Naumenko
 */

namespace app\widgets\career;
use frontend\modules\careersRequest\models\form\CareersFormModel;

/**
 * Class Widget
 *
 * @package app\modules\banner\widgets\headBannerSlider
 */
class Career extends \yii\base\Widget
{
    public $buttonLabel;

    public $model = false;

    public function run()
    {
        $model = $this->model ? $this->model : new CareersFormModel();

        return $this->render('career', [
            'model' => $model,
            'buttonLabel' => $this->buttonLabel
        ]);
    }
}
