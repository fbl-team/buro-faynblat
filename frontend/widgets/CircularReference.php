<?php
namespace frontend\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This widget disable the link in the page which this link refers to
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class CircularReference extends Widget
{
    /**
     * @var bool Is widget call in homepage
     */
    public $homePage = false;
    /**
     * @var string Route for link
     */
    public $href;
    /**
     * @var null|string Link content
     */
    public $content = null;
    /**
     * @var array HTML option for link
     */
    public $linkOptions = [];

    /**
     * @var bool
     */
    protected $_isCurrentRoute = false;


    /**
     *@inheritdoc
     */
    public function init()
    {
        if ($this->homePage) {
            $controller = Yii::$app->controller;
            $this->_isCurrentRoute = ($controller->id == Yii::$app->defaultRoute) &&
                ($controller->action->id == $controller->defaultAction);
        }
        else {
            $this->_isCurrentRoute = Url::current() == $this->href;
        }
    }

    /**
     * Renders open link tag
     */
    public function start()
    {
        if (!$this->_isCurrentRoute) {
            $this->linkOptions['href'] = $this->href;
            return Html::beginTag('a', $this->linkOptions);
        }
    }

    /**
     * Renders close link tag
     */
    public function run()
    {
        if ($this->content === null) {
            return Html::endTag('a');
        }
        elseif (!$this->_isCurrentRoute) {
            return Html::a($this->content, $this->href, $this->linkOptions);
        }
        else {
            return $this->content;
        }
    }

    /**
     * @return bool
     */
    public function isCurrentRoute()
    {
        return $this->_isCurrentRoute;
    }
}
