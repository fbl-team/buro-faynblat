<?php
use yii\helpers\Html;

/**
 * @var \yii\web\View $this
 * @var string $currentLang
 * @var array $langList[]
 */
?>
<div class="header-main__lang ">
    <div class="lang ">
        <div class="lang__title">
            <?= $currentLang ?>
            <i class="arrow-icon">
                <svg width="9" height="7">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow-drop"></use>
                </svg>
            </i>
        </div>
        <div class="lang__content">
            <?php foreach ($langList as $key => $lang): ?>
                <?php $langLabel = ucwords($lang->code); ?>
                <?php if ($lang->code == $currentLang): ?>
                    <span class="active lang__content-item">
                        <?= $langLabel ?>
                    </span>
                <?php else: ?>
                    <?= Html::a(
                        $langLabel,
                        '/' . $lang->code . '/' . \Yii::$app->getRequest()->getPathInfo(),
                        ['class' => 'lang__content-item']
                    ) ?>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>