<?php
/**
 * Author: Pavel Naumenko
 */

namespace frontend\widgets\lang;
use common\helpers\LanguageHelper;

/**
 * Class Widget
 *
 * @package app\modules\banner\widgets\headBannerSlider
 */
class Lang extends \yii\base\Widget
{

    public function run()
    {
        $currentLang = \Yii::$app->language;
        $langDefault = LanguageHelper::getDefaultLanguage()->code;
        $langList = $this->getLangListWithFirstElementCurrent($currentLang);

        if (count($langList) > 1) {
            return $this->render('lang', [
                'currentLang' => $currentLang,
                'langList' => $langList,
                'langDefault' => $langDefault,
            ]);
        }
    }

    public function getLangListWithFirstElementCurrent($currentLang)
    {
        $langList = LanguageHelper::getLanguageModels();
        $resultArray = [];
        foreach ($langList as $lang) {
            if ($lang->code == $currentLang) {
                array_unshift($resultArray, $lang);
            } else {
                $resultArray[] = $lang;
            }
        }
        return $resultArray;
    }
}
