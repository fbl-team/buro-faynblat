<?php
/**
 * @author myha
 */

use frontend\components\TypicalFunction;
use frontend\components\TypicalFunctionFaynblat;
use frontend\helpers\ShowMoreButtonHelper;
use frontend\helpers\SiteUrlHelper;
use frontend\widgets\lang\Lang;
use yii\widgets\Menu;

?>

<div class="other-works__iw">
    <?php foreach ($showMoreElements as $key => $element) { ?>
        <?php if (++$key > $step) {
            break;
        } ?>
        <a class="other-works__item b-item" href="#">
            <div class="img-anim-w posi">
                <?= TypicalFunction::getImage($element, 'blogSmallImage', ['blog', 'anotherblog'], ['class' => 'img-anim-w__img']) ?>
            </div>
            <div class="other-works__info">
                <span class="other-works__info-caption"><?= $element->label ?></span>
                <span class="other-works__info-line"><?= $element->city_and_square ?></span>
            </div>
        </a>
    <?php } ?>

</div>

<div class="show-more-test"></div>
<?php if ($step < $key) { ?>

    <div class="our-awards__load-more ajax-link <?= ShowMoreButtonHelper::getAjaxReplaceClass($type) ?>"
         href="<?= SiteUrlHelper::createLoadMoreElementsUrl([
             'type' => $type,
             'parentModelId' => $parentModelId,
             'iteration' => ++$iteration,
             'buttonLabel' => $buttonLabel,
         ]) ?>">
        <!--у подгружаемого контента вместо классов "posi" через 1 секунду после добавления на страницу добавлять класс "animated"-->
        <!--после нажатия на кнопку "show-more" добавляем ей класс "active", а после того, как получили success
    и подгрузили контент, убираем этот класс и делаем плавный скролл страницы к кнопке, чтобы она
    оставалась в поле зрения - если нужно, то дам код подьезжания-->
        <button class="show-more posi">
            <span class="show-more__txt"><?= $buttonLabel ?></span>
        </button>
    </div>

<?php } ?>
<div class="<?= ShowMoreButtonHelper::getAjaxReplaceClass($type) . '-andry' ?>"></div>

