<?php
/**
 * @author myha
 */

use frontend\components\TypicalFunction;
use frontend\components\TypicalFunctionFaynblat;
use frontend\helpers\ShowMoreButtonHelper;
use frontend\helpers\SiteUrlHelper;
use frontend\widgets\lang\Lang;
use yii\widgets\Menu;

?>

<div class="our-awards__iw">

    <?php foreach ($showMoreElements as $key => $element) { ?>
        <?php if (++$key > $step) {
            break;
        } ?>
        <a class="our-awards__item b-item posi">
            <div class="img-anim-w posi">
                <?= TypicalFunction::getImageFromFPMWithOutResize($element->elementImage, ['class' => 'img-anim-w__img']) ?>
            </div>
            <div class="our-awards__info">
                <?php if ($element->label) { ?>
                    <span class="our-awards__info-caption"><?= $element->label ?></span>
                <?php } ?>
                <?php if ($element->label) { ?>
                    <span class="our-awards__info-line"><?= $element->description ?></span>
                <?php } ?>
            </div>
        </a>
    <?php } ?>

</div>

<?php if ($step < $key) { ?>

<div class="our-awards__load-more ajax-link <?= ShowMoreButtonHelper::getAjaxReplaceClass($type) ?>" href="<?= SiteUrlHelper::createLoadMoreElementsUrl([
    'type' => $type,
    'parentModelId' => $parentModelId,
    'iteration' => ++$iteration,
    'buttonLabel' => $buttonLabel,
]) ?>">

    <!--у подгружаемого контента вместо классов "posi" через 1 секунду после добавления на страницу добавлять класс "animated"-->
    <!--после нажатия на кнопку "show-more" добавляем ей класс "active", а после того, как получили success
и подгрузили контент, убираем этот класс и делаем плавный скролл страницы к кнопке, чтобы она
оставалась в поле зрения - если нужно, то дам код подьезжания-->

    <button class="show-more posi">
        <span class="show-more__txt"><?= $buttonLabel ?></span>
    </button>
</div>

<?php } ?>
<div class="<?= ShowMoreButtonHelper::getAjaxReplaceClass($type) . '-andry'?>"></div>
