<?php
/**
 * @author myha
 */

use frontend\components\TypicalFunction;
use frontend\components\TypicalFunctionFaynblat;
use frontend\helpers\ShowMoreButtonHelper;
use frontend\helpers\SiteUrlHelper;
use frontend\widgets\lang\Lang;
use yii\widgets\Menu;

?>

<div class="media-block__iw">
    <?php foreach ($showMoreElements as $key => $element) { ?>
        <?php if (++$key > $step) {break;}  ?>
        <a class="media-block__item posi" <?= TypicalFunction::getPdfUrl($element, 'blockElementPdf') ?> target="_blank">
            <div class="media-block__item-img">
                <div class="img-anim-w posi">
                    <?= TypicalFunction::getImage($element, 'blockElementImage', ['pagemedia', 'writeblock'], ['class' => 'img-anim-w__img']) ?>
                </div>
            </div>
            <?php if ($element->label) { ?>
                <div class="media-block__info">
                    <span class="media-block__info-caption"><?= $element->label ?></span>
                </div>
            <?php } ?>
        </a>
    <?php } ?>
</div>

<?php if ($step < $key) { ?>
    <div class="media-block__load-more ajax-link <?= ShowMoreButtonHelper::getAjaxReplaceClass($type) ?>" href="<?= SiteUrlHelper::createLoadMoreElementsUrl([
        'type' => $type,
        'parentModelId' => $parentModelId,
        'iteration' => ++$iteration,
    ]) ?>">
        <!--у подгружаемого контента вместо классов "posi" должен быть класс "animated"-->
        <!--после нажатия на кнопку "show-more" добавляем ей класс "active", а после того, как получили success
    и подгрузили контент, убираем этот класс и делаем плавный скролл страницы к кнопке, чтобы она
    оставалась в поле зрения-->
        <button class="show-more posi">
            <span class="show-more__txt"><?= $buttonLabel ?></span>
        </button>
    </div>
<?php } ?>
<div class="<?= ShowMoreButtonHelper::getAjaxReplaceClass($type) . '-andry'?>"></div>
