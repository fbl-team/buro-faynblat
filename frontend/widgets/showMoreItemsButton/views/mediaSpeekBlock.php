<?php
/**
 * @author myha
 */

use frontend\components\TypicalFunction;
use frontend\components\TypicalFunctionFaynblat;
use frontend\helpers\ShowMoreButtonHelper;
use frontend\helpers\SiteUrlHelper;
use frontend\widgets\lang\Lang;
use yii\widgets\Menu;

?>

<div class="video-block__iw">
    <?php foreach ($showMoreElements as $key => $element) { ?>
        <?php if (++$key > $step) {break;}  ?>
        <a class="video-block__item posi js-vpo" href="<?= $element->you_tube_video_code ?>">
            <div class="video-block__item-img">
                <div class="img-anim-w posi">
                    <?= TypicalFunction::getImage($element, 'blockElementImage', ['pagemedia', 'speekblock'], ['class' => 'img-anim-w__img']) ?>
                </div>
            </div>
            <div class="video-block__info">
                <?php if ($element->label) { ?>
                    <span class="video-block__info-caption"><?= $element->label ?></span>
                <?php } ?>
            </div>
        </a>
    <?php } ?>
</div>

<?php if ($step < $key) { ?>
<div class="video-block__load-more ajax-link <?= ShowMoreButtonHelper::getAjaxReplaceClass($type) ?>" href="<?= SiteUrlHelper::createLoadMoreElementsUrl([
    'type' => $type,
    'parentModelId' => $parentModelId,
    'iteration' => ++$iteration,
]) ?>">
    <!--у подгружаемого контента вместо классов "posi" через 1 секунду после добавления на страницу добавлять класс "animated"-->
    <!--после нажатия на кнопку "show-more" добавляем ей класс "active", а после того, как получили success
и подгрузили контент, убираем этот класс и делаем плавный скролл страницы к кнопке, чтобы она
оставалась в поле зрения - если нужно, то дам код подьезжания-->
    <button class="show-more posi">
        <span class="show-more__txt"><?= $buttonLabel ?></span>
    </button>
</div>
<?php } ?>
<div class="<?= ShowMoreButtonHelper::getAjaxReplaceClass($type) . '-andry'?>"></div>
