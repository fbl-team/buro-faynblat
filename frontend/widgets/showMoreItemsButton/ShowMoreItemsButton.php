<?php
/**
 * Author: Pavel Naumenko
 */

namespace frontend\widgets\showMoreItemsButton;

use backend\modules\blogs\models\Blogs;
use common\helpers\BlogsHelper;
use common\helpers\MenusHelper;
use common\helpers\ProjectsHelper;
use frontend\components\TypicalFunction;
use frontend\helpers\ShowMoreButtonHelper;
use frontend\modules\blogs\models\BlogContent;
use frontend\modules\menus\models\Menus;
use frontend\modules\pages\models\about\PageAboutOurDiploms;
use frontend\modules\pages\models\media\PageMediaSpeekBlock;
use frontend\modules\pages\models\media\PageMediaWriteBlock;
use frontend\modules\projects\models\ProjectContent;
use frontend\modules\projects\models\ProjectRelatedProject;
use frontend\modules\projects\models\Projects;
use frontend\modules\projects\models\ProjectsStylesRelated;
use metalguardian\fileProcessor\helpers\FPM;
use yii\helpers\Html;

/**
 * Class Widget
 *
 * @package app\modules\banner\widgets\headBannerSlider
 */
class ShowMoreItemsButton extends \yii\base\Widget
{
    public $type;
    public $parentModelId;
    public $iteration;
    public $buttonLabel;

    public function run()
    {
        $view = $this->getWidgetView($this->type, $this->parentModelId);
        $showMoreElements = $this->getWidgetModels($this->type, $this->parentModelId);

        return $this->render($view, [
                'buttonLabel' => $this->buttonLabel,
                'showMoreElements' => $showMoreElements,
                'step' => ShowMoreButtonHelper::getStepByType($this->type),
                'iteration' => $this->iteration,
                'type' => $this->type,
                'parentModelId' => $this->parentModelId,
            ]
        );
    }

    protected function getViewAndModels($type, $parentModelId)
    {
        $view = '';
        $models = [];
        $step = ShowMoreButtonHelper::getStepByType($type);
        switch ($type) {
            case (ShowMoreButtonHelper::TYPE_MEDIA_WRITE_BLOCK):
                $view = 'mediaWriteBlock';
                $models = PageMediaWriteBlock::find()
                    ->where(['page_media_id' => $parentModelId])
                    ->isPublished()
                    ->offset($this->iteration*$step)
                    ->limit($step+1)
                    ->orderBy('position')
                    ->all();
                break;
            case (ShowMoreButtonHelper::TYPE_MEDIA_SPEEK_BLOCK):
                $view = 'mediaSpeekBlock';
                $models = PageMediaSpeekBlock::find()
                    ->where(['page_media_id' => $parentModelId])
                    ->isPublished()
                    ->offset($this->iteration*$step)
                    ->limit($step+1)
                    ->orderBy('position')
                    ->all();
                break;
            case (ShowMoreButtonHelper::TYPE_ABOUT_DIPLOMS_BLOCK):
                $view = 'aboutDiplomsBlock';
                $models = PageAboutOurDiploms::find()
                    ->where(['page_about_id' => $parentModelId])
                    ->isPublished()
                    ->offset($this->iteration*$step)
                    ->limit($step+1)
                    ->orderBy('position')
                    ->all();
                break;
            case (ShowMoreButtonHelper::TYPE_ONE_PROJECT_ANOTHER_PROJECT):
                $view = 'oneProjectAnotherProjectBlock';
                $models = Projects::find()
                    ->alias('t')
                    ->select('*')
                    ->joinWith(['projectsRelatedProjects pro'])
                    ->where([
                        't.id' => $parentModelId,
                        'pro.published' => 1
                    ])
                    ->offset($this->iteration*$step)
                    ->limit($step+1)
                    ->all();
                break;
            case (ShowMoreButtonHelper::TYPE_ONE_PROJECT_CONTENT):
                $view = 'oneProjectContent';
                $models = ProjectContent::find()
                    ->where(['project_id' => $parentModelId])
                    ->offset($this->iteration*$step)
                    ->limit($step+1)
                    ->orderBy('position')
                    ->all();
//                \help\dump($models);
                break;
            case (ShowMoreButtonHelper::TYPE_ONE_BLOG_ANOTHER_BLOG):
                $view = 'oneBlogAnotherBlogBlock';
                $models = Blogs::find()
                    ->alias('t')
                    ->select('*')
                    ->joinWith(['blogsRelatedBlogs pro'])
                    ->where([
                        't.id' => $parentModelId,
                        'pro.published' => 1
                    ])
                    ->offset($this->iteration*$step)
                    ->limit($step+1)
                    ->all();
                break;
            case (ShowMoreButtonHelper::TYPE_ONE_BLOG_CONTENT):
                $view = 'oneBlogContent';
                $models = BlogContent::find()
                    ->where(['blog_id' => $parentModelId])
                    ->offset($this->iteration*$step)
                    ->limit($step+1)
                    ->orderBy('position')
                    ->all();
//                \help\dump($models);
                break;
            default:
                $view = '';
                $models = [];
                break;

        }
        return [
            'view' => $view,
            'models' => $models,
        ];
    }

    protected function getWidgetView($type, $model)
    {
        return $this->getViewAndModels($type, $model)['view'];
    }

    protected function getWidgetModels($type, $model)
    {
        return $this->getViewAndModels($type, $model)['models'];
    }

    public function getOneProjectContentByType($model)
    {

        $result = '';
        switch ($model->type) {
            case (ProjectsHelper::CONTENT_TYPE_TWO_TEXT_COLUMN):
                $result .= Html::beginTag('div', ['class' => 'one-proj__text']);
                $result .= Html::tag('p', $model->label);
                $result .= Html::tag('p', $model->content);
                $result .= Html::endTag('div');
                break;
            case (ProjectsHelper::CONTENT_TYPE_SINGLE_IMAGE):
                $result .= Html::beginTag('div', ['class' => 'one-proj__img-w']);
                $result .= Html::beginTag('div', ['class' => 'img-anim-w posi']);
                $result .= TypicalFunction::getImage($model, 'singleImage', ['project', 'contentsingleimage'], ['class' => 'img-anim-w__img']);
                $result .= Html::endTag('div');
                $result .= Html::endTag('div');
                break;
            case (ProjectsHelper::CONTENT_TYPE_TWO_IMAGE):
                $result .= Html::beginTag('div', ['class' => 'one-proj__two-img-w']);
                foreach ($model->twoImages as $image) {
                    $result .= Html::beginTag('div', ['class' => 'img-anim-w posi']);
                    $result .= Html::tag('img', '', ['src' => FPM::src($image->file_id, 'project', 'contenttwoimage'), 'class' => 'img-anim-w__img']);
                    $result .= Html::endTag('div');
                }
                $result .= Html::endTag('div');
                break;
            default:
                $result = '';
                break;
        }
        return $result;
    }

    public function getOneBlogContentByType($model)
    {

        $result = '';
        switch ($model->type) {
            case (BlogsHelper::CONTENT_TYPE_TWO_TEXT_COLUMN):
                $result .= Html::beginTag('div', ['class' => 'one-proj__text']);
                $result .= Html::tag('p', $model->label);
                $result .= Html::tag('p', $model->content);
                $result .= Html::endTag('div');
                break;
            case (BlogsHelper::CONTENT_TYPE_SINGLE_IMAGE):
                $result .= Html::beginTag('div', ['class' => 'one-proj__img-w']);
                $result .= Html::beginTag('div', ['class' => 'img-anim-w posi']);
                $result .= TypicalFunction::getImage($model, 'singleImage', ['blog', 'contentsingleimage'], ['class' => 'img-anim-w__img']);
                $result .= Html::endTag('div');
                $result .= Html::endTag('div');
                break;
            case (BlogsHelper::CONTENT_TYPE_TWO_IMAGE):
                $result .= Html::beginTag('div', ['class' => 'one-proj__two-img-w']);
                foreach ($model->twoImages as $image) {
                    $result .= Html::beginTag('div', ['class' => 'img-anim-w posi']);
                    $result .= Html::tag('img', '', ['src' => FPM::src($image->file_id, 'blog', 'contenttwoimage'), 'class' => 'img-anim-w__img']);
                    $result .= Html::endTag('div');
                }
                $result .= Html::endTag('div');
                break;
            default:
                $result = '';
                break;
        }
        return $result;
    }


}
