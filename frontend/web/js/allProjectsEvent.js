$(function(){

    $(document).on('ready', function() {
        setAllStylesTabToActive()
    });
    $(document).on('pjax:end', function() {
        allProjectsBlockAnimatedElementAfterAjax()
        setDisplayNoneToSvgBlock($('.projects__col').length)
        rightSolutionFromAndyForFilters()
        rightSolutionFromAndyForFiltersStyles()
    });

    $(document).on('click', '.projects-filter-element', function (event) {
        event.preventDefault()
        toggleActiveFilterClass(this)

        var url = $('.projects-filter').data('url') + '/';
        var allActiveFilter = $('.projects-filter-element.active');
        var initStyle = false;

        allActiveFilter.each(function() {
            var filterType = $(this).closest('.data-type').data('type');
            var filterElementLabel = $(this).data('label');

            switch (filterType) {
                case 'category':
                    url += filterElementLabel;
                    break;
                case 'styles':
                    if (!initStyle) {
                        url += '/' + filterElementLabel;
                        initStyle = true;
                    }
                    else {
                        url += ';' + filterElementLabel;
                    }
                    break;
            }
        })
        console.log(url)
        if (url) {
            //window.location.href = url;
            $.pjax.reload('#projects-items', {
                url: url
            });
        }
    });
});

function toggleActiveFilterClass($el) {
    $that = $($el);
    var filterType = $($that).closest('.data-type').data('type');

    if (filterType == 'styles') {
        if ($that.hasClass('active')) {
            $that.removeClass('active')
            if ($($that).data('label') == 'all') {
                $('.projects-filter-element-styles').removeClass('active')
            } else {
                $('.projects-filter-element-styles-all').removeClass('active')
            }
        } else {
            $that.addClass('active')
            if ($($that).data('label') == 'all') {
                $('.projects-filter-element-styles').addClass('active')
            } else {
                setAllStylesTabToActive()
            }
        }
    } else {
        $('.projects-filter-element-category').removeClass('active')
        $that.addClass('active')
    }
}

function allProjectsBlockAnimatedElementAfterAjax() {
    setTimeout(function(){
        $('.projects__row.item').not('.animated').addClass('animated')
        // console.log($('.img-anim-w.posi').length)
    }, 500);
}

function setDisplayNoneToSvgBlock(elementCount) {
    if (elementCount < 3) {
        allprojectSvgBlock = $('.svg-all-project-block');
        if (!allprojectSvgBlock.hasClass('hide-svg')) {
            allprojectSvgBlock.addClass('hide-svg')
        }
    }
}

function setAllStylesTabToActive() {
    var stylesCount = $('.filter-styles-wrapper').data('styles-count');
    var alreadyActiveStyles = $('.projects-filter-element-styles:not(.projects-filter-element-styles-all).active').length;
    if (alreadyActiveStyles == stylesCount) {
        $('.projects-filter-element-styles-all').addClass('active')
    }
}