/**
 * Created by walter on 17.12.15.
 */

function parseResponse(response) {
    if (response.replaces instanceof Array) {
        for (var i = 0, ilen = response.replaces.length; i < ilen; i++) {
            $(response.replaces[i].what).replaceWith(response.replaces[i].data);
        }
    }
    if (response.append instanceof Array) {
        for (i = 0, ilen = response.append.length; i < ilen; i++) {
            $(response.append[i].what).append(response.append[i].data);
        }
    }
    if (response.content instanceof Array) {
        for (i = 0, ilen = response.content.length; i < ilen; i++) {
            $(response.content[i].what).html(response.content[i].data);
        }
    }
    if (response.js) {
        $("body").append(response.js);
    }
    if (response.refresh) {
        window.location.reload(true);
    }
    if (response.redirect) {
        window.location.href = response.redirect;
    }
}

$(function(){
    $(document).on('ready', function (event) {
        rightSolutionFromAndyForFilters()
        rightSolutionFromAndyForFiltersStyles()
    });
    $(document).on('click', '.ajax-link', function (event) {
        event.preventDefault();
        var url = $(this).attr('href');
        // console.log(url)
        // return false;
        var data = $('meta[name=csrf-param]').attr("content") + '=' + $('meta[name=csrf-token]').attr("content");
        executeAjaxRequest(url, data);
        return false;
    });
    $(document).on('submit', 'form.ajax-form', function (event) {
        event.preventDefault();
        var that = this;
        jQuery.ajax({
            'cache': false,
            'type': 'POST',
            'dataType': 'json',
            'data': $(that).serialize(),
            'success': function (response) {
                parseResponse(response);
            },
            'error': function (response) {
                alert(response.responseText);
            },
            'beforeSend': function () {
            },
            'complete': function () {
            },
            'url': this.action
        });
        return false;
    });
    $(document).on("click", ".form-submit", function (e) {
        e.preventDefault();
        $(this).parents('form').submit();
        return false;
    });
});

function executeAjaxRequest(url, data, completeCallback) {
    var postData = '';
    postData = data ? postData + data : postData;
    jQuery.ajax({
        'cache': false, 'type': 'POST', 'dataType': 'json', 'data': postData, 'success': function (response) {
            parseResponse(response);
        }, 'error': function (response) {
            alert(response.responseText);
        }, 'beforeSend': function () {
        }, 'complete': completeCallback ? completeCallback : function () {
        }, 'url': url
    });
}

function mediaWriteBlockAnimatedElementAfterAjax($parentClass) {
    setTimeout(function(){
        $('.img-anim-w.posi').not('.animated').addClass('animated')
        console.log($('.img-anim-w.posi').length)
    }, 500);
    $('body').animate({scrollTop: $('body').find('.' + $parentClass).offset().top - window.innerHeight + 85 }, 500);
    $('.js-vpo').modaal({ type: 'video' });
}

function showPopup() {
    $('.faynblat-call-you button').addClass('action-btn_done');
	window.location = "https://buro-faynblat.com/thankyou.html";
}

function rightSolutionFromAndyForFilters() {
    var filterBlock = $('.filter-type');

    filterBlock.find('.filter-type__chosen-text').text(filterBlock.find('.active').text());
}

function rightSolutionFromAndyForFiltersStyles() {
    // var filterBlock = $('.filter-style');
    //
    // filterBlock.find('.filter-type__chosen-text').text(filterBlock.find('.active').text());
}