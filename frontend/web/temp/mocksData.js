/* Module data structure */

// moduleName: {
//     dataType: {
//         property: value
//     }
// }

/* Module data example */

_template: {
    big: {
        title: 'Hello world',
        age: 10,
        button: false
    }
},

'advantage': {
    first: {
        number: '1',
        title: 'Индивидуальный подход',
        text: 'Каждый проект - воплощение вашей идеи. Поэтому и уникален',
    },
    second: {
        number: '2',
        title: 'Качество материалов',
        text: 'Делаем надолго, а значит не экономим на качестве. Закупаем лучшие материалы по оптимальным ценам',
    },
    third: {
        number: '3',
        title: 'Опыт и Команда',
        text: 'За 12 лет собрали команду профессионалов, которая знает, как создать лучший ландшафт и решить сложности во время проведения всех этапов работ.',
    },
    fourth: {
        number: '4',
        title: 'Полный спектр услуг',
        text: 'Организовываем все - от плана жилья, проводки и ремонта, до договоренностей с соседями и коммунальными службами.',
    },
    fifth: {
        number: '5',
        title: 'Конфиденциальность',
        text: 'Информация о проекте и клиенте не разглашается',
    }
},

whatWeDo: {
    first: {
        number: '1',
        title: 'Консультация',
        text: 'Бесплатно консультируем и отвечаем на все вопросы',
    },
    second: {
        number: '2',
        title: 'Конфиденциальность',
        text: 'При пожелании клиента, сохраняем его конфиденциальность',
    },
    third: {
        number: '3',
        title: 'Продуманность в деталях',
        text: 'Продумываем до мелочей каждый проект, основываясь на лайфстайле и привычках клиента',
    },
    fourth: {
        number: '4',
        title: 'Внесение правок',
        text: 'Вносим правки в дизайн-проект до полного удовлетворения клиента.',
    },
    fifth: {
        number: '5',
        title: 'Персональный менеджер',
        text: 'Выделяем личного менеджера, который находится на связи 24/7, на каждый проект',
    },
    sixth: {
        number: '6',
        title: 'Качественные материалы',
        text: 'Выбираем только лучшие материалы и подрядчиков',
    },
    seventh: {
        number: '7',
        title: 'Оперативность',
        text: 'Решаем все вопросы: договоренности с коммунальными службами и оформление документации',
    },
    eighth: {
        number: '8',
        title: 'Ответственность',
        text: 'Проект завершенн только тогда, когда передаем клиенту ключ',
    },
    ninth: {
        number: '9',
        title: 'Фиксированная стоимость',
        text: 'Стоимость  авторского сопровождения не меняется даже при увеличении сроков проекта',
    },
}
,

'isBase': {
    noneClass: {
        className: ''
    },
    base: {
        className: 'a-menu_base'
    },
}
,

'head': {
    defaults: {
        title: 'default title'

    }

}
,

'symbols': {
    s_symb: {
        symbol : '<!--S_symb-->\n' +
        '<svg class="n-symbol n-symbol__main-form" viewBox="0 0 132 207.7">\n' +
            '<path d="M113.4,0c-1,4.6-2.3,7.6-3.9,9.2c-1.7,1.5-3.9,2.4-6.2,2.3c-2.2,0-6.1-1.2-11.6-3.7 C79.9,2.6,68.7,0,58.2,0C41.3,0,27.3,5.2,16.4,15.5C5.5,25.8,0,38,0,52.3c-0.1,7.9,1.8,15.6,5.6,22.6c4.1,7.3,9.7,13.7,16.3,18.8 c7.2,5.7,19.3,13.3,36.3,22.8s27.4,15.7,31.2,18.7c5.1,3.7,9.4,8.4,12.5,13.9c2.6,4.5,3.9,9.6,4,14.8c0,8.8-3.5,16.4-10.6,22.9 c-7.1,6.4-16.7,9.7-28.9,9.7c-10.1,0.1-20.1-2.3-29.1-7c-8.8-4.7-15.4-10.6-19.7-17.7c-4.3-7.1-7.7-17.7-10.3-32H1.9v68.1h5.4 c0.7-4.6,1.7-7.6,3.1-9.1c1.5-1.5,3.6-2.3,5.8-2.2c2.5,0,8.6,1.5,18.1,4.6c9.5,3.1,15.8,4.9,18.8,5.3c5.4,0.9,10.8,1.3,16.3,1.3 c18.4,0,33.4-5.4,45-16.3c11.7-10.9,17.5-23.9,17.5-38.9c0-7.9-1.9-15.7-5.5-22.7c-3.8-7.4-9.1-13.8-15.7-18.8 c-6.8-5.3-19.5-13-38-22.9C50,75.8,35.6,66.1,29.6,58.7c-4-4.6-6.3-10.5-6.3-16.6c0-7.9,3.3-15,10-21.2c6.6-6.2,15-9.3,25.2-9.3 c9.1,0,18.1,2.4,26.1,6.9c8.1,4.3,14.8,10.7,19.4,18.6c4.5,7.8,7.7,18.3,9.4,31.5h5.4V0L113.4,0L113.4,0z"/>\n' +
        '</svg>'
    },
    t_symb: {
        symbol : '<!--T_symb-->\n' +
        '<svg class="n-symbol n-symbol__main-form" viewBox="0 0 166.7 198.6">\n' +
            '<path d="M2.3,0L0,46.6h5.6c0.7-9,2.8-16.1,6.4-21.4c3.6-5.3,8-8.8,13-10.7c3.9-1.4,10.5-2,19.8-2.1h23.9v151.8c0,11.1-1.1,18.3-3.2,21.4c-3.5,5.1-9.5,7.6-17.9,7.6h-7v5.4h84.2v-5.4h-6.9c-7.7,0-13.4-2.1-17.1-6.1c-2.6-3-4-10.6-4-22.9V12.4h28c8.2,0,14.8,1.3,19.8,4c5.2,2.9,9.4,7.2,12.1,12.5c1.9,3.5,3.3,9.4,4.4,17.6h5.6L164.5,0H2.3z"/>\n' +
        '</svg>'
    },
    a_symb: {
        symbol : '<!--A_symb-->\n' +
        '<svg class="n-symbol n-symbol__main-form" viewBox="0 0 210.9 203.2">\n' +
            '<path d="M146.9,165.2c3.9,9.3,5.9,16.1,5.9,20.4c0,3.2-1.6,6.3-4.2,8.2c-2.8,2.2-8.1,3.6-16,3.9v5.4h78.4v-5.4 c-8.1-0.5-14.4-2.7-19-6.7s-9.6-12.6-15.2-25.8L107.7,0h-5.1L32.7,163.5c-6.1,14.3-11.6,23.9-16.6,28.6c-2.4,2.3-7.8,4.3-16.1,5.7 v5.4h62.5v-5.4c-9.6-0.7-15.8-2.2-18.7-4.5c-2.9-2.3-4.3-5-4.3-8c0-3.8,1.7-9.6,5-17.3L58,136.7h76.9L146.9,165.2z M62.5,125.8 l34.6-80.3l33.7,80.3H62.5L62.5,125.8z"/>\n' +
        '</svg>'
    },
    none: {
        symbol : ''
    }

}
,

'stepWork': {
    first: {
        number: '1',
        text: 'Важный этап формирования доверия. Знакомимся и формируем картинку ваших желаний.',
        title: 'Знакомство'
    },
    second: {
        number: '2',
        text: 'Эскиз. Утверждение эскиза. Финальная версия проекта. Авторское сопровождение. ',
        title: 'Эскиз проекта'
    },
    third: {
        number: '3',
        text: 'Последний штрих - детали дизайна и декор.',
        title: 'Декорирование'

    },
    firstAbout: {
        number: '5',
        text: 'Последний штрих - детали дизайна и декор.',
        title: 'Декорирование'

    }
},

aboutStep: {
    first: {
        number: '1',
        text: 'Интерьер должен быть не только эстетически привлекательным, но и долговечным',
        title: 'Лучшие материалы и подрядчики'

    },
    second: {
        number: '2',
        text: 'Информация о проекте и клиенте не подлежит огласке',
        title: 'Абсолютная конфиденциальность'

    },
    third: {
        number: '3',
        text: 'Виктория лично сопровождает каждый проект: от первых эскизов до перерезанной красной ленточки в вашем новом доме',
        title: 'Авторский надзор'

    },
    fourth: {
        number: '4',
        text: 'Каждая деталь - логичное продолжение взглядов и мировоззрения заказчика. Материальное воплощение его души',
        title: 'Интерьер, как философия клиента'

    },
    fifth: {
        number: '5',
        text: 'Берем на себя подготовку всей документации. Личный менеджер в каждом проект находится на связи 24/7, решая любые вопросы – никаких форс-мажоров для клиента',
        title: 'Полное сопровождение'

    },
    sixth: {
        number: '6',
        text: 'Виктория лично сопровождает каждый проект: от первых эскизов до перерезанной красной ленточки в вашем новом доме',
        title: 'Авторский надзор'

    }
}
,

'text_h2-line': {
    h2: {
        title1: 'назначить встречу с викторией'
    },
    stepWorkh2:{
        title1: 'ЭТАПЫ РАБОТЫ'
    }

}
,

'text_h3': {
    h3: {
        title1: 'проекты'
    }

}

,

__pages: [{
                name: '00_home_faynblat',
                href: '00_home_faynblat.html'
             },{
                name: '01_projects_faynblat',
                href: '01_projects_faynblat.html'
             },{
                name: '02_open_project_faynblat',
                href: '02_open_project_faynblat.html'
             },{
                name: '03_about_faynblat',
                href: '03_about_faynblat.html'
             },{
                name: '04_services_faynblat',
                href: '04_services_faynblat.html'
             },{
                name: '05_design-interior_faynblat',
                href: '05_design-interior_faynblat.html'
             },{
                name: '06_building_faynblat',
                href: '06_building_faynblat.html'
             },{
                name: '07_landscaping_faynblat',
                href: '07_landscaping_faynblat.html'
             },{
                name: '08_architecture_faynblat',
                href: '08_architecture_faynblat.html'
             },{
                name: '09_projection_faynblat',
                href: '09_projection_faynblat.html'
             },{
                name: '10_media_faynblat',
                href: '10_media_faynblat.html'
             },{
                name: '12_contacts_faynblat',
                href: '12_contacts_faynblat.html'
             },{
                name: '404_faynblat',
                href: '404_faynblat.html'
             },{
                name: 'index',
                href: 'index.html'
             },{
                name: 'root',
                href: 'root.html'
             }]