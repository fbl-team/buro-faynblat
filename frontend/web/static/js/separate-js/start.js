$(document).ready(function() {
    //animation main page
    $(window).on("load", function() {
        tLogo.play();
        $_body.removeClass('preload-progress preload-cover');

        var msVideo = $('.main-screen__video-bg video');
        if(msVideo.length){
            msVideo.get(0).play();
        }
    });


    // vars
    var tLogo;
    var infoDigitSlider = $('.js-indigit-slider');
    var advantageSlider = $('.js-advantage-slider');
    var stepWorkSlider = $('.js-step-work-slider');
    var $_body = $('body');
    var _mainFormData = $('.main-form__data');
    var _isTabletSize = window.innerWidth >= 768 &&  window.innerWidth < 1200;
    var _isDescSize = window.innerWidth >= 1200;
    var _isMobSize = window.innerWidth < 768;

    // variables for aside menu
    var aMenu = $('.a-menu');
    var aMenuHeight = aMenu.height();
    var jsAncol = $('.js-ancolor');

    // создаем массив JQuery-объектов с классами-маркерами 'js-ancolor'
    var jsAncolors = [];
    for(var i = 0; i < jsAncol.length; ++i){
        jsAncolors[i] = $(jsAncol[i]);
    }

    // vars for service-video blocks
    var servicesVideoBlocks = $('.js-video-inview');
    var jsServVideos = [];
    // создаем массив JQuery-объектов с классами-маркерами 'js-video-inview'
    for(var i = 0; i < servicesVideoBlocks.length; ++i){
        jsServVideos[i] = $(servicesVideoBlocks[i]);
    }

    $(window).resize(function () {
        _isTabletSize = window.innerWidth >= 768 &&  window.innerWidth < 1200;
        _isDescSize = window.innerWidth >= 1200;
        _isMobSize = window.innerWidth < 768;

        videoFullscreenBackground();

        if(!_isMobSize){
            infoDigitSliderInit(infoDigitSlider);
            advantageSliderInit(advantageSlider);
            stepWorkSliderInit(stepWorkSlider);
        } else {
            if(infoDigitSlider.hasClass('slick-initialized')){
                infoDigitSlider.slick('unslick');
            }
            if(advantageSlider.hasClass('slick-initialized')){
                advantageSlider.slick('unslick');
            }
            if(stepWorkSlider.hasClass('slick-initialized')){
                stepWorkSlider.slick('unslick');
            }
        }
    });

    // вспомогательные переменные для aside-menu, чтобы, при вызове функции, каждый раз не создавать новые переменные
    var _temp_aMenuOffsetTop = 0;
    var _temp_itemOffsetTop = 0;
    // функция для проверки, находится ли плавающее меню на помеченном блоке
    function isAMenuInMarkedColor(item){
         _temp_aMenuOffsetTop =  aMenu.offset().top;
         _temp_itemOffsetTop =  item.offset().top;
        return (_temp_aMenuOffsetTop + aMenuHeight * .45 > _temp_itemOffsetTop && _temp_aMenuOffsetTop + aMenuHeight * .55 < _temp_itemOffsetTop + item.outerHeight());
    }

    // функция для проверки, находится ли ".js-video-inview" в области видимости и запуска его в этом случае или паузы
    function playPauseServVideosInView() {
        for (var i = 0; i < jsServVideos.length; i++) {
            if(jsServVideos[i].offset().top > window.scrollY && jsServVideos[i].offset().top + jsServVideos[i].outerHeight() < window.scrollY + window.innerHeight){
                // element is now visible in the viewport
                jsServVideos[i].get(0).play(); //console.log('play');
            } else {
                // element has gone out of viewport
                jsServVideos[i].get(0).pause(); //console.log('pause');
            }
        }
    }
    playPauseServVideosInView();

    window.onscroll = function() {
        if (!_isMobSize) {
            if (window.pageYOffset > 5) {
                aMenu.addClass('a-menu_center a-menu_close').removeClass('a-menu_open');
            } else {
                aMenu.removeClass('a-menu_center a-menu_close').addClass('a-menu_open');
            }

            // если a-menu на помеченном блоке - добавляем класс, меняющий цвета меню
            if( jsAncolors.some(isAMenuInMarkedColor) ){
                aMenu.addClass('a-menu_inv');
            } else {
                aMenu.removeClass('a-menu_inv');
            }

            if(!_isTabletSize){
                playPauseServVideosInView();
            }
        }

    };

    //open/close menu, mainScreen animation
    (function mainScreenAnimation_OpenCloseMenu() {
        //var for animation main page
        var logoPart1 = $(".first-part");
        var logoPart2 = $(".second-part");
        var logoPart3 = $(".third-part");
        var langTitle = $(".lang__title");
        var menuTitle = $(".menu__title_start");
        var line1 = $(".line1");
        var line2 = $(".line2");
        var line3 = $(".line3");
        var menuHeader = $(".menu");
        var mainText = $('.main-screen__content h1').find('span');
        var titleLine = $(".title__line");
        var videoBtn = $(".video-btn");
        var mainLang = $(".header-main__lang");
        var videoBtnText = $('.video-btn__text');
        var mouseScrollDown = $('.mouseScrollDown');


        // timeline for animation main page
        tLogo = new TimelineMax({paused: true, reversed: true});
        tLogo
            .from(logoPart1, 0.7,{autoAlpha: 0, y:25, delay:.2})
            .fromTo(line3, 0.2,{ y:25},{autoAlpha: 1, y:0},"-=0.6")
            .set(line3,{className:'+=line3_show'})
            .set(line2,{className:'+=line_start'})
            .set(line1,{className:'+=line_start'})
            .from(logoPart2, 0.7,{autoAlpha: 0, x:10}, "-=0.2")
            .from(logoPart3, 0.7,{autoAlpha: 0, y:25}, "-=0.7")
            .from(langTitle, 0.7,{autoAlpha: 0, y:25}, "-=0.7")
            .from(menuTitle, 0.7,{autoAlpha: 0, y:25}, "-=0.7")
            .from(titleLine, 0.5,{autoAlpha: 0, css:{height:"0"}})
            .staggerFrom(mainText, 0.7,{autoAlpha: 0, y:20},.2)
            .from(videoBtn, 0.7,{autoAlpha: 0, scale:0, rotation:360})
            .from(videoBtnText, 0.7,{autoAlpha: 0, y:-25})
            .from(mouseScrollDown, 0.7,{autoAlpha: 0, y:15},"-=0.7")
            .set([menuTitle,logoPart3,logoPart2,line1,line2,line3],{clearProps:'all'})
            .set(menuHeader,{className:'+=menu_show'})
            .set(mainLang,{className:'+=header-main__lang_show'});

        if(langTitle.length){
            tLogo.set(langTitle,{clearProps:'all'});
        }


        // menu-open
        var menuItem = $(".menu-open__sub").find('li');
        var lang = $('.header-main__lang');
        var social = $('.menu-open__social').find('.social');
        var menu = $('.menu-open');
        var tMenu = new TimelineMax({paused: true, reversed: true});


        var $window = $(window);
        var $HTML = $('html');
        var currBufTop = 0;

        //classes for menu logic
        var CML = {
            isHidden    :'is-hidden',
            noBodyScroll:'no-scroll'
        };

        function mobileNavOpen() {
            currBufTop = $window.scrollTop();
            setTimeout(function () {
                $HTML.addClass(CML.isHidden);
                $_body.addClass(CML.noBodyScroll).css('top', -currBufTop);
            }, 1200);
        }

        function mobileNavClose() {
            $HTML.removeClass(CML.isHidden);
            $_body.removeAttr('style').removeClass(CML.noBodyScroll);
            $_body.add($HTML).scrollTop(currBufTop);
        }

        $(document).on('click', '.menu', function (e) {
            $(this).toggleClass('js-change_menu-button');
            if(tMenu.reversed()) {
                tMenu.play();
                mobileNavOpen();
            } else {
                tMenu.reverse();
                mobileNavClose();
            }
        });


        tMenu
            .to(lang, 0.4,{ autoAlpha: 0, x: -20})
            .set(menu,{className:'+=js-menu-open'})
            .set(lang, {className:'+=js-lang_open'})
            .to(lang, 0.4, { autoAlpha: 1, x: 0})
            .staggerFrom( menuItem, 0.6, {y: -30, autoAlpha: 0}, 0.1)
            .from(social, 0.4,{autoAlpha: 0, x:-20});
        // end menu-open

        //about & services pages video start
        if(_isDescSize && ($_body.hasClass('about-page') || $_body.hasClass('services-page'))){
            // видео таймлайн для fscreen-блоков где есть видео
            var _cVideo = {
                videoEl: $('.fscreen__video-bg video').get(0),
                playw: $('.js-vplay'),
                mutew: $('.js-vmute'),
                currentTime: 0,
                playProgressInterval: '',
                playProgressBar: document.querySelector('.js-vprogress'),
                progressHolder: document.querySelector('.js-vprogress-w'),
                muteOnOff: function() {
                    _cVideo.videoEl.muted = !_cVideo.videoEl.muted;

                    if(_cVideo.videoEl.muted){
                        _cVideo.mutew.find('.mute-on').hide().siblings().show();
                    } else {
                        _cVideo.mutew.find('.mute-off').hide().siblings().show();
                    }
                },
                playPause: function() {
                    if ( _cVideo.videoEl.paused || _cVideo.videoEl.ended ) {
                        if ( _cVideo.videoEl.ended ) { _cVideo.videoEl.currentTime = 0; }
                        _cVideo.videoEl.play();
                        _cVideo.trackPlayProgress();
                        _cVideo.playw.find('.play').hide().siblings().show();
                    }
                    else {
                        _cVideo.videoEl.pause();
                        _cVideo.stopTrackingPlayProgress();
                        _cVideo.playw.find('.pause').hide().siblings().show();
                    }
                },
                updatePlayProgress : function(){
                    _cVideo.playProgressBar.style.width = ((_cVideo.progressHolder.offsetWidth /  _cVideo.videoEl.duration) *  _cVideo.videoEl.currentTime) + 'px';
                },
                trackPlayProgress : function(){
                    (function progressTrack() {
                        _cVideo.updatePlayProgress();
                        _cVideo.playProgressInterval = setTimeout(progressTrack, 50);
                    })();
                },
                stopTrackingPlayProgress : function(){
                    clearTimeout( _cVideo.playProgressInterval );
                }
            };

            if(_cVideo.videoEl) {
                _cVideo.videoEl.addEventListener('ended', function () {
                    _cVideo.stopTrackingPlayProgress();
                    _cVideo.playw.find('.pause').hide().siblings().show();
                }, false);
                _cVideo.videoEl.addEventListener('pause', function () {
                    // Video was paused, stop tracking progress.
                    _cVideo.stopTrackingPlayProgress();
                }, false);
                _cVideo.videoEl.addEventListener('play', function () {
                    // Video was paused, stop tracking progress.
                    _cVideo.trackPlayProgress();
                }, false);
                _cVideo.videoEl.addEventListener('click', this.playPause, false);

                setTimeout(function () {
                    aMenu.addClass('a-menu_close').removeClass('a-menu_open');
                    $('.js-video-control').css('opacity', '1');
                    $(_cVideo.videoEl).parent().css({'opacity': 1, 'z-index': 1}).end().get(0).play();
                    $('.fscreen__content-iw').addClass('color-main');
                }, 3000);

                $('.js-vplay').on('click', function () {
                    _cVideo.playPause();
                });
                $('.js-vmute').on('click', function () {
                    _cVideo.muteOnOff();
                });
            }
        }

        //input logic
        $('.form-control')
            .blur(function(){
                var $_this = $(this);
                var _parentWrap = $_this.parent('.form-group');

                if ( $_this.val() ) {
                    _parentWrap.addClass('form-group_hide-label');
                } else {
                    _parentWrap.removeClass('form-group_hide-label');
                }
            })
            .on('click', function () {
                var _parentWrap = $(this).parent('.form-group');

                if(_parentWrap.hasClass('has-error')){
                    _parentWrap.removeClass('has-error');
                }
            });


        //scroll down
        mouseScrollDown.click(function (e) {
            e.preventDefault();
            $('body, html').animate({scrollTop: window.innerHeight}, 700);
        });
    })();

    // возникновение события, когда блок появился в области видимости
    $('.projects__row, .heading, .main-form, .posi, .n-symbol_hide, .js-num-anim').on('inview', function(event, isInView) {
        var $_this = $(this);

        if (isInView) {
            // element is now visible in the viewport
            $_this.addClass('animated');
            if($_this.hasClass('projects__row')){
                setTimeout(function () {
                    $_this.addClass('hover');
                }, 2000);
            }
        } else {
            // element has gone out of viewport
            // $_this.off('inview');
            $_this.removeClass('animated');

            if($_this.hasClass('projects__row')){
                $_this.removeClass('hover');
            }
        }
    });

    // counting, когда блок ".info-in-digits__value" появился в области видимости
    $('.info-in-digits__value').on('inview', function(event, isInView) {
        var $_this = $(this);

        if (isInView) {
            // element is now visible in the viewport
            if(!$_this.hasClass('animated')) {

                var delay = (_isMobSize ? 0 : $_this.closest('.info-in-digits__item').data('num') * 500);

                setTimeout(function () {
                    $_this.addClass('animated');
                    $_this.animate({
                        counter: $_this.data('count')
                    }, {
                        duration: 3000,
                        easing: 'swing',
                        step: function (now) {
                            $_this.text(Math.ceil(now));
                        }
                    });
                    $_this.siblings('.info-in-digits__column').addClass('animated');
                }, delay);
            }
        } else {
            // element has gone out of viewport
            // $_this.off('inview');
            $_this.finish().animate({ counter: 0 }).text('0').siblings('.info-in-digits__column').andSelf().removeClass('animated');
        }
    });

    setFullStrokeDashOffsetArray($('.sq-figure').find('path'));
    // blocks with digits inside square wrapper
    $('.js-sq-item-w').on('inview', function(event, isInView) {
        var $_this = $(this);
        if (isInView) {
            // element is now visible in the viewport
            if(!$_this.hasClass('animated')) {
                var delay = (_isMobSize ? 0 : $_this.data('dnum') * 700);
                setTimeout(function () {
                    new TimelineLite()
                        .to($_this.find('path'), 1.8,  {'stroke-dashoffset': 0, ease: Power1.easeInOut})
                        .set($_this,{className: '+=animated'})
                        .to($_this.find('path'), 1.8,  {'stroke': '#fff', ease: Power1.easeInOut})
                    ;
                }, delay);
            }
        } else {
            $_this.removeClass('animated').find('path').attr('style', '');
            setFullStrokeDashOffsetArray($_this.find('path'));
        }
    });

    /**
     *
     * установка параметров для stroke-dasharray и stroke-dashoffset в размер всей длинны "пути" getTotalLength
     * @param pathArray - массив с элементами path, которым будут установлены параметры
     */
    function setFullStrokeDashOffsetArray(pathArray){
        if(pathArray.length) {
            for (var i = 0; i < pathArray.length; i++) {
                var currLength = pathArray[i].getTotalLength();
                $(pathArray[i]).css({
                    'stroke-dasharray': currLength + ' ' + currLength,
                    'stroke-dashoffset': currLength
                });
            }
        }
    }

    // ресайз video с классом .video-to-parentsize по размерам родительского блока с классом .parentsize
    function videoFullscreenBackground() {
        var blockToResize = $('.video-to-parentsize');
        var parentBlock;

        if(blockToResize.length) {

            var ratio = 16 / 9;
            var width,
                pWidth, // player width, to be defined
                height,
                pHeight; // player height, tbd

            blockToResize.each(function () {
                var $_this = $(this);

                parentBlock = blockToResize.closest('.parentsize');
                width = parentBlock.width();
                height = parentBlock.height();

                // when screen aspect ratio differs from video, video must center and underlay one dimension
                if (width / ratio < height) { // if new video height < window height (gap underneath)
                    pWidth = Math.ceil(height * ratio); // get new player width
                    $_this.width(pWidth).height(height).css({left: (width - pWidth) / 2, top: 0}); // player width is greater, offset left; reset top
                } else { // new video width < window width (gap to right)
                    pHeight = Math.ceil(width / ratio); // get new player height
                    $_this.width(width).height(pHeight).css({left: 0, top: (height - pHeight) / 2}); // player height is greater, offset top; reset left
                }
            });
        }
    }
    videoFullscreenBackground();

    // запуск/пауза видео на домашней странице
    (function homeVideoVimeo() {
        var _innerVideoContainer = $("#video-home-fs");
        var _videoPlayBtn = $(".js-video-home");

        if(_innerVideoContainer.length){
            var _videoContainerWrap = _innerVideoContainer.parent();

            var vimeoOption = {
                id: _innerVideoContainer.data('video-src'),
                width: _videoContainerWrap.width(),
                height: _videoContainerWrap.height(),
                'scrolling' : 'yes'
            };

            var vimeoPlayer = new Vimeo.Player(_innerVideoContainer[0], vimeoOption);
            vimeoPlayer.disableTextTrack();

            vimeoPlayer.on('pause', function (){
                _videoContainerWrap.removeClass('active');
            });
            vimeoPlayer.on('ended', function (){
                _videoContainerWrap.removeClass('active');
            });


            _videoPlayBtn.on('click',  function (e) {
                e.preventDefault();

                // _videoPlayBtn.fadeOut();
                // setTimeout( function () {
                _videoContainerWrap.addClass('active');
                vimeoPlayer.play();
                // }, 1000);

                return false;
            });

            $('.main-screen').on("click", function (){
                vimeoPlayer.pause();
            });
        }
    })();

    // запуск видео в видео-галерее
    (function videoGalleryPlay() {
        $('.js-vpo').modaal({
            type: 'video'
        });
    })();
    // дропдауны на странице проектов
    (function projectsFilters() {
        var projectsFilters = $('.js-filter');

        if(projectsFilters.length) {
            // открытие или закрытие дропдауна
            projectsFilters.click(function () {
                var $_this = $(this);
                var $_thisParentBlock = $_this.parent();

                // закрытие второго дропдауна
                $_thisParentBlock.siblings('.js-filter-block').css('z-index', 0).removeClass('is-open').find('.js-filter-list').slideUp();
                $_this.next().slideToggle();
                $_thisParentBlock.toggleClass('is-open');

                if ($_thisParentBlock.hasClass('is-open')) {
                    $_thisParentBlock.css('z-index', 5);
                } else if (!$_thisParentBlock.hasClass('filter-type')) {
                    $_thisParentBlock.css('z-index', 0);
                }
            });

            // закрытие фильтра при клике вне его области
            $(document).mouseup(function (e) {
                if (_isMobSize) {
                    if (!$(e.target).closest('.js-filter-block').length && projectsFilters.parent().is('.is-open')) {
                        projectsFilters.siblings('.js-filter-list').slideUp().closest('.js-filter-block').removeClass('is-open');
                        event.stopPropagation();
                        return;
                    }
                    event.stopPropagation();
                }
            });
        }
    })();

//////////////////////////////////////////////////////////////////////////////////////////////////////////
// slick sliders init

    // слайдер для блока info-in-digits на странице about. Входядщий параметр обьект jQuery
    function infoDigitSliderInit(infoDigitSlider){
        if(infoDigitSlider.length && !_isMobSize && !infoDigitSlider.hasClass('slick-initialized')){
            var nextArrow = $('<svg class="pagin__arrow pagin__arrow_prev"><use xlink:href="#arrow-drop"></use></svg>');
            var prevArrow = $('<svg class="pagin__arrow pagin__arrow_next"><use xlink:href="#arrow-drop"></use></svg>');
            var navBlock = $('.info-in-digits .pagin');

            infoDigitSlider.on('init breakpoint', function (slick) {
                navBlock.empty();
                $(navBlock, infoDigitSlider).append(nextArrow);
                $(navBlock, infoDigitSlider).append(prevArrow);

                nextArrow.click(function () {
                    infoDigitSlider.slick('slickPrev');
                });
                prevArrow.click(function () {
                    infoDigitSlider.slick('slickNext');
                });
            });

            infoDigitSlider.on('destroy', function (slick) {
                navBlock.empty();
            });

            infoDigitSlider.slick({
                dots: false,
                speed: 650,
                slidesToShow: 6,
                slidesToScroll: 1,
                infinite: false,
                arrows: false,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 1,
                            dots: false,
                        }
                    },
                ]
            });
        }
    }
    if(_isTabletSize || _isDescSize && infoDigitSlider.children().length > 6) {
        infoDigitSliderInit(infoDigitSlider);
    }

    // слайдеры для блоков advantage. Входядщий параметр обьект jQuery
    function advantageSliderInit(advantageSlider){
        if(advantageSlider.length && !_isMobSize && !advantageSlider.hasClass('slick-initialized')){

            var nextArrow = $('<svg class="pagin__arrow pagin__arrow_prev"><use xlink:href="#arrow-drop"></use></svg>');
            var prevArrow = $('<svg class="pagin__arrow pagin__arrow_next"><use xlink:href="#arrow-drop"></use></svg>');
            var navBlock = $('.advantage-work .pagin');

            advantageSlider.on('init breakpoint', function (slick) {
                navBlock.empty();
                $(navBlock, advantageSlider).append(nextArrow);
                $(navBlock, advantageSlider).append(prevArrow);

                nextArrow.click(function () {
                    advantageSlider.slick('slickPrev');
                });
                prevArrow.click(function () {
                    advantageSlider.slick('slickNext');
                });
            });

            advantageSlider.on('destroy', function (slick) {
                navBlock.empty();
            });

            advantageSlider.slick({
                dots: false,
                speed: 650,
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: false,
                arrows: false,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 3,
                        }
                    },
                ]
            });
        }
    }

    var advantageSLidesLength =  advantageSlider.children().length;
    if(_isTabletSize && advantageSLidesLength > 3 || _isDescSize && advantageSLidesLength > 4) {
        advantageSliderInit(advantageSlider);
    }

    // слайдеры для блоков step-work. Входядщий параметр обьект jQuery
    function stepWorkSliderInit(stepWorkSlider){
        if(stepWorkSlider.length && !_isMobSize && !stepWorkSlider.hasClass('slick-initialized')){
            var nextArrow = $('<svg class="pagin__arrow pagin__arrow_prev"><use xlink:href="#arrow-drop"></use></svg>');
            var prevArrow = $('<svg class="pagin__arrow pagin__arrow_next"><use xlink:href="#arrow-drop"></use></svg>');
            var navBlock = $('.step-work .pagin');

            stepWorkSlider.on('init breakpoint', function (slick) {
                navBlock.empty();
                $(navBlock, stepWorkSlider).append(nextArrow);
                $(navBlock, stepWorkSlider).append(prevArrow);

                nextArrow.click(function () {
                    stepWorkSlider.slick('slickPrev');
                });
                prevArrow.click(function () {
                    stepWorkSlider.slick('slickNext');
                });
            });

            stepWorkSlider.on('destroy', function (slick) {
                navBlock.empty();
            });

            stepWorkSlider.slick({
                dots: false,
                speed: 650,
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: false,
                arrows: false,
                adaptiveHeight: true,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 3,
                        }
                    },
                ]
            });
        }
    }

    var stepWorkSLidesLength =  stepWorkSlider.children().length;
    if(_isTabletSize  && stepWorkSLidesLength > 3 || _isDescSize && stepWorkSLidesLength > 4) {
        stepWorkSliderInit(stepWorkSlider);
    }

// end slick sliders init
//////////////////////////////////////////////////////////////////////////////////////////////////////////

    // подьезд к форме обратной связи
    $('.js-to-main-form').click(function (e) {
        e.preventDefault();
        $('body, html').animate({scrollTop: _mainFormData.offset().top - 100}, 700);
    });

    // main-text show all
    $('.js-read-all').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var $_this = $(this);
        var mainTextContent = $_this.closest('.main-text').find('.main-text__content');


        if(mainTextContent.hasClass('sized')) {
            mainTextContent.removeClass('sized').css('height', '');
            $_this.addClass('read-all__link_hidden');
        } else {
            var textItems = mainTextContent.find('.main-text__item');
            var maxheight = mainTextContent.outerHeight();

            if(_isDescSize){
                for (var i = 0; i < textItems.length; ++i) {
                    maxheight = Math.max(maxheight, $(textItems[i]).outerHeight());
                }
            } else {
                maxheight = 0;
                for (var i = 0; i < textItems.length; ++i) {
                    maxheight += $(textItems[i]).outerHeight();
                }
            }
            $_this.removeClass('read-all__link_hidden');
            mainTextContent.addClass('sized').css('height', maxheight + 'px');
        }
    });

    // aside-menu__link click
    aMenu.find('.a-menu__link').click(function (e) {
        if(_isMobSize) {
            var $_thisParent = $(this).closest('.a-menu__item');
            if($_thisParent.hasClass('active')){
                e.preventDefault();
                e.stopPropagation();

                $_thisParent.siblings().slideToggle();
                aMenu.toggleClass('a-menu_close').toggleClass('a-menu_open');
            }
        }
    });
    // aside-menu click
    aMenu.click(function (e) {
        var $_this = $(this);
        if(!_isDescSize || $_this.hasClass('a-menu_close')) {
            $_this.toggleClass('a-menu_close').toggleClass('a-menu_open');
        }
    });
    // aside-menu hover
    aMenu.hover(
        function (e) {
            if(_isDescSize) {
                $(this).removeClass('a-menu_close').addClass('a-menu_open');
            }
        },
        function (e) {
            if(_isDescSize && window.pageYOffset > 5) {
                $(this).addClass('a-menu_close').removeClass('a-menu_open');
            }
        }
    );


    if(_isMobSize){
        // aMenu.addClass('a-menu_close').removeClass('a-menu_open');
        // aMenu.find('.a-menu__link').click();
    } else if (window.pageYOffset > 5 && !_isMobSize) {
        aMenu.addClass('a-menu_center');
    }


    var fscreenContent = $('.fscreen__content');
    matrixStyleRounding(fscreenContent);

    // пересчет матрицы (округление значений к интеджерам) для абсолютно спозиционированного
    // по центру блока, чтобы не было размытия шрифта из-за плавающих значений. Входящий параметр обьект JQuery
    function matrixStyleRounding(inObj) {
        if(inObj.length){
            for(var i = 0; i < inObj.length; ++i){
                var tmpObj = $(inObj[i]);
                var matrix = tmpObj.css('transform');
                matrix = matrix.slice(7, -1).split(' ').map(function(item) {
                    return parseInt(item);
                });
                tmpObj.css('transform', 'matrix(' + matrix.join(',') + ')');
            }
        }
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// fs-slider
    /**
     * основа взята отсюда http://codepen.io/Creaticode/full/xIfmw
     */
    $(function() {
        /** -----------------------------------------
         * Slider Module
         -------------------------------------------*/
        var controlWrapper = $('#fs-slider-controls');
        var controlItems = $('.fs-slider__controls-item', controlWrapper);

        var SliderModule = (function() {
            var pb = {};
            pb.el = $('.fs-slider');
            pb.items = {
                panels: pb.el.find('.fs-slider__slide'),
            }

            // Slider Interval
            var SliderInterval,
                currentSlider = 0,
                nextSlider = 1,
                lengthSlider = pb.items.panels.length;

            // Slider Constructor
            pb.init = function(settings) {
                this.settings = settings || {duration: 8000};

                // Slider Activate
                activateSlider();
                // Events controls
                controlWrapper.on('click', '.fs-slider__controls-item', function(e) {
                    var $this = $(this);
                    if(!(currentSlider === $this.index())) {
                        changePanel($this.index());
                    }
                });
            };

            // Function to activate the Slider
            var activateSlider = function() {
                SliderInterval = setInterval(pb.startSlider, pb.settings.duration);
            }

            // Function for animation
            pb.startSlider = function() {
                var items = pb.items;
                // Check if the last panel to restart the count
                if(nextSlider >= lengthSlider) {
                    nextSlider = 0;
                    currentSlider = lengthSlider-1;
                }

                controlItems.removeClass('active').eq(nextSlider).addClass('active');
                items.panels.eq(currentSlider).removeClass('current-slide');
                items.panels.eq(nextSlider).addClass('current-slide');

                // Update data slider
                currentSlider = nextSlider;
                nextSlider += 1;
            }

            // Panel switching function with Controls
            var changePanel = function(id) {
                clearInterval(SliderInterval);
                var items = pb.items;
                // We check if the ID is available between panels
                if(id >= lengthSlider) {
                    id = 0;
                } else if(id < 0) {
                    id = lengthSlider-1;
                }

                controlItems.removeClass('active').eq(id).addClass('active');
                items.panels.eq(currentSlider).removeClass('current-slide');
                items.panels.eq(id).addClass('current-slide');

                // We return to update the data slider
                currentSlider = id;
                nextSlider = id+1;
                // Reactivate slider
                activateSlider();
            };

            return pb;
        }());

        SliderModule.init({duration: 5000});

    });
// end fs-slider
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  svg draw-and-fill

    /**
     * svganimations.js v1.0.0
     * http://tympanus.net/codrops/2013/12/30/svg-drawing-animation/
     *
     * the svg path animation is based on http://24ways.org/2013/animating-vectors-with-svg/ by Brian Suda (@briansuda)
     *
     * Licensed under the MIT license.
     * http://www.opensource.org/licenses/mit-license.php
     *
     * Copyright 2013, Codrops
     * http://www.codrops.com
     */
    (function() {
        'use strict';
        var docElem = window.document.documentElement;

        window.requestAnimFrame = function(){
            return (
                window.requestAnimationFrame       ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame    ||
                window.oRequestAnimationFrame      ||
                window.msRequestAnimationFrame     ||
                function(/* function */ callback){
                    window.setTimeout(callback, 1000 / 60);
                }
            );
        }();

        window.cancelAnimFrame = function(){
            return (
                window.cancelAnimationFrame       ||
                window.webkitCancelAnimationFrame ||
                window.mozCancelAnimationFrame    ||
                window.oCancelAnimationFrame      ||
                window.msCancelAnimationFrame     ||
                function(id){
                    window.clearTimeout(id);
                }
            );
        }();

        function SVGEl( el ) {
            this.el = el;
            this.current_frame = 0;
            this.total_frames = 90;
            this.path = [];
            this.length = [];
            this.handle = 0;
            this.init();
        }

        SVGEl.prototype.init = function() {
            var self = this;
            [].slice.call( this.el.querySelectorAll( 'path' ) ).forEach( function( path, i ) {
                self.path[i] = path;
                var l = self.path[i].getTotalLength();
                self.length[i] = l;
                self.path[i].style.strokeDasharray = l + ' ' + l;
                self.path[i].style.strokeDashoffset = l;
            } );
        };

        SVGEl.prototype.render = function() {
            if( this.rendered ) return;
            this.rendered = true;
            this.draw();
        };

        SVGEl.prototype.draw = function() {
            var self = this,
                progress = this.current_frame/this.total_frames;
            if (progress > 1) {
                window.cancelAnimFrame(this.handle);
                this.fillShape();
            } else {
                this.current_frame++;
                for(var j=0, len = this.path.length; j<len;j++){
                    this.path[j].style.strokeDashoffset = Math.floor(this.length[j] * (1 - progress));
                }
                this.handle = window.requestAnimFrame(function() { self.draw(); });
            }
        };

        // включаем заливку
        SVGEl.prototype.fillShape = function() {
            this.el.classList.add( 'filled-active' );
            // this.el.classList.add( 'nsToHide' );

            var $_this = this;
            setTimeout(function () {
                if($_this.el.classList.contains('filled-active')) {
                    $_this.el.classList.add( 'nsToHide', 'filled-base' );
                }
            }, 1500);
        };

        function getViewportH() {
            var client = docElem['clientHeight'],
                inner = window['innerHeight'];

            if( client < inner )
                return inner;
            else
                return client;
        }

        function scrollY() {
            return window.pageYOffset || docElem.scrollTop;
        }

        function inViewport( el, h ) {
            var elH = el.getBBox().height,
                scrolled = scrollY(),
                viewed = scrolled + getViewportH(),
                elTop = $(el).offset().top,
                elBottom = elTop + elH,
                // if 0, the element is considered in the viewport as soon as it enters.
                // if 1, the element is considered in the viewport only when it's fully inside
                // value in percentage (1 >= h >= 0)
                h = h || 0;

            return (elTop + elH * h) <= viewed && (elBottom) >= scrolled;
        }

        function init() {
            var svgs = Array.prototype.slice.call( document.querySelectorAll( 'svg.n-symbol' ) ),
                svgArr = [],
                didScroll = false,
                resizeTimeout;

            // the svgs already shown...
            svgs.forEach( function( el, i ) {
                var svg = new SVGEl( el );
                svgArr[i] = svg;
                setTimeout(function( el ) {
                    return function() {
                        if( inViewport( el ) ) {
                            svg.render();
                        }
                    };
                }( el ), 250 );
            } );

            var scrollHandler = function() {
                    if( !didScroll ) {
                        didScroll = true;
                        setTimeout( function() { scrollPage(); }, 60 );
                    }
                },
                scrollPage = function() {
                    svgs.forEach( function( el, i ) {
                        if( inViewport( el, 0.5 ) ) {
                            svgArr[i].render();
                        } else {
                            if(svgArr[i].el.classList.contains('filled-active')) {
                                svgArr[i].current_frame = 0;
                                svgArr[i].total_frames = 90;
                                svgArr[i].path = [];
                                svgArr[i].length = [];
                                svgArr[i].handle = 0;
                                svgArr[i].init();
                                svgArr[i].rendered = false;
                                svgArr[i].el.classList.remove('filled-base', 'nsToHide', 'filled-active');
                            }
                        }
                    });
                    didScroll = false;
                },
                resizeHandler = function() {
                    function delayed() {
                        scrollPage();
                        resizeTimeout = null;
                    }
                    if ( resizeTimeout ) {
                        clearTimeout( resizeTimeout );
                    }
                    resizeTimeout = setTimeout( delayed, 200 );
                };

            window.addEventListener( 'scroll', scrollHandler, false );
            window.addEventListener( 'resize', resizeHandler, false );
        }

        init();

    })();

// end svg draw-and-fill
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
});
