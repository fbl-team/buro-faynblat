<?php
namespace frontend\controllers;

use frontend\components\MetaTagRegisterCustom;
use frontend\modules\pages\models\oneBlog\PageOneBlog;
use common\helpers\BlogsHelper;
use common\models\Robots;
use frontend\components\FrontendController;
use frontend\modules\pages\models\allBlogs\PageAllBlogs;
use frontend\modules\blogs\helpers\HuuHelper;
use frontend\modules\blogs\models\Blogs;
use notgosu\yii2\modules\metaTag\components\MetaTagRegister;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class BlogController extends FrontendController
{

    public function actionAllBlogs()
    {
        $model = PageAllBlogs::find()->one();

        if(!$model) {
            throw new NotFoundHttpException;
        }

        MetaTagRegister::register($model);
//        $this->registerMetaTags($model, $model->label, $model->description);

        //значения выбраных фильтров
        $category = $this->getFiltersParam('category');
        $styles = $this->getFiltersParam('styles');

        //проэкты в пагинации
        $blogsQuery = Blogs::find()
            ->alias('t')
            ->orderBy('position ASC')
            ->andWhere(['t.published' => true]);

        if ($category && $category !== 'all') {
            $blogsQuery->andWhere(['category_id' => $category]);
        }

        if (!empty($styles)) {
            $blogsQuery->joinWith('blogsStyles')
                ->andWhere(['in', 'blogs_styles_related.blog_style_id', $styles]);
        }

        $dataProviderBlogs = new ActiveDataProvider([
            'query' => $blogsQuery,
            'pagination' => [
                'pageSize' => BlogsHelper::PAGE_ALL_BLOG_PAGINATION_COUNT,
                'pageSizeParam' => false
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ]
        ]);


        return $this->render('all-blogs',
            [
                'model' => $model,
                'dataProviderBlogs' => $dataProviderBlogs,
                'selectedCategories' => $category === null ? [] : [$category],
                'selectedStyles' => $styles === null ? [] : [$styles],
            ]
        );
    }

    public function actionOneBlog($alias)
    {
        $model = PageOneBlog::find()->one();
        $blog = Blogs::find()
            ->where([
                'alias' => $alias,
                'published' => true
            ])->one();

        if(!($model && $blog)) {
            throw new NotFoundHttpException;
        }

        $this->registerMetaTags($blog->blogBigImage, $blog->label, $blog->city_and_square);
        MetaTagRegister::register($blog);

        return $this->render('one-blog',
            [
                'model' => $model,
                'blog' => $blog
            ]
        );
    }

    public function getFiltersParam($type)
    {
        if ($type === 'category') {
            return HuuHelper::getCategoryId(
                Yii::$app->getRequest()->get('category')
            );
        }
        elseif ($type === 'styles') {
            $data = Yii::$app->getRequest()->get('styles');
            if (!empty($data)) {
                $styles = explode(';', $data);
                $res = [];
                foreach ($styles as $label) {
                    $res[] = HuuHelper::getStyleId($label);
                }

                return $res;
            }
        }

        return null;
    }
}
