<?php
namespace frontend\controllers;

use frontend\components\MetaTagRegisterCustom;
use frontend\modules\pages\models\oneProject\PageOneProject;
use common\helpers\ProjectsHelper;
use common\models\Robots;
use frontend\components\FrontendController;
use frontend\modules\pages\models\allProjects\PageAllProjects;
use frontend\modules\projects\helpers\HuuHelper;
use frontend\modules\projects\models\Projects;
use notgosu\yii2\modules\metaTag\components\MetaTagRegister;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class ProjectController extends FrontendController
{

    public function actionAllProjects()
    {
        $model = PageAllProjects::find()->one();

        if(!$model) {
            throw new NotFoundHttpException;
        }

        MetaTagRegister::register($model);
//        $this->registerMetaTags($model, $model->label, $model->description);

        //значения выбраных фильтров
        $category = $this->getFiltersParam('category');
        $styles = $this->getFiltersParam('styles');

        //проэкты в пагинации
        $projectsQuery = Projects::find()
            ->alias('t')
            ->orderBy('position ASC')
            ->andWhere(['t.published' => true]);

        if ($category && $category !== 'all') {
            $projectsQuery->andWhere(['category_id' => $category]);
        }

        if (!empty($styles)) {
            $projectsQuery->joinWith('projectsStyles')
                ->andWhere(['in', 'projects_styles_related.project_style_id', $styles]);
        }

        $dataProviderProjects = new ActiveDataProvider([
            'query' => $projectsQuery,
            'pagination' => [
                'pageSize' => ProjectsHelper::PAGE_ALL_PROJECTS_PAGINATION_COUNT,
                'pageSizeParam' => false
            ],
            'sort' => [
                'defaultOrder' => [
                    'created_at' => SORT_DESC,
                ],
            ]
        ]);


        return $this->render('all-projects',
            [
                'model' => $model,
                'dataProviderProjects' => $dataProviderProjects,
                'selectedCategories' => $category === null ? [] : [$category],
                'selectedStyles' => $styles === null ? [] : [$styles],
            ]
        );
    }

    public function actionOneProject($alias)
    {
        $model = PageOneProject::find()->one();
        $project = Projects::find()
            ->where([
                'alias' => $alias,
                'published' => true
            ])->one();

        if(!($model && $project)) {
            throw new NotFoundHttpException;
        }

        $this->registerMetaTags($project->projectBigImage, $project->label, $project->city_and_square);
        MetaTagRegister::register($project);

        return $this->render('one-project',
            [
                'model' => $model,
                'project' => $project
            ]
        );
    }

    public function getFiltersParam($type)
    {
        if ($type === 'category') {
            return HuuHelper::getCategoryId(
                Yii::$app->getRequest()->get('category')
            );
        }
        elseif ($type === 'styles') {
            $data = Yii::$app->getRequest()->get('styles');
            if (!empty($data)) {
                $styles = explode(';', $data);
                $res = [];
                foreach ($styles as $label) {
                    $res[] = HuuHelper::getStyleId($label);
                }

                return $res;
            }
        }

        return null;
    }
}
