<?php
namespace frontend\controllers;

use common\models\Robots;
use frontend\components\FrontendController;
use frontend\components\MetaTagRegisterCustom;
use frontend\modules\pages\models\media\PageMedia;
use notgosu\yii2\modules\metaTag\components\MetaTagRegister;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class MediaController extends FrontendController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = PageMedia::find()->with([
            'speekBlocks',
            'writeBlocks',
        ])->one();

        if(!$model) {
            throw new NotFoundHttpException;
        }

        MetaTagRegister::register($model);
//        $this->registerMetaTags($model->, 'asdasda1', 'asdasdasmda.,m');

        return $this->render('index',
            [
                'model' => $model,
            ]
        );
    }
}
