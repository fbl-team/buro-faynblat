<?php
namespace frontend\controllers;

use common\models\Robots;
use frontend\components\FrontendController;
use frontend\components\MetaTagRegisterCustom;
use frontend\modules\pages\models\services\PageServices;
use notgosu\yii2\modules\metaTag\components\MetaTagRegister;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class ServicesController extends FrontendController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = PageServices::find()->with([
            'firstScreenImage',
            'firstScreenVideos',
            'howWeDoBlock',
            'whatWeDoBlock',
        ])->one();

        if(!$model) {
            throw new NotFoundHttpException;
        }

        MetaTagRegister::register($model);
        $this->registerMetaTags($model->firstScreenImage, $model->label, $model->description);

        return $this->render('index',
            [
                'model' => $model,
            ]
        );
    }
}
