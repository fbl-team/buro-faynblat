<?php
namespace frontend\controllers;

use common\models\Robots;
use frontend\components\FrontendController;
use frontend\components\MetaTagRegisterCustom;
use frontend\components\TypicalFunctionFaynblat;
use frontend\modules\pages\models\about\PageAbout;
use notgosu\yii2\modules\metaTag\components\MetaTagRegister;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class AboutController extends FrontendController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = PageAbout::find()->with([
            'firstScreenImage',
            'pageAboutWorkPrinciples',
        ])->one();

        if(!$model) {
            throw new NotFoundHttpException;
        }

        MetaTagRegister::register($model);
        $this->registerMetaTags($model->firstScreenImage, $model->first_screen_label, $model->first_screen_description);

        return $this->render('index',
            [
                'model' => $model,
            ]
        );
    }
}
