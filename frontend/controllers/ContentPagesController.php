<?php
namespace frontend\controllers;

use backend\modules\pages\models\about\PageAbout;
use common\models\Robots;
use frontend\components\FrontendController;
use frontend\components\MetaTagRegisterCustom;
use frontend\modules\contentPages\models\ContentPages;
use notgosu\yii2\modules\metaTag\components\MetaTagRegister;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class ContentPagesController extends FrontendController
{
    /**
     * @return string
     */
    public function actionIndex($alias)
    {
        $model = ContentPages::find()
            ->andWhere(['alias' => $alias])
            ->with(['sliderImages'])
            ->one();

        if(!$model) {
            throw new NotFoundHttpException;
        }

        MetaTagRegister::register($model);
        $metaTagImage = $model->sliderImages[0] ?? null;
        $this->registerMetaTags($metaTagImage, $model->first_screen_label);

        return $this->render('index',
            [
                'model' => $model,
                'alias' => $alias,
            ]
        );
    }
}
