<?php
namespace frontend\controllers;

use common\models\Robots;
use frontend\components\FrontendController;
use frontend\helpers\ShowMoreButtonHelper;
use frontend\helpers\SiteUrlHelper;
use frontend\widgets\showMoreItemsButton\ShowMoreItemsButton;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends FrontendController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionRobots()
    {
        $robots = Robots::find()->one();

        if (!$robots) {
            throw new NotFoundHttpException();
        }

        $this->layout = false;

        \Yii::$app->response->format = Response::FORMAT_RAW;
        $headers = \Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/plain');

        $text = $robots->text;

        $text .= "\nSitemap: " . Url::to(['/sitemap/default/index'], true);

        return $this->renderContent($text);
    }

    /**
     * @return string
     */
    public function actionLoadMoreElements($type, $parentModelId, $iteration, $buttonLabel = '')
    {
        $data = false;
        $jsfunctionAfterAjax = 'mediaWriteBlockAnimatedElementAfterAjax("' . ShowMoreButtonHelper::getAjaxReplaceClass($type) . '-andry")';

        if (\Yii::$app->request->isAjax) {
            $data = [
                'replaces' => [
                    [
                        'data' => ShowMoreItemsButton::widget([
                            'type' => $type,
                            'parentModelId' => $parentModelId,
                            'iteration' => $iteration,
                            'buttonLabel' => $buttonLabel,
                        ]),
                        'what' => '.' . ShowMoreButtonHelper::getAjaxReplaceClass($type)
                    ],
                ],
                'js' => Html::script($jsfunctionAfterAjax),
            ];
            echo Json::encode($data);
        }

    }
}
