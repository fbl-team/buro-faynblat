<?php
namespace frontend\controllers;

use common\helpers\MenusHelper;
use common\models\Robots;
use frontend\components\FrontendController;
use frontend\components\MetaTagRegisterCustom;
use frontend\modules\pages\models\about\PageAbout;
use frontend\modules\pages\models\home\PageMain;
use notgosu\yii2\modules\metaTag\components\MetaTagRegister;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class HomeController extends FrontendController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = PageMain::find()->with([
            'firstScreenVideos',
            'filosophyBlockImage'
        ])->one();

        if(!$model) {
            throw new NotFoundHttpException;
        }

        MetaTagRegister::register($model);
        $this->registerMetaTags($model->filosophyBlockImage, $model->first_screen_label);

        return $this->render('index',
            [
                'model' => $model
            ]
        );
    }
}
