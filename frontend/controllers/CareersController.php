<?php
namespace frontend\controllers;

use common\helpers\ContactsHelper;
use common\models\Robots;
use frontend\components\FrontendController;
use frontend\components\MetaTagRegisterCustom;
use frontend\components\TypicalFunctionFaynblat;
use frontend\modules\pages\models\about\PageAbout;
use frontend\modules\pages\models\careers\PageCareers;
use notgosu\yii2\modules\metaTag\components\MetaTagRegister;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class CareersController extends FrontendController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = PageCareers::find()->one();

        if(!$model) {
            throw new NotFoundHttpException;
        }

        MetaTagRegister::register($model);

        return $this->render('index',
            [
                'model' => $model,
            ]
        );
    }
}
