<?php
namespace frontend\controllers;

use common\helpers\ContactsHelper;
use common\models\Robots;
use frontend\components\FrontendController;
use frontend\components\MetaTagRegisterCustom;
use frontend\modules\pages\models\contacts\PageContacts;
use notgosu\yii2\modules\metaTag\components\MetaTagRegister;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class ContactsController extends FrontendController
{
    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex()
    {
        $contactsElements = PageContacts::find()->one();

        if (!$contactsElements) {
            throw new NotFoundHttpException;
        }

        MetaTagRegister::register($contactsElements);
//        $this->registerMetaTags(null, 'asdasda1', 'asdasdasmda.,m');

        return $this->render('index',
            [
                'contactsElements' => $contactsElements,
            ]
        );
    }

    public function getContactsColumn($contactsElements, $relation)
    {
        $result = '';
        $columnElements = $contactsElements->{$relation};
        if (!empty($columnElements)) {
            $result .= Html::beginTag('div', ['class' => 'contact-us__list']);
            $result .= Html::tag('div', $this->getColumnLabel($contactsElements, $relation), ['class' => 'contact-us__title']);
            $result .= Html::beginTag('ul', ['class' => 'contact-us__items-w']);
            foreach ($columnElements as $element) {
                $result .= $this->getColumnElementByType($element);
            }
            $result .= Html::endTag('ul');
            $result .= Html::endTag('div');
        }
        return $result;
    }

    public function getColumnLabel($contactsElements, $relation)
    {
        switch ($relation) {
            case ('pageContactsElementsColumn1'):
                $result = $contactsElements->first_column_label;
                break;
            case ('pageContactsElementsColumn2'):
                $result = $contactsElements->second_column_label;
                break;
            case ('pageContactsElementsColumn3'):
                $result = $contactsElements->third_column_label;
                break;
            default:
                $result = '';
        }
        return $result;
    }

    public function getColumnElementByType($element)
    {
        $result = '';
        if ($element->content) {
            switch ($element->content_type) {
                case (ContactsHelper::TYPE_PHONE):
                    $result .= Html::tag('li', Html::tag('a', $element->content, ['href' => 'tel:' . preg_replace('/\s+/', '', $element->content)]), ['class' => 'contact-us__item']);
                    break;
                case (ContactsHelper::TYPE_EMAIL):
                    $result .= Html::tag('li', Html::tag('a', $element->content, ['href' => 'mailto:' . $element->content]), ['class' => 'contact-us__item']);
                    break;
                default:
                    $result = '';
            }
        }
        return $result;
    }
}
