<?php
namespace frontend\controllers;

use common\models\Robots;
use frontend\components\FrontendController;
use frontend\helpers\ShowMoreButtonHelper;
use frontend\helpers\SiteUrlHelper;
use frontend\modules\pages\models\home\PageMain;
use frontend\widgets\showMoreItemsButton\ShowMoreItemsButton;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class TestController extends FrontendController
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $val = PageMain::find()->one();
        $test = $val->firstScreenImage;
        \help\dump($test);
        return $this->render('index');
    }

}
