<?php
namespace frontend\components;
use common\models\AltImages;
use common\models\EntityToFile;
use metalguardian\fileProcessor\helpers\FPM;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;


/**
 * Site controller
 */
class TypicalFunction
{
    /**
     * @param $imageId
     * @return bool
     */
    public static function isImageExistInFpm($imageId)
    {
        return (boolean) \metalguardian\fileProcessor\models\File::findOne((int) $imageId);
    }

    /**
     * @param $model
     * @param $relation
     * @return bool
     */
    public static function isImageExist($model, $relation)
    {
        return (isset($model->{$relation}) && $model->{$relation}->file_id);
    }

    /**
     * @param $model
     * @param $relation
     * @param $size
     * @param array $htmlOptions
     * @return null|string
     */
    public static function getImage($model, $relation, $size, $htmlOptions = [])
    {
        $htmlOptions['alt'] = $model->{$relation}?AltImages::getAltTag($model->{$relation}->file_id):'image';
        return static::isImageExist($model, $relation)
            ? FPM::image(
                $model->{$relation}->file_id,
                $size[0] ,
                $size[1],
                $htmlOptions
            )
            : null;

    }

    /**
     * @param $model
     * @param $relation
     * @param array $htmlOptions
     * @return null|string
     */
    public static function getImageWithoutResize($model, $relation, $htmlOptions = [])
    {
        $htmlOptions['alt'] = AltImages::getAltTag($model->{$relation}->file_id);
        return static::isImageExist($model, $relation)
            ? Html::img(
                FPM::originalSrc($model->{$relation}->file_id), $htmlOptions)
            : null;

    }

    /**
     * @param $model
     * @param $attribute
     * @param array $htmlOptions
     * @return null|string
     */
    public static function getOriginalImage($model, $attribute, $htmlOptions = [])
    {

        $url = self::getFileUrl($model, $attribute);
        return $url ? Html::img($url, $htmlOptions) : null;

    }

    /**
     * @param $model
     * @param $relation
     * @param $size
     * @return null|string
     */
    public static function getImageUrl($model, $relation, $size)
    {

        return (isset($model->{$relation}) && $model->{$relation}->file_id)
            ? FPM::src($model->{$relation}->file_id, $size[0] , $size[1])
            : null;

    }

    /**
     * @param $model
     * @param array $htmlOptions
     * @return null|string
     */
    public static function getImageFromFPMWithOutResize($model, $htmlOptions = [])
    {
        $htmlOptions['alt'] = AltImages::getAltTag($model->file->id);
        return isset($model->file->id)
            ? Html::img(FPM::originalSrc($model->file->id), $htmlOptions)
            : null;

    }

    /**
     * @param $model
     * @param $relation
     * @param $size
     * @return null|string
     */
    public static function getImageUrlWithoutRelation($model, $relation, $size)
    {

        return (isset($model->{$relation}) && $model->{$relation}->file_id)
            ? FPM::src($model->{$relation}->file_id, $size[0] , $size[1])
            : null;

    }

    /**
     * @param $model
     * @param $relation
     * @return null|string
     */
    public static function getVideoUrl($model, $relation)
    {

        return (isset($model->{$relation}) && $model->{$relation}->file_id)
            ? FPM::originalSrc($model->{$relation}->file_id)
            : null;

    }

    /**
     * @param $model
     * @param $relation
     * @return null|string
     */
    public static function getPdfUrl($model, $relation)
    {

        return (isset($model->{$relation}) && $model->{$relation}->file_id)
            ? 'href=' . FPM::originalSrc($model->{$relation}->file_id)
            : null;

    }

    /**
     * @param $model
     * @param $relation
     * @return null|string
     */
    public static function getVideo($model, $relation, $options)
    {
        $result = '';
        $videoModels = $model->{$relation};

        if (!empty($videoModels)) {
            $result .= Html::beginTag('video', $options);
            foreach ($videoModels as $videoModel) {
                $result .= self::getVideoSource($videoModel);
            }
            $result .= Html::endTag('video');
        }

        return $result;

    }

    /**
     * @param $model
     * @return null|string
     */
    public static function getVideoSource($model)
    {
        $result = '';
        $fileModel = $model->file;

        if (isset($fileModel)) {
            $option = [];
            $option['src'] = FPM::originalSrc($fileModel->id);
            $option['type'] = 'video/' . $fileModel->extension;
            $result .= Html::beginTag('source', $option);
            $result .= Html::endTag('source');
        }

        return $result;

    }

    /**
     * @param $model
     * @param $attribute
     * @return null|string
     */
    public static function getFileUrl($model, $attribute){

        return (isset($model->{$attribute}) && $model->{$attribute})
            ? FPM::originalSrc($model->{$attribute})
            : null;

    }

    /**
     * @param $configKey
     * @param $defaultValue
     * @return mixed
     */
    public static function isKeyExist($configKey, $defaultValue)
    {
        $configValue = \Yii::$app->config->get($configKey);

        return $configValue ? $configValue : $defaultValue;

    }

    public static function isBlockFilled($fields)
    {
        $result = false;
        foreach ($fields as $field) {
            if ($field) {
                $result = true;
                break;
            }
        }
        return $result;
    }

    /**
     * @param null $relation
     * @return string
     */
    public static function getShareImageUrl($relation = null)
    {
        $shareImgId = (isset($relation) && $relation->file_id)
            ? $relation->file_id
            : self::isKeyExist('Share: image', '');

        return Url::to(FPM::src($shareImgId, 'share', 'share'), '');
    }

    /**
     * @param $type
     * @return string
     */
    public static function getShareLinks($type)
    {

        $resultUrl = \yii\helpers\Url::canonical();

        switch ($type) {
            case 'facebook':
                $resultUrl = 'http://www.facebook.com/dialog/share?app_id=1099293753425966&href='.$resultUrl;
                break;
            case 'twitter':
                $resultUrl = 'https://twitter.com/intent/tweet?url='.$resultUrl;
                break;
            case 'vk':
                $resultUrl = 'https://vk.com/share.php?url='.$resultUrl;
                break;
            default:
                $resultUrl = '';
                break;
        }

        return $resultUrl;

    }

    /**
     * @return array
     */
    public static function monthArrayEng()
    {
        return [

            '1' => 'January',
            '2' => 'February',
            '3' => 'March',
            '4' => 'April',
            '5' => 'May',
            '6' => 'June',
            '7' => 'July',
            '8' => 'August',
            '9' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',

        ];
    }

    /**
     * @return array
     */
    public static function monthArrayRus()
    {
        return [

            '1' => 'Январь',
            '2' => 'Февраль',
            '3' => 'Март',
            '4' => 'Апрель',
            '5' => 'Май',
            '6' => 'Июнь',
            '7' => 'Июль',
            '8' => 'Август',
            '9' => 'Сентябрь',
            '10' => 'Октябрь',
            '11' => 'Ноябрь',
            '12' => 'Декабрь',

        ];
    }

}
