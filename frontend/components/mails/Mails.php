<?php
namespace frontend\components\mails;

use frontend\components\TypicalFunction;
use frontend\modules\career\models\CareerTrainership;
use frontend\modules\career\models\CareerVacancies;
use metalguardian\fileProcessor\helpers\FPM;
use rmrevin\yii\postman\ViewLetter;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;


/**
 * Mails - класс созданный для хранения всех писем в одном месте
 */
class Mails
{
    const TYPE_REQUEST = 1;
    const TYPE_CAREER = 2;

    /**
     * @param $formModel - обьект класа Model, содержит все атрибуты зополняемые юзером в форме
     * @param $type - тип письма (форма обратного звонка, сервис запрос, запрос разработчика и т.д.)
     * @param $model - результат сохранения модели в базу, true или false (были сохранены данные формы в базу или нет)
     * @return bool - true если письмо отправлено, false если нет
     */

    public static function sendEmail($formModel, $type, $model)
    {
        if (!$model) {
            return false;
        }
        switch ($type) {
            case self::TYPE_REQUEST:
                //письмо админу
                $letter = (new ViewLetter())
                    ->setSubject("You have new request from Faynblat web-site!")
                    ->setBodyFromView('@mailsFolder/admins/request.php', ['formModel' => $formModel]);

                $emailArray = self::getAdminEmailsList();

                foreach($emailArray as $email) {
                    $letter->addAddress($email);
                }

                $letter->send();

                //письмо юзеру
                if ($userEmail = $formModel->email) {
                    $letterUser = (new ViewLetter())
                        ->setSubject("Your request has been saved in Faynblat-website database and wait for approve!")
                        ->setBodyFromView('@mailsFolder/users/request.php');

                    $letterUser->addAddress($userEmail);

                    $letterUser->send();
                }

                break;
            case self::TYPE_CAREER:
                //письмо админу
                $letter = (new ViewLetter())
                    ->setSubject("You have new career request from Faynblat web-site!")
                    ->setBodyFromView('@mailsFolder/admins/career-request.php', ['formModel' => $formModel]);

                $emailArray = self::getAdminEmailsList();

                foreach($emailArray as $email) {
                    $letter->addAddress($email);
                }

                $letter->send();

                //письмо юзеру
                if ($userEmail = $formModel->email) {
                    $letterUser = (new ViewLetter())
                        ->setSubject("Your request has been saved in Faynblat-website database and wait for approve!")
                        ->setBodyFromView('@mailsFolder/users/career-request.php');

                    $letterUser->addAddress($userEmail);

                    $letterUser->send();
                }

                break;
        }
        return true;
    }

    public static function getAdminEmailsList()
    {
        return array_filter(array_map('trim', explode(',', TypicalFunction::isKeyExist('Email: admins email list', 'louse2007@ukr.net'))));
    }
}
