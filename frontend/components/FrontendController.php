<?php
/**
 * @author walter
 */

namespace frontend\components;


use yii\helpers\ArrayHelper;
use yii\web\Controller;

class FrontendController extends Controller
{

    public $shareTitle = '';
    public $shareDescription = '';
    public $shareImageUrl = '';

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $baseUrl = \Yii::$app->request->getAbsoluteUrl();

            preg_match("/(http|https):\/\/(www.)*/", $baseUrl, $match);

            if (count($match)) {
                if (ArrayHelper::getValue($match, 2) == 'www.') {
                    $url = str_replace('www.', '', $baseUrl);

                    return $this->redirect($url, 301);
                }
            }

            $pos = strpos($baseUrl, '/index.php');

            if ($pos) {
                return $this->redirect(\Yii::$app->getHomeUrl());
            }
            
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param null $relation
     * @param null $label
     * @param null $description
     */
    public function registerMetaTags($relation = null, $label = null, $description = null)
    {
        $this->shareImageUrl = TypicalFunction::getShareImageUrl($relation);
        $this->shareTitle = $label ? $label : '';
        $this->shareDescription = $description ? strip_tags($description) : '';
    }

}