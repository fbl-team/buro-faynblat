<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MainAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'static/css/main.css',
        'css/frontend.css',
    ];
    public $js = [

//        'https://player.vimeo.com/api/player.js',
        'js/player.js',
//        'static/js/separate-js/jquery-1.9.1.min.js',
        'static/js/separate-js/start.js',
        'static/js/main.min.js',
        'js/frontend.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset'
    ];
}
