<?php

use app\widgets\footer\Footer;
use \app\widgets\header\Header;
use common\helpers\ProjectsHelper;
use frontend\assets\AllProjectAsset;
use frontend\modules\projects\widgets\projectsFilters\ProjectsFilters;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/**
 * @var \yii\web\View $this
 * @var array $selectedCategories
 */

$this->params['breadcrumbs'][] = Yii::t('breadcrumbs', 'Projects');

AllProjectAsset::register($this);
?>

<?= Header::widget() ?>

<?php if ($model->label) { ?>
	<h1>
    <span class="page-title page-title_center posi" id="projects">
        <?= $model->label; ?>
    </span>
	</h1>
<?php } ?>

<?= ProjectsFilters::widget([
    'selectedCategories' => $selectedCategories,
    'selectedStyles' => $selectedStyles,
]) ?>

<?= $this->render('svg_word', ['dataProviderProjects' => $dataProviderProjects]) ?>

<!--projects-->
<section class="projects">
    <div class="container__inner">
        <div class="heading">
            <?php if ($model->description) { ?>
            <div class="heading__title">
                <?= $model->description ?>
            </div>
            <?php } ?>
        </div>
    </div>
    <?php Pjax::begin(['id' => 'projects-items']) ?>
    <div class="container">
        <?php
        echo ListView::widget([
            'dataProvider' => $dataProviderProjects,
            'layout' => "{items}<div class='pagin'>{pager}</div>",
            'itemView' => function ($model, $key, $index, $widget) {
                $view = ProjectsHelper::allProjectsWhatViewRender($index);
                return $this->render($view, ['model' => $model]);
            },
            'options' => [
                'class' => 'projects__iw'
            ],
            'itemOptions' => [
                'class' => 'projects__row item'
            ],
//            'emptyTextOptions' => ['class' => 'news-section__row news clearfix'],
            'pager' => [
                'class' => 'frontend\components\CustomFaynblatPagination',

            ]

        ]);
        ?>
    </div>
    <?php Pjax::end() ?>

</section>

<?= Footer::widget(['staticModel' => $model]); ?>