<?php
use app\widgets\footer\Footer;
use \app\widgets\header\Header;
use frontend\helpers\ShowMoreButtonHelper;
use frontend\helpers\SiteUrlHelper;
use frontend\widgets\showMoreItemsButton\ShowMoreItemsButton;

/**
 * @var \yii\web\View $this
 * @var \frontend\modules\pages\models\oneProject\PageOneProject $model
 * @var \frontend\modules\projects\models\Projects $project
 */

$this->params['breadcrumbs'][] = [
    'label' => Yii::t('breadcrumbs', 'Projects'),
    'url' => SiteUrlHelper::createAllProjectsUrl()
];
$this->params['breadcrumbs'][] = $project->label;
?>

<?= Header::widget() ?>

<!--one-project-->
<section class="one-proj">
    <div class="container__inner">
        <?php if ($model->breadcrums_label && $model->breadcrums_url) { ?>
        <a class="link-back posi" href="<?= $model->breadcrums_url ?>">
            <span class="link-back__txt"><?= $model->breadcrums_label ?></span>
        </a>
        <?php } ?>
        <h1 class="page-title posi"><?= $project->label ?></h1>
        <?php if($project->project_parameters) { ?>
        <div class="one-proj__info posi">
            <?= $project->project_parameters ?>
        </div>
        <?php } ?>

        <?= ShowMoreItemsButton::widget([
            'type' => ShowMoreButtonHelper::TYPE_ONE_PROJECT_CONTENT,
            'iteration' => 0,
            'parentModelId' => $project->id,
            'buttonLabel' => $model->add_content_button_label
        ]) ?>

    </div>
</section>

<!--other-works-->
<?php if (!empty($model->projectsRelatedProjects)) { ?>
<section class="other-works">
    <div class="container__inner">
        <div class="heading">
            <?php if ($model->another_project_block_label) { ?>
                <span class="heading__title"><?= $model->another_project_block_label ?></span>
            <?php } ?>
        </div>

        <!--show more elements block-->
        <?= ShowMoreItemsButton::widget([
            'type' => ShowMoreButtonHelper::TYPE_ONE_PROJECT_ANOTHER_PROJECT,
            'iteration' => 0,
            'parentModelId' => $project->id,
            'buttonLabel' => $model->another_project_button_label
        ]) ?>
    </div>
</section>
<?php } ?>

<?= Footer::widget(['staticModel' => $project, 'staticModelOnePage' => $model]); ?>
