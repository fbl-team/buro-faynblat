<?php

use \frontend\components\TypicalFunction;
use \common\helpers\NewsHelper;
use \frontend\components\SiteUrl;
use frontend\helpers\SiteUrlHelper;

?>


    <div class="projects__col projects__col_bigger">
        <a class="projects__img-wrap projects__img-wrap_left" href="<?= SiteUrlHelper::createOneProjectUrl(['alias' => $model->alias]) ?>">
            <?= TypicalFunction::getImage($model, 'projectBigImage', ['project', 'bigimage'], ['class' => 'projects__img']) ?>
        </a>
    </div>
    <div class="projects__col projects__col_smaller">
        <a class="projects__img-wrap projects__img-wrap_right" href="<?= SiteUrlHelper::createOneProjectUrl(['alias' => $model->alias]) ?>">
            <?= TypicalFunction::getImage($model, 'projectSmallImage', ['project', 'smallimage'], ['class' => 'projects__img']) ?>
        </a>
        <div class="projects__info">
            <a class="projects__info_iw" href="<?= SiteUrlHelper::createOneProjectUrl(['alias' => $model->alias]) ?>">
                <?php if ($model->label) { ?>
                    <span class="projects__info-caption"><?= $model->label ?></span>
                <?php } ?>
                <?php if ($model->city_and_square) { ?>
                    <span class="projects__info-line"><?= $model->city_and_square ?></span>
                <?php } ?>
            </a>
        </div>
    </div>


