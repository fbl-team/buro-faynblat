<?php
use app\widgets\footer\Footer;
use app\widgets\svgSprite\SvgSprite;
use frontend\components\TypicalFunction;
use frontend\helpers\BodyTagClassHelper;
use frontend\helpers\SiteUrlHelper;
use frontend\widgets\openGraphMetaTags\Widget;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\MainAsset;
use frontend\widgets\Alert;
use yii\widgets\Menu;

/* @var $this \yii\web\View */
/* @var $content string */

MainAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?= Yii::$app->config->get('endHead') ?>
</head>

<body class="<?= BodyTagClassHelper::getBodyClass(Yii::$app->controller->action->uniqueId) ?>">
<?php $this->beginBody() ?>

<?= SvgSprite::widget(); ?>

<?= Widget::widget([
    'title' => 'Test title',
    'url' => Url::to(Url::current(['_pjax' => null]), true),
    'description' => 'Some test description',
    'image' => 'http://pbs.twimg.com/media/CaNtqoYUMAAENl3.jpg',
]); ?>

<?= $content ?>

<!--footer-->
<footer class="main-footer">
    <div class="container">

        <div class="footer__row clearfix">
            <!--logo-->
            <div class="main-footer__logo main-footer__item">
                <div class="logo-small">
                    <a href="<?= SiteUrlHelper::createHomeUrl() ?>">
                        <svg width="26" height="21">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#logo-footer"></use>
                        </svg>
                        <svg class="show-o-tab" viewBox="0 0 95 26" height="21" width="95">
                            <path fill="#010202" d="M0 19h3v-4h6v-3h-6v-2h7V7l-10 .1V19zm16-12l-5 12h3l1-2h5l1 2h3l-5-12h-3zm0 7l1-3h1l1 3h-3zm66-7l-5 12h3l1-2h5l1 2h3l-5-12h-3zm0 7l1-3h1l1 3h-3zm-51.5-2l-3.5-5h-3l5 7v5h3v-5l5-7h-3l-3.5 5zm16.5 1l-5-6h-3v12h3v-7l5.3 7h2.7V7h-3v6zm13.6-.3c1.2-.5 1.8-1.4 1.8-2.7 0-.8-.3-1.4-.8-1.9-.7-.7-1.8-1.1-3.2-1.1h-5.5v12h5.5c1.4 0 2.5-.3 3.3-.9.8-.6 1.2-1.4 1.2-2.4 0-.8-.2-1.4-.6-1.9-.3-.4-.9-.8-1.7-1.1zm-5-3h2.1c.5 0 .9.1 1.1.3.3.2.4.4.4.8 0 .7-.5 1.1-1.6 1.1h-2.1V9.7zm3.7 6.4c-.3.2-.7.3-1.2.3h-2.5v-2.2h2.5c.5 0 .9.1 1.2.3.3.2.4.5.4.8 0 .3-.1.6-.4.8zm9.7-9.1h-3v12h9v-3h-6V7zm21 0v3h4v9h3v-9h4V7h-11z"></path>
                        </svg>
                        <!--copy-->
                        <div class="main-footer__copy show-mob">
                            <span>Faynblat 2016</span>
                        </div>
                    </a>
                </div>
            </div>
            <!--nav-->
            <div class="main-footer__nav main-footer__item">
                <?= Menu::widget([
                    'items' => $menuItems
                ]) ?>
            </div>
            <!--social-->
            <div class="main-footer__social">
                <div class="social ">
                    <i>
                        <a href="<?= TypicalFunction::isKeyExist('Social web: Twitter group link', '#') ?>" target="_blank">
                            <svg width="14" height="11">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tw"></use>
                            </svg>
                        </a>

                    </i>
                    <i>
                        <a href="<?= TypicalFunction::isKeyExist('Social web: Facebook group link', '#') ?>" target="_blank">
                            <svg width="7" height="13">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#facebook"></use>
                            </svg>
                        </a>

                    </i>
                    <i>
                        <a href="<?= TypicalFunction::isKeyExist('Social web: Vk group link', '#') ?>" target="_blank">
                            <svg width="17" height="10">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#vk"></use>
                            </svg>
                        </a>

                    </i>
                </div>
            </div>
        </div>
        <div class="footer__row clearfix">
            <!--copy-->
            <div class="main-footer__copy">
                <span>Faynblat 2016</span>
            </div>
            <!--by-->
            <div class="main-footer__by">
                <span>by</span>
                <a href="http://vintage.com.ua/" target="_blank">
                    <svg width="13" height="15">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#vintage"></use>
                    </svg>
                </a>
            </div>
        </div>
    </div>



</footer>

<?php $this->endBody() ?>
<?= Yii::$app->config->get('endBody') ?>
</body>
</html>
<?php $this->endPage() ?>
