<?php
use app\widgets\footer\Footer;
use app\widgets\svgSprite\SvgSprite;
use frontend\helpers\BodyTagClassHelper;
use frontend\widgets\openGraphMetaTags\Widget;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\MainAsset;
use frontend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

MainAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?= Yii::$app->config->get('endHead') ?>
    <link rel="icon" type="image/x-icon" href="/favicon.png">
    <link rel="alternate" href="//buro-faynblat.com/ru" hreflang="ru">
    <link rel="alternate" href="//buro-faynblat.com/en" hreflang="en">
    <link rel="alternate" href="//buro-faynblat.com/uk" hreflang="uk">
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-96577305-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-96577305-1');
  setTimeout(function(){
		gtag('event', location.pathname, {
		'event_category': 'Новый посетитель'
		});
	}, 15000);
</script>
</head>

<body class="<?= BodyTagClassHelper::getBodyClass(Yii::$app->controller->action->uniqueId) ?>">
<?php $this->beginBody() ?>

<?= SvgSprite::widget(); ?>

<?= Widget::widget([
    'title' => $this->context->shareTitle,
    'url' => Url::to(Url::current(['_pjax' => null]), ''),
    'description' => $this->context->shareDescription,
    'image' => $this->context->shareImageUrl,
]); ?>

<?= $content ?>

<?php $this->endBody() ?>
<?= Yii::$app->config->get('endBody') ?>
<!-- remarketing tag -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 845399574;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/845399574/?guid=ON&amp;script=0"/>
</div>
</noscript>
</body>
</html>
<?php $this->endPage() ?>
