<?php
/**
 * @var \frontend\modules\pages\models\contacts\PageContacts $contactsElements
 */

use app\widgets\footer\Footer;
use app\widgets\header\Header;

$this->params['breadcrumbs'][] = Yii::t('breadcrumbs', 'Contacts');
?>

<?= Header::widget(); ?>

<!--contact-us-->
<div class="contact-us">
    <div class="container__inner">
        <?php if ($contactsElements->label) { ?>
        <div class="heading">
            <h1><span class="heading__caption"><?= $contactsElements->label ?></span></h1>
        </div>
        <?php } ?>
        <div class="contact-us__iw">

            <?= $this->context->getContactsColumn($contactsElements, 'pageContactsElementsColumn1'); ?>

            <?= $this->context->getContactsColumn($contactsElements, 'pageContactsElementsColumn2'); ?>

            <?= $this->context->getContactsColumn($contactsElements, 'pageContactsElementsColumn3'); ?>
            <div class="contact-us__list">
                <div class="contact-us__title">
                    <?= Yii::t('front/contacts2', 'Address') ?>
                </div>
                <ul class="contact-us__items-w">
                   <li class="contact-us__item">
                       <?= $contactsElements->four_column_label ?>
                   </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<?= Footer::widget(['staticModel' => $contactsElements]); ?>
