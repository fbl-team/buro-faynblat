<?php

use frontend\components\TypicalFunction;

$whatWeDoBlockElements = $model->whatWeDoBlock;
//\help\dump($model);element_url
?>

<section class="delineation">
    <?php if (!empty($whatWeDoBlockElements)) { ?>
        <div class="container__inner">
            <div class="heading">
                <?php if ($model->what_we_do_block_label) { ?>
                    <span class="heading__caption"><?= $model->what_we_do_block_label ?></span>
                <?php } ?>
                <?php if ($model->what_we_do_block_description) { ?>
                <div class="heading__title">
                    <?= $model->what_we_do_block_description ?>
                </div>
                <?php } ?>
            </div>
            <div class="delineation__iw">
                <?php foreach ($whatWeDoBlockElements as $key => $element) { ?>
                    <?php if (!($key % 2)) { ?>
                        <div class="delineation__row posi">
                            <div class="delineation__col delineation__col_bigger">
                                <?php if ($element->label) { ?>
                                    <a class="delineation__caption show-mob" href="<?= $element->element_url ?>">
                                        <?= $element->label ?>
                                    </a>
                                <?php } ?>
                                <a class="delineation__img-w" href="<?= $element->element_url ?>">
                                    <?= TypicalFunction::getImage($element, 'elementImage', ['pageservices', 'whatwedo'], ['class' => 'delineation__img']) ?>
                                    <?= TypicalFunction::getVideo($element, 'elementVideos', [
                                        'class' => 'delineation__video js-video-inview',
                                        'preload' => 'auto', 'loop' => 'loop', 'muted' => 'muted'
                                    ])
                                    ?>
                                </a>
                            </div>
                            <div class="delineation__col delineation__col_smaller">
                                <div class="delineation__text-w">
                                    <?php if ($element->label) { ?>
                                    <a class="delineation__caption hide-mob" href="<?= $element->element_url ?>">
                                            <?= $element->label ?>
                                    </a>
                                    <?php } ?>
                                    <?php if ($element->description) { ?>
                                        <p class="delineation__text">
                                            <?= $element->description ?>
                                        </p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="delineation__row posi">
                            <?php if ($element->label) { ?>
                                <a class="delineation__caption show-mob" href="<?= $element->element_url ?>">
                                    <?= $element->description ?>
                                </a>
                            <?php } ?>
                            <div class="delineation__col delineation__col_smaller">
                                <a class="delineation__img-w" href="<?= $element->element_url ?>">
                                    <?= TypicalFunction::getImage($element, 'elementImage', ['pageservices', 'whatwedo'], ['class' => 'delineation__img']) ?>
                                    <?= TypicalFunction::getVideo($element, 'elementVideos', [
                                        'class' => 'delineation__video js-video-inview',
                                        'preload' => 'auto', 'loop' => 'loop', 'muted' => 'muted'
                                    ])
                                    ?>
                                </a>
                            </div>
                            <div class="delineation__col delineation__col_bigger">
                                <div class="delineation__text-w">
                                    <?php if ($element->label) { ?>
                                        <a class="delineation__caption hide-mob" href="<?= $element->element_url ?>">
                                            <?= $element->label ?>
                                        </a>
                                    <?php } ?>
                                    <?php if ($element->description) { ?>
                                        <p class="delineation__text">
                                            <?= $element->description ?>
                                        </p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
</section>

