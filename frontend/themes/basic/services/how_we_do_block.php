<?php

use frontend\components\TypicalFunction;

$howWeDoElements = $model->howWeDoBlock;

?>

<?php if (!empty($howWeDoElements)) { ?>
<section class="advantage-work advantage-work_how-we-do js-ancolor">
    <!--I_symb-->
    <svg class="n-symbol n-symbol_services-i" viewBox="0 0 85.1 198.6">
        <path d="M77.9,193.2c-4.7,0-9.1-1.2-13.3-3.5c-3.1-1.8-5.2-3.9-6.2-6.3c-1.3-3.2-1.9-9.9-1.9-19.9V35.2

	c0-11.9,1.2-19.4,3.7-22.6c3.7-4.8,9.6-7.2,17.7-7.2h7.2V0H0v5.4h7c4.7,0,9.2,1.2,13.5,3.5c3,1.8,5,3.9,6,6.3

	c1.3,3.2,1.9,9.9,1.9,19.9v128.3c0,11.9-1.2,19.4-3.5,22.6c-3.7,4.8-9.7,7.2-17.9,7.2H0v5.4h85.1v-5.4H77.9z" />
    </svg>
    <!--C_symb-->
    <svg class="n-symbol n-symbol_services-c" viewBox="0 0 178.9 207.7">
        <path d="M164.6,0c-1.1,4.7-2.9,8.3-5.4,11c-2,1.9-4.2,2.8-6.9,2.8c-1.8,0-4.7-1.1-8.8-3.2c-14-7-28.1-10.5-42.5-10.5

	C82.6,0,65.6,4.6,50,13.7c-15.6,9.1-27.8,22-36.7,38.6C4.4,68.9,0,87.1,0,106.9c0,24.6,6.7,46,20.2,64.2

	c18.1,24.4,43.6,36.6,76.5,36.6c18.3,0,34-4.1,47.3-12.3c13.3-8.2,24.9-21.1,34.9-38.8l-4.5-2.9c-11.8,16.4-22.7,27.4-32.5,32.9

	c-9.9,5.5-21.2,8.3-34.1,8.3c-14.8,0-28.1-3.5-39.6-10.5c-11.6-7-20.2-17.1-26-30.3c-5.8-13.2-8.6-28.8-8.6-46.7

	c0-21.8,3.1-40,9.2-54.6c6.1-14.6,14.5-25.3,25.1-32c10.6-6.7,22.6-10,35.7-10c15.7,0,29.2,4.4,40.4,13.3

	c11.2,8.9,19.9,23.4,25.9,43.7h4.5L169.8,0H164.6z" />
    </svg>

    <div class="container__inner clearfix">
        <div class="heading heading_inv">
            <?php if ($model->how_we_do_that_block_label) { ?>
                <span class="heading__caption"><?= $model->how_we_do_that_block_label ?></span>
            <?php } ?>
            <?php if ($model->how_we_do_that_block_description) { ?>
            <div class="heading__title">
                <?= $model->how_we_do_that_block_description ?>
            </div>
            <?php } ?>
        </div>
        <div class="advantage-work__list">
            <div class="advantage-inner inversion">
                <?php foreach ($howWeDoElements as $key => $element) { ?>
                    <div class="advantage-item">
                    <div class="advantage-item__number">
                        <?= ++$key ?>
                    </div>
                    <?php if ($element->label) { ?>
                    <div class="advantage-item__title">
                        <?= $element->label ?>
                    </div>
                    <?php } ?>
                    <?php if ($element->description) { ?>
                    <div class="advantage-item__text">
                        <?= $element->description ?>
                    </div>
                    <?php } ?>
                    <div class="advantage-item__number advantage-item__number_toHide js-num-anim">
                        <?= $key ?>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<?php } ?>
