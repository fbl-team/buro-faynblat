<?php
use frontend\components\TypicalFunction;
use yii\widgets\Breadcrumbs;
?>
<div class="fscreen parentsize">
    <div class="container">
        <div class="breadcrumbs">
            <?= Breadcrumbs::widget([
                'links' => [
                    Yii::t('breadcrumbs', 'Services'),
                ]
            ]) ?>
        </div>
    </div>
    <div class="fscreen__img-bg posi" style="background-image: url(<?= TypicalFunction::getImageUrl($model, 'firstScreenImage', ['pageservices', 'firstscreen']) ?>)"></div>
    <!--video-->
    <div class="fscreen__video-bg">
        <?= \frontend\components\TypicalFunction::getVideo($model, 'firstScreenVideos',
            ['class' => 'video-to-parentsize', 'width' => '100%', 'height' => '640', 'preload' => 'auto', 'muted' => 'muted',])
        ?>
    </div>
    <div class="container__inner">
        <div class="fscreen__content fscreen__content_rvc">
            <div class="fscreen__content-iw posi">
                <?php if ($model->label) { ?>
                    <span class="fscreen__title"><?= $model->label ?></span>
                <?php } ?>
                <?php if ($model->description) { ?>
                <div class="fscreen__descr">
                    <?= $model->description ?>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="fscreen__video-controls js-video-control">
            <div class="fscreen__video-play js-vplay">
                <div class="play">
                    <svg viewBox="0 0 7 12" width="7" height="12">
                        <polygon points="7 6 0 12 0 0 7 6" />
                    </svg>
                </div>
                <div class="pause">
                    <svg viewBox="0 0 9 12" width="9" height="12">
                        <path d="M0,0H3.12V12H0V0Z" />
                        <path d="M5.8,0H9V12H5.8V0Z" />
                    </svg>
                </div>
            </div>
            <div class="fscreen__video-progress-w js-vprogress-w">
                <span class="fscreen__video-progress js-vprogress"></span>
            </div>
            <div class="fscreen__video-mute js-vmute">
                <div class="mute-on">
                    <svg x="-2px" y="0px" viewBox="0 0 18.3 15" width="18" height="15">
                        <polygon points="9.8,15 9.8,0 5.2,4.3 0,4.3 0,10.7 5.2,10.7" />
                        <path d="M12.6,12.4c1-1.4,1.6-3.1,1.6-4.9c0-1.8-0.6-3.6-1.6-4.9l-1,0.8C12.5,4.5,13,5.9,13,7.5s-0.5,3-1.4,4.2L12.6,12.4z" />
                        <path d="M15.9,14.7c1.5-2,2.4-4.5,2.4-7.2s-0.9-5.2-2.4-7.2l-1,0.8c1.3,1.8,2.1,4,2.1,6.4s-0.8,4.6-2.1,6.4L15.9,14.7z" />
                    </svg>
                </div>
                <div class="mute-off">
                    <svg x="0px" y="0px" viewBox="0 0 20.8 15" width="20.8" height="15">
                        <polygon points="9.8,15 9.8,0 5.2,4.3 0,4.3 0,10.7 5.2,10.7 " />
                        <rect x="10.8" y="6.8" transform="matrix(-0.7071 -0.7071 0.7071 -0.7071 22.7407 24.3254)" width="11.2" height="1.3" />
                        <rect x="10.8" y="6.8" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -0.4642 13.7853)" width="11.2" height="1.3" />
                    </svg>
                </div>
            </div>
        </div>
    </div>
</div>
