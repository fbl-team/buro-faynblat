<?php

use app\widgets\footer\Footer;
use app\widgets\header\Header;

$this->params['breadcrumbs'][] = Yii::t('breadcrumbs', 'Careers');

?>

<?= Header::widget() ?>

<div class="contact-us">
    <div class="container__inner">
        <div class="heading">
            <h1><span class="heading__caption"><?=$model->label?></span></h1>
        </div>
        <div class="careers__description">
            <?=$model->description?>
        </div>
    </div>
</div>
<?php

?>
<?= Footer::widget(['staticModel' => $model, 'career' => true]); ?>
