<?php

use frontend\components\TypicalFunction;
use metalguardian\fileProcessor\helpers\FPM;

$arrayForPublishedBlock = [
    $model->second_screen_label1,
    $model->second_screen_label2,
    $model->second_screen_description1,
    $model->second_screen_description2,
];

?>

<?php if (TypicalFunction::isBlockFilled($arrayForPublishedBlock)) { ?>
<div class="main-text">

    <div class="container__inner">
        <!--S_symb-->
        <svg class="n-symbol n-symbol_serv-s-1" viewBox="0 0 132 207.7">
            <path d="M113.4,0c-1,4.6-2.3,7.6-3.9,9.2c-1.7,1.5-3.9,2.4-6.2,2.3c-2.2,0-6.1-1.2-11.6-3.7
	C79.9,2.6,68.7,0,58.2,0C41.3,0,27.3,5.2,16.4,15.5C5.5,25.8,0,38,0,52.3c-0.1,7.9,1.8,15.6,5.6,22.6c4.1,7.3,9.7,13.7,16.3,18.8
	c7.2,5.7,19.3,13.3,36.3,22.8s27.4,15.7,31.2,18.7c5.1,3.7,9.4,8.4,12.5,13.9c2.6,4.5,3.9,9.6,4,14.8c0,8.8-3.5,16.4-10.6,22.9
	c-7.1,6.4-16.7,9.7-28.9,9.7c-10.1,0.1-20.1-2.3-29.1-7c-8.8-4.7-15.4-10.6-19.7-17.7c-4.3-7.1-7.7-17.7-10.3-32H1.9v68.1h5.4
	c0.7-4.6,1.7-7.6,3.1-9.1c1.5-1.5,3.6-2.3,5.8-2.2c2.5,0,8.6,1.5,18.1,4.6c9.5,3.1,15.8,4.9,18.8,5.3c5.4,0.9,10.8,1.3,16.3,1.3
	c18.4,0,33.4-5.4,45-16.3c11.7-10.9,17.5-23.9,17.5-38.9c0-7.9-1.9-15.7-5.5-22.7c-3.8-7.4-9.1-13.8-15.7-18.8
	c-6.8-5.3-19.5-13-38-22.9C50,75.8,35.6,66.1,29.6,58.7c-4-4.6-6.3-10.5-6.3-16.6c0-7.9,3.3-15,10-21.2c6.6-6.2,15-9.3,25.2-9.3
	c9.1,0,18.1,2.4,26.1,6.9c8.1,4.3,14.8,10.7,19.4,18.6c4.5,7.8,7.7,18.3,9.4,31.5h5.4V0L113.4,0L113.4,0z" />
        </svg>

        <div class="heading">
            <?php if ($model->second_screen_label1) { ?>
            <span class="heading__caption"><?= $model->second_screen_label1 ?></span>
            <?php } ?>
            <?php if ($model->second_screen_label2) { ?>
            <div class="heading__title">
                <?= $model->second_screen_label2 ?>
            </div>
            <?php } ?>
        </div>
        <div class="main-text__content ">
            <?php if ($model->second_screen_description1) { ?>
            <div class="main-text__item">
                <?= $model->second_screen_description1 ?>
            </div>
            <?php } ?>
            <?php if ($model->second_screen_description2) { ?>
            <div class="main-text__item">
                <?= $model->second_screen_description2 ?>
            </div>
            <?php } ?>
        </div>
    </div>

</div>
<?php } ?>

