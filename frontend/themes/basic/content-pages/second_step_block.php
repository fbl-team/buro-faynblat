<?php

use frontend\components\TypicalFunction;
use metalguardian\fileProcessor\helpers\FPM;

$secondStepBlockElements = $model->secondEtapBlock;

?>

<?php if (!empty($secondStepBlockElements)) { ?>
    <section class="advantage-work js-ancolor">
        <div class="container__inner">
            <?php if ($model->etap_second_block_label) { ?>
            <div class="advantage-work__title">
                <span class="h2 h2_line"><?= $model->etap_second_block_label ?></span>
            </div>
            <?php } ?>
            <div class="advantage-work__list">
                <div class="advantage-inner js-advantage-slider">
                    <?php foreach ($secondStepBlockElements as $key => $element) { ?>
                    <div class="advantage-item">
                        <div class="advantage-item__number">
                            <?= ++$key ?>
                        </div>
                        <?php if ($element->label) { ?>
                        <div class="advantage-item__title">
                            <?= $element->label ?>
                        </div>
                        <?php } ?>
                        <?php if ($element->description) { ?>
                        <div class="advantage-item__text">
                            <?= $element->description ?>
                        </div>
                        <?php } ?>
                        <div class="advantage-item__number advantage-item__number_toHide js-num-anim">
                            <?= $key ?>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <div class="pagin pagin_slider"></div>
            </div>
        </div>
    </section>
<?php } ?>

