<?php

use frontend\components\TypicalFunction;

$InsideWorkBlockElements = $model->insideWorkBlock;
//\help\dump($model);
?>

<section class="delineation">
    <?php if (!empty($InsideWorkBlockElements)) { ?>
        <div class="container__inner">
            <div class="heading">
                <?php if ($model->inside_service_block_label) { ?>
                    <span class="heading__caption"><?= $model->inside_service_block_label ?></span>
                <?php } ?>
                <?php if ($model->inside_service_block_description) { ?>
                <div class="heading__title">
                    <?= $model->inside_service_block_description ?>
                </div>
                <?php } ?>
            </div>
            <div class="delineation__iw">
                <?php foreach ($InsideWorkBlockElements as $key => $element) { ?>
                    <?php if (!($key % 2)) { ?>
                        <div class="delineation__row posi">
                            <div class="delineation__col delineation__col_bigger">
                                <?php if ($element->label) { ?>
                                    <span class="delineation__caption show-mob">
                                        <?= $element->description ?>
                                    </span>
                                <?php } ?>
                                <div class="delineation__img-w">
                                    <?= TypicalFunction::getImage($element, 'elementImage', ['contentpage', 'insideimage'], ['class' => 'delineation__img']) ?>
                                    <?= TypicalFunction::getVideo($element, 'elementVideos', [
                                        'class' => 'delineation__video js-video-inview',
                                        'preload' => 'auto', 'loop' => 'loop', 'muted' => 'muted'
                                    ])
                                    ?>
                                </div>
                            </div>
                            <div class="delineation__col delineation__col_smaller">
                                <div class="delineation__text-w">
                                    <?php if ($element->label) { ?>
                                        <h3 class="delineation__caption hide-mob">
                                            <?= $element->label ?>
                                        </h3>
                                    <?php } ?>
                                    <?php if ($element->description) { ?>
                                        <p class="delineation__text">
                                            <?= $element->description ?>
                                        </p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="delineation__row posi">
                            <?php if ($element->label) { ?>
                                <span class="delineation__caption show-mob">
                                    <?= $element->description ?>
                                </span>
                            <?php } ?>
                            <div class="delineation__col delineation__col_smaller">
                                <div class="delineation__img-w">
                                    <?= TypicalFunction::getImage($element, 'elementImage', ['contentpage', 'insideimage'], ['class' => 'delineation__img']) ?>
                                    <?= TypicalFunction::getVideo($element, 'elementVideos', [
                                        'class' => 'delineation__video js-video-inview',
                                        'preload' => 'auto', 'loop' => 'loop', 'muted' => 'muted'
                                    ])
                                    ?>
                                </div>
                            </div>
                            <div class="delineation__col delineation__col_bigger">
                                <div class="delineation__text-w">
                                    <?php if ($element->label) { ?>
                                        <h3 class="delineation__caption hide-mob">
                                            <?= $element->label ?>
                                        </h3>
                                    <?php } ?>
                                    <?php if ($element->description) { ?>
                                        <p class="delineation__text">
                                            <?= $element->description ?>
                                        </p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
</section>

