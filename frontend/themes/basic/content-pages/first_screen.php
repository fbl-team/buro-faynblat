<?php
use common\models\AltImages;
use frontend\helpers\SiteUrlHelper;
use metalguardian\fileProcessor\helpers\FPM;
use yii\widgets\Breadcrumbs;

$sliderImages = $model->sliderImages;
?>

<div class="fscreen posi">
    <div class="container">
        <div class="breadcrumbs">
            <?= Breadcrumbs::widget([
                'links' => [
                    [
                        'label' => Yii::t('breadcrumbs', 'Services'),
                        'url' => SiteUrlHelper::createServicesUrl()
                    ],
                    Yii::t('breadcrumbs', $alias),
                ]
            ]) ?>
        </div>
    </div>
    <div class="fs-slider">
        <ul class="fs-slider__iw">
            <?php foreach ($sliderImages as $key => $image): ?>
                <li class="fs-slider__slide <?= !$key ? 'current-slide' : ''?>">
                    <img class="fs-slider__img"
                         src="<?= FPM::src($image->file_id, 'contentpage', 'slider') ?>"
                         alt="<?= AltImages::getAltTag($image->file_id) ?>">
                </li>
            <?php endforeach; ?>
        </ul>
    </div>

    <div class="container__inner">
        <div class="fscreen__content">
            <?php if ($model->breadcrums_label && $model->breadcrums_url) { ?>
            <a class="link-back posi" href="<?= $model->breadcrums_url ?>">
                <span class="link-back__txt"><?= $model->breadcrums_label ?></span>
            </a>
            <?php } ?>
            <?php if ($model->first_screen_label) { ?>
            <div class="fscreen__content-iw posi">
                <h1 class="fscreen__title">
                    <?= $model->first_screen_label ?>
                </h1>
            </div>
            <?php } ?>
            <?php if ($model->first_screen_button_label) { ?>
                <a class='action-btn js-to-main-form' href="#">
                    <?= $model->first_screen_button_label ?>
                </a>
            <?php } ?>
        </div>

        <!-- Slider Controls -->
        <?php if (count($sliderImages) > 1) { ?>
        <div id="fs-slider-controls" class="fs-slider__controls">
            <?php foreach ($sliderImages as $key => $image) { ?>
            <i class="fs-slider__controls-item <?= !$key ? 'active' : ''?>"></i>
            <?php } ?>
        </div>
        <?php } ?>
    </div>
    
</div>
