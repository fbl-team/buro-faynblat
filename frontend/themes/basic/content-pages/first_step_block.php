<?php

use frontend\components\TypicalFunction;
use metalguardian\fileProcessor\helpers\FPM;

$firstStepBlockElements = $model->firstEtapBlock;

?>

<?php if (!empty($firstStepBlockElements)) { ?>
    <section class="step-work">
        <div class="container__inner">
            <div class="step-work__title">
                <?php if ($model->etap_block_label) { ?>
                <div class="heading">
                    <h2 class="heading__title"><?= $model->etap_block_label ?></h2>
                </div>
                <?php } ?>
            </div>
            <div class="step-work__list">
                <div class="step-inner step-inner_col-3 js-step-work-slider js-sq-all-items-w">
                    <?php foreach ($firstStepBlockElements as $key => $element) { ?>
                        <div class="step-item">
                            <div class="step-item__number js-sq-item-w" data-dnum="<?= ($key%3) ?>">
                                <?= ++$key ?>
                            <svg class="sq-figure" viewBox="0 0 60 60">
                                <path class="st0" d="M59.5,29 59.5,0.5 0.5,0.5 0.5,59.5 59.5,59.5 59.5,29"/>
                            </svg>
                            </div>
                            <?php if ($element->label) { ?>
                                <div class="step-item__title">
                                    <?= $element->label ?>
                                </div>
                            <?php } ?>
                            <?php if ($element->description) { ?>
                                <div class="step-item__text">
                                    <?= $element->description?>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>

                </div>
                <div class="pagin pagin_slider"></div>
            </div>
            <!--E_symb-->
            <svg class="n-symbol n-symbol_serv-e-1" viewBox="0 0 170.2 198.6">
                <path d="M111,10.8c9.9,0,16.7,0.7,20.7,2.2c4.9,2.1,8.6,5,11.1,8.9c2.5,3.9,4.8,11.1,6.8,21.5h5.7L153.1,0H0v5.4h7
	c8.2,0,14.1,2,17.6,5.9c2.5,2.9,3.8,10.7,3.8,23.3v129.1c0,9.7-0.6,16.1-1.7,19.2c-1.1,3.1-3.2,5.5-6.4,7c-4.2,2.2-8.6,3.4-13.3,3.4
	H0v5.4h153.1l17.1-49.8h-5.9c-6.3,12.2-12.5,21.3-18.6,27.4c-4.7,4.6-9.6,7.7-14.6,9.2c-5.1,1.6-13.2,2.3-24.5,2.3H72.9
	c-5.5,0-9.2-0.6-11.1-1.7c-2-1.1-3.3-2.6-4.1-4.5c-0.8-1.9-1.2-7.3-1.2-16V100h43.7c8.3,0,14.5,1,18.5,2.9c4,2,6.9,4.6,8.6,8.1
	c1.4,2.7,2.7,8.9,4.1,18.6h5.4V60.5h-5.4c-0.6,11.3-3.4,19.2-8.5,23.7c-3.8,3.4-11.4,5.1-22.7,5.1H56.5V10.8H111z" />
            </svg>
        </div>
    </section>
<?php } ?>

