<?php

use app\widgets\footer\Footer;
use \app\widgets\header\Header;
use common\helpers\BlogsHelper;
use frontend\assets\AllBlogAsset;
use frontend\modules\blogs\widgets\blogsFilters\BlogsFilters;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/**
 * @var \yii\web\View $this
 * @var array $selectedCategories
 */

$this->params['breadcrumbs'][] = Yii::t('breadcrumbs', 'Blog');

AllBlogAsset::register($this);
?>

<?= Header::widget() ?>

<?php if ($model->label) { ?>
	<h1 class="f-blog-title">
    <span class="page-title page-title_center posi" id="projects">
        <?= $model->label; ?>
    </span>
	</h1>
<?php } ?>

<?= BlogsFilters::widget([
    'selectedCategories' => $selectedCategories,
    'selectedStyles' => $selectedStyles,
]) ?>

<?= $this->render('svg_word', ['dataProviderBlogs' => $dataProviderBlogs]) ?>

<!--projects-->
<section class="projects f-blog">
    <div class="container__inner">
        <div class="heading">
            <?php if ($model->description) { ?>
            <div class="heading__title">
                <?= $model->description ?>
            </div>
            <?php } ?>
        </div>
    </div>
    <?php Pjax::begin(['id' => 'blog-items']) ?>
    <div class="container">
        <?php
        echo ListView::widget([
            'dataProvider' => $dataProviderBlogs,
            'layout' => "{items}<div class='pagin'>{pager}</div>",
            'itemView' => function ($model, $key, $index, $widget) {
                $view = BlogsHelper::allBlogsWhatViewRender($index);
                return $this->render($view, ['model' => $model]);
            },
            'options' => [
                'class' => 'projects__iw'
            ],
            'itemOptions' => [
                'class' => 'projects__row item'
            ],
//            'emptyTextOptions' => ['class' => 'news-section__row news clearfix'],
            'pager' => [
                'class' => 'frontend\components\CustomFaynblatPagination',

            ]

        ]);
        ?>
    </div>
    <?php Pjax::end() ?>

</section>

<?= Footer::widget(['staticModel' => $model]); ?>