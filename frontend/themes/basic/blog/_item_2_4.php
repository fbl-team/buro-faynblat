<?php

use \frontend\components\TypicalFunction;
use \common\helpers\NewsHelper;
use \frontend\components\SiteUrl;
use frontend\helpers\SiteUrlHelper;

?>


    <div class="projects__col projects__col_smaller">
        <div class="projects__info">
            <a class="projects__info_iw" href="<?= SiteUrlHelper::createOneBlogUrl(['alias' => $model->alias]) ?>">
                <?php if ($model->label) { ?>
                    <span class="projects__info-caption"><?= $model->label ?></span>
                <?php } ?>
                <?php if ($model->created_at) { ?>
                    <span class="projects__info-line"><?= Yii::$app->formatter->asDatetime($model->created_at,'short') ?></span>
                <?php } ?>
            </a>
        </div>
        <a class="projects__img-wrap projects__img-wrap_left" href="<?= SiteUrlHelper::createOneBlogUrl(['alias' => $model->alias]) ?>">
            <?= TypicalFunction::getImage($model, 'blogSmallImage', ['blog', 'smallimage'], ['class' => 'projects__img']) ?>
        </a>
    </div>
    <div class="projects__col projects__col_bigger">
        <div class="projects__info">
            <a class="projects__img-wrap projects__img-wrap_left f-blog-announcement" href="<?= SiteUrlHelper::createOneBlogUrl(['alias' => $model->alias]) ?>">
                <span class="projects__info-caption"><?= $model->announcement ?></span>
            </a>
        </div>
    </div>


