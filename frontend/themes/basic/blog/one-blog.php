<?php
use app\widgets\footer\Footer;
use \app\widgets\header\Header;
use frontend\helpers\ShowMoreButtonHelper;
use frontend\helpers\SiteUrlHelper;
use frontend\widgets\showMoreItemsButton\ShowMoreItemsButton;

/**
 * @var \yii\web\View $this
 * @var \frontend\modules\pages\models\oneBlog\PageOneBlog $model
 * @var \frontend\modules\blogs\models\Blogs $blog
 */

$this->params['breadcrumbs'][] = [
    'label' => Yii::t('breadcrumbs', 'Blog'),
    'url' => SiteUrlHelper::createAllBlogsUrl()
];
$this->params['breadcrumbs'][] = $blog->label;
?>

<?= Header::widget() ?>

<!--one-blog-->
<section class="one-proj single-f-blog">
    <div class="container__inner">
        <?php if ($model->breadcrums_label && $model->breadcrums_url) { ?>
        <a class="link-back posi" href="<?= $model->breadcrums_url ?>">
            <span class="link-back__txt"><?= $model->breadcrums_label ?></span>
        </a>
        <?php } ?>
        <h1 class="page-title posi"><?= $blog->label ?></h1>
        <?php if($blog->blog_parameters) { ?>
        <div class="one-proj__info posi">
            <?= $blog->blog_parameters ?>
        </div>
        <?php } ?>

        <?= ShowMoreItemsButton::widget([
            'type' => ShowMoreButtonHelper::TYPE_ONE_BLOG_CONTENT,
            'iteration' => 0,
            'parentModelId' => $blog->id,
            'buttonLabel' => $model->add_content_button_label
        ]) ?>

    </div>
</section>

<!--other-works-->
<?php if (!empty($model->blogsRelatedBlogs)) { ?>
<section class="other-works">
    <div class="container__inner">
        <div class="heading">
            <?php if ($model->another_blog_block_label) { ?>
                <span class="heading__title"><?= $model->another_blog_block_label ?></span>
            <?php } ?>
        </div>

        <!--show more elements block-->
        <?= ShowMoreItemsButton::widget([
            'type' => ShowMoreButtonHelper::TYPE_ONE_BLOG_ANOTHER_BLOG,
            'iteration' => 0,
            'parentModelId' => $blog->id,
            'buttonLabel' => $model->another_blog_button_label
        ]) ?>
    </div>
</section>
<?php } ?>

<?= Footer::widget(['staticModel' => $blog, 'staticModelOnePage' => $model]); ?>
