<?php

use app\widgets\footer\Footer;
use app\widgets\header\Header;
use common\helpers\ProjectsHelper;
use frontend\widgets\projectsBlock\ProjectsBlock;
use frontend\widgets\rightMenu\RightMenu;

?>

<?= Header::widget() ?>

<!--fscreen-->
<?= $this->render('first_screen', ['model' => $model]); ?>

<!--aside-menu-->
<?= RightMenu::widget(['allActive' => true]) ?>

<!--main-text-->
<?= $this->render('about_block', ['model' => $model]); ?>

<!--work principles-->
<?= $this->render('work_principle_block', ['model' => $model]); ?>

<!--info-in-digits-->
<?= $this->render('info_in_digits', ['model' => $model]); ?>

<!--our-awards-->
<?= $this->render('our_diploms', ['model' => $model]); ?>

<!--video-block-->
<?= $this->render('canal_block', ['model' => $model]); ?>

<div class="container__inner">
    <!--U_symb-->
    <svg class="n-symbol n-symbol_about-u" viewBox="0 0 211.8 203.3">
        <path d="M149,5.4c7.5,0,13.2,2.1,17,6.4c2.7,3.1,4.1,10.9,4.1,23.3v80.4c0,21.1-1.9,36.1-5.6,44.9
	c-3.8,8.8-10.8,16.1-21,21.8c-10.3,5.7-21.9,8.5-34.9,8.5c-10.2,0-18.7-1.8-25.7-5.4c-7-3.6-12.3-8.2-16-13.8
	c-3.7-5.6-6.3-13.3-7.7-23.3c-1.4-10-2.1-18.8-2.1-26.5V35.2c0-11.1,1.2-18.5,3.7-22C64.2,8,70,5.4,78.2,5.4h7.6V0H0v5.4h7.5
	c7.5,0,13,1.9,16.3,5.7c3.4,3.8,5.1,11.8,5.1,24v77.6c0,23.9,1.6,40.2,4.7,48.9c4.7,12.7,12.6,22.8,23.9,30.3
	c11.2,7.5,27.7,11.3,49.4,11.3c19.9,0,35.7-3.9,47.2-11.8c11.6-7.9,19.3-17.3,23.3-28.3c4-11,5.9-26.4,5.9-46.4V36.3
	c0-11,1-18.1,3.1-21.1c4.2-6.5,10.2-9.8,18-9.8h7.5V0h-70.3v5.4H149z" />
    </svg>

    <div class=" n-symbol-w">
        <!--U_symb-->
        <svg class="n-symbol n-symbol_about-u n-symbol_toHide" viewBox="0 0 211.8 203.3">
            <path d="M149,5.4c7.5,0,13.2,2.1,17,6.4c2.7,3.1,4.1,10.9,4.1,23.3v80.4c0,21.1-1.9,36.1-5.6,44.9
	c-3.8,8.8-10.8,16.1-21,21.8c-10.3,5.7-21.9,8.5-34.9,8.5c-10.2,0-18.7-1.8-25.7-5.4c-7-3.6-12.3-8.2-16-13.8
	c-3.7-5.6-6.3-13.3-7.7-23.3c-1.4-10-2.1-18.8-2.1-26.5V35.2c0-11.1,1.2-18.5,3.7-22C64.2,8,70,5.4,78.2,5.4h7.6V0H0v5.4h7.5
	c7.5,0,13,1.9,16.3,5.7c3.4,3.8,5.1,11.8,5.1,24v77.6c0,23.9,1.6,40.2,4.7,48.9c4.7,12.7,12.6,22.8,23.9,30.3
	c11.2,7.5,27.7,11.3,49.4,11.3c19.9,0,35.7-3.9,47.2-11.8c11.6-7.9,19.3-17.3,23.3-28.3c4-11,5.9-26.4,5.9-46.4V36.3
	c0-11,1-18.1,3.1-21.1c4.2-6.5,10.2-9.8,18-9.8h7.5V0h-70.3v5.4H149z" />
        </svg>
    </div>
</div>

<!--projects_due-->
<?= ProjectsBlock::widget(['type' => ProjectsHelper::SHOW_ON_ABOUT, 'staticModel' => $model]) ?>

<?= Footer::widget(['staticModel' => $model]); ?>
