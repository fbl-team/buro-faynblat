<?php

use frontend\components\TypicalFunction;

$arrayForPublishedBlock = [
    $model->short_about_block_label1,
    $model->short_about_block_label2,
    $model->short_about_block_description1,
    $model->short_about_block_description2,
];

?>
<?php if (TypicalFunction::isBlockFilled($arrayForPublishedBlock)) { ?>
<div class="main-text">
    <div class="container__inner">
        <!--A_symb-->
        <svg class="n-symbol n-symbol_about-a" viewBox="0 0 210.9 203.2">
            <path d="M146.9,165.2c3.9,9.3,5.9,16.1,5.9,20.4c0,3.2-1.6,6.3-4.2,8.2c-2.8,2.2-8.1,3.6-16,3.9v5.4h78.4v-5.4
	c-8.1-0.5-14.4-2.7-19-6.7s-9.6-12.6-15.2-25.8L107.7,0h-5.1L32.7,163.5c-6.1,14.3-11.6,23.9-16.6,28.6c-2.4,2.3-7.8,4.3-16.1,5.7
	v5.4h62.5v-5.4c-9.6-0.7-15.8-2.2-18.7-4.5c-2.9-2.3-4.3-5-4.3-8c0-3.8,1.7-9.6,5-17.3L58,136.7h76.9L146.9,165.2z M62.5,125.8
	l34.6-80.3l33.7,80.3H62.5L62.5,125.8z" />
        </svg>

        <div class="heading">
            <?php if ($model->short_about_block_label1) { ?>
            <span class="heading__caption">
                <?= $model->short_about_block_label1 ?>
            </span>
            <?php } ?>
            <?php if ($model->short_about_block_label2) { ?>
            <div class="heading__title">
                <?= $model->short_about_block_label2 ?>
            </div>
            <?php } ?>
        </div>
        <div class="main-text__content ">
            <?php if ($model->short_about_block_description1) { ?>
            <div class="main-text__item">
                <?= $model->short_about_block_description1 ?>
            </div>
            <?php } ?>
            <?php if ($model->short_about_block_description2) { ?>
            <div class="main-text__item">
                <?= $model->short_about_block_description2 ?>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php } ?>
