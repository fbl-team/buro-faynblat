<?php

use frontend\components\TypicalFunction;

$workPrincipleElements = $model->pageAboutWorkPrinciples;

?>
<?php if (!empty($workPrincipleElements)) { ?>
    <section class="principles">
        <div class="container__inner">
            <!--B_symb-->
            <svg class="n-symbol n-symbol_about-b" viewBox="0 0 178.7 198.6">
                <path d="M160.4,79.8c6-7.6,9.2-17,9.1-26.7c0.1-11.4-4-22.3-11.6-30.8C150.2,13.2,139,7,124.4,3.5
        C114.7,1.2,101.9,0,85.9,0H0v5.4h7.5c7.5,0,13.2,2.1,17,6.4c2.8,3.2,4.2,11,4.2,23.3v128.3c0,11.1-1.1,18.4-3.4,21.8
        c-3.6,5.3-9.6,7.9-17.9,7.9H0v5.4h93.8c22.4,0,39-2.1,49.9-6.2c10.4-3.8,19.4-10.7,25.7-19.8c6.2-9,9.4-18.5,9.4-28.3
        c0.2-12.5-5-24.5-14.2-33c-6.8-6.4-17.1-11.1-30.9-14.1C145.4,93.4,154.4,87.6,160.4,79.8z M56.9,13c8.6-2.2,17.5-3.3,26.4-3.2
        c18.2,0,32,4.1,41.5,12.3c9.5,8.2,14.2,18.1,14.2,29.7c0.1,7.3-1.9,14.5-5.7,20.8c-3.8,6.4-9.6,11.3-16.5,14.1
        c-7.2,3.1-17.9,4.6-32.2,4.6c-5.6,0-10.7-0.2-15.3-0.5c-4.1-0.3-8.3-0.8-12.4-1.5L56.9,13L56.9,13z M56.9,102.8
        c3.4-0.6,6.9-0.9,10.4-1c4.3-0.2,9.5-0.3,15.5-0.3c14,0,25.7,2,35.2,6.1c9.5,4.1,16.3,9.7,20.5,16.8c4.1,6.7,6.3,14.4,6.3,22.3
        c0,11.7-4.6,21.6-13.9,29.5s-22.8,11.9-40.4,11.9c-11.3,0-22.5-1.4-33.5-3.9V102.8z" />
            </svg>

            <div class="heading">
                <?php if ($model->work_block_label) { ?>
                <span class="heading__caption">
                    <?= $model->work_block_label ?>
                </span>
                <?php } ?>
                <?php if ($model->work_block_description) { ?>
                <div class="heading__title">
                    <?= $model->work_block_description ?>
                </div>
                <?php } ?>
            </div>
            <div class="principles__list js-sq-all-items-w">
                <?php foreach ($workPrincipleElements as $key => $element) { ?>
                <div class="dinfo-block">
                    <div class="dinfo-block__number js-sq-item-w" data-dnum="<?= ($key%3) ?>">
                        <?= ++$key; ?>
                    <svg class="sq-figure" viewBox="0 0 60 60">
                        <path class="st0" d="M59.5,29 59.5,0.5 0.5,0.5 0.5,59.5 59.5,59.5 59.5,29"/>
                    </svg>
                    </div>
                    <?php if($element->label) { ?>
                    <div class="dinfo-block__title">
                        <?= $element->label ?>
                    </div>
                    <?php } ?>
                    <?php if($element->description) { ?>
                    <div class="dinfo-block__text">
                        <?= $element->description ?>
                    </div>
                    <?php } ?>
                </div>
                <?php } ?>
            </div>
        </div>
    </section>
<?php } ?>
