<?php

use frontend\components\TypicalFunction;

$canalDisain = $model->pageAboutCanalDisains;

?>

<?php if (!empty($canalDisain)) { ?>
    <section class="video-block">
        <div class="container__inner">

            <div class="heading">
                <?php if ($model->canal_block_label) { ?>
                <span class="heading__title">
                    <?= $model->canal_block_label ?>
                </span>
                <?php } ?>
            </div>

            <div class="video-block__iw">
                <?php foreach ($canalDisain as $disain) { ?>
                <a class="video-block__item posi js-vpo" href="<?= $disain->description ?>">
                    <div class="video-block__item-img">
                        <div class="img-anim-w posi">
                            <?= TypicalFunction::getImage($disain, 'disainImage', ['pageabout', 'canal'], ['class' => 'img-anim-w__img']) ?>
                        </div>
                    </div>
                    <?php if ($disain->label) { ?>
                    <div class="video-block__info">
                        <span class="video-block__info-caption"><?= $disain->label ?></span>
                    </div>
                    <?php } ?>
                </a>
                <?php } ?>
           </div>

            <?php if ($model->canal_block_button_label) { ?>
            <div class="video-block__link">
                <a class="link-to" href="<?= $model->canal_url ?>" target="_blank">
                    <span class='link-to__text'>
                        <?= $model->canal_block_button_label ?>
                    </span>
                </a>
            </div>
            <?php } ?>

        </div>
    </section>
<?php } ?>
