<?php

use app\widgets\header\Header;
use frontend\components\TypicalFunction;
use frontend\helpers\ShowMoreButtonHelper;
use frontend\widgets\rightMenu\RightMenu;
use frontend\widgets\showMoreItemsButton\ShowMoreItemsButton;

$ourDiploms = $model->pageAboutOurDiploms;

?>

<?php if (!empty($ourDiploms)) { ?>

<section class="our-awards">
    <div class="container__inner">
        <!--O_symb-->
        <svg class="n-symbol n-symbol_about-o" viewBox="0 0 194.5 207.7">
            <path d="M32.2,25.9C10.7,45.8,0,71.6,0,103.1c0,30.9,9.3,56,27.9,75.4c18.6,19.4,41.6,29.2,68.9,29.2
	c27,0,50.1-10,69.1-29.9c19-19.9,28.6-45,28.6-75.1c0-29.3-9.4-53.7-28.3-73.3C147.3,9.8,125,0,99.2,0C73.2,0,50.9,8.6,32.2,25.9
	L32.2,25.9z M140.9,30.6c13.4,16.7,20.1,42,20.1,76c0,31.3-6.1,54.3-18.2,68.8c-12.2,14.6-27.6,21.8-46.2,21.8
	c-17.5,0-31.5-6.6-42.2-19.9c-13.9-17.5-20.8-42.4-20.8-74.9c0-31.6,6.7-55.7,20.1-72.1c10.7-13.2,25-19.8,42.8-19.8
	C115.3,10.7,130.2,17.3,140.9,30.6L140.9,30.6z" />
        </svg>

        <div class="heading">
            <?php if ($model->our_diplom_block_label) { ?>
            <span class="heading__title">
                <?= $model->our_diplom_block_label ?>
            </span>
            <?php } ?>
        </div>

        <?= ShowMoreItemsButton::widget([
            'type' => ShowMoreButtonHelper::TYPE_ABOUT_DIPLOMS_BLOCK,
            'iteration' => 0,
            'parentModelId' => $model->id,
        ]) ?>

    </div>
</section>

<?php } ?>
