<?php

use frontend\components\TypicalFunction;
use frontend\helpers\ShowMoreButtonHelper;
use frontend\widgets\showMoreItemsButton\ShowMoreItemsButton;

$speekBlockElements = $model->speekBlocks;

//BlockElementImage
?>

<?php if (!empty($speekBlockElements)) { ?>
    <section class="video-block">
        <div class="container__inner">
            <!--D_symb-->
            <svg class="n-symbol n-symbol_media-d" viewBox="0 0 200.1 198.6">
                <path d="M88.8,198.6c38.6,0,67.3-10.3,86.1-31.1c16.8-18.6,25.2-41.6,25.2-69.3c0-20.6-4.7-38.8-14.1-54.5
	c-9.4-15.7-21.9-27-37.4-33.7C133.1,3.4,110.5,0,80.9,0H0v5.4h7.5c7.5,0,13.2,2.1,17,6.4c2.7,3.1,4.1,10.9,4.1,23.3v128.3
	c0,11.2-1.1,18.5-3.2,21.7c-3.5,5.4-9.5,8.1-17.9,8.1H0v5.4H88.8z M56.7,15.4c11.2-2.8,21.2-4.2,30-4.2c23.4,0,42.6,7.9,57.6,23.7
	c14.9,15.8,22.4,37.5,22.4,64.9c0,27.2-7.5,48.8-22.4,64.7c-14.9,15.9-33.7,23.9-56.4,23.9c-8.4,0-18.8-1.4-31.2-4.1V15.4z" />
            </svg>
            <div class="n-symbol-w">
                <!--D_symb-->
                <svg class="n-symbol n-symbol_media-d n-symbol_toHide" viewBox="0 0 200.1 198.6">
                    <path d="M88.8,198.6c38.6,0,67.3-10.3,86.1-31.1c16.8-18.6,25.2-41.6,25.2-69.3c0-20.6-4.7-38.8-14.1-54.5
	c-9.4-15.7-21.9-27-37.4-33.7C133.1,3.4,110.5,0,80.9,0H0v5.4h7.5c7.5,0,13.2,2.1,17,6.4c2.7,3.1,4.1,10.9,4.1,23.3v128.3
	c0,11.2-1.1,18.5-3.2,21.7c-3.5,5.4-9.5,8.1-17.9,8.1H0v5.4H88.8z M56.7,15.4c11.2-2.8,21.2-4.2,30-4.2c23.4,0,42.6,7.9,57.6,23.7
	c14.9,15.8,22.4,37.5,22.4,64.9c0,27.2-7.5,48.8-22.4,64.7c-14.9,15.9-33.7,23.9-56.4,23.9c-8.4,0-18.8-1.4-31.2-4.1V15.4z" />
                </svg>
            </div>
            
            <?php if ($model->speak_block_label) { ?>
            <div class="heading">
                <span class="heading__caption"><?= $model->speak_block_label ?></span>
            </div>
            <?php } ?>

            <!--video blocks-->
            <?= ShowMoreItemsButton::widget([
                'type' => ShowMoreButtonHelper::TYPE_MEDIA_SPEEK_BLOCK,
                'iteration' => 0,
                'parentModelId' => $model->id,
                'buttonLabel' => $model->speak_block_button_label
            ]) ?>

            <!--I_symb-->
            <svg class="n-symbol n-symbol_media-i" viewBox="0 0 85.1 198.6">
                <path d="M77.9,193.2c-4.7,0-9.1-1.2-13.3-3.5c-3.1-1.8-5.2-3.9-6.2-6.3c-1.3-3.2-1.9-9.9-1.9-19.9V35.2
	c0-11.9,1.2-19.4,3.7-22.6c3.7-4.8,9.6-7.2,17.7-7.2h7.2V0H0v5.4h7c4.7,0,9.2,1.2,13.5,3.5c3,1.8,5,3.9,6,6.3
	c1.3,3.2,1.9,9.9,1.9,19.9v128.3c0,11.9-1.2,19.4-3.5,22.6c-3.7,4.8-9.7,7.2-17.9,7.2H0v5.4h85.1v-5.4H77.9z" />
            </svg>
            <div class="n-symbol-w">
                <!--I_symb-->
                <svg class="n-symbol n-symbol_media-i n-symbol_toHide" viewBox="0 0 85.1 198.6">
                    <path d="M77.9,193.2c-4.7,0-9.1-1.2-13.3-3.5c-3.1-1.8-5.2-3.9-6.2-6.3c-1.3-3.2-1.9-9.9-1.9-19.9V35.2
	c0-11.9,1.2-19.4,3.7-22.6c3.7-4.8,9.6-7.2,17.7-7.2h7.2V0H0v5.4h7c4.7,0,9.2,1.2,13.5,3.5c3,1.8,5,3.9,6,6.3
	c1.3,3.2,1.9,9.9,1.9,19.9v128.3c0,11.9-1.2,19.4-3.5,22.6c-3.7,4.8-9.7,7.2-17.9,7.2H0v5.4h85.1v-5.4H77.9z" />
                </svg>
            </div>
        </div>
    </section>
<?php } ?>
