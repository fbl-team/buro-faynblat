<?php
use app\widgets\footer\Footer;
use app\widgets\header\Header;

/**
 * @var \yii\web\View $this
 */

$this->params['breadcrumbs'][] = Yii::t('breadcrumbs', 'Media');
?>

<?= Header::widget(); ?>

<div class="container__inner">
    <!--M_symb-->
    <svg class="n-symbol n-symbol_media-m" viewBox="0 0 255.5 198.6">
        <path d="M122.6,198.6l76.6-167.1v132.7c0,11.1-1.1,18.3-3.2,21.4c-3.4,5.1-9.4,7.6-17.9,7.6h-7v5.4h84.4v-5.4h-6.9
	c-7.8,0-13.5-2.1-17.1-6.2c-2.7-3-4.1-10.6-4.1-22.9V34.4c0-11.1,1.1-18.3,3.2-21.4c3.5-5.1,9.5-7.6,18-7.6h6.9V0h-56.2l-70.9,155.4
	L56.2,0H0v5.4c8.1,0,14,1,17.7,2.9s6.2,4.3,7.5,7.1c2,3.9,2.9,10.3,2.9,19v129.8c0,11.1-1.1,18.3-3.2,21.4
	c-3.5,5.1-9.5,7.6-17.9,7.6H0v5.4h69.1v-5.4h-7c-7.8,0-13.5-2.1-17.1-6.2c-2.6-3-4-10.6-4-22.9V31.5l76.8,167.1H122.6z" />
    </svg>
    <div class="n-symbol-w">
        <!--M_symb-->
        <svg class="n-symbol n-symbol_media-m n-symbol_toHide" viewBox="0 0 255.5 198.6">
            <path d="M122.6,198.6l76.6-167.1v132.7c0,11.1-1.1,18.3-3.2,21.4c-3.4,5.1-9.4,7.6-17.9,7.6h-7v5.4h84.4v-5.4h-6.9
	c-7.8,0-13.5-2.1-17.1-6.2c-2.7-3-4.1-10.6-4.1-22.9V34.4c0-11.1,1.1-18.3,3.2-21.4c3.5-5.1,9.5-7.6,18-7.6h6.9V0h-56.2l-70.9,155.4
	L56.2,0H0v5.4c8.1,0,14,1,17.7,2.9s6.2,4.3,7.5,7.1c2,3.9,2.9,10.3,2.9,19v129.8c0,11.1-1.1,18.3-3.2,21.4
	c-3.5,5.1-9.5,7.6-17.9,7.6H0v5.4h69.1v-5.4h-7c-7.8,0-13.5-2.1-17.1-6.2c-2.6-3-4-10.6-4-22.9V31.5l76.8,167.1H122.6z" />
        </svg>
    </div>
    <?php if ($model->label) { ?>
        <h1><span class="page-title posi"><?= $model->label ?></span></h1>
    <?php } ?>
</div>

<!--media-block-->
<?= $this->render('write_block', ['model' => $model]); ?>

<!--video-block-->
<?= $this->render('speek_block', ['model' => $model]); ?>

<?= Footer::widget(['staticModel' => $model]); ?>
