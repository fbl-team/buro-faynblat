<?php
use app\widgets\header\Header;
use frontend\components\TypicalFunction;
use frontend\helpers\SiteUrlHelper;
use yii\widgets\Menu;

?>

<!--main-screen-->
<div class="fof parentsize">
    <?= Header::widget(); ?>

    <!--video-->
    <div class="fof__video-bg">
        <video class="video-to-parentsize" width="100%" height="640" preload="auto" autoplay="autoplay" loop="loop" muted="muted">
            <source src="video/home_video-bg.mp4" type="video/mp4">
            <source src="video/home_video-bg.webm" type="video/webm">
        </video>
    </div>

    <div class="fof__iw">
        <div class="fof__txt-404">404</div>
        <div class="fof__txt">Page not found</div>
        <a class="link-to" href="<?= SiteUrlHelper::createHomeUrl() ?>">
            <span class='link-to__text'>Home page</span>
        </a>
    </div>
</div>