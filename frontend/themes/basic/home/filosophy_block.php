<?php

use frontend\components\TypicalFunction;
use frontend\components\TypicalFunctionFaynblat;

$philosophyImage = TypicalFunction::getImage($model, 'filosophyBlockImage', ['pagemain', 'filosophy'],
    ['class' => 'home-about__img']);

$arrayForPublishedBlock = [
    $philosophyImage
];

?>
<?php if (TypicalFunction::isBlockFilled($arrayForPublishedBlock)) { ?>
        <section class="home-about posi">
            <div class="home-about__img-w">
                <?= $philosophyImage ?>
            </div>
            <div class="home-about__descr-w">
                <?php if ($model->filosophy_block_label || $model->filosophy_block_short_description) { ?>
                <div class="heading heading_lelan">
                    <?php if ($model->filosophy_block_label) { ?>
                        <span class="heading__caption"><?= $model->filosophy_block_label ?></span>
                    <?php } ?>
                    <?php if ($model->filosophy_block_short_description) { ?>
                        <div class="heading__title">
                            <?= TypicalFunctionFaynblat::generateAnimatedString($model->filosophy_block_short_description); ?>
                        </div>
                    <?php } ?>
                </div>
                <?php } ?>
                <?php if ($model->filosophy_block_full_description) { ?>
                    <div class="home-about__descr">
                        <?= $model->filosophy_block_full_description ?>
                    </div>
                <?php } ?>
                <?php if ($model->filosophy_block_button_label) { ?>
                    <a class='action-btn js-to-main-form' href="#"><?= $model->filosophy_block_button_label ?></a>
                <?php } ?>
            </div>
            <svg class="n-symbol n-symbol_home-f" viewBox="0 0 149.7 198.6">
                <!--F_symb-->
                <path d="M100.3,10.8c10.4,0,18,1,22.9,2.9c4.9,2,9.2,5.2,12.5,9.4c3.4,4.3,6.3,11.2,8.9,20.4h5.1L147.8,0H0v5.4
	h7c4.7,0,9.4,1.2,13.5,3.5c2.7,1.4,4.8,3.6,6,6.3c1.3,3.2,1.9,9.9,1.9,19.9v128.3c0,11.9-1.2,19.4-3.5,22.6
	c-3.7,4.8-9.7,7.2-17.9,7.2H0v5.4h85.3v-5.4h-7.2c-4.7,0-9.4-1.2-13.5-3.5c-3.1-1.8-5.2-3.9-6.2-6.3c-1.3-3.2-1.9-9.9-1.9-19.9
	v-62.4h36.2c6.6,0,11.6,0.9,15,2.7c3.4,1.8,6.2,4.7,7.9,8.1c1.9,3.6,2.9,9.3,3,17h5.4V61.7h-5.4c-1.3,10.8-3.8,18.1-7.7,21.8
	s-9.9,5.5-18.2,5.5H56.5V10.8H100.3L100.3,10.8z" />
            </svg>
        </section>
<?php } ?>
