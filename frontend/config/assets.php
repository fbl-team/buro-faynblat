<?php
/**
 * Configuration file for the "yii asset" console command.
 */

Yii::setAlias('@webroot', __DIR__ . '/../web');
Yii::setAlias('@web', '/');

return [
    'jsCompressor' => 'java -jar compiler.jar --js {from} --js_output_file {to}',
    'cssCompressor' => 'java -jar yuicompressor.jar --type css {from} -o {to}',
    'bundles' => [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'frontend\assets\MainAsset'
    ],
    'targets' => [
        'all' => [
            'class' => 'yii\web\AssetBundle',
            'basePath' => '@webroot/compressed',
            'baseUrl' => '@web/compressed',
            'js' => 'js/site.min.js',
            'css' => 'css/site.min.css?v=1',
        ],
    ],
    'assetManager' => [
        'basePath' => '@webroot/assets',
        'baseUrl' => '@web/assets',
    ],
];