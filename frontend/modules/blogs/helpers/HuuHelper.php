<?php
namespace frontend\modules\blogs\helpers;

/**
 * Helper for human understand URL in blog filters
 *
 * @author Vladimir Kuprienko <vldmr.kuprienko@gmail.com>
 */
class HuuHelper
{
    // categories
    const CATEGORY_INTERIOR             = 'interior';
    const CATEGORY_APARTMENTS           = 'apartments';
    const CATEGORY_HOUSES_COTTAGES      = 'houses-cottages';
    const CATEGORY_RESTAURANTS_CAFES    = 'restaurants-cafes';
    const CATEGORY_OFFICES              = 'offices';

    const CATEGORY_MIND              = 'mind';
    const CATEGORY_NEWS              = 'news';
    const CATEGORY_TEST              = 'test';

    // styles
    const STYLE_CLASSICS        = 'classics';
    const STYLE_NEO_CLASSICS    = 'neo-classics';
    const STYLE_ART_DECOR       = 'art-decor';
    const STYLE_PROVENCE        = 'provence';
    const STYLE_LOFT            = 'loft';
    const STYLE_MINIMALISM      = 'minimalism';

    /**
     * @var array
     */
    private static $_categoryDictionary = [
        self::CATEGORY_INTERIOR             => 1,
        self::CATEGORY_APARTMENTS           => 2,
        self::CATEGORY_HOUSES_COTTAGES      => 3,
        self::CATEGORY_RESTAURANTS_CAFES    => 4,
        self::CATEGORY_OFFICES              => 5,

        self::CATEGORY_MIND              => 6,
        self::CATEGORY_NEWS              => 7,
        self::CATEGORY_TEST              => 8,
    ];
    /**
     * @var array
     */
    private static $_styleDictionary = [
        self::STYLE_CLASSICS        => 1,
        self::STYLE_NEO_CLASSICS    => 2,
        self::STYLE_ART_DECOR       => 3,
        self::STYLE_PROVENCE        => 4,
        self::STYLE_LOFT            => 5,
        self::STYLE_MINIMALISM      => 6,
    ];

    /**
     * Returns label for category by ID
     *
     * @param integer $id
     * @return mixed
     */
    public static function getCategoryLabel($id)
    {
        $label = array_search($id, static::$_categoryDictionary);

        if($label)
            return $label;
        else
            return 'category'.$id;
    }

    /**
     * Returns label for style by ID
     *
     * @param integer $id
     * @return mixed
     */
    public static function getStyleLabel($id)
    {
        return array_search($id, static::$_styleDictionary);
    }

    /**
     * Returns category ID by label
     *
     * @param string $label
     * @return integer|null
     */
    public static function getCategoryId($label)
    {
        if (isset(static::$_categoryDictionary[$label])) {
            return static::$_categoryDictionary[$label];
        }elseif(stripos($label, "category") !== false){
            return str_replace("category", "", $label);
        }

        return null;
    }

    /**
     * Returns style ID by label
     *
     * @param string $label
     * @return integer|null
     */
    public static function getStyleId($label)
    {
        if (isset(static::$_styleDictionary[$label])) {
            return static::$_styleDictionary[$label];
        }

        return null;
    }
}
