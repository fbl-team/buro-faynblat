<?php

namespace frontend\modules\blogs;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\blogs\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
