<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 7/13/16
 * Time: 2:53 PM
 */

use backend\modules\article\helpers\ArticleConstructorHelper;
use backend\modules\article\models\Article;
use backend\modules\blogs\models\Blogs;

/* @var \backend\modules\blogs\helpers\BlogsConstructorHelper $constructor */
/* @var integer $type */

$model = new Blogs();
echo $constructor->getContentByType($model, $type);