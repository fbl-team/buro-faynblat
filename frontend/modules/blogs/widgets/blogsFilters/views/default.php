<?php

use frontend\helpers\SiteUrlHelper;
use frontend\modules\blogs\helpers\HuuHelper;

?>

<div class="filters-wrap posi blogs-filter" data-url="<?= SiteUrlHelper::createAllBlogsUrl() ?>">
    <div class="container__inner">
        <!--filter-type-->
        <div class="filter-type js-filter-block">
            <div class="filter-type__chosen-option js-filter">
                <span class="filter-type__chosen-text blogs-filter-element-category"><?= Yii::t('front/all_blogs', 'Все') ?></span>
                <svg class="dropdown-arrow">
                    <use xlink:href="#arrow-drop"></use>
                </svg>
            </div>
            <ul class="filter-type__items-w js-filter-list data-type" data-type="category">
                
                <li class="filter-type__item-w">
                    <a class="filter-type__item blogs-filter-element blogs-filter-element-category <?= $this->context->isActiveCategory('all') ?>" data-label="all" href="#"><?= Yii::t('front/all_blogs', 'Все') ?></a>
                </li>
                
                <?php foreach ($categories as $category): ?>
                <li class="filter-type__item-w">
                    <a class="filter-type__item blogs-filter-element blogs-filter-element-category <?= $this->context->isActiveCategory($category->id) ?>"
                       data-label="<?= HuuHelper::getCategoryLabel($category->id) ?>" href="#">
                        <?= $category->label ?>
                    </a>
                </li>
                <?php endforeach; ?>
                
            </ul>
        </div>
        <!--filter-style-->
        <div class="filter-style js-filter-block">
            <div class="filter-style__chosen-option js-filter">
                <span class="filter-type__chosen-text blogs-filter-element-styles"><?= Yii::t('front/all_blogs', 'Стили') ?></span>
                <svg class="dropdown-arrow">
                    <use xlink:href="#arrow-drop"></use>
                </svg>
            </div>
            <ul class="filter-style__items-w js-filter-list data-type filter-styles-wrapper" data-type="styles" data-styles-count="<?= count($styles) ?>">
                
                <li class="filter-style__item-w">
                    <a class="filter-style__item blogs-filter-element blogs-filter-element-styles blogs-filter-element-styles-all" href="#" data-label="all"><?= Yii::t('front/all_blogs', 'Все стили') ?></a>
                </li>

                <?php foreach ($styles as $style): ?>
                <li class="filter-style__item-w">
                    <a class="filter-style__item blogs-filter-element blogs-filter-element-styles <?= $this->context->isActiveStyles($style->id) ?>"
                       data-label="<?= HuuHelper::getStyleLabel($style->id) ?>" href="#">
                        <?= $style->label ?>
                    </a>
                </li>
                <?php endforeach; ?>
                
            </ul>
        </div>
    </div>
</div>