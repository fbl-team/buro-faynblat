<?php

namespace frontend\modules\blogs\models;

use common\models\EntityToFile;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%blog_related_blog}}".
 *
 * @property integer $id
 * @property integer $blog_id
 * @property integer $related_blog_id
 *
 * @property Blogs $blog
 * @property Blogs $relatedBlog
 */
class BlogRelatedBlog extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_related_blog}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlog()
    {
        return $this->hasOne(Blogs::className(), ['id' => 'related_blog_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedBlog()
    {
        return $this->hasOne(Blogs::className(), ['id' => 'related_blog_id']);
    }

}
