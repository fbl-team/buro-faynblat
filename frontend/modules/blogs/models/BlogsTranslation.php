<?php

namespace frontend\modules\blogs\models;

use Yii;

/**
* This is the model class for table "{{%blogs_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $city_and_square
*/
class BlogsTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%blogs_translation}}';
    }

}
