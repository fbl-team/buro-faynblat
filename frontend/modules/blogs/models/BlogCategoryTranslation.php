<?php

namespace frontend\modules\blogs\models;

use Yii;

/**
* This is the model class for table "{{%blog_category_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $content
*/
class BlogCategoryTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%blog_category_translation}}';
    }
}
