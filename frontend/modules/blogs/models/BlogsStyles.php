<?php

namespace frontend\modules\blogs\models;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\models\EntityToFile;

/**
 * This is the model class for table "{{%blogs_styles}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $published
 * @property integer $position
 *
 * @property BlogsStylesRelated[] $blogsStylesRelateds
 * @property BlogsStylesTranslation[] $translations
 */
class BlogsStyles extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blogs_styles}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogsStylesRelateds()
    {
        return $this->hasMany(BlogsStylesRelated::className(), ['blog_style_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(BlogsStylesTranslation::className(), ['model_id' => 'id']);
    }

}
