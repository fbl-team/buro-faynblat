<?php

namespace frontend\modules\blogs\models;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\models\EntityToFile;

/**
 * This is the model class for table "{{%blogs}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $city_and_square
 * @property integer $category_id
 * @property integer $show_on_main
 * @property integer $show_on_about
 * @property integer $show_on_services
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property BlogContent[] $blogContents
 * @property BlogCategory $category
 * @property BlogsTranslation[] $translations
 */
class Blogs extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blogs}}';
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'announcement',
            'city_and_square',
            'blog_parameters',

            'footer_request_block_label',
            'footer_request_block_button_label',
            'footer_seo_block_label',
            'footer_seo_block_button_label',
            'footer_seo_block_text_left_column',
            'footer_seo_block_text_left_right',
            'footer_seo_block_button_label_hide',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogContents()
    {
        return $this->hasMany(BlogContent::className(), ['blog_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(BlogCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(BlogsTranslation::className(), ['model_id' => 'id']);
    }

    public function getBlogBigImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_BLOG_BIG_IMAGE])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

    public function getBlogSmallImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_BLOG_SMALL_IMAGE])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogsParameters()
    {
        return $this->hasMany(BlogParametrs::className(), ['blog_id' => 'id'])
            ->orderBy('position');
    }

    public function getBlogsStyles()
    {
        return $this->hasMany(BlogsStyles::className(), ['id' => 'blog_style_id'])
            ->viaTable(BlogsStylesRelated::tableName(), ['blog_id' => 'id']);
    }

    public function getBlogsRelatedBlogs()
    {
        return $this->hasMany(Blogs::className(), ['id' => 'related_blog_id'])
            ->viaTable(BlogRelatedBlog::tableName(), ['blog_id' => 'id']);
    }

    public function getBlogsRelated()
    {
        return $this->hasOne(BlogRelatedBlog::className(), ['blog_id' => 'id']);
    }

}
