<?php

namespace frontend\modules\blogs\models;

use Yii;

/**
* This is the model class for table "{{%blog_parametrs_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $description
*/
class BlogParametrsTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%blog_parametrs_translation}}';
    }

}
