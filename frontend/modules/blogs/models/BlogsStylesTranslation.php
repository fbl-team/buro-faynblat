<?php

namespace frontend\modules\blogs\models;

use Yii;

/**
* This is the model class for table "{{%blogs_styles_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
*/
class BlogsStylesTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%blogs_styles_translation}}';
    }

}
