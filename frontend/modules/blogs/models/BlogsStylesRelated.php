<?php

namespace frontend\modules\blogs\models;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\models\EntityToFile;

/**
 * This is the model class for table "{{%blogs_styles_related}}".
 *
 * @property integer $id
 * @property integer $blog_id
 * @property integer $blog_style_id
 *
 * @property Blogs $blog
 * @property BlogsStyles $blogStyle
 */
class BlogsStylesRelated extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blogs_styles_related}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlog()
    {
        return $this->hasOne(Blogs::className(), ['id' => 'blog_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogStyle()
    {
        return $this->hasOne(BlogsStyles::className(), ['id' => 'blog_style_id']);
    }

}
