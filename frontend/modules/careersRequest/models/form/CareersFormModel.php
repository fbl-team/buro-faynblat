<?php
namespace frontend\modules\careersRequest\models\form;

use frontend\components\mails\Mails;
use frontend\components\TypicalFunction;
use frontend\modules\careersRequest\models\Careers;
use frontend\modules\request\models\Request;
use rmrevin\yii\postman\ViewLetter;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class CareersFormModel extends Model
{
    public $name;
    public $phone;
    public $email;
    public $text;


    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => \Yii::t('front/careers', 'Name'),
            'phone' => \Yii::t('front/careers', 'Phone'),
            'email' => \Yii::t('front/careers', 'Email'),
            'text' => \Yii::t('front/careers', 'Text'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'email', 'text'], 'required', 'message' => Yii::t('front/careers', 'Please enter a value for {attribute}.')],
            [['email'], 'email', 'message' => Yii::t('front/careers', 'Email do not correct.')],
        ];
    }

    /**
     * @return null|\yii\web\IdentityInterface
     */
    public function save()
    {
        if ($this->validate()) {

            $newRequest = new Careers();

            $newRequest->name = $this->name;
            $newRequest->phone = $this->phone;
            $newRequest->email = $this->email;
            $newRequest->text = $this->text;

            $newRequest->created_at = time();

            $saveResult = $newRequest->save(false);

            Mails::sendEmail($this, Mails::TYPE_CAREER, $saveResult);

            return $saveResult;

        }

        return null;
    }
}
