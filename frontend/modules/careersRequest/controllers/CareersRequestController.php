<?php

namespace frontend\modules\careersRequest\controllers;

use app\widgets\career\Career;
use app\widgets\request\Request;
use frontend\modules\careersRequest\models\form\CareersFormModel;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\Controller;

/**
 * Default controller for the `request` module
 */
class CareersRequestController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSend()
    {
        $data = null;

        if (\Yii::$app->request->isAjax) {
            $model = new CareersFormModel();
            if ($model->load(\Yii::$app->request->post())) {
                if ($model->save()) {
                    //сброс заполненных данных и ошибок валидации в форме
                    $model = new CareersFormModel();
                    $data = [
                        'replaces' => [
//                            [
//                                'data' => $this->renderPartial('subscribeThanks', []),
//                                'what' => '.popup'
//                            ],
                            [
                                'data' => Career::widget(['model' => $model]),
                                'what' => '.faynblat-career'
                            ]
                        ],
                        'js' => Html::script('showPopupCareers();')
                    ];
                }
            }

            $data = $data
                ? $data
                : [
                    'replaces' => [
                        [
                            'data' => Career::widget(['model' => $model]),
                            'what' => '.faynblat-career'
                        ]
                    ],
                ];

            echo Json::encode($data);
        }
    }

}
