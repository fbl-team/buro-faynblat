<?php

namespace frontend\modules\request\controllers;

use app\widgets\request\Request;
use frontend\helpers\SiteUrlHelper;
use frontend\modules\request\models\form\RequestFormModel;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\Controller;

/**
 * Default controller for the `request` module
 */
class RequestController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionFaynblatCallYou()
    {
        $data = null;

        if (\Yii::$app->request->isAjax) {
            $model = new RequestFormModel();
            if ($model->load(\Yii::$app->request->post())) {
                if ($model->save()) {
                    //сброс заполненных данных и ошибок валидации в форме
                    $model = new RequestFormModel();
                    $data = [
                        'replaces' => [
//                            [
//                                'data' => $this->renderPartial('subscribeThanks', []),
//                                'what' => '.popup'
//                            ],
                            [
                                'data' => Request::widget(['model' => $model]),
                                'what' => '.faynblat-call-you'
                            ]
                        ],
                        'js' => Html::script('showPopup();')
                    ];
                }
            }

            $data = $data
                ? $data
                : [
                    'replaces' => [
                        [
                            'data' => Request::widget(['model' => $model]),
                            'what' => '.faynblat-call-you'
                        ]
                    ],
                ];

            echo Json::encode($data);
        }
    }

}
