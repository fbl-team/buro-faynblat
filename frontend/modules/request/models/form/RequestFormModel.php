<?php
namespace frontend\modules\request\models\form;

use frontend\components\mails\Mails;
use frontend\components\TypicalFunction;
use frontend\modules\request\models\Request;
use rmrevin\yii\postman\ViewLetter;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class RequestFormModel extends Model
{
    public $name;
    public $phone;
    public $email;


    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => \Yii::t('front/request', 'Name'),
            'phone' => \Yii::t('front/request', 'Phone'),
            'email' => \Yii::t('front/request', 'Email'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'email'], 'required', 'message' => Yii::t('front/request', 'Please enter a value for {attribute}.')],
            [['email'], 'email', 'message' => Yii::t('front/request', 'Email do not correct.')],
        ];
    }

    /**
     * @return null|\yii\web\IdentityInterface
     */
    public function save()
    {
        if ($this->validate()) {

            $newRequest = new Request();

            $newRequest->name = $this->name;
            $newRequest->phone = $this->phone;
            $newRequest->email = $this->email;

            $newRequest->created_at = time();

            $saveResult = $newRequest->save(false);

            Mails::sendEmail($this, Mails::TYPE_REQUEST, $saveResult);

            return $saveResult;

        }

        return null;
    }
}
