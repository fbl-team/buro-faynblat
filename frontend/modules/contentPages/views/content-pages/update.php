<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model \backend\components\BackendModel|\yii\db\ActiveRecord */

$this->title = $model->getTitle();
$this->params['breadcrumbs'][] = ['label' => $model->getTitle()];

?>
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="panel-title"><?= Html::encode($this->title) ?></span>
    </div>

    <div class="panel-body">
        <?= $this->render('//templates/_form', [
            'model' => $model,
        ]) ?>
    </div>

</div>
