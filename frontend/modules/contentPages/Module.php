<?php

namespace frontend\modules\contentPages;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\contentPages\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
