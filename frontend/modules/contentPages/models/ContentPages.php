<?php

namespace frontend\modules\contentPages\models;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\models\EntityToFile;

/**
 * This is the model class for table "{{%content_pages}}".
 *
 * @property integer $id
 * @property string $breadcrums_label
 * @property string $breadcrums_url
 * @property string $first_screen_label
 * @property string $first_screen_button_label
 * @property string $second_screen_label1
 * @property string $second_screen_label2
 * @property string $second_screen_description1
 * @property string $second_screen_description2
 * @property string $etap_block_label
 * @property string $inside_service_block_label
 * @property string $inside_service_block_description
 * @property string $etap_second_block_label
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ContentPagesTranslation[] $translations
 */
class ContentPages extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%content_pages}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'breadcrums_label',
            'first_screen_label',
            'first_screen_button_label',
            'second_screen_label1',
            'second_screen_label2',
            'second_screen_description1',
            'second_screen_description2',
            'etap_block_label',
            'inside_service_block_label',
            'inside_service_block_description',
            'etap_second_block_label',

            'footer_request_block_label',
            'footer_request_block_button_label',
            'footer_seo_block_label',
            'footer_seo_block_button_label',
            'footer_seo_block_text_left_column',
            'footer_seo_block_text_left_right',
            'footer_seo_block_button_label_hide',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ContentPagesTranslation::className(), ['model_id' => 'id']);
    }

    public function getSliderImages()
    {
        return $this->hasMany(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::CONTENT_PAGE_SLIDER])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstEtapBlock()
    {
        return $this->hasMany(ContentPagesFirstEtapBlock::className(), ['content_page_id' => 'id'])
            ->andWhere(['published' => true])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondEtapBlock()
    {
        return $this->hasMany(ContentPagesSecondEtapBlock::className(), ['content_page_id' => 'id'])
            ->andWhere(['published' => true])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInsideWorkBlock()
    {
        return $this->hasMany(ContentPageInsideBlock::className(), ['content_page_id' => 'id'])
            ->andWhere(['published' => true])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getSecondEtapBlock()
//    {
//        return $this->hasMany(PageFooterElementsColumn1::className(), ['footer_page_id' => 'id'])
//            ->andWhere(['column_type' => FooterHelper::FOOTER_COLUMN_1])
//            ->orderBy('position');
//    }

}
