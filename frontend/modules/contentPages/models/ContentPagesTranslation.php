<?php

namespace frontend\modules\contentPages\models;

use Yii;

/**
* This is the model class for table "{{%content_pages_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $breadcrums_label
* @property string $first_screen_label
* @property string $first_screen_button_label
* @property string $second_screen_label1
* @property string $second_screen_label2
* @property string $second_screen_description1
* @property string $second_screen_description2
* @property string $etap_block_label
* @property string $inside_service_block_label
* @property string $inside_service_block_description
* @property string $etap_second_block_label
*/
class ContentPagesTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%content_pages_translation}}';
    }

}
