<?php

namespace frontend\modules\contentPages\models;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\models\EntityToFile;

/**
 * This is the model class for table "{{%content_page_inside_block}}".
 *
 * @property integer $id
 * @property integer $content_page_id
 * @property string $label
 * @property string $description
 * @property integer $published
 * @property integer $position
 *
 * @property ContentPages $contentPage
 * @property ContentPageInsideBlockTranslation[] $translations
 */
class ContentPageInsideBlock extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%content_page_inside_block}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContentPage()
    {
        return $this->hasOne(ContentPages::className(), ['id' => 'content_page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ContentPageInsideBlockTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return $this
     */
    public function getElementImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_CONTENT_PAGE_INSIDE_BLOCK_IMAGE])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

    /**
     * @return $this
     */
    public function getElementVideos()
    {
        return $this->hasMany(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_CONTENT_PAGE_INSIDE_BLOCK_VIDEO])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

}
