<?php

namespace frontend\modules\contentPages\models;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\models\EntityToFile;

/**
 * This is the model class for table "{{%content_pages_second_etap_block}}".
 *
 * @property integer $id
 * @property integer $content_page_id
 * @property string $label
 * @property string $description
 * @property integer $published
 * @property integer $position
 *
 * @property ContentPages $contentPage
 * @property ContentPagesSecondEtapBlockTranslation[] $translations
 */
class ContentPagesSecondEtapBlock extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%content_pages_second_etap_block}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContentPage()
    {
        return $this->hasOne(ContentPages::className(), ['id' => 'content_page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ContentPagesSecondEtapBlockTranslation::className(), ['model_id' => 'id']);
    }

}
