<?php

namespace frontend\modules\contentPages\models;

use Yii;

/**
* This is the model class for table "{{%content_pages_second_etap_block_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $description
*/
class ContentPagesSecondEtapBlockTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%content_pages_second_etap_block_translation}}';
    }

}
