<?php

namespace frontend\modules\projects\models;

use Yii;

/**
* This is the model class for table "{{%project_parametrs_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $description
*/
class ProjectParametrsTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%project_parametrs_translation}}';
    }

}
