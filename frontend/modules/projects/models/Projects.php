<?php

namespace frontend\modules\projects\models;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\models\EntityToFile;

/**
 * This is the model class for table "{{%projects}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $city_and_square
 * @property integer $category_id
 * @property integer $show_on_main
 * @property integer $show_on_about
 * @property integer $show_on_services
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProjectContent[] $projectContents
 * @property ProjectCategory $category
 * @property ProjectsTranslation[] $translations
 */
class Projects extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%projects}}';
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'city_and_square',
            'project_parameters',

            'footer_request_block_label',
            'footer_request_block_button_label',
            'footer_seo_block_label',
            'footer_seo_block_button_label',
            'footer_seo_block_text_left_column',
            'footer_seo_block_text_left_right',
            'footer_seo_block_button_label_hide',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectContents()
    {
        return $this->hasMany(ProjectContent::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProjectCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProjectsTranslation::className(), ['model_id' => 'id']);
    }

    public function getProjectBigImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_PROJECT_BIG_IMAGE])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

    public function getProjectSmallImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_PROJECT_SMALL_IMAGE])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectsParameters()
    {
        return $this->hasMany(ProjectParametrs::className(), ['project_id' => 'id'])
            ->orderBy('position');
    }

    public function getProjectsStyles()
    {
        return $this->hasMany(ProjectsStyles::className(), ['id' => 'project_style_id'])
            ->viaTable(ProjectsStylesRelated::tableName(), ['project_id' => 'id']);
    }

    public function getProjectsRelatedProjects()
    {
        return $this->hasMany(Projects::className(), ['id' => 'related_project_id'])
            ->viaTable(ProjectRelatedProject::tableName(), ['project_id' => 'id']);
    }

    public function getProjectsRelated()
    {
        return $this->hasOne(ProjectRelatedProject::className(), ['project_id' => 'id']);
    }

}
