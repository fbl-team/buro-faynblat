<?php

namespace frontend\modules\projects\models;

use Yii;

/**
* This is the model class for table "{{%projects_styles_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
*/
class ProjectsStylesTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%projects_styles_translation}}';
    }

}
