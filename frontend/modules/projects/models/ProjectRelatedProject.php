<?php

namespace frontend\modules\projects\models;

use common\models\EntityToFile;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%project_related_project}}".
 *
 * @property integer $id
 * @property integer $project_id
 * @property integer $related_project_id
 *
 * @property Projects $project
 * @property Projects $relatedProject
 */
class ProjectRelatedProject extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_related_project}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'related_project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'related_project_id']);
    }

}
