<?php

namespace frontend\modules\projects\models;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\models\EntityToFile;


/**
 * This is the model class for table "{{%project_content}}".
 *
 * @property integer $id
 * @property integer $project_id
 * @property string $type
 * @property string $label
 * @property string $content
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Projects $project
 * @property ProjectContentTranslation[] $translations
 */
class ProjectContent extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_content}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'content',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProjectContentTranslation::className(), ['model_id' => 'id']);
    }

    public function getSingleImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_PROJECT_CONSTRUCTOR_BIG_IMAGE])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

    public function getTwoImages()
    {
        return $this->hasMany(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_PROJECT_CONSTRUCTOR_TWO_IMAGES])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC')
            ->limit(2);
    }

}
