<?php

namespace frontend\modules\projects\models;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\models\EntityToFile;

/**
 * This is the model class for table "{{%projects_styles_related}}".
 *
 * @property integer $id
 * @property integer $project_id
 * @property integer $project_style_id
 *
 * @property Projects $project
 * @property ProjectsStyles $projectStyle
 */
class ProjectsStylesRelated extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%projects_styles_related}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectStyle()
    {
        return $this->hasOne(ProjectsStyles::className(), ['id' => 'project_style_id']);
    }

}
