<?php

namespace frontend\modules\projects;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\projects\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
