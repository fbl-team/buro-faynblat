<?php
/**
 * Author: Pavel Naumenko
 */

namespace frontend\modules\projects\widgets\projectsFilters;

use frontend\modules\projects\models\ProjectCategory;
use frontend\modules\projects\models\ProjectsStyles;


/**
 * Class Widget
 * @package app\modules\career\widgets\vacanciesFilter
 */
class ProjectsFilters extends \yii\base\Widget
{
    public $selectedCategories = [];
    public $selectedStyles = [];

    /**
     * Executes the widget.
     * @return string the result of widget execution to be outputted.
     */
    public function run()
    {
        $categories = ProjectCategory::find()->orderBy('position ASC')->isPublished()->all();
        $styles = ProjectsStyles::find()->orderBy('position ASC')->isPublished()->all();

        return $this->render('default',
            [
                'categories' => $categories,
                'styles' => $styles,
                'selectedCategories' => $this->selectedCategories,
                'selectedStyles' => $this->selectedStyles,
            ]
        );
    }

    public function isActiveCategory($elementId)
    {
        if (empty($this->selectedCategories) && empty($this->selectedStyles) && $elementId == 'all') {
            return 'active';
        }
        return (in_array($elementId, $this->selectedCategories)) ? 'active' : '';
    }

    public function isActiveStyles($elementId)
    {
        return (in_array($elementId, $this->selectedStyles)) ? 'active' : '';
    }

}
