<?php

namespace frontend\modules\pages\models\allBlogs;

use Yii;

/**
* This is the model class for table "{{%page_all_blogs_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $description
*/
class PageAllBlogsTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_all_blogs_translation}}';
    }
}
