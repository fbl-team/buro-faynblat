<?php

namespace frontend\modules\pages\models\about;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\models\EntityToFile;
use frontend\modules\pages\models\home\PageMain;

/**
 * This is the model class for table "{{%page_about_about_our_work}}".
 *
 * @property integer $id
 * @property integer $page_about_id
 * @property string $label
 * @property string $description
 * @property string $label_under_description
 * @property integer $published
 * @property integer $position
 *
 * @property PageAbout $pageAbout
 * @property PageAboutAboutOurWorkTranslation[] $translations
 */
class PageAboutAboutOurWork extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;
    /**
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_about_about_our_work}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
            'label_under_description',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageAbout()
    {
        return $this->hasOne(PageAbout::className(), ['id' => 'page_about_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageAboutAboutOurWorkTranslation::className(), ['model_id' => 'id']);
    }

}
