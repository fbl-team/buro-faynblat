<?php

namespace frontend\modules\pages\models\about;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\models\EntityToFile;

/**
 * This is the model class for table "{{%page_about_work_principle}}".
 *
 * @property integer $id
 * @property integer $page_about_id
 * @property string $label
 * @property string $description
 * @property integer $published
 * @property integer $position
 *
 * @property PageAbout $pageAbout
 * @property PageAboutWorkPrincipleTranslation[] $translations
 */
class PageAboutWorkPrinciple extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_about_work_principle}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageAbout()
    {
        return $this->hasOne(PageAbout::className(), ['id' => 'page_about_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageAboutWorkPrincipleTranslation::className(), ['model_id' => 'id']);
    }

}
