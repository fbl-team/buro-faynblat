<?php

namespace frontend\modules\pages\models\about;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\models\EntityToFile;

/**
 * This is the model class for table "{{%page_about}}".
 *
 * @property integer $id
 * @property string $first_screen_label
 * @property string $first_screen_description
 * @property string $first_screen_image
 * @property string $short_about_block_label1
 * @property string $short_about_block_label2
 * @property string $short_about_block_description1
 * @property string $short_about_block_description2
 * @property string $work_block_label
 * @property string $work_block_description
 * @property string $about_our_work_label
 * @property string $our_diplom_block_label
 * @property string $our_diplom_block_button_label
 * @property string $canal_block_label
 * @property string $canal_block_button_label
 * @property string $projects_block_label
 * @property string $projects_block_description
 * @property string $projects_block_button_label
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property PageAboutAboutOurWork[] $pageAboutAboutOurWorks
 * @property PageAboutCanalDisain[] $pageAboutCanalDisains
 * @property PageAboutOurDiploms[] $pageAboutOurDiploms
 * @property PageAboutTranslation[] $translations
 * @property PageAboutWorkPrinciple[] $pageAboutWorkPrinciples
 */
class PageAbout extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_about}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'first_screen_label',
            'first_screen_description',
            'first_screen_image',
            'short_about_block_label1',
            'short_about_block_label2',
            'short_about_block_description1',
            'short_about_block_description2',
            'work_block_label',
            'work_block_description',
            'about_our_work_label',
            'our_diplom_block_label',
            'our_diplom_block_button_label',
            'canal_block_label',
            'canal_block_button_label',
            'projects_block_label',
            'projects_block_description',
            'projects_block_button_label',

            'footer_request_block_label',
            'footer_request_block_button_label',
            'footer_seo_block_label',
            'footer_seo_block_button_label',
            'footer_seo_block_text_left_column',
            'footer_seo_block_text_left_right',
            'footer_seo_block_button_label_hide',
            'footer_seo_block_button_label_hide',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageAboutTranslation::className(), ['model_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageAboutWorkPrinciples()
    {
        return $this->hasMany(PageAboutWorkPrinciple::className(), ['page_about_id' => 'id'])
            ->andWhere(['published' => true])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageAboutAboutOurWorks()
    {
        return $this->hasMany(PageAboutAboutOurWork::className(), ['page_about_id' => 'id'])
            ->andWhere(['published' => true])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageAboutOurDiploms()
    {
        return $this->hasMany(PageAboutOurDiploms::className(), ['page_about_id' => 'id'])
            ->andWhere(['published' => true])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageAboutCanalDisains()
    {
        return $this->hasMany(PageAboutCanalDisain::className(), ['page_about_id' => 'id'])
            ->andWhere(['published' => true])
            ->orderBy('position');
    }

    /**
     * @return $this
     */
    public function getFirstScreenImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_PAGE_ABOUT_FIRST_SCREEN_IMAGE])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

    /**
     * @return $this
     */
    public function getFirstScreenVideo()
    {
        return $this->hasMany(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_PAGE_ABOUT_FIRST_SCREEN_VIDEO])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }
}
