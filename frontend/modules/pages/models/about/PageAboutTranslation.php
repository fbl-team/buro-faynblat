<?php

namespace frontend\modules\pages\models\about;

use Yii;

/**
* This is the model class for table "{{%page_about_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $first_screen_label
* @property string $first_screen_description
* @property string $first_screen_image
* @property string $short_about_block_label1
* @property string $short_about_block_label2
* @property string $short_about_block_description1
* @property string $short_about_block_description2
* @property string $work_block_label
* @property string $work_block_description
* @property string $about_our_work_label
* @property string $our_diplom_block_label
* @property string $our_diplom_block_button_label
* @property string $canal_block_label
* @property string $canal_block_button_label
* @property string $projects_block_label
* @property string $projects_block_description
* @property string $projects_block_button_label
*/
class PageAboutTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_about_translation}}';
    }

}
