<?php

namespace frontend\modules\pages\models\about;

use Yii;

/**
* This is the model class for table "{{%page_about_canal_disain_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $description
*/
class PageAboutCanalDisainTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_about_canal_disain_translation}}';
    }
}
