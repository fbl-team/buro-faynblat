<?php

namespace frontend\modules\pages\models\allProjects;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\models\EntityToFile;
use Yii;

/**
 * This is the model class for table "{{%page_all_projects}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $description
 *
 * @property PageAllProjectsTranslation[] $translations
 */
class PageAllProjects extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_all_projects}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',

            'footer_request_block_label',
            'footer_request_block_button_label',
            'footer_seo_block_label',
            'footer_seo_block_button_label',
            'footer_seo_block_text_left_column',
            'footer_seo_block_text_left_right',
            'footer_seo_block_button_label_hide',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageAllProjectsTranslation::className(), ['model_id' => 'id']);
    }

}
