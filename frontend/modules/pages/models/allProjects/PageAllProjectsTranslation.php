<?php

namespace frontend\modules\pages\models\allProjects;

use Yii;

/**
* This is the model class for table "{{%page_all_projects_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $description
*/
class PageAllProjectsTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_all_projects_translation}}';
    }
}
