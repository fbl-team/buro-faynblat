<?php

namespace frontend\modules\pages\models\contacts;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\helpers\ContactsHelper;
use common\models\EntityToFile;

/**
 * This is the model class for table "{{%page_contacts}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $first_column_label
 * @property string $second_column_label
 * @property string $third_column_label
 * @property string $four_column_label
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property PageContactsSuperContent[] $pageContactsSuperContents
 * @property PageContactsTranslation[] $translations
 */
class PageContacts extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_contacts}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'first_column_label',
            'second_column_label',
            'third_column_label',
            'four_column_label',

            'footer_request_block_label',
            'footer_request_block_button_label',
            'footer_seo_block_label',
            'footer_seo_block_button_label',
            'footer_seo_block_text_left_column',
            'footer_seo_block_text_left_right',
            'footer_seo_block_button_label_hide',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageContactsTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageContactsElementsColumn1()
    {
        return $this->hasMany(PageContactsSuperContent::className(), ['page_contacts_id' => 'id'])
            ->andWhere(['column_type' => ContactsHelper::CONTACTS_COLUMN_1])
            ->andWhere(['published' => true])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageContactsElementsColumn2()
    {
        return $this->hasMany(PageContactsSuperContent::className(), ['page_contacts_id' => 'id'])
            ->andWhere(['column_type' => ContactsHelper::CONTACTS_COLUMN_2])
            ->andWhere(['published' => true])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageContactsElementsColumn3()
    {
        return $this->hasMany(PageContactsSuperContent::className(), ['page_contacts_id' => 'id'])
            ->andWhere(['column_type' => ContactsHelper::CONTACTS_COLUMN_3])
            ->andWhere(['published' => true])
            ->orderBy('position');
    }

}
