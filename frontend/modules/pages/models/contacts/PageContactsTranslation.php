<?php

namespace frontend\modules\pages\models\contacts;

use Yii;

/**
* This is the model class for table "{{%page_contacts_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $first_column_label
* @property string $second_column_label
* @property string $third_column_label
*/
class PageContactsTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_contacts_translation}}';
    }
}
