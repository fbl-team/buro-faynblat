<?php

namespace frontend\modules\pages\models\contacts;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\models\EntityToFile;
use Yii;

/**
 * This is the model class for table "{{%page_contacts_super_content}}".
 *
 * @property integer $id
 * @property integer $page_contacts_id
 * @property integer $content_type
 * @property string $content
 * @property integer $published
 * @property integer $position
 *
 * @property PageContacts $pageContacts
 * @property PageContactsSuperContentTranslation[] $translations
 */
class PageContactsSuperContent extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_contacts_super_content}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'content',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageContacts()
    {
        return $this->hasOne(PageContacts::className(), ['id' => 'page_contacts_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageContactsSuperContentTranslation::className(), ['model_id' => 'id']);
    }

}
