<?php

namespace frontend\modules\pages\models\oneBlog;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\models\EntityToFile;
use Yii;

/**
 * This is the model class for table "{{%page_one_blog}}".
 *
 * @property integer $id
 * @property string $breadcrums_label
 * @property string $breadcrums_url
 * @property string $another_blog_block_label
 * @property string $another_blog_block_published
 * @property string $another_blog_button_label
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property PageOneBlogTranslation[] $translations
 */
class PageOneBlog extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_one_blog}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['add_content_button_label', 'breadcrums_label', 'breadcrums_url', 'another_blog_block_label', 'another_blog_block_published', 'another_blog_button_label'], 'string'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/page_one_blog', 'ID'),
            'breadcrums_label' => Yii::t('back/page_one_blog', 'Breadcrums Label'),
            'breadcrums_url' => Yii::t('back/page_one_blog', 'Breadcrums Url'),
            'add_content_button_label' => Yii::t('back/page_one_blog', 'Add More Content Button Label'),
            'another_blog_block_label' => Yii::t('back/page_one_blog', 'Another Blog Block Label'),
            'another_blog_block_published' => Yii::t('back/page_one_blog', 'Another Blog Block Published'),
            'another_blog_button_label' => Yii::t('back/page_one_blog', 'Another Blog Button Label'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'breadcrums_label',
            'breadcrums_url',
            'another_blog_block_label',
            'another_blog_block_published',
            'another_blog_button_label',
            'add_content_button_label',
            'footer_request_block_label',
            'footer_request_block_button_label',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageOneBlogTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return Yii::t('app', 'Page One Blog');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    // 'breadcrums_label:ntext',
                    'breadcrums_url:url',
                    // 'another_blog_block_label:ntext',
                    'another_blog_block_published:boolean',
                    // 'another_blog_button_label:ntext',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    [
                        'attribute' => 'breadcrums_label',
                        'format' => 'html',
                    ],
                    'breadcrums_url:url',
                    [
                        'attribute' => 'another_blog_block_label',
                        'format' => 'html',
                    ],
                    'another_blog_block_published:boolean',
                    [
                        'attribute' => 'another_blog_button_label',
                        'format' => 'html',
                    ],
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new PageOneBlogSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
            'breadcrums_label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT
            ],
            'breadcrums_url' => [
                'type' => ActiveFormBuilder::INPUT_TEXT
            ],
            'add_content_button_label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT
            ],
            'another_blog_block_label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT
            ],
            'another_blog_block_published' => [
                'type' => ActiveFormBuilder::INPUT_TEXT
            ],
            'another_blog_button_label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT
            ],
            
        ];
    }

}
