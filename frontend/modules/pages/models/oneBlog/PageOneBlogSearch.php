<?php

namespace backend\modules\pages\models\oneBlog;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PageOneBlogSearch represents the model behind the search form about `PageOneBlog`.
 */
class PageOneBlogSearch extends PageOneBlog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['breadcrums_label', 'breadcrums_url', 'another_project_block_label', 'another_project_block_published', 'another_project_button_label'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PageOneBlog::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'breadcrums_label', $this->breadcrums_label])
            ->andFilterWhere(['like', 'breadcrums_url', $this->breadcrums_url])
            ->andFilterWhere(['like', 'another_project_block_label', $this->another_project_block_label])
            ->andFilterWhere(['like', 'another_project_block_published', $this->another_project_block_published])
            ->andFilterWhere(['like', 'another_project_button_label', $this->another_project_button_label]);

        return $dataProvider;
    }
}
