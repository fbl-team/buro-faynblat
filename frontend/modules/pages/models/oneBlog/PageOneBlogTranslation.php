<?php

namespace frontend\modules\pages\models\oneBlog;

use Yii;

/**
* This is the model class for table "{{%page_one_project_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $breadcrums_label
* @property string $breadcrums_url
* @property string $another_project_block_label
* @property string $another_project_block_published
* @property string $another_project_button_label
*/
class PageOneBlogTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_one_project_translation}}';
    }
}
