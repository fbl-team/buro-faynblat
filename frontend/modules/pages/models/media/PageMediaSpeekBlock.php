<?php

namespace frontend\modules\pages\models\media;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\models\EntityToFile;

/**
 * This is the model class for table "{{%page_media_speek_block}}".
 *
 * @property integer $id
 * @property integer $page_media_id
 * @property string $label
 * @property integer $published
 * @property integer $position
 *
 * @property PageMedia $pageMedia
 * @property PageMediaSpeekBlockTranslation[] $translations
 */
class PageMediaSpeekBlock extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_media_speek_block}}';
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageMedia()
    {
        return $this->hasOne(PageMedia::className(), ['id' => 'page_media_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageMediaSpeekBlockTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return $this
     */
    public function getBlockElementImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_PAGE_MEDIA_SPEEK_BLOCK_IMAGE])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

}
