<?php

namespace frontend\modules\pages\models\media;

use Yii;

/**
* This is the model class for table "{{%page_media_write_block_translation}}".
*
* @property integer $model_id
* @property string $language
*/
class PageMediaWriteBlockTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_media_write_block_translation}}';
    }

}
