<?php

namespace frontend\modules\pages\models\media;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\models\EntityToFile;

/**
 * This is the model class for table "{{%page_media}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $write_block_label
 * @property string $write_block_button_label
 * @property string $speak_block_label
 * @property string $speak_block_button_label
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property PageMediaSpeekBlock[] $pageMediaSpeekBlocks
 * @property PageMediaTranslation[] $translations
 * @property PageMediaWriteBlock[] $pageMediaWriteBlocks
 */
class PageMedia extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_media}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'write_block_label',
            'write_block_button_label',
            'speak_block_label',
            'speak_block_button_label',

            'footer_request_block_label',
            'footer_request_block_button_label',
            'footer_seo_block_label',
            'footer_seo_block_button_label',
            'footer_seo_block_text_left_column',
            'footer_seo_block_text_left_right',
            'footer_seo_block_button_label_hide',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageMediaTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpeekBlocks()
    {
        return $this->hasMany(PageMediaSpeekBlock::className(), ['page_media_id' => 'id'])
            ->andWhere(['published' => true])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWriteBlocks()
    {
        return $this->hasMany(PageMediaWriteBlock::className(), ['page_media_id' => 'id'])
            ->andWhere(['published' => true])
            ->orderBy('position');
    }

}
