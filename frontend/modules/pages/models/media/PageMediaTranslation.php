<?php

namespace frontend\modules\pages\models\media;

use Yii;

/**
* This is the model class for table "{{%page_media_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $write_block_label
* @property string $write_block_button_label
* @property string $speak_block_label
* @property string $speak_block_button_label
*/
class PageMediaTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_media_translation}}';
    }
}
