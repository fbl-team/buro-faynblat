<?php

namespace frontend\modules\pages\models\services;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\models\EntityToFile;

/**
 * This is the model class for table "{{%page_services}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $description
 * @property string $what_we_do_block_label
 * @property string $what_we_do_block_description
 * @property string $how_we_do_that_block_label
 * @property string $how_we_do_that_block_description
 * @property string $project_block_label
 * @property string $project_block_description
 * @property string $project_block_button_label
 *
 * @property PageServicesHowWeDoBlock[] $pageServicesHowWeDoBlocks
 * @property PageServicesTranslation[] $translations
 * @property PageServicesWhatWeDoBlock[] $pageServicesWhatWeDoBlocks
 */
class PageServices extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_services}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
            'what_we_do_block_label',
            'what_we_do_block_description',
            'how_we_do_that_block_label',
            'how_we_do_that_block_description',
            'project_block_label',
            'project_block_description',
            'project_block_button_label',

            'footer_request_block_label',
            'footer_request_block_button_label',
            'footer_seo_block_label',
            'footer_seo_block_button_label',
            'footer_seo_block_text_left_column',
            'footer_seo_block_text_left_right',
            'footer_seo_block_button_label_hide',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageServicesHowWeDoBlocks()
    {
        return $this->hasMany(PageServicesHowWeDoBlock::className(), ['page_services_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageServicesTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageServicesWhatWeDoBlocks()
    {
        return $this->hasMany(PageServicesWhatWeDoBlock::className(), ['page_services_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWhatWeDoBlock()
    {
        return $this->hasMany(PageServicesWhatWeDoBlock::className(), ['page_services_id' => 'id'])
            ->andWhere(['published' => true])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHowWeDoBlock()
    {
        return $this->hasMany(PageServicesHowWeDoBlock::className(), ['page_services_id' => 'id'])
            ->andWhere(['published' => true])
            ->orderBy('position');
    }

    /**
     * @return $this
     */
    public function getFirstScreenImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_SERVICES_FIRST_SCREEN_IMAGE])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

    /**
     * @return $this
     */
    public function getFirstScreenVideos()
    {
        return $this->hasMany(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_SERVICES_FIRST_SCREEN_VIDEO])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

}
