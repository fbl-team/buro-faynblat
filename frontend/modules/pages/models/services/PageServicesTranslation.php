<?php

namespace frontend\modules\pages\models\services;

use Yii;

/**
* This is the model class for table "{{%page_services_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $description
* @property string $what_we_do_block_label
* @property string $what_we_do_block_description
* @property string $how_we_do_that_block_label
* @property string $how_we_do_that_block_description
* @property string $project_block_label
* @property string $project_block_description
* @property string $project_block_button_label
*/
class PageServicesTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_services_translation}}';
    }

}
