<?php

namespace frontend\modules\pages\models\services;

use Yii;

/**
* This is the model class for table "{{%page_services_what_we_do_block_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $description
*/
class PageServicesWhatWeDoBlockTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_services_what_we_do_block_translation}}';
    }

}
