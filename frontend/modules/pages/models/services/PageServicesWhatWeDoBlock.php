<?php

namespace frontend\modules\pages\models\services;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\models\EntityToFile;

/**
 * This is the model class for table "{{%page_services_what_we_do_block}}".
 *
 * @property integer $id
 * @property integer $page_services_id
 * @property string $label
 * @property string $description
 * @property integer $published
 * @property integer $position
 *
 * @property PageServices $pageServices
 * @property PageServicesWhatWeDoBlockTranslation[] $translations
 */
class PageServicesWhatWeDoBlock extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_services_what_we_do_block}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageServices()
    {
        return $this->hasOne(PageServices::className(), ['id' => 'page_services_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageServicesWhatWeDoBlockTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return $this
     */
    public function getElementImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_SERVICES_WHAT_WE_DO_IMAGE])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

    /**
     * @return $this
     */
    public function getElementVideos()
    {
        return $this->hasMany(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_SERVICES_WHAT_WE_DO_VIDEO])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

}
