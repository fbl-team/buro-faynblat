<?php

namespace frontend\modules\pages\models\home;

use Yii;

/**
* This is the model class for table "{{%page_main_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $first_screen_label
* @property string $filosophy_block_label
* @property string $filosophy_block_short_description
* @property string $filosophy_block_full_description
* @property string $filosophy_block_button_label
* @property string $project_block_label
* @property string $project_block_short_description
* @property string $project_block_button_label
*/
class PageMainTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_main_translation}}';
    }

}
