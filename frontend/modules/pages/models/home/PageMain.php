<?php

namespace frontend\modules\pages\models\home;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\models\EntityToFile;

/**
 * This is the model class for table "{{%page_main}}".
 *
 * @property integer $id
 * @property string $first_screen_label
 * @property string $filosophy_block_label
 * @property string $filosophy_block_short_description
 * @property string $filosophy_block_full_description
 * @property string $filosophy_block_button_label
 * @property string $project_block_label
 * @property string $project_block_short_description
 * @property string $project_block_button_label
 *
 * @property PageMainTranslation[] $translations
 */
class PageMain extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_main}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'first_screen_label',
            'filosophy_block_label',
            'filosophy_block_short_description',
            'filosophy_block_full_description',
            'filosophy_block_button_label',
            'project_block_label',
            'project_block_short_description',
            'project_block_button_label',
            'blog_block_label',
            'blog_block_short_description',
            'blog_block_button_label',
            'first_screen_under_play_button_text',

            'footer_request_block_label',
            'footer_request_block_button_label',
            'footer_seo_block_label',
            'footer_seo_block_button_label',
            'footer_seo_block_text_left_column',
            'footer_seo_block_text_left_right',
            'footer_seo_block_button_label_hide',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageMainTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return $this
     */
    public function getFilosophyBlockImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_PAGE_HOME_FILOSOPHY_BLOCK_IMAGE])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

    /**
     * @return $this
     */
    public function getFirstScreenVideos()
    {
        return $this->hasMany(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_PAGE_HOME_FIRST_SCREEN_VIDEO])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

    /**
     * @return $this
     */
    public function getFirstScreenImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_PAGE_HOME_FIRST_SCREEN_IMAGE])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }
}
