<?php

namespace frontend\modules\pages\models\careers;

use Yii;

class PageCareersTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_careers_translation}}';
    }
}
