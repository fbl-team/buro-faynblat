<?php

namespace frontend\modules\pages\models\careers;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;


class PageCareers extends \common\components\model\ActiveRecord implements Translateable
{
    use TranslateableTrait;
    use Translate;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_careers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description','label'], 'string']
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'description',
            'label',
            'footer_request_block_label',
            'footer_request_block_button_label',
            'footer_seo_block_label',
            'footer_seo_block_button_label',
            'footer_seo_block_text_left_column',
            'footer_seo_block_text_left_right',
            'footer_seo_block_button_label_hide'
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageCareersTranslation::className(), ['model_id' => 'id']);
    }
}
