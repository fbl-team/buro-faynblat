<?php

namespace frontend\modules\menus;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'frontend\modules\menus\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
