<?php

namespace frontend\modules\menus\models;

use Yii;

/**
* This is the model class for table "{{%menus_element_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
*/
class MenusTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%menus_element_translation}}';
    }
}
