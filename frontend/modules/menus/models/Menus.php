<?php

namespace frontend\modules\menus\models;

use common\components\model\Translateable;
use common\components\Translate;
use common\components\TranslateableTrait;
use common\models\EntityToFile;
use Yii;

/**
 * This is the model class for table "{{%menus_element}}".
 *
 * @property integer $id
 * @property integer $type_id
 * @property string $label
 * @property string $url
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property MenusElementTranslation[] $menusElementTranslations
 */
class Menus extends \common\components\model\ActiveRecord implements  Translateable
{
    use TranslateableTrait;
    use Translate;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%menus_element}}';
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(MenusTranslation::className(), ['model_id' => 'id']);
    }
}
