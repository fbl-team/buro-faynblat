<?php

use console\components\Migration;

/**
 * Class m161119_074932_create_tbl_for_page_about migration
 */
class m161119_074932_create_tbl_for_page_about extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%page_about}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'first_screen_label' => $this->text()->defaultValue(null)->comment('Label'),
                'first_screen_description' => $this->text()->defaultValue(null),
                'first_screen_image' => $this->text()->defaultValue(null),

                'short_about_block_label1' => $this->text()->defaultValue(null),
                'short_about_block_label2' => $this->text()->defaultValue(null),

                'short_about_block_description1' => $this->text()->defaultValue(null),
                'short_about_block_description2' => $this->text()->defaultValue(null),

                'work_block_label' => $this->text()->defaultValue(null),
                'work_block_description' => $this->text()->defaultValue(null),

                'about_our_work_label' => $this->text()->defaultValue(null),

                'our_diplom_block_label' => $this->text()->defaultValue(null),
                'our_diplom_block_button_label' => $this->text()->defaultValue(null),

                'canal_block_label' => $this->text()->defaultValue(null),
                'canal_block_button_label' => $this->text()->defaultValue(null),

                'projects_block_label' => $this->text()->defaultValue(null),
                'projects_block_description' => $this->text()->defaultValue(null),
                'projects_block_button_label' => $this->text()->defaultValue(null),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->insert(
            $this->tableName,
            [
                'first_screen_label' => 'ДАВАЙТЕ ЗНАКОМИТЬСЯ',
                'created_at' => time(),
                'updated_at' => time(),
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
