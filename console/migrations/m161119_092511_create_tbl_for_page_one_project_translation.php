<?php

use console\components\Migration;

/**
 * Class m161119_092511_create_tbl_for_page_one_project_translation migration
 */
class m161119_092511_create_tbl_for_page_one_project_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%page_one_project_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%page_one_project}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'breadcrums_label' => $this->text()->defaultValue(null),
                'breadcrums_url' => $this->text()->defaultValue(null),

                'another_project_block_label' => $this->text()->defaultValue(null),
                'another_project_block_published' => $this->text()->defaultValue(null),
                'another_project_button_label' => $this->text()->defaultValue(null),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-page_one_project_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-page_one_project_translation-model_id-page_one_project-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

