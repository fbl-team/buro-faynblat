<?php

use console\components\Migration;

/**
 * Class m161118_062608_create_tbl_for_page_services migration
 */
class m161118_062608_create_tbl_for_page_services extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%page_services}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'label' => $this->text()->defaultValue(null),
                'description' => $this->text()->defaultValue(null),

                'what_we_do_block_label' => $this->text()->defaultValue(null),
                'what_we_do_block_description' => $this->text()->defaultValue(null),

                'how_we_do_that_block_label' => $this->text()->defaultValue(null),
                'how_we_do_that_block_description' => $this->text()->defaultValue(null),

                'project_block_label' => $this->text()->defaultValue(null),
                'project_block_description' => $this->text()->defaultValue(null),
                'project_block_button_label' => $this->text()->defaultValue(null),
            ],
            $this->tableOptions
        );

        $this->insert(
            $this->tableName,
            [
                'label' => 'УСЛУГИ'
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
