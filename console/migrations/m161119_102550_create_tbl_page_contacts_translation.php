<?php

use console\components\Migration;

/**
 * Class m161119_102550_create_tbl_page_contacts_translation migration
 */
class m161119_102550_create_tbl_page_contacts_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%page_contacts_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%page_contacts}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->string()->notNull()->comment('Label'),
                'first_column_label' => $this->text()->defaultValue(null)->comment('Content'),
                'second_column_label' => $this->text()->defaultValue(null)->comment('Content'),
                'third_column_label' => $this->text()->defaultValue(null)->comment('Content'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-page_contacts_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-page_contacts_translation-model_id-page_contacts-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

