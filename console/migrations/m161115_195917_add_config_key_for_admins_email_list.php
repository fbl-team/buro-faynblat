<?php

use console\components\Migration;

/**
 * Class m160315_114931_add_config_key_for_admins_email_list migration
 */
class m161115_195917_add_config_key_for_admins_email_list extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%configuration}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $var = 'louse2007@ukr.net';

        $this->insert(
            $this->tableName,
            [
                'id' => 'Email: admins email list',
                'value' => $var,
                'description' => "Список E-mail адресов: параметр - строка, где через запятую необходимо ввести e-mail адреса на которые будет приходить уведомление о отправленных заявках",
                'type' => 0,
                'created_at' => time(),
                'updated_at' => time(),
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->tableName, 'id = :val',array(':val'=>'Email: admins email list'));
    }
}
