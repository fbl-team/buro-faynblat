<?php

use console\components\Migration;

/**
 * Class m161117_092246_create_tbl_for_projects migration
 */
class m161117_092246_create_tbl_for_projects extends Migration
{
    /**
     * migration table name
     */
    public $tableNameRelated = '{{%project_category}}';

    public $tableName = '{{%projects}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'label' => $this->text()->defaultValue(null),
                'city_and_square' => $this->text()->defaultValue(null),

                'category_id' => $this->integer()->defaultValue(null),

                'show_on_main' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
                'show_on_about' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
                'show_on_services' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),

                'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk_from_projects_to_category_id',
            $this->tableName,
            'category_id',
            $this->tableNameRelated,
            'id',
            'SET NULL', 'NO ACTION'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
