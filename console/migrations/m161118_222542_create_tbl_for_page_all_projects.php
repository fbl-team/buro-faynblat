<?php

use console\components\Migration;

/**
 * Class m161118_222542_create_tbl_for_page_all_projects migration
 */
class m161118_222542_create_tbl_for_page_all_projects extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%page_all_projects}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'label' => $this->text()->defaultValue(null),
                'description' => $this->text()->defaultValue(null),
            ],
            $this->tableOptions
        );

        $this->insert(
            $this->tableName,
            [
                'label' => 'ПРОЕКТЫ',
                'description' => 'ВДОХНОВЛЯЮЩИЕ ИНТЕРЬЕРЫ ДЛЯ КОМФОРТНОЙ ЖИЗНИ'
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
