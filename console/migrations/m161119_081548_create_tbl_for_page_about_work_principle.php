<?php

use console\components\Migration;

/**
 * Class m161119_081548_create_tbl_for_page_about_work_principle migration
 */
class m161119_081548_create_tbl_for_page_about_work_principle extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%page_about_work_principle}}';

    public $tableNameRelated = '{{%page_about}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'page_about_id' => $this->integer()->notNull(),

                'label' => $this->text()->defaultValue(null),
                'description' => $this->text()->defaultValue(null),

                'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),

            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk_from_work_principle_to_page_about',
            $this->tableName,
            'page_about_id',
            $this->tableNameRelated,
            'id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
