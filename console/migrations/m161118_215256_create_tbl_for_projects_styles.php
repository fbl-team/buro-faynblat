<?php

use console\components\Migration;

/**
 * Class m161118_215256_create_tbl_for_projects_styles migration
 */
class m161118_215256_create_tbl_for_projects_styles extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%projects_styles}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'label' => $this->string()->notNull()->comment('Label'),

                'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
