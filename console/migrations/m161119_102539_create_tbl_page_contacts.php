<?php

use console\components\Migration;

/**
 * Class m161119_102539_create_tbl_page_contacts migration
 */
class m161119_102539_create_tbl_page_contacts extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%page_contacts}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'label' => $this->string()->notNull()->comment('Label'),
                'first_column_label' => $this->text()->defaultValue(null),
                'second_column_label' => $this->text()->defaultValue(null),
                'third_column_label' => $this->text()->defaultValue(null),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->insert(
            $this->tableName,
            [
                'label' => 'СВЯЗАТЬСЯ С НАМИ',
                'created_at' => time(),
                'updated_at' => time(),
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
