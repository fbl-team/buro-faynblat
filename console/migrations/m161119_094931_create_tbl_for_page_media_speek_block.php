<?php

use console\components\Migration;

/**
 * Class m161119_094931_create_tbl_for_page_media_speek_block migration
 */
class m161119_094931_create_tbl_for_page_media_speek_block extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%page_media_speek_block}}';

    public $tableNameRelated = '{{%page_media}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'page_media_id' => $this->integer()->notNull(),

                'label' => $this->text()->defaultValue(null),

                'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),

            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk_from_media_speeck_block_to_page_about',
            $this->tableName,
            'page_media_id',
            $this->tableNameRelated,
            'id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
