<?php

use console\components\Migration;

/**
 * Class m161128_144406_add_field_to_all_static_pages_translation migration
 */
class m161128_144406_add_field_to_all_static_pages_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableArray = [
        '{{%page_main_translation}}',
        '{{%page_all_projects_translation}}',
        '{{%projects_translation}}',
        '{{%page_about_translation}}',
        '{{%page_services_translation}}',
        '{{%content_pages_translation}}',
        '{{%page_media_translation}}',
        '{{%page_contacts_translation}}',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->tableArray as $tableName) {

            $this->addColumn(
                $tableName,
                'footer_seo_block_button_label_hide',
                $this->text()->defaultValue(null)
            );

        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->tableArray as $tableName) {

            $this->dropColumn(
                $tableName,
                'footer_seo_block_button_label_hide'
            );

        }
    }
}

