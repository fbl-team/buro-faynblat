<?php

use console\components\Migration;

/**
 * Class m161127_120208_change_pojects_tbl_column_type migration
 */
class m161127_120208_change_pojects_tbl_column_type extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%projects}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->alterColumn(
            $this->tableName,
            'show_on_main',
            $this->smallInteger(1)->unsigned()->notNull()->defaultValue(0)
        );

        $this->alterColumn(
            $this->tableName,
            'show_on_about',
            $this->smallInteger(1)->unsigned()->notNull()->defaultValue(0)
        );

        $this->alterColumn(
            $this->tableName,
            'show_on_services',
            $this->smallInteger(1)->unsigned()->notNull()->defaultValue(0)
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->alterColumn(
            $this->tableName,
            'show_on_main',
            $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)
        );

        $this->alterColumn(
            $this->tableName,
            'show_on_about',
            $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)
        );

        $this->alterColumn(
            $this->tableName,
            'show_on_services',
            $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)
        );
    }
}
