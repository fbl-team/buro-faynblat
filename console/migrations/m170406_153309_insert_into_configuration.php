<?php
use common\models\Configuration;
use console\components\Migration;

/**
 * Class m170406_153309_insert_into_configuration migration
 */
class m170406_153309_insert_into_configuration extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%configuration}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $time = time();

        $this->insert($this->tableName, [
            'id'            => 'logo_title',
            'value'         => 'Faynblat logo',
            'type'          => Configuration::TYPE_STRING,
            'description'   => 'SEO title for site logo',
            'created_at'    => $time,
            'updated_at'    => $time
        ]);
        $this->insert($this->tableName, [
            'id'            => 'logo_description',
            'value'         => 'Faynblat',
            'type'          => Configuration::TYPE_STRING,
            'description'   => 'SEO description for site logo',
            'created_at'    => $time,
            'updated_at'    => $time
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 'logo_title']);
        $this->delete($this->tableName, ['id' => 'logo_description']);
    }
}
