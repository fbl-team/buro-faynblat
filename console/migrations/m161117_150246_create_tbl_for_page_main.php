<?php

use console\components\Migration;

/**
 * Class m161117_150246_create_tbl_for_page_main migration
 */
class m161117_150246_create_tbl_for_page_main extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%page_main}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'first_screen_label' => $this->text()->defaultValue(null),
                'first_screen_vimeo_video' => $this->text()->defaultValue(null),
                'first_screen_under_play_button_text' => $this->text()->defaultValue(null),

                'filosophy_block_label' => $this->text()->defaultValue(null),
                'filosophy_block_short_description' => $this->text()->defaultValue(null),
                'filosophy_block_full_description' => $this->text()->defaultValue(null),
                'filosophy_block_button_label' => $this->text()->defaultValue(null),

                'project_block_label' => $this->text()->defaultValue(null),
                'project_block_short_description' => $this->text()->defaultValue(null),
                'project_block_button_label' => $this->text()->defaultValue(null),
            ],
            $this->tableOptions
        );

        $this->insert(
            $this->tableName,
            [
                'first_screen_label' => 'С НАМИ ВАШИ МЕЧТЫ ПРЕВРАЩАЮТСЯ В ПРОСТРАНСТВО ДЛЯ ЖИЗНИ'
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
