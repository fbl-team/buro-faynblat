<?php

use console\components\Migration;

/**
 * Class m161115_195916_create_tbl_for_request migration
 */
class m161115_195916_create_tbl_for_request extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%request}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'type_id' => $this->integer()->defaultValue(1),

                'name' => $this->text()->defaultValue(null),
                'phone' => $this->text()->defaultValue(null),
                'email' => $this->text()->defaultValue(null),

                'published' => $this->smallInteger(0)->unsigned()->notNull()->defaultValue(1)->comment('Published'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
