<?php

use console\components\Migration;

/**
 * Class m161119_094453_create_tbl_for_page_madia migration
 */
class m161119_094453_create_tbl_for_page_media extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%page_media}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'label' => $this->text()->defaultValue(null),
                'write_block_label' => $this->text()->defaultValue(null),
                'write_block_button_label' => $this->text()->defaultValue(null),
                'speak_block_label' => $this->text()->defaultValue(null),
                'speak_block_button_label' => $this->text()->defaultValue(null),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->insert(
            $this->tableName,
            [
                'label' => 'МЕДИА',
                'created_at' => time(),
                'updated_at' => time(),
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
