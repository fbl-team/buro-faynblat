<?php

use console\components\Migration;

/**
 * Class m161117_092246_create_tbl_for_projects migration
 */
class m161117_092240_create_tbl_for_project_category extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%project_category}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'label' => $this->text()->defaultValue(null),

                'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
