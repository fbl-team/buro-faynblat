<?php

use console\components\Migration;

/**
 * Class m161125_113001_add_field_to_one_projects migration
 */
class m161125_113001_add_field_to_one_projects extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%page_one_project}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->tableName,
            'add_content_button_label',
            $this->text()->defaultValue(null)
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'add_content_button_label');
    }
}
