<?php

use yii\db\Schema;
use console\components\Migration;

/**
 * Class m150218_110439_create_configuration_table migration
 */
class m150218_110439_create_configuration_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%configuration}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->char(100)->comment('Key'),
                'value' => $this->text()->defaultValue(null)->comment('Value'),
                'type' => $this->integer()->notNull()->comment('Field type'),
                'description' => $this->string()->defaultValue(null)->comment('Description'),
                'preload' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Preload'),
                'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
                'created_at' => $this->integer()->notNull()->comment('Created at'),
                'updated_at' => $this->integer()->notNull()->comment('Updated at'),
            ],
            $this->tableOptions
        );

        $this->addPrimaryKey('configuration_primary_key', $this->tableName, ['id']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
