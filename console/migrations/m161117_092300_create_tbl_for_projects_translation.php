<?php

use console\components\Migration;

/**
 * Class m161117_092300_create_tbl_for_projects_translation migration
 */
class m161117_092300_create_tbl_for_projects_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%projects_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%projects}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->text()->defaultValue(null),
                'city_and_square' => $this->text()->defaultValue(null),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-projects_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-projects_translation-model_id-projects-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

