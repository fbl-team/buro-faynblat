<?php

use console\components\Migration;

/**
 * Class m161118_064543_create_tbl_for_page_services_how_we_do_block migration
 */
class m161118_064543_create_tbl_for_page_services_how_we_do_block extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%page_services_how_we_do_block}}';

    public $tableNameRelated = '{{%page_services}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'page_services_id' => $this->integer()->notNull(),

                'label' => $this->text()->defaultValue(null),
                'description' => $this->text()->defaultValue(null),

                'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk_how_we_do_block_to_page_services',
            $this->tableName,
            'page_services_id',
            $this->tableNameRelated,
            'id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
