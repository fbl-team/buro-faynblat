<?php

use console\components\Migration;

/**
 * Class m161119_081600_create_tbl_for_page_about_work_principle_translation migration
 */
class m161119_081600_create_tbl_for_page_about_work_principle_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%page_about_work_principle_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%page_about_work_principle}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->text()->defaultValue(null),
                'description' => $this->text()->defaultValue(null),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-p_a_w_p_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-p_a_w_p_translation-model_id-p_a_w_p-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

