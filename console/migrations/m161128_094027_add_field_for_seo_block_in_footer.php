<?php

use console\components\Migration;

/**
 * Class m161128_094027_add_field_for_seo_block_in_footer migration
 */
class m161128_094027_add_field_for_seo_block_in_footer extends Migration
{
    /**
     * migration table name
     */
    public $tableArray = [
        '{{%page_main}}',
        '{{%page_all_projects}}',
        '{{%projects}}',
        '{{%page_about}}',
        '{{%page_services}}',
        '{{%content_pages}}',
        '{{%page_media}}',
        '{{%page_contacts}}',
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->tableArray as $tableName) {

            $this->addColumn(
                $tableName,
                'footer_request_block_label',
                $this->text()->defaultValue(null)
            );

            $this->addColumn(
                $tableName,
                'footer_request_block_button_label',
                $this->text()->defaultValue(null)
            );

            $this->addColumn(
                $tableName,
                'footer_seo_block_label',
                $this->text()->defaultValue(null)
            );

            $this->addColumn(
                $tableName,
                'footer_seo_block_button_label',
                $this->text()->defaultValue(null)
            );

            $this->addColumn(
                $tableName,
                'footer_seo_block_text_left_column',
                $this->text()->defaultValue(null)
            );

            $this->addColumn(
                $tableName,
                'footer_seo_block_text_left_right',
                $this->text()->defaultValue(null)
            );

        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->tableArray as $tableName) {

            $this->dropColumn(
                $tableName,
                'footer_request_block_label'
            );

            $this->dropColumn(
                $tableName,
                'footer_request_block_button_label'
            );

            $this->dropColumn(
                $tableName,
                'footer_seo_block_label'
            );

            $this->dropColumn(
                $tableName,
                'footer_seo_block_button_label'
            );

            $this->dropColumn(
                $tableName,
                'footer_seo_block_text_left_column'
            );

            $this->dropColumn(
                $tableName,
                'footer_seo_block_text_left_right'
            );

        }
    }
}
