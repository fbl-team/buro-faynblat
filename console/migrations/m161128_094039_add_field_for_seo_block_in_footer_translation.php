<?php

use console\components\Migration;

/**
 * Class m161128_094039_add_field_for_seo_block_in_footer_translation migration
 */
class m161128_094039_add_field_for_seo_block_in_footer_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableArray = [
        '{{%page_main_translation}}',
        '{{%page_all_projects_translation}}',
        '{{%projects_translation}}',
        '{{%page_about_translation}}',
        '{{%page_services_translation}}',
        '{{%content_pages_translation}}',
        '{{%page_media_translation}}',
        '{{%page_contacts_translation}}',
    ];

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->tableArray as $tableName) {

            $this->addColumn(
                $tableName,
                'footer_request_block_label',
                $this->text()->defaultValue(null)
            );

            $this->addColumn(
                $tableName,
                'footer_request_block_button_label',
                $this->text()->defaultValue(null)
            );

            $this->addColumn(
                $tableName,
                'footer_seo_block_label',
                $this->text()->defaultValue(null)
            );

            $this->addColumn(
                $tableName,
                'footer_seo_block_button_label',
                $this->text()->defaultValue(null)
            );

            $this->addColumn(
                $tableName,
                'footer_seo_block_text_left_column',
                $this->text()->defaultValue(null)
            );

            $this->addColumn(
                $tableName,
                'footer_seo_block_text_left_right',
                $this->text()->defaultValue(null)
            );

        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->tableArray as $tableName) {

            $this->dropColumn(
                $tableName,
                'footer_request_block_label'
            );

            $this->dropColumn(
                $tableName,
                'footer_request_block_button_label'
            );

            $this->dropColumn(
                $tableName,
                'footer_seo_block_label'
            );

            $this->dropColumn(
                $tableName,
                'footer_seo_block_button_label'
            );

            $this->dropColumn(
                $tableName,
                'footer_seo_block_text_left_column'
            );

            $this->dropColumn(
                $tableName,
                'footer_seo_block_text_left_right'
            );

        }
    }
}

