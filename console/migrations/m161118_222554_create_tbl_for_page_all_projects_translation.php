<?php

use console\components\Migration;

/**
 * Class m161118_222554_create_tbl_for_page_all_projects_translation migration
 */
class m161118_222554_create_tbl_for_page_all_projects_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%page_all_projects_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%page_all_projects}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->text()->defaultValue(null),
                'description' => $this->text()->defaultValue(null),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-all_projects_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-all_projects_translation-model_id-all_projects-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

