<?php

use common\helpers\ContentPagesHelper;
use console\components\Migration;

/**
 * Class m161116_081933_create_tbl_for_content_pages migration
 */
class m161116_081933_create_tbl_for_content_pages extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%content_pages}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'alias' => $this->text()->defaultValue(null),

                'breadcrums_label' => $this->text()->defaultValue(null),
                'breadcrums_url' => $this->text()->defaultValue(null),

                'first_screen_label' => $this->text()->defaultValue(null),
                'first_screen_button_label' => $this->text()->defaultValue(null),

                'second_screen_label1' => $this->text()->defaultValue(null),
                'second_screen_label2' => $this->text()->defaultValue(null),

                'second_screen_description1' => $this->text()->defaultValue(null),
                'second_screen_description2' => $this->text()->defaultValue(null),

                'etap_block_label' => $this->text()->defaultValue(null),

                'inside_service_block_label' => $this->text()->defaultValue(null),
                'inside_service_block_description' => $this->text()->defaultValue(null),

                'etap_second_block_label' => $this->text()->defaultValue(null),

                'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->batchInsert(
            $this->tableName,
            [
                'alias', 'created_at', 'updated_at'
            ],
            [
                [ContentPagesHelper::ELEMENT_DESIGN, time(), time()],
                [ContentPagesHelper::ELEMENT_BUILDINGS, time(), time()],
                [ContentPagesHelper::ELEMENT_LANDSCAPING, time(), time()],
                [ContentPagesHelper::ELEMENT_ARCHITECTURE, time(), time()],
                [ContentPagesHelper::ELEMENT_PLANNING, time(), time()],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
