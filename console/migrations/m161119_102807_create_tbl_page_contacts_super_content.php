<?php

use console\components\Migration;

/**
 * Class m161119_102807_create_tbl_page_contacts_super_content migration
 */
class m161119_102807_create_tbl_page_contacts_super_content extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%page_contacts_super_content}}';

    public $tableNameRelated = '{{%page_contacts}}';


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'column_type' => $this->integer()->notNull(),
                'page_contacts_id' => $this->integer()->notNull(),

                'content_type' => $this->integer()->notNull(),
                'content' => $this->text()->defaultValue(null),

                'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk_from_contacts_content_to_page_contacts',
            $this->tableName,
            'page_contacts_id',
            $this->tableNameRelated,
            'id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
