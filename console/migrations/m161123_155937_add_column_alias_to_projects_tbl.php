<?php

use console\components\Migration;

/**
 * Class m161123_155937_add_column_alias_to_projects_tbl migration
 */
class m161123_155937_add_column_alias_to_projects_tbl extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%projects}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->tableName,
            'alias',
            $this->text()->defaultValue(null)
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'alias');
    }
}
