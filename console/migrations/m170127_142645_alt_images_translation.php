<?php

use console\components\Migration;

/**
 * Class m170127_142645_alt_images_translation migration
 */
class m170127_142645_alt_images_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%alt_images_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%alt_images}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),
                'label' => $this->string()->comment('Alt image')
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-alt_images_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-alt_images_translation-model_id-alt_images-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

