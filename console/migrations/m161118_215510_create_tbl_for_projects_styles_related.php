<?php

use console\components\Migration;

/**
 * Class m161118_215510_create_tbl_for_projects_styles_related migration
 */
class m161118_215510_create_tbl_for_projects_styles_related extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%projects_styles_related}}';

    public $tableNameRelated1 = '{{%projects}}';

    public $tableNameRelated2 = '{{%projects_styles}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'project_id' => $this->integer()->notNull(),
                'project_style_id' => $this->integer()->notNull(),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk_from_projects_styles_related_to_projects',
            $this->tableName,
            'project_id',
            $this->tableNameRelated1,
            'id',
            'CASCADE', 'CASCADE'
        );

        $this->addForeignKey(
            'fk_from_projects_styles_related_to_styles',
            $this->tableName,
            'project_style_id',
            $this->tableNameRelated2,
            'id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
