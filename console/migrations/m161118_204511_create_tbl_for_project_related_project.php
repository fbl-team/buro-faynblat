<?php

use console\components\Migration;

/**
 * Class m161118_204511_create_tbl_for_project_related_project migration
 */
class m161118_204511_create_tbl_for_project_related_project extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%project_related_project}}';

    public $tableNameRelated = '{{%projects}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'project_id' => $this->integer()->notNull(),
                'related_project_id' => $this->integer()->notNull(),
            ],
            $this->tableOptions
        );

        $this->addForeignKey('fk_from_related_project_project_to_project',
            $this->tableName,
            'project_id',
            $this->tableNameRelated,
            'id',
            'CASCADE', 'CASCADE');

        $this->addForeignKey('fk_from_related_project_related_to_project',
            $this->tableName,
            'related_project_id',
            $this->tableNameRelated,
            'id',
            'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
