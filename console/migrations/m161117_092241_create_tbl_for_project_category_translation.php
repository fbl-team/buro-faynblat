<?php

use console\components\Migration;

/**
 * Class m161117_092246_create_tbl_for_projects migration
 */
class m161117_092241_create_tbl_for_project_category_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%project_category_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%project_category}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->text()->defaultValue(null)->comment('Content'),
            ],
            $this->tableOptions
        );


        $this->addPrimaryKey('pk-project_content_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-project_category_translation-model_id-project_category-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
