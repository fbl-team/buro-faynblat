<?php

use console\components\Migration;

/**
 * Class m161117_100543_create_tbl_for_progect_content migration
 */
class m161117_100543_create_tbl_for_progect_content extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%project_content}}';

    public $tableNameRalated = '{{%projects}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'project_id' => $this->integer()->notNull(),
                'type' => $this->text()->defaultValue(null)->comment('Content'),

                'label' => $this->text()->defaultValue(null)->comment('Content'),
                'content' => $this->text()->defaultValue(null)->comment('Content'),

                'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk_from_project_content_to_project',
            $this->tableName,
            'project_id',
            $this->tableNameRalated,
            'id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
