<?php

use console\components\Migration;

/**
 * Class m161129_135428_add_column_to_projects_translation migration
 */
class m161129_135428_add_column_to_projects_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%projects_translation}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->tableName,
            'project_parameters',
            $this->text()->defaultValue(null)
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'project_parameters');
    }
}

