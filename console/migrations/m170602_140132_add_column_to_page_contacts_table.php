<?php
use console\components\Migration;

/**
 * Class m170602_140132_add_column_to_page_contacts_table migration
 */
class m170602_140132_add_column_to_page_contacts_table extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%page_contacts}}';
    /**
     * migration table name
     */
    public $translationTableName = '{{%page_contacts_translation}}';


    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->tableName,
            'four_column_label',
            $this->text()->null()->defaultValue(null)
        );
        $this->addColumn(
            $this->translationTableName,
            'four_column_label',
            $this->text()->null()->defaultValue(null)
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'four_column_label');
        $this->dropColumn($this->translationTableName, 'four_column_label');
    }
}
