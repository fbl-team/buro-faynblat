<?php

use console\components\Migration;

/**
 * Class m161117_150259_create_tbl_for_page_main_translation migration
 */
class m161117_150259_create_tbl_for_page_main_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%page_main_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%page_main}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'first_screen_label' => $this->text()->defaultValue(null),
                'first_screen_under_play_button_text' => $this->text()->defaultValue(null),

                'filosophy_block_label' => $this->text()->defaultValue(null),
                'filosophy_block_short_description' => $this->text()->defaultValue(null),
                'filosophy_block_full_description' => $this->text()->defaultValue(null),
                'filosophy_block_button_label' => $this->text()->defaultValue(null),

                'project_block_label' => $this->text()->defaultValue(null),
                'project_block_short_description' => $this->text()->defaultValue(null),
                'project_block_button_label' => $this->text()->defaultValue(null),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-page_main_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-page_main_translation-model_id-page_main-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

