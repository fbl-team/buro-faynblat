<?php

use console\components\Migration;

/**
 * Class m161118_211257_create_tbl_for_projects_parametrs migration
 */
class m161118_211257_create_tbl_for_projects_parametrs extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%project_parametrs}}';

    public $tableNameRelated = '{{%projects}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'project_id' => $this->integer()->notNull(),

                'label' => $this->text()->defaultValue(null),
                'description' => $this->text()->defaultValue(null),

                'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),

            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk_from_projects_parametrs_to_projects',
            $this->tableName,
            'project_id',
            $this->tableNameRelated,
            'id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
