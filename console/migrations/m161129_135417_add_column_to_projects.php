<?php

use console\components\Migration;

/**
 * Class m161129_135417_add_column_to_projects migration
 */
class m161129_135417_add_column_to_projects extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%projects}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->tableName,
            'project_parameters',
            $this->text()->defaultValue(null)
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'project_parameters');
    }
}
