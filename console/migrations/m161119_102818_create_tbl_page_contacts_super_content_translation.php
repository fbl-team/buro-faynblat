<?php

use console\components\Migration;

/**
 * Class m161119_102818_create_tbl_page_contacts_super_content_translation migration
 */
class m161119_102818_create_tbl_page_contacts_super_content_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%page_contacts_super_content_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%page_contacts_super_content}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'content' => $this->text()->defaultValue(null),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-p_c_s_c_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-p_c_s_c_translation-model_id-p_c_s_c-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

