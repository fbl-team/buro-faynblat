<?php

use console\components\Migration;

/**
 * Class m161128_144354_add_field_to_all_static_pages migration
 */
class m161128_144354_add_field_to_all_static_pages extends Migration
{
    /**
     * migration table name
     */
    public $tableArray = [
        '{{%page_main}}',
        '{{%page_all_projects}}',
        '{{%projects}}',
        '{{%page_about}}',
        '{{%page_services}}',
        '{{%content_pages}}',
        '{{%page_media}}',
        '{{%page_contacts}}',
    ];
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        foreach ($this->tableArray as $tableName) {

            $this->addColumn(
                $tableName,
                'footer_seo_block_button_label_hide',
                $this->text()->defaultValue(null)
            );

        }

    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        foreach ($this->tableArray as $tableName) {

            $this->dropColumn(
                $tableName,
                'footer_seo_block_button_label_hide'
            );

        }
    }
}
