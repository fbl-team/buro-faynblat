<?php

use console\components\Migration;

/**
 * Class m161116_115546_create_tbl_for_content_page_write_block migration
 */
class m161116_115546_create_tbl_for_content_page_first_etap_block extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%content_pages_first_etap_block}}';

    public $relatedTableName = '{{%content_pages}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'content_page_id' => $this->integer()->notNull(),

                'label' => $this->text()->defaultValue(null),
                'description' => $this->text()->defaultValue(null),

                'published' => $this->boolean()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),
            ],
            $this->tableOptions
        );

        $this->addForeignKey(
            'fk_first_etap_block_to_content_page',
            $this->tableName,
            'content_page_id',
            $this->relatedTableName,
            'id',
            'CASCADE', 'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
