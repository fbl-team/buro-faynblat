<?php

use console\components\Migration;

/**
 * Class m161118_064052_create_tbl_for_page_services_what_we_do_block_translation migration
 */
class m161118_064052_create_tbl_for_page_services_what_we_do_block_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%page_services_what_we_do_block_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%page_services_what_we_do_block}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->text()->defaultValue(null),
                'description' => $this->text()->defaultValue(null),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-p_s_w_w_d_b_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-p_s_w_w_d_b_translation-model_id-p_s_w_w_d_b-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

