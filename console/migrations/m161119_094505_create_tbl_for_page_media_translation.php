<?php

use console\components\Migration;

/**
 * Class m161119_094505_create_tbl_for_page_madia_translation migration
 */
class m161119_094505_create_tbl_for_page_media_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%page_media_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%page_media}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->text()->defaultValue(null),
                'write_block_label' => $this->text()->defaultValue(null),
                'write_block_button_label' => $this->text()->defaultValue(null),
                'speak_block_label' => $this->text()->defaultValue(null),
                'speak_block_button_label' => $this->text()->defaultValue(null),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-page_media_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-page_media_translation-model_id-page_media-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

