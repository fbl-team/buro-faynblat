<?php

use console\components\Migration;

/**
 * Class m161129_153057_add_column_to_page_services_what_we_do_block migration
 */
class m161129_153057_add_column_to_page_services_what_we_do_block extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%page_services_what_we_do_block}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->tableName,
            'element_url',
            $this->text()->defaultValue(null)
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'element_url');
    }
}
