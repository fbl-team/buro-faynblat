<?php

use console\components\Migration;

/**
 * Class m161124_104732_add_field_to_page_media_speek_block migration
 */
class m161124_104732_add_field_to_page_media_speek_block extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%page_media_speek_block}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->tableName,
            'you_tube_video_code',
            $this->text()->defaultValue(null)
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'you_tube_video_code');
    }
}
