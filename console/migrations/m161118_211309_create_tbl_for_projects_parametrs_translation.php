<?php

use console\components\Migration;

/**
 * Class m161118_211309_create_tbl_for_projects_parametrs_translation migration
 */
class m161118_211309_create_tbl_for_projects_parametrs_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%project_parametrs_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%project_parametrs}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->text()->defaultValue(null),
                'description' => $this->text()->defaultValue(null),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-project_parametrs_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-project_parametrs_translation-model_id-project_parametrs-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

