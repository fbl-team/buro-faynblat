<?php

use console\components\Migration;

/**
 * Class m180515_082948_issue_careers migration
 */
class m180515_082948_issue_careers extends Migration
{

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            'careers',
            [
                'id' => $this->primaryKey(),
                'type_id' => $this->integer(11)->defaultValue(1),
                'name' => $this->text(),
                'phone' => $this->text(),
                'email' => $this->text(),
                'published' => $this->smallInteger(6)->notNull()->defaultValue(1),
                'created_at' => $this->integer(11)->notNull(),

            ]
        );

        $this->insert('configuration',[
            'id' => 'Footer address',
            'value' => 'г.Киев, ул. Белоцерковская 3',
            'type' => '0',
            'description' => 'Петропавловская Борщаговка.',
            'preload' => 1,
            'published' => 1,
            'created_at' => 1479920830,
            'updated_at' => 1479920830,
            'show' => 1
        ]);

        $this->insert('menus_element',[
            'id' => 18,
            'type_id' => 1,
            'label' => 'Вакансии',
            'url' => '/careers',
            'published' => 1,
            'position' => 6,
            'created_at' => 1479920828,
            'updated_at' => 1526044766,
        ]);

        $this->insert('menus_element_translation',[
            'model_id' => 18,
            'language' => 'en',
            'label' => 'Career',
        ]);

        $this->insert('menus_element_translation',[
            'model_id' => 18,
            'language' => 'uk',
            'label' => 'Вакансії',
        ]);

        $this->createTable(
            'page_careers',
            [
                'id'=> $this->primaryKey(),
                'description'=> $this->text(),
                'label'=> $this->string(255),
                'footer_request_block_label'=> $this->text(),
                'footer_request_block_button_label'=> $this->text(),
                'footer_seo_block_label'=> $this->text(),
                'footer_seo_block_button_label'=> $this->text(),
                'footer_seo_block_text_left_column'=> $this->text(),
                'footer_seo_block_text_left_right'=> $this->text(),
                'footer_seo_block_button_label_hide'=> $this->text(),
                'created_at'=> $this->integer(11)->defaultValue(NULL),
                'updated_at'=> $this->integer(11),
            ]
        );

        $this->createTable(
            'page_careers_translation',
            [
                'model_id'=> $this->integer()->notNull(),
                'language' => $this->string(16)->notNull(),
                'description'=> $this->text(),
                'label'=> $this->string(255),
                'footer_request_block_label'=> $this->text(),
                'footer_request_block_button_label'=> $this->text(),
                'footer_seo_block_label'=> $this->text(),
                'footer_seo_block_button_label'=> $this->text(),
                'footer_seo_block_text_left_column'=> $this->text(),
                'footer_seo_block_text_left_right'=> $this->text(),
                'footer_seo_block_button_label_hide'=> $this->text()
            ]
        );
        $this->addPrimaryKey('model_id_id_pk', 'page_careers_translation', ['model_id', 'language']);
        $this->addForeignKey(
            'page_careers_translation_page_careers_id_fk',
            'page_careers_translation',
            'model_id',
            'page_careers',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->insert('page_careers',[
            'id' => 1,
            'description' => '<p>рус</p>',
            'label' => 'ВАКАНСИИ',
            'footer_request_block_label' => 'Предложить свою кандидатуру',
            'footer_request_block_button_label' => 'Отправить заявку',
            'footer_seo_block_label' => '',
            'footer_seo_block_button_label' => '',
            'footer_seo_block_text_left_column' => '',
            'footer_seo_block_text_left_right' => '',
            'footer_seo_block_button_label_hide' => '',
            'created_at' => NULL,
            'updated_at' => 1525704567
        ]);

        $this->insert('page_careers_translation',[
            'model_id' => 1,
            'language' => 'en',
            'description' => '<p>eng</p>',
            'label' => 'Career',
            'footer_request_block_label' => 'Propose',
            'footer_request_block_button_label' => 'Отправить заявку',
            'footer_seo_block_label' => '',
            'footer_seo_block_button_label' => '',
            'footer_seo_block_text_left_column' => '',
            'footer_seo_block_text_left_right' => '',
            'footer_seo_block_button_label_hide' => ''
        ]);

        $this->insert('page_careers_translation',[
            'model_id' => 1,
            'language' => 'uk',
            'description' => '<p>юа опис<br></p>',
            'label' => 'Career',
            'footer_request_block_label' => 'Вакансії',
            'footer_request_block_button_label' => 'Відправити заявку на роботу',
            'footer_seo_block_label' => 'Отправить заявку',
            'footer_seo_block_button_label' => '',
            'footer_seo_block_text_left_column' => '',
            'footer_seo_block_text_left_right' => '',
            'footer_seo_block_button_label_hide' => ''
        ]);

        $this->addColumn('page_main','blog_block_label', $this->text()->after('project_block_button_label'));
        $this->addColumn('page_main','blog_block_short_description', $this->text()->after('blog_block_label'));
        $this->addColumn('page_main','blog_block_button_label', $this->text()->after('blog_block_short_description'));

        $this->update('page_main',[
            'blog_block_label' => 'Blog',
            'blog_block_short_description' => 'Blog',
            'blog_block_button_label' => 'Все записи',
        ],['ID' => 1]);

        $this->addColumn('page_main_translation','blog_block_label', $this->text()->after('project_block_button_label'));
        $this->addColumn('page_main_translation','blog_block_short_description', $this->text()->after('blog_block_label'));
        $this->addColumn('page_main_translation','blog_block_button_label', $this->text()->after('blog_block_short_description'));

        $this->update('page_main_translation',[
            'blog_block_label' => 'Blog',
            'blog_block_short_description' => 'Blog',
            'blog_block_button_label' => 'All Posts',
        ],['model_id' => 1, 'language' => 'en']);

        $this->update('page_main_translation',[
            'blog_block_label' => 'Блог',
            'blog_block_short_description' => 'Блог',
            'blog_block_button_label' => 'Все записи',
        ],['model_id' => 1, 'language' => 'uk']);


    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('page_main_translation', ['model_id' => 1]);

        $this->dropColumn('page_main_translation','blog_block_label');
        $this->dropColumn('page_main_translation','blog_block_short_description');
        $this->dropColumn('page_main_translation','blog_block_button_label');

        $this->delete('page_main', ['ID' => 1]);

        $this->dropColumn('page_main','blog_block_label');
        $this->dropColumn('page_main','blog_block_short_description');
        $this->dropColumn('page_main','blog_block_button_label');

        $this->delete('page_careers_translation', ['model_id' => 1]);
        $this->delete('page_careers', ['id' => 1]);

        $this->dropForeignKey('page_careers_translation_page_careers_id_fk','page_careers_translation');
        $this->dropPrimaryKey('model_id_id_pk','page_careers_translation');

        $this->dropTable('page_careers_translation');
        $this->dropTable('page_careers');

        $this->delete('menus_element_translation',['model_id' => 18]);
        $this->delete('menus_element',['id' => 18]);
        $this->delete('configuration',['id' => 'Footer address']);

        $this->dropTable('careers');
    }
}
