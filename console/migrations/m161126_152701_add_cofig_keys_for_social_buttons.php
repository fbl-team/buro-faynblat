<?php

use console\components\Migration;

/**
 * Class m161126_152701_add_cofig_keys_for_social_buttons migration
 */
class m161126_152701_add_cofig_keys_for_social_buttons extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%configuration}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {

        $this->insert(
            $this->tableName,
            [
                'id' => 'Social web: Twitter group link',
                'value' => '#',
                'description' => "Социальные сети: параметр строка - ссылка на групу в Twitter",
                'type' => 0,
                'created_at' => time(),
                'updated_at' => time(),
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'id' => 'Social web: Facebook group link',
                'value' => '#',
                'description' => "Социальные сети: параметр строка - ссылка на групу в Facebook",
                'type' => 0,
                'created_at' => time(),
                'updated_at' => time(),
            ]
        );

        $this->insert(
            $this->tableName,
            [
                'id' => 'Social web: Vk group link',
                'value' => '#',
                'description' => "Социальные сети: параметр строка - ссылка на групу в Vk",
                'type' => 0,
                'created_at' => time(),
                'updated_at' => time(),
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete($this->tableName, 'id = :val',array(':val'=>'Social web: Twitter group link'));
        $this->delete($this->tableName, 'id = :val',array(':val'=>'Social web: Facebook group link'));
        $this->delete($this->tableName, 'id = :val',array(':val'=>'Social web: Vk group link'));
    }
}
