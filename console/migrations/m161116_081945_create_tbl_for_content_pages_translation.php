<?php

use console\components\Migration;

/**
 * Class m161116_081945_create_tbl_for_content_pages_translation migration
 */
class m161116_081945_create_tbl_for_content_pages_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%content_pages_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%content_pages}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'breadcrums_label' => $this->text()->defaultValue(null),

                'first_screen_label' => $this->text()->defaultValue(null),
                'first_screen_button_label' => $this->text()->defaultValue(null),

                'second_screen_label1' => $this->text()->defaultValue(null),
                'second_screen_label2' => $this->text()->defaultValue(null),

                'second_screen_description1' => $this->text()->defaultValue(null),
                'second_screen_description2' => $this->text()->defaultValue(null),

                'etap_block_label' => $this->text()->defaultValue(null),

                'inside_service_block_label' => $this->text()->defaultValue(null),
                'inside_service_block_description' => $this->text()->defaultValue(null),

                'etap_second_block_label' => $this->text()->defaultValue(null),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-content_pages_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-content_pages_translation-model_id-content_pages-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

