<?php

use console\components\Migration;

/**
 * Class m161119_074944_create_tbl_for_page_about_translation migration
 */
class m161119_074944_create_tbl_for_page_about_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%page_about_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%page_about}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'first_screen_label' => $this->text()->defaultValue(null)->comment('Label'),
                'first_screen_description' => $this->text()->defaultValue(null),
                'first_screen_image' => $this->text()->defaultValue(null),

                'short_about_block_label1' => $this->text()->defaultValue(null),
                'short_about_block_label2' => $this->text()->defaultValue(null),

                'short_about_block_description1' => $this->text()->defaultValue(null),
                'short_about_block_description2' => $this->text()->defaultValue(null),

                'work_block_label' => $this->text()->defaultValue(null),
                'work_block_description' => $this->text()->defaultValue(null),

                'about_our_work_label' => $this->text()->defaultValue(null),

                'our_diplom_block_label' => $this->text()->defaultValue(null),
                'our_diplom_block_button_label' => $this->text()->defaultValue(null),

                'canal_block_label' => $this->text()->defaultValue(null),
                'canal_block_button_label' => $this->text()->defaultValue(null),

                'projects_block_label' => $this->text()->defaultValue(null),
                'projects_block_description' => $this->text()->defaultValue(null),
                'projects_block_button_label' => $this->text()->defaultValue(null),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-page_about_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-page_about_translation-model_id-page_about-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

