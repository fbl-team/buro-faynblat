<?php

use console\components\Migration;

/**
 * Class m161118_215313_create_tbl_for_projects_styles_translation migration
 */
class m161118_215313_create_tbl_for_projects_styles_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%projects_styles_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%projects_styles}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->string()->notNull()->comment('Label'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-projects_styles_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-projects_styles_translation-model_id-projects_styles-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

