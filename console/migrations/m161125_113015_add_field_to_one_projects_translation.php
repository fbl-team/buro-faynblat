<?php

use console\components\Migration;

/**
 * Class m161125_113015_add_field_to_one_projects_translation migration
 */
class m161125_113015_add_field_to_one_projects_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%page_one_project_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%page_one_project}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->tableName,
            'add_content_button_label',
            $this->text()->defaultValue(null)
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'add_content_button_label');
    }
}

