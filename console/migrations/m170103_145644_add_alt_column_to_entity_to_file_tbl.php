<?php

use console\components\Migration;

/**
 * Class m170103_145644_add_alt_column_to_entity_to_file_tbl migration
 */
class m170103_145644_add_alt_column_to_entity_to_file_tbl extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%entity_to_file}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'alt', $this->text()->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'alt');
    }
}
