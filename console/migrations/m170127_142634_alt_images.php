<?php

use console\components\Migration;

/**
 * Class m170127_142634_alt_images migration
 */
class m170127_142634_alt_images extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%alt_images}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),
                'label' => $this->string()->notNull()->comment('Label'),
                'file_id' => $this->integer()->notNull()->comment('File id')
            ],
            $this->tableOptions
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
