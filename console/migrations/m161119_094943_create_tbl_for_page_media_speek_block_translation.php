<?php

use console\components\Migration;

/**
 * Class m161119_094943_create_tbl_for_page_media_speek_block_translation migration
 */
class m161119_094943_create_tbl_for_page_media_speek_block_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%page_media_speek_block_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%page_media_speek_block}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->text()->defaultValue(null),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-p_m_s_b_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-p_m_s_b_translation-model_id-p_m_s_b-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

