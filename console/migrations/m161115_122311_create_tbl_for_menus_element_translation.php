<?php

use console\components\Migration;

/**
 * Class m161115_122311_create_tbl_for_menus_element_translation migration
 */
class m161115_122311_create_tbl_for_menus_element_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%menus_element_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%menus_element}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->text()->defaultValue(null),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-menus_element_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-menus_element_translation-model_id-menus_element-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

