<?php

use console\components\Migration;

/**
 * Class m161122_212854_add_column_to_page_about_canal_url migration
 */
class m161122_212854_add_column_to_page_about_canal_url extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%page_about}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->tableName,
            'canal_url',
            $this->text()->defaultValue(null)
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'canal_url');
    }
}
