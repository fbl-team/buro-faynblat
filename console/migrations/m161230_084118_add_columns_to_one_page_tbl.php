<?php

use console\components\Migration;

/**
 * Class m161230_084118_add_columns_to_one_page_tbl migration
 */
class m161230_084118_add_columns_to_one_page_tbl extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%page_one_project}}';
    public $tableNameTranslate = '{{%page_one_project_translation}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->tableName,
            'footer_request_block_label',
            $this->text()->defaultValue(null)
        );

        $this->addColumn(
            $this->tableName,
            'footer_request_block_button_label',
            $this->text()->defaultValue(null)
        );

        $this->addColumn(
            $this->tableNameTranslate,
            'footer_request_block_label',
            $this->text()->defaultValue(null)
        );

        $this->addColumn(
            $this->tableNameTranslate,
            'footer_request_block_button_label',
            $this->text()->defaultValue(null)
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'footer_request_block_label');
        $this->dropColumn($this->tableName, 'footer_request_block_button_label');
        $this->dropColumn($this->tableNameTranslate, 'footer_request_block_label');
        $this->dropColumn($this->tableNameTranslate, 'footer_request_block_button_label');
    }
}
