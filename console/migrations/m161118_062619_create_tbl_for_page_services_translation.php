<?php

use console\components\Migration;

/**
 * Class m161118_062619_create_tbl_for_page_services_translation migration
 */
class m161118_062619_create_tbl_for_page_services_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%page_services_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%page_services}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->text()->defaultValue(null),
                'description' => $this->text()->defaultValue(null),

                'what_we_do_block_label' => $this->text()->defaultValue(null),
                'what_we_do_block_description' => $this->text()->defaultValue(null),

                'how_we_do_that_block_label' => $this->text()->defaultValue(null),
                'how_we_do_that_block_description' => $this->text()->defaultValue(null),

                'project_block_label' => $this->text()->defaultValue(null),
                'project_block_description' => $this->text()->defaultValue(null),
                'project_block_button_label' => $this->text()->defaultValue(null),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-page_services_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-page_services_translation-model_id-page_services-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

