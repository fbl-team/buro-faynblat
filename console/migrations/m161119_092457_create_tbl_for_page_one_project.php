<?php

use console\components\Migration;

/**
 * Class m161119_092457_create_tbl_for_page_one_project migration
 */
class m161119_092457_create_tbl_for_page_one_project extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%page_one_project}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'breadcrums_label' => $this->text()->defaultValue(null),
                'breadcrums_url' => $this->text()->defaultValue(null),

                'another_project_block_label' => $this->text()->defaultValue(null),
                'another_project_block_published' => $this->text()->defaultValue(null),
                'another_project_button_label' => $this->text()->defaultValue(null),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->insert(
            $this->tableName,
            [
                'breadcrums_label' => 'ПРОЕКТЫ',
                'created_at' => time(),
                'updated_at' => time(),
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
