<?php

use console\components\Migration;

/**
 * Class m161116_124908_create_tbl_for_content_page_second_etap_block_translation migration
 */
class m161116_124908_create_tbl_for_content_page_second_etap_block_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%content_pages_second_etap_block_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%content_pages_second_etap_block}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->text()->defaultValue(null),
                'description' => $this->text()->defaultValue(null),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-c_p_s_e_b_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-c_p_s_e_b_translation-model_id-c_p_s_e_b-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

