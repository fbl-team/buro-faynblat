<?php

use console\components\Migration;

/**
 * Class m161117_100558_create_tbl_for_progect_content_translation migration
 */
class m161117_100558_create_tbl_for_progect_content_translation extends Migration
{
    /**
     * Migration related table name
     */
    public $tableName = '{{%project_content_translation}}';

    /**
     * main table name, to make constraints
     */
    public $tableNameRelated = '{{%project_content}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'model_id' => $this->integer()->notNull()->comment('Related model id'),
                'language' => $this->string(16)->notNull()->comment('Language'),

                'label' => $this->text()->defaultValue(null)->comment('Content'),
                'content' => $this->text()->defaultValue(null)->comment('Content'),
            ],
            $this->tableOptions
        );

        
        $this->addPrimaryKey('pk-project_content_translation', $this->tableName, ['model_id', 'language']);

        $this->addForeignKey(
            'fk-project_content_translation-model_id-project_content-id',
            $this->tableName,
            'model_id',
            $this->tableNameRelated,
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}

