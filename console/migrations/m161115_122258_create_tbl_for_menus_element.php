<?php

use console\components\Migration;

/**
 * Class m161115_122258_create_tbl_for_menus_element migration
 */
class m161115_122258_create_tbl_for_menus_element extends Migration
{
    /**
     * migration table name
     */
    public $tableName = '{{%menus_element}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            $this->tableName,
            [
                'id' => $this->primaryKey(),

                'type_id' => $this->integer()->defaultValue(1)->comment('1-header; 2-footer; 3-right;'),

                'label' => $this->text()->defaultValue(null),
                'url' => $this->text()->notNull()->comment('Url'),

                'published' => $this->smallInteger(1)->unsigned()->notNull()->defaultValue(1)->comment('Published'),
                'position' => $this->integer()->notNull()->defaultValue(0)->comment('Position'),

                'created_at' => $this->integer()->notNull()->comment('Created At'),
                'updated_at' => $this->integer()->notNull()->comment('Updated At'),
            ],
            $this->tableOptions
        );

        $this->batchInsert(
            $this->tableName,
            ['type_id', 'label', 'url', 'created_at', 'updated_at'],
            [
                ['1', 'Главная', '/home', time(), time()],
                ['1', 'Портфолио', '/projects', time(), time()],
                ['1', 'О виктории', '/about', time(), time()],
                ['1', 'Услуги', '/services', time(), time()],
                ['1', 'Пресса', '/media', time(), time()],
                ['1', 'Контакты', '/contacts', time(), time()],

                ['2', 'Дизайн интерьеров', '/design', time(), time()],
                ['2', 'Строительство', '/buildings', time(), time()],
                ['2', 'Ландшафтный дизайн', '/landscaping', time(), time()],
                ['2', 'Архитектура', '/architecture', time(), time()],
                ['2', 'Проектирование', '/planning', time(), time()],

                ['3', 'Дизайн интерьеров', '/design', time(), time()],
                ['3', 'Строительство', '/buildings', time(), time()],
                ['3', 'Ландшафтный дизайн', '/landscaping', time(), time()],
                ['3', 'Архитектура', '/architecture', time(), time()],
                ['3', 'Проектирование', '/planning', time(), time()],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
