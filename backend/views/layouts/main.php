<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/* @var $this \yii\web\View */
/* @var $content string */

$this->title = Yii::$app->name;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <div class="header-main__logo">
            <div class="logo">
                <a href="/">
                    <svg version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 148.8 30.6" xml:space="preserve">
                                <style type="text/css">
                                    .st0{fill:#A86280;}
                                </style>
                        <g>
                            <polygon class="st0 first-part" points="29.3,0.1 14.7,30.5 23,30.5 37.8,0.1 	" />
                            <polygon class="st0 second-part" points="16.7,17.6 14.3,12.9 19,12.9 21.4,8.2 12,8.2 10.8,4.7 22.5,4.7 24.9,0 0,0 12.4,26.2 	" />
                        </g>
                        <g class="third-part">
                            <path d="M50.3,0.2h3.5l2.5,7.1l2.5-7.1h3.4l-4.5,11.3h-2.9L50.3,0.2z" />
                            <path d="M65.1,11.4V0.2h3.1v11.2H65.1z" />
                            <path d="M71.8,11.4V0.2h3.1v4.5l3.9-4.5h3.7L78.2,5l4.4,6.4h-3.7l-2.8-4.2l-1.1,1.3v2.9H71.8z" />
                            <path d="M84.3,2.9V0.2h9.8v2.7h-3.4v8.5h-3.1V2.9H84.3z" />
                            <path d="M96.2,5.8c0-0.8,0.2-1.5,0.5-2.3s0.7-1.3,1.3-1.9c0.5-0.5,1.2-0.9,1.9-1.2c0.8-0.3,1.5-0.5,2.4-0.5s1.6,0.2,2.4,0.5
                                    c0.7,0.3,1.4,0.7,1.9,1.2c0.5,0.5,0.9,1.1,1.2,1.9c0.3,0.7,0.5,1.5,0.5,2.3c0,0.8-0.2,1.5-0.5,2.3c-0.3,0.7-0.7,1.3-1.3,1.9
                                    c-0.5,0.5-1.2,0.9-1.9,1.2c-0.7,0.3-1.5,0.5-2.4,0.5c-1.1,0-2.2-0.3-3.1-0.8c-0.9-0.5-1.6-1.2-2.2-2.1S96.2,6.9,96.2,5.8z
                                     M99.4,5.8c0,0.8,0.3,1.5,0.8,2.1s1.2,0.9,2.1,0.9c0.8,0,1.5-0.3,2.1-0.9s0.8-1.3,0.8-2.1c0-0.8-0.3-1.5-0.8-2.1
                                    c-0.5-0.6-1.2-0.9-2.1-0.9c-0.8,0-1.5,0.3-2,0.9C99.7,4.3,99.4,5,99.4,5.8z" />
                            <path d="M111.5,11.4V0.2h5.3c1.6,0,2.8,0.4,3.6,1.2c0.7,0.7,1,1.6,1,2.7c0,0.8-0.2,1.5-0.6,2.1s-1,1-1.7,1.3l2.7,3.9h-3.6L116,8
                                    h-1.4v3.4H111.5z M114.6,5.6h2c0.5,0,0.9-0.1,1.2-0.4s0.4-0.6,0.4-1c0-0.4-0.1-0.8-0.4-1s-0.7-0.3-1.2-0.3h-2V5.6z" />
                            <path d="M124.7,11.4V0.2h3.1v11.2H124.7z" />
                            <path d="M130.5,11.4l4.8-11.3h3l4.8,11.3h-3.3l-0.8-2h-4.3l-0.8,2H130.5z M135.5,7h2.5l-1.2-3.2L135.5,7z" />
                            <path d="M51.2,30.6V19.4h9v2.7h-5.9V24h5.3v2.6h-5.3v4H51.2z" />
                            <path d="M61,30.6l4.8-11.3h3l4.8,11.3h-3.3l-0.8-2h-4.3l-0.8,2H61z M66,26.2h2.5L67.3,23L66,26.2z" />
                            <path d="M73.5,19.4H77l2.3,4.1l2.3-4.1h3.5l-4.3,7v4.2h-3.1v-4.2L73.5,19.4z" />
                            <path d="M87.7,30.6V19.4h2.9l4.6,5.9v-5.9h3.1v11.2h-2.7l-4.8-6.1v6.1H87.7z" />
                            <path d="M101.8,30.6V19.4h5.7c1.4,0,2.4,0.3,3.1,1c0.5,0.5,0.7,1.1,0.7,1.8c0,1.2-0.6,2-1.7,2.5c0.7,0.2,1.3,0.6,1.7,1
                                    c0.4,0.4,0.6,1,0.6,1.8c0,1-0.4,1.7-1.1,2.2s-1.8,0.8-3.1,0.8H101.8z M104.8,23.9h1.9c1,0,1.5-0.3,1.5-1c0-0.3-0.1-0.6-0.4-0.7
                                    c-0.2-0.2-0.6-0.3-1-0.3h-2V23.9z M104.8,28.2h2.4c0.5,0,0.8-0.1,1.1-0.3c0.3-0.2,0.4-0.5,0.4-0.8c0-0.3-0.1-0.6-0.4-0.8
                                    s-0.6-0.3-1.1-0.3h-2.4V28.2z" />
                            <path d="M114.8,30.6V19.4h3.1v8.5h5.4v2.7H114.8z" />
                            <path d="M125.4,30.6l4.8-11.3h3l4.8,11.3h-3.3l-0.8-2h-4.3l-0.8,2H125.4z M130.4,26.2h2.5l-1.2-3.2L130.4,26.2z" />
                            <path d="M138.6,22.1v-2.7h9.8v2.7H145v8.5h-3.1v-8.5H138.6z" />
                        </g>
                            </svg>
                </a>
            </div>
            <div class="logo-small">
                <a href="00_Home_Faynblat.html">
                    <svg width="26" height="21">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#logo-footer"></use>
                    </svg>
                </a>
            </div>
        </div>
        <?php
            NavBar::begin([
//                'brandLabel' => Yii::$app->name,
//                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top navbar-padding-for-logo',
                ],
            ]);
            echo \backend\modules\menu\widgets\MainMenu::widget();
            echo \backend\modules\menu\widgets\RightBarMenu::widget();
            echo \backend\modules\menu\widgets\LoginMenu::widget();
            NavBar::end();
        ?>

        <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= \backend\widgets\Alert::widget([
            'closeButton' => [
                'label' => ''
            ]
        ]); ?>
        <?= $content ?>
        </div>
    </div>
    <div class="modal modal-hidden fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <p>With love
                        <a class="vintage-logo" href="http://www.vintage.com.ua/">
                            <svg viewBox="63.9 301.6 84.2 115.5">
                                <polygon points="85.6,301.6 63.9,301.6 68.9,321.4 90.5,321.4 "></polygon>
                                <polygon points="127,301.6 97.6,417.1 118.6,417.1 148,301.6 "></polygon>
                            </svg>
                        </a></p>
                </div>
                <div class="col-sm-4">
                    <p class="text-center">© by Vintage, All Rights Reserved. <?= date('Y') ?></p>
                </div>
                <div class="col-sm-4">
                    <p class="text-right"><?= Yii::powered() ?></p>
                </div>
            </div>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
