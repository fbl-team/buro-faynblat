<?php

use backend\modules\projects\models\projectsConstructorStrategies\MultiImageStrategy;
use backend\modules\projects\models\projectsConstructorStrategies\SingleImageStrategy;
use backend\modules\projects\helpers\ProjectsConstructorHelper;
use backend\modules\projects\models\projectsConstructorStrategies\TwoColumnsStrategy;

use backend\modules\blogs\models\blogsConstructorStrategies\MultiImageStrategy as BlogMultiImageStrategy;
use backend\modules\blogs\models\blogsConstructorStrategies\SingleImageStrategy as BlogSingleImageStrategy;
use backend\modules\blogs\helpers\BlogsConstructorHelper as BlogsConstructorHelper;
use backend\modules\blogs\models\blogsConstructorStrategies\TwoColumnsStrategy as BlogTwoColumnsStrategy;

Yii::$container->set('ProjectsConstructor', function () {
    $constructor = new ProjectsConstructorHelper();
    $constructor->addConstructorStrategy(
        TwoColumnsStrategy::TYPE,
        new TwoColumnsStrategy
    );
    $constructor->addConstructorStrategy(
        SingleImageStrategy::TYPE,
        new SingleImageStrategy
    );
    $constructor->addConstructorStrategy(
        MultiImageStrategy::TYPE,
        new MultiImageStrategy
    );
    return $constructor;
});

Yii::$container->set('BlogsConstructor', function () {
    $constructor = new BlogsConstructorHelper();
    $constructor->addConstructorStrategy(
        BlogTwoColumnsStrategy::TYPE,
        new BlogTwoColumnsStrategy
    );
    $constructor->addConstructorStrategy(
        BlogSingleImageStrategy::TYPE,
        new BlogSingleImageStrategy
    );
    $constructor->addConstructorStrategy(
        BlogMultiImageStrategy::TYPE,
        new BlogMultiImageStrategy
    );
    return $constructor;
});