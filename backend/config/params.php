<?php
return [
    'adminEmail' => 'admin@example.com',
    'menuItems' => require(__DIR__ . '/menu-items.php'),
    'rightBarMenuItems' => require(__DIR__ . '/right-bar-menu-items.php'),
    'ajaxCheckboxWithOnlyOneExistValue' => [
        \backend\modules\projects\models\Projects::className() => [
//            'show_on_main',
            'show_on_about',
            'show_on_services',
        ],
        \backend\modules\blogs\models\Blogs::className() => [
//            'show_on_main',
            'show_on_about',
            'show_on_services',
        ]
    ]
];
