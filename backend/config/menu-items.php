<?php

use common\helpers\ContentPagesHelper;
use common\helpers\MenusHelper;

return [
    [
        'label' => Yii::t('back/menu', 'menus'),
        'items' => [
            [
                'label' => Yii::t('back/menu', 'header menu'),
                'url' => ['/menus/header-menu/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'footer menu'),
                'url' => ['/menus/footer-menu/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'right menu'),
                'url' => ['/menus/right-menu/index'],
            ],
        ],
    ],
    [
        'label' => Yii::t('back/menu', 'pages'),
        'items' => [
            [
                'label' => Yii::t('back/menu', 'content pages'),
                'items' => [
                    [
                        'label' => ContentPagesHelper::getPageLabel(ContentPagesHelper::ELEMENT_DESIGN),
                        'url' => ['/contentPages/content-pages/index', 'alias' => ContentPagesHelper::ELEMENT_DESIGN],
                    ],
                    [
                        'label' => ContentPagesHelper::getPageLabel(ContentPagesHelper::ELEMENT_BUILDINGS),
                        'url' => ['/contentPages/content-pages/index', 'alias' => ContentPagesHelper::ELEMENT_BUILDINGS],
                    ],
                    [
                        'label' => ContentPagesHelper::getPageLabel(ContentPagesHelper::ELEMENT_LANDSCAPING),
                        'url' => ['/contentPages/content-pages/index', 'alias' => ContentPagesHelper::ELEMENT_LANDSCAPING],
                    ],
                    [
                        'label' => ContentPagesHelper::getPageLabel(ContentPagesHelper::ELEMENT_ARCHITECTURE),
                        'url' => ['/contentPages/content-pages/index', 'alias' => ContentPagesHelper::ELEMENT_ARCHITECTURE],
                    ],
                    [
                        'label' => ContentPagesHelper::getPageLabel(ContentPagesHelper::ELEMENT_PLANNING),
                        'url' => ['/contentPages/content-pages/index', 'alias' => ContentPagesHelper::ELEMENT_PLANNING],
                    ],
                ],
            ],
            [
                'label' => Yii::t('back/menu', 'main page'),
                'url' => ['/pages/page-main/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'services'),
                'url' => ['/pages/page-services/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'all projects'),
                'url' => ['/pages/page-all-projects/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'all blogs'),
                'url' => ['/pages/page-all-blogs/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'about'),
                'url' => ['/pages/page-about/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'one project'),
                'url' => ['/pages/page-one-project/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'one blog'),
                'url' => ['/pages/page-one-blog/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'media'),
                'url' => ['/pages/page-media/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'contacts'),
                'url' => ['/pages/page-contacts/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'careers'),
                'url' => ['/pages/page-careers/index'],
            ],
        ],
    ],
    [
        'label' => Yii::t('back/menu', 'projects'),
        'items' => [
            [
                'label' => Yii::t('back/menu', 'projects category'),
                'url' => ['/projects/project-category/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'projects styles'),
                'url' => ['/projects/projects-styles/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'projects'),
                'url' => ['/projects/projects/index'],
            ],
        ],
    ],
    [
        'label' => Yii::t('back/menu', 'blog'),
        'items' => [
            [
                'label' => Yii::t('back/menu', 'blog category'),
                'url' => ['/blogs/blog-category/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'blog styles'),
                'url' => ['/blogs/blogs-styles/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'blog posts'),
                'url' => ['/blogs/blogs/index'],
            ],
        ],
    ],
    [
        'label' => Yii::t('back/menu', 'request'),
        'url' => ['/request/request/index'],
    ],
    [
        'label' => Yii::t('back/menu', 'settings'),
        'items' => [
            [
                'label' => Yii::t('back/menu', 'configurations'),
                'url' => ['/configuration/default/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'translations'),
                'url' => ['/i18n/default/index'],
            ],
            [
                'label' => Yii::t('back/menu', 'languages'),
                'url' => ['/language/language/index'],
            ],
        ],
    ],
];

