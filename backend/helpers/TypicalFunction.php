<?php
namespace backend\helpers;
use metalguardian\fileProcessor\helpers\FPM;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


/**
 * Site controller
 */
class TypicalFunction
{

    public static function isKeyExist($configKey, $defaultValue)
    {
        $configValue = \Yii::$app->config->get($configKey);

        return $configValue ? $configValue : $defaultValue;

    }

    public static function getImage($model, $relation, $size, $htmlOptions = [])
    {

        return (isset($model->{$relation}->file_id)) ? FPM::image($model->{$relation}->file_id, $size[0] , $size[1], $htmlOptions) : 'image not downloaded!';

    }

    public static function getImageUrl($model, $relation, $size)
    {

        return (isset($model->{$relation}->file_id)) ? FPM::src($model->{$relation}->file_id, $size[0] , $size[1]) : null;

    }

    public static function getImageById($model, $attribute, $size, $htmlOptions = [])
    {

        return (isset($model->{$attribute})) ? FPM::image($model->{$attribute}, $size[0] , $size[1], $htmlOptions) : 'image not uploaded!';

    }

    public static function getFileRawValue($model, $attribute)
    {

        return $model->{$attribute}
            ? 'Current downloaded file: <strong>'
            . $model->file->base_name.'.'.$model->file->extension
            . '</strong>. <br> Loading new file will replace the current !'
            : null;

    }
}
