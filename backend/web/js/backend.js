function parseResponse(response) {
    if (!response){
        return false;
    }
    if (response.replaces instanceof Array) {
        for (var i = 0, ilen = response.replaces.length; i < ilen; i++) {
            $(response.replaces[i].what).replaceWith(response.replaces[i].data);
        }
    }
    if (response.append instanceof Array) {
        for (i = 0, ilen = response.append.length; i < ilen; i++) {
            $(response.append[i].what).append(response.append[i].data);
        }
    }
    if (response.content instanceof Array) {
        for (i = 0, ilen = response.content.length; i < ilen; i++) {
            $(response.content[i].what).html(response.content[i].data);
        }
    }
    if (response.js) {
        $("body").append(response.js);
    }
    if (response.refresh) {
        window.location.reload(true);
    }
    if (response.redirect) {
        window.location.href = response.redirect;
    }
}

//Multi-upload widget
$(function(){
    $(document).ready(function(){
        initImageSorting();
        fixMultiUploadImageCropUrl();
        fixMultiUploadImageAltTags();
        checkTabErrors();
        initSortableGrid();
        initPositionOnclickEvent();
    });

    $(document).on('click', '.save-alt', function (event) {
        event.preventDefault();
        var $this = $(this),
            url = $this.attr('href'),
            _file_id = $this.parents('.container').children('#file_id').val(),
            _alt = $this.parents('.container').children('#altimages-label').val(),
            lang, _lang_alt, langData = {};
        $('.language_alt').each(function () {
            lang = $(this).children('#altimagestranslation-language').val();
            _lang_alt = $(this).children('#altimagestranslation-label').val();
            langData[lang] = _lang_alt;
        });
        jQuery.ajax({
            'cache': false,
            'type': 'POST',
            'dataType': 'json',
            'data': {id: _file_id, label: _alt, langData: langData},
            'success': function (response) {
                parseResponse(response);
            }, 'error': function (response) {
                alert(response.responseText);
            }, 'beforeSend': function () {
            }, 'complete': function () {
            }, 'url': url
        });

    });

    $(document).on('click', '.save-cropped', function(event){
        event.preventDefault();
        var that = this;
        var url = $(that).attr('href');
        var data = {
            startX: $('#dataX').val(),
            startY: $('#dataY').val(),
            width: $('#dataWidth').val(),
            height: $('#dataHeight').val(),
            fileId: $('#fileId').val()
        };

        jQuery.ajax({
            'cache': false,
            'type': 'POST',
            'dataType': 'json',
            'data':'data='+JSON.stringify(data),
            'success':
                function (response) {
                    parseResponse(response);
                }, 'error': function (response) {
                alert(response.responseText);
            }, 'beforeSend': function () {
            }, 'complete': function () {
            }, 'url': url});

    });

    $(document).on('click', '.cancel-crop', function(event){
        event.preventDefault();

        hideModal('.modal');
    });

    $(document).on('hidden.bs.modal', '.modal', function (e) {
        $(this).removeData('bs.modal');
        $('.modal-dialog .modal-content').empty();
    });

    $(document).on('shown.bs.modal', '.modal', function (e) {
        var $dataX = $("#dataX"),
            $dataY = $("#dataY"),
            $dataHeight = $("#dataHeight"),
            $dataWidth = $("#dataWidth");

        $(".img-container > img").cropper({
            viewMode: 1,
            aspectRatio: $('.actual-aspect-ratio').val(),
            preview: ".img-preview",
            crop: function(data) {
                $dataX.val(Math.round(data.x));
                $dataY.val(Math.round(data.y));
                $dataHeight.val(Math.round(data.height));
                $dataWidth.val(Math.round(data.width));
            }
        });
    });

    $(document).on('click', '.crop-link', function(){
        var aspectRatio = $('.actual-aspect-ratio');
        var value = $(this).parents('div.form-group').find('.aspect-ratio').val();
        if (!aspectRatio.length){
            $('.container').append('<input type="hidden" name="aspectRatio" class="actual-aspect-ratio" value="'+ value +'">');
        } else {
            aspectRatio.val(value);
        }

    });

    //For file deleting in forms
    $(document).on('click', '.delete-file', function(){
        var that = $(this);
        jQuery.ajax({
            'cache': false,
            'type': 'POST',
            'data': {
                'modelId': that.data('modelid'),
                'modelName': that.data('modelname'),
                'attribute': that.data('attribute'),
                'language': that.data('language')
            },
            'success':
                function (response) {
                    if (response.error) {
                        alert('Не удалось удалить файл');
                    } else {
                        that.parent('.file-name').remove();
                    }
                }, 'error': function (response) {
                alert(response.responseText);
            },
            'url': '/site/delete-file'});
    });

    //For changing configuration form
    $(document).on('change', '.config-type', function (event) {
        event.preventDefault();
        var that = this;
        var url = $(that).data('url');
        var form = $(this).parents('form');
        var action = form.attr('action');

        jQuery.ajax({
            'cache': false,
            'type': 'POST',
            'dataType': 'json',
            'data': form.serialize()+'&action='+action,
            'success':
                function (response) {
                    parseResponse(response);
                }, 'error': function (response) {
                alert(response.responseText);
            }, 'beforeSend': function () {
            }, 'complete': function () {
                addClassForNewAdminStyle();
                customSelect2();
            }, 'url': url});
    });

    $('.template-builder').sortable({
        handle: ".btn-template-mover",
        items: ".content-append",
        cancel: ''
    });
    $('.template-builder-constructor').sortable({
        handle: ".btn-template-mover",
        items: ".content-append-constructor",
        cancel: ''
    });

});

function hideModal(elem)
{
    $(elem).modal('hide');
}

function initImageSorting()
{
    if ($('.file-preview-thumbnails').length) {
        $('.file-preview-thumbnails').sortable({
            update: function (event, ui) {
                saveSort();
            }
        });
    }
}

function saveSort()
{
    var url = $('#urlForSorting').val();
    var data = $(".kv-file-remove.btn").map(
        function () {return $(this).data('key');}
    ).get().join(",");


    jQuery.ajax({
        'cache': false,
        'type': 'POST',
        'dataType': 'json',
        'data': 'sort='+data,
        'success':
            function (response) {
                parseResponse(response);
            }, 'error': function (response) {
            alert(response.responseText);
        }, 'beforeSend': function () {
        }, 'complete': function () {
        }, 'url': url});
}

function fixMultiUploadImageCropUrl()
{
    $('.crop-link').each(function(){
        var href = $(this).attr('href');
        var key = $(this).data('key');
        var isKeyAdded = parseInt(href.match(/\d+/));

        if (key && isNaN(isKeyAdded)) {
            $(this).attr('href', href + key);
        }
    });
}

function fixMultiUploadImageAltTags() {
    $('.alt-link').each(function () {
        var href = $(this).attr('href');
        var key = $(this).data('key');
        var isKeyAdded = parseInt(href.match(/\d+/));
        if (key && isNaN(isKeyAdded)) {
            $(this).attr('href', href + key);
        }
    });
}

//Multi-upload widget

function addClassForNewAdminStyle()
{
    $('.grid-view .filters .form-control').addClass('form-control-filters');
    if ($('.glyphicon.glyphicon-pencil').length) {
        $('.grid-view .table').addClass('vertical-options');
    }

    if (!$('.grid-view .filters td input').length) {
        $('.grid-view table').addClass('no-filters');
    }

    if ($('.grid-view .filters td').length) {
        $('.grid-view .filters td').addClass('form-group');
    }

    if ($('li.active a[href="/configuration/configuration/index"]').length) {
        $('.grid-view .table').addClass('l-height');
    }
}

addClassForNewAdminStyle();

$(function () {
    $(document).on('pjax:success', function(event, data) {
        addClassForNewAdminStyle();
        customSelect2();
    });
    //For checkbox in gridView (index page)
    $(document).on('click', '.ajax-checkbox', function(){
        var that = $(this);
        jQuery.ajax({
            'cache': false,
            'type': 'POST',
            'dataType': 'json',
            'data': {'modelId': that.data('id'), 'modelName': that.data('modelname'), 'attribute': that.data('attribute')},
            'success': function (response) {
                parseResponse(response);
            }, 'error': function (response) {
                alert(response.responseText);
            }, 'beforeSend': function () {
            },
            'url': '/site/ajax-checkbox'});
    });

    //For file deleting in forms
    $(document).on('click', '.delete-file', function(){
        var that = $(this);
        jQuery.ajax({
            'cache': false,
            'type': 'POST',
            'data': {
                'modelId': that.data('modelid'),
                'modelName': that.data('modelname'),
                'attribute': that.data('attribute'),
                'language': that.data('language')
            },
            'success':
                function (response) {
                    if (response.error) {
                        alert('Не удалось удалить файл');
                    } else {
                        that.parent('.file-name').remove();
                    }
                }, 'error': function (response) {
                alert(response.responseText);
            },
            'url': '/site/delete-file'});
    });

    //For changing configuration form
    $(document).on('change', '.config-type', function (event) {
        event.preventDefault();
        var that = this;
        var url = $(that).data('url');
        var form = $(this).parents('form');
        var action = form.attr('action');

        jQuery.ajax({
            'cache': false,
            'type': 'POST',
            'dataType': 'json',
            'data': form.serialize()+'&action='+action,
            'success':
                function (response) {
                    parseResponse(response);
                }, 'error': function (response) {
                alert(response.responseText);
            }, 'beforeSend': function () {
            }, 'complete': function () {
                addClassForNewAdminStyle();
                customSelect2();
            }, 'url': url});
    });

    if ($('.s_name').length) {
        $('.s_name:first').liTranslit({
            elAlias: $('.s_alias')
        });
    }
});

function checkTabErrors() {
    var tabs = $('.tab-content .tab-pane');
    if (tabs.length) {
        tabs.each(function(index, el){
            var that = $(el);
            if (that.find('.has-error').length) {
                var id = that.attr('id');
                $('a[href="#' + id + '"]').addClass('tab-error');
            }
        });
    }
}

$(document).on('click', '.btn-template-delete', function(event) {
    event.preventDefault();

    if (confirm("Confirm deleting")) {
        $(this).parent('.content-append').remove();
    }

});

$(document).on('click', '.btn-template-delete-constructor', function(event) {
    event.preventDefault();

    if (confirm("Confirm deleting")) {
        $(this).parent('.content-append-constructor').remove();
    }

});

$(document).on('click', '.btn-template-builder-constructor', function (event) {
    event.preventDefault();
    var that = this;
    var url = $(that).data('url');
    var val = $('.select2-container.component-select').select2('data').id;

    jQuery.ajax({
        'cache': false,
        'type': 'POST',
        'dataType': 'json',
        'data': 'type='+val,
        'success':
            function (response) {
                parseResponse(response);
            }, 'error': function (response) {
            alert(response.responseText);
        }, 'beforeSend': function () {
        }, 'complete': function () {
        }, 'url': url});

    return false;
});

function initSortableGrid() {
    if ($('span.sortable-grid .glyphicon-move').length) {
        var rowElements = $('.grid-view table tbody');
        if(rowElements.length) {
            rowElements.sortable({
                handle: '.glyphicon-move',
                update: function (event, ui) {
                    var sortableArray = [];
                    $(this).find('td span.sortable-grid').each(function(index){
                        sortableArray[index] = $(this).data('id');
                    })
                    var url = $('#sort-url').data('url')
                    var className =  $('#sort-url').data('class-name');
                    var sortableString = sortableArray.toString()
                    jQuery.ajax({
                        'cache': false,
                        'type': 'POST',
                        'dataType': 'json',
                        'data': {sort: sortableString, className: className},
                        'url': url});
                }
            })
        }
    }
}

function clearAllCheckboxForAttribute (attribute, id) {
    $('.checkbox input[data-attribute="' + attribute + '"]').each(function() {
        el = $(this);
        if (el.data('id') != id) {
            $(this).prop("checked", false);
        }
    });
}

function initPositionOnclickEvent() {
    $(document).on('change', '.position_input', function () {
        var url = $(this).data('url')
        var modelName = $(this).data('model-name')
        var modelId = $(this).data('id')
        var positionValue = $(this).val()

        if (isNormalInteger(positionValue)) {
            jQuery.ajax({
                'cache': false,
                'type': 'POST',
                'dataType': 'json',
                'data': {modelId: modelId, modelName: modelName, positionValue: positionValue},
                'url': url
            });
        } else {
            alert('The value entered is not valid. You can enter only numbers!')
            $(this).val('Only numbers!')
        }

    })
}

function isNormalInteger(str) {
    return /^\+?(0|[1-9]\d*)$/.test(str);
}