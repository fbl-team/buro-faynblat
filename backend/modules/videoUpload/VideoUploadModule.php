<?php

namespace backend\modules\videoUpload;

class VideoUploadModule extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\videoUpload\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
