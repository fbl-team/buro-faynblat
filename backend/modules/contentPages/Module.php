<?php

namespace backend\modules\contentPages;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\contentPages\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
