<?php

namespace backend\modules\contentPages\controllers;

use backend\components\BackendController;
use backend\components\Model;
use backend\components\StaticController;
use backend\modules\contentPages\models\ContentPages;
use yii\web\NotFoundHttpException;
use backend\modules\configuration\components\ConfigurationModel;
use common\components\model\ActiveRecord;
use common\components\model\Translateable;
use metalguardian\fileProcessor\behaviors\UploadBehavior;
use Yii;
use yii\base\ErrorException;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * ContentPagesController implements the CRUD actions for ContentPages model.
 */
class ContentPagesController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ContentPages::className();
    }

    public function actionIndex()
    {
        $model =  $this->getModelClass();
        $pageType = \Yii::$app->request->get('alias');
        $page = $model::find()->where(['alias' => $pageType])->one();

        if (!$page) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->redirect(['update', 'id' => $page->id]);
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        /** @var ActiveRecord $model */
        $model = $this->findModel($id);

        $config = $this->getRelatedFormActionConfig($model);
        if (!empty($config)) {
            return $this->relatedFormAction($model, $config);
        }

        if ($this->loadModels($model) && $model->save()) {
            \Yii::$app->getSession()->setFlash('info', \Yii::t('app', 'Record successfully updated!'));
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Menu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        \Yii::$app->getSession()->setFlash('info', \Yii::t('app', 'Record successfully updated!'));

        $this->redirect(['update', 'id' => $id]);
    }
}
