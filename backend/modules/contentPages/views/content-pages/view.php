<?php

use yii\helpers\Html;
use \backend\modules\seo\models\Robots;

/* @var $this yii\web\View */
/* @var $model \backend\components\BackendModel */

$this->title = $model->getTitle();
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title"><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="panel-body">
        <p>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?php if (!$model instanceof Robots) { ?>
                <?php if($model->showDeleteButton){ ?>
                    <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                            'method' => 'post',
                        ],
                    ]) ?>
                <?php } ?>
                <?php if($model->showCreateButton){ ?>
                    <?= Html::a(Yii::t('app', 'Create'), ['create'], ['class' => 'btn btn-success']) ?>
                <?php } ?>
            <?php } ?>
        </p>

        <?= \backend\components\LanguageDetailView::widget([
            'model' => $model,
            'attributes' => $model->getColumns('view'),
        ]) ?>
    </div>

</div>
