<?php

namespace backend\modules\contentPages\models;

use backend\modules\videoUpload\widgets\videoUpload\VideoUpload;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;
use backend\modules\imagesUpload\models\ImagesUploadModel;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%content_page_inside_block}}".
 *
 * @property integer $id
 * @property integer $content_page_id
 * @property string $label
 * @property string $description
 * @property integer $published
 * @property integer $position
 *
 * @property ContentPages $contentPage
 * @property ContentPageInsideBlockTranslation[] $translations
 */
class ContentPageInsideBlock extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;
    public $titleImage;
    public $titleVideo;

    /**
    * Temporary sign which used for saving images before model save
    * @var
    */
    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%content_page_inside_block}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['content_page_id'], 'required'],
            [['content_page_id', 'published', 'position'], 'integer'],
            [['label', 'description'], 'string'],
            [['content_page_id'], 'exist', 'targetClass' => ContentPages::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['sign'], 'string', 'max' => 255],
    
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/content_pages', 'ID'),
            'content_page_id' => Yii::t('back/content_pages', 'Content Page ID'),
            'label' => Yii::t('back/content_pages', 'Label'),
            'titleImage' => Yii::t('back/page_services', 'Element Image'),
            'titleVideo' => Yii::t('back/page_services', 'Element Video'),
            'description' => Yii::t('back/content_pages', 'Description'),
            'published' => Yii::t('back/content_pages', 'Published'),
            'position' => Yii::t('back/content_pages', 'Position'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContentPage()
    {
        return $this->hasOne(ContentPages::className(), ['id' => 'content_page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ContentPageInsideBlockTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return Yii::t('back/content_pages', 'Inside work block element');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'content_page_id',
                    // 'label:ntext',
                    // 'description:ntext',
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'content_page_id',
                    [
                        'attribute' => 'label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                    ],
                    'published:boolean',
                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new ContentPageInsideBlockSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
//            'content_page_id' => [
//                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
//                'items' => ContentPages::getItems(),
//                'options' => [
//                    'prompt' => '',
//                ],
//            ],
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT
            ],
            'description' => [
                'type' => ActiveFormBuilder::INPUT_TEXTAREA
            ],
            'titleImage' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'hint' => Yii::t('back/page_services', 'For correct view images on main page downloaded files must be in 700*450(px) format, or fold last one'),
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => 'titleImage',
                    'saveAttribute' => EntityToFile::TYPE_CONTENT_PAGE_INSIDE_BLOCK_IMAGE,
                    'aspectRatio' => 700/450, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ])
            ],
            'titleVideo' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'hint' => Yii::t('back/page_services', 'For correctly display video on web-site we recommend download 2 same video files with MP4 and WEBM formats'),
                'value' => VideoUpload::widget([
                    'model' => $this,
                    'attribute' => 'titleVideo',
                    'saveAttribute' => EntityToFile::TYPE_CONTENT_PAGE_INSIDE_BLOCK_VIDEO,
                    //'aspectRatio' => 300/200, //Пропорция для кропа
                    'multiple' => true, //Вкл/выкл множественную загрузку
                ])
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
//            'position' => [
//                'type' => ActiveFormBuilder::INPUT_TEXT,
//            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],

        ];
    }

    /**
    * @inheritdoc
    */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
    * @inheritdoc
    */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }
}
