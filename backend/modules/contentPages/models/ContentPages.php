<?php

namespace backend\modules\contentPages\models;

use common\helpers\ContentPagesHelper;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;
use backend\modules\imagesUpload\models\ImagesUploadModel;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%content_pages}}".
 *
 * @property integer $id
 * @property string $breadcrums_label
 * @property string $breadcrums_url
 * @property string $first_screen_label
 * @property string $first_screen_button_label
 * @property string $second_screen_label1
 * @property string $second_screen_label2
 * @property string $second_screen_description1
 * @property string $second_screen_description2
 * @property string $etap_block_label
 * @property string $inside_service_block_label
 * @property string $inside_service_block_description
 * @property string $etap_second_block_label
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ContentPagesTranslation[] $translations
 */
class ContentPages extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;
    public $titleImage;

    public $showDeleteButton = false;
    public $showCreateButton = false;

    /**
    * Temporary sign which used for saving images before model save
    * @var
    */
    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%content_pages}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['breadcrums_label', 'breadcrums_url', 'first_screen_label', 'first_screen_button_label', 'second_screen_label1', 'second_screen_label2', 'second_screen_description1', 'second_screen_description2', 'etap_block_label', 'inside_service_block_label', 'inside_service_block_description', 'etap_second_block_label'], 'string'],
            [['published', 'position'], 'integer'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['sign'], 'string', 'max' => 255],
    
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/content_pages', 'ID'),
            'titleImage' => Yii::t('back/content_pages', 'Slider images'),
            'breadcrums_label' => Yii::t('back/content_pages', 'Breadcrums Label'),
            'breadcrums_url' => Yii::t('back/content_pages', 'Breadcrums Url'),
            'first_screen_label' => Yii::t('back/content_pages', 'First Screen Label'),
            'first_screen_button_label' => Yii::t('back/content_pages', 'First Screen Button Label'),
            'second_screen_label1' => Yii::t('back/content_pages', 'Second Screen Label1'),
            'second_screen_label2' => Yii::t('back/content_pages', 'Second Screen Label2'),
            'second_screen_description1' => Yii::t('back/content_pages', 'Second Screen Description1'),
            'second_screen_description2' => Yii::t('back/content_pages', 'Second Screen Description2'),
            'etap_block_label' => Yii::t('back/content_pages', 'Etap Block Label'),
            'inside_service_block_label' => Yii::t('back/content_pages', 'Inside Service Block Label'),
            'inside_service_block_description' => Yii::t('back/content_pages', 'Inside Service Block Description'),
            'etap_second_block_label' => Yii::t('back/content_pages', 'Etap Second Block Label'),
            'published' => Yii::t('back/content_pages', 'Published'),
            'position' => Yii::t('back/content_pages', 'Position'),

            'footer_request_block_label' => Yii::t('back/content_pages', 'Footer Request Block Label'),
            'footer_request_block_button_label' => Yii::t('back/content_pages', 'Footer Request Block Button Label'),
            'footer_seo_block_label' => Yii::t('back/content_pages', 'Footer Seo Block Label'),
            'footer_seo_block_button_label' => Yii::t('back/content_pages', 'Footer Seo Block Button Label'),
            'footer_seo_block_text_left_column' => Yii::t('back/content_pages', 'Footer Seo Block Text Left Column'),
            'footer_seo_block_text_left_right' => Yii::t('back/content_pages', 'Footer Seo Block Text Right Column'),
            'footer_seo_block_button_label_hide' => Yii::t('back/content_pages', 'Footer Seo Block Button Hide Label'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'breadcrums_label',
            'first_screen_label',
            'first_screen_button_label',
            'second_screen_label1',
            'second_screen_label2',
            'second_screen_description1',
            'second_screen_description2',
            'etap_block_label',
            'inside_service_block_label',
            'inside_service_block_description',
            'etap_second_block_label',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
            'footer' => [
                'class' => \backend\components\FooterBehavior::className(),
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ContentPagesTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return ContentPagesHelper::getPageLabel($this->alias);
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    // 'breadcrums_label:ntext',
                    'breadcrums_url:url',
                    // 'first_screen_label:ntext',
                    // 'first_screen_button_label:ntext',
                    // 'second_screen_label1:ntext',
                    // 'second_screen_label2:ntext',
                    // 'second_screen_description1:ntext',
                    // 'second_screen_description2:ntext',
                    // 'etap_block_label:ntext',
                    // 'inside_service_block_label:ntext',
                    // 'inside_service_block_description:ntext',
                    // 'etap_second_block_label:ntext',
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    [
                        'attribute' => 'breadcrums_label',
                        'format' => 'html',
                    ],
                    'breadcrums_url:url',
                    [
                        'attribute' => 'first_screen_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'first_screen_button_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'second_screen_label1',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'second_screen_label2',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'second_screen_description1',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'second_screen_description2',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'etap_block_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'inside_service_block_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'inside_service_block_description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'etap_second_block_label',
                        'format' => 'html',
                    ],
                    'published:boolean',
                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new ContentPagesSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
            'form-set' => [
                Yii::t('back/content_pages', 'Main content') => [
                    'titleImage' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'hint' => Yii::t('back/projects', 'For correct view images on main page downloaded files must be in 1350*500(px) format, or fold last one'),
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'titleImage',
                            'saveAttribute' => EntityToFile::CONTENT_PAGE_SLIDER,
                            'aspectRatio' => 1350 / 500, //Пропорция для кропа
                            'multiple' => true, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'sign' => [
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => false,
                    ],
                    'breadcrums_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'breadcrums_url' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'hint' => Yii::t('back/content_pages', 'enter relative link, like - /about'),
                    ],
                    'first_screen_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'first_screen_button_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'second_screen_label1' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'second_screen_label2' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'second_screen_description1' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
                    'second_screen_description2' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
//                    'published' => [
//                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
//                    ],
//                    'position' => [
//                        'type' => ActiveFormBuilder::INPUT_TEXT,
//                    ],
                ],
                Yii::t('back/content_pages', 'First etap block') => [
                    'etap_block_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    $this->getRelatedFormConfig()['first_etap_block']
                ],
                Yii::t('back/content_pages', 'Second etap block') => [
                    'etap_second_block_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    $this->getRelatedFormConfig()['second_etap_block']
                ],
                Yii::t('back/content_pages', 'Inside work block') => [
                    'inside_service_block_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'inside_service_block_description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
                    $this->getRelatedFormConfig()['inside_work_block']
                ],
            ]

        ];
    }

    /**
    * @inheritdoc
    */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
    * @inheritdoc
    */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }

    public function getSliderImages()
    {
        return $this->hasMany(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::CONTENT_PAGE_SLIDER])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstEtapBlock()
    {
        return $this->hasMany(ContentPagesFirstEtapBlock::className(), ['content_page_id' => 'id'])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecondEtapBlock()
    {
        return $this->hasMany(ContentPagesSecondEtapBlock::className(), ['content_page_id' => 'id'])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInsideWorkBlock()
    {
        return $this->hasMany(ContentPageInsideBlock::className(), ['content_page_id' => 'id'])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getSecondEtapBlock()
//    {
//        return $this->hasMany(PageFooterElementsColumn1::className(), ['footer_page_id' => 'id'])
//            ->andWhere(['column_type' => FooterHelper::FOOTER_COLUMN_1])
//            ->orderBy('position');
//    }

    /**
     * @return array
     */
    public function getRelatedFormConfig()
    {
        return [
            'first_etap_block' => [
                'relation' => 'firstEtapBlock', //имя реляции (всегда с маленькой буквы, от названия метода реляции убрать get)
            ],
            'second_etap_block' => [
                'relation' => 'secondEtapBlock', //имя реляции (всегда с маленькой буквы, от названия метода реляции убрать get)
            ],
            'inside_work_block' => [
                'relation' => 'insideWorkBlock', //имя реляции (всегда с маленькой буквы, от названия метода реляции убрать get)
            ],

        ];
    }
}
