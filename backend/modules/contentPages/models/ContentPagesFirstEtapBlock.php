<?php

namespace backend\modules\contentPages\models;

use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;

/**
 * This is the model class for table "{{%content_pages_first_etap_block}}".
 *
 * @property integer $id
 * @property integer $content_page_id
 * @property string $label
 * @property string $description
 * @property integer $published
 * @property integer $position
 *
 * @property ContentPages $contentPage
 * @property ContentPagesFirstEtapBlockTranslation[] $translations
 */
class ContentPagesFirstEtapBlock extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%content_pages_first_etap_block}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['content_page_id'], 'required'],
            [['content_page_id', 'published', 'position'], 'integer'],
            [['label', 'description'], 'string'],
            [['content_page_id'], 'exist', 'targetClass' => ContentPages::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/content_pages', 'ID'),
            'content_page_id' => Yii::t('back/content_pages', 'Content Page ID'),
            'label' => Yii::t('back/content_pages', 'Label'),
            'description' => Yii::t('back/content_pages', 'Description'),
            'published' => Yii::t('back/content_pages', 'Published'),
            'position' => Yii::t('back/content_pages', 'Position'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContentPage()
    {
        return $this->hasOne(ContentPages::className(), ['id' => 'content_page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ContentPagesFirstEtapBlockTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return Yii::t('back/content_pages', 'First etap block element');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'content_page_id',
                    // 'label:ntext',
                    // 'description:ntext',
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'content_page_id',
                    [
                        'attribute' => 'label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                    ],
                    'published:boolean',
                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new ContentPagesFirstEtapBlockSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
//            'content_page_id' => [
//                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
//                'items' => ContentPages::getItems(),
//                'options' => [
//                    'prompt' => '',
//                ],
//            ],
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,

            ],
            'description' => [
                'type' => ActiveFormBuilder::INPUT_TEXTAREA,

            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
//            'position' => [
//                'type' => ActiveFormBuilder::INPUT_TEXT,
//            ],
            
        ];
    }

}
