<?php

namespace backend\modules\contentPages\models;

use Yii;

/**
* This is the model class for table "{{%content_page_inside_block_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $description
*/
class ContentPageInsideBlockTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%content_page_inside_block_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/content_pages', 'Label') . ' [' . $this->language . ']',
            'description' => Yii::t('back/content_pages', 'Description') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label', 'description'], 'string'],
         ];
    }
}
