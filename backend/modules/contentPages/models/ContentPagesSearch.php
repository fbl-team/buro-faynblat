<?php

namespace backend\modules\contentPages\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ContentPagesSearch represents the model behind the search form about `ContentPages`.
 */
class ContentPagesSearch extends ContentPages
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'published', 'position'], 'integer'],
            [['breadcrums_label', 'breadcrums_url', 'first_screen_label', 'first_screen_button_label', 'second_screen_label1', 'second_screen_label2', 'second_screen_description1', 'second_screen_description2', 'etap_block_label', 'inside_service_block_label', 'inside_service_block_description', 'etap_second_block_label'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContentPages::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'published' => $this->published,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'breadcrums_label', $this->breadcrums_label])
            ->andFilterWhere(['like', 'breadcrums_url', $this->breadcrums_url])
            ->andFilterWhere(['like', 'first_screen_label', $this->first_screen_label])
            ->andFilterWhere(['like', 'first_screen_button_label', $this->first_screen_button_label])
            ->andFilterWhere(['like', 'second_screen_label1', $this->second_screen_label1])
            ->andFilterWhere(['like', 'second_screen_label2', $this->second_screen_label2])
            ->andFilterWhere(['like', 'second_screen_description1', $this->second_screen_description1])
            ->andFilterWhere(['like', 'second_screen_description2', $this->second_screen_description2])
            ->andFilterWhere(['like', 'etap_block_label', $this->etap_block_label])
            ->andFilterWhere(['like', 'inside_service_block_label', $this->inside_service_block_label])
            ->andFilterWhere(['like', 'inside_service_block_description', $this->inside_service_block_description])
            ->andFilterWhere(['like', 'etap_second_block_label', $this->etap_second_block_label]);

        return $dataProvider;
    }
}
