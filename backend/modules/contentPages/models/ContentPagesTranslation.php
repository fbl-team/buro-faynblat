<?php

namespace backend\modules\contentPages\models;

use Yii;

/**
* This is the model class for table "{{%content_pages_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $breadcrums_label
* @property string $first_screen_label
* @property string $first_screen_button_label
* @property string $second_screen_label1
* @property string $second_screen_label2
* @property string $second_screen_description1
* @property string $second_screen_description2
* @property string $etap_block_label
* @property string $inside_service_block_label
* @property string $inside_service_block_description
* @property string $etap_second_block_label
*/
class ContentPagesTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%content_pages_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'breadcrums_label' => Yii::t('back/content_pages', 'Breadcrums Label') . ' [' . $this->language . ']',
            'first_screen_label' => Yii::t('back/content_pages', 'First Screen Label') . ' [' . $this->language . ']',
            'first_screen_button_label' => Yii::t('back/content_pages', 'First Screen Button Label') . ' [' . $this->language . ']',
            'second_screen_label1' => Yii::t('back/content_pages', 'Second Screen Label1') . ' [' . $this->language . ']',
            'second_screen_label2' => Yii::t('back/content_pages', 'Second Screen Label2') . ' [' . $this->language . ']',
            'second_screen_description1' => Yii::t('back/content_pages', 'Second Screen Description1') . ' [' . $this->language . ']',
            'second_screen_description2' => Yii::t('back/content_pages', 'Second Screen Description2') . ' [' . $this->language . ']',
            'etap_block_label' => Yii::t('back/content_pages', 'Etap Block Label') . ' [' . $this->language . ']',
            'inside_service_block_label' => Yii::t('back/content_pages', 'Inside Service Block Label') . ' [' . $this->language . ']',
            'inside_service_block_description' => Yii::t('back/content_pages', 'Inside Service Block Description') . ' [' . $this->language . ']',
            'etap_second_block_label' => Yii::t('back/content_pages', 'Etap Second Block Label') . ' [' . $this->language . ']',

            'footer_request_block_label' => Yii::t('back/content_pages', 'Footer Request Block Label') . ' [' . $this->language . ']',
            'footer_request_block_button_label' => Yii::t('back/content_pages', 'Footer Request Block Button Label') . ' [' . $this->language . ']',
            'footer_seo_block_label' => Yii::t('back/content_pages', 'Footer Seo Block Label') . ' [' . $this->language . ']',
            'footer_seo_block_button_label' => Yii::t('back/content_pages', 'Footer Seo Block Button Label') . ' [' . $this->language . ']',
            'footer_seo_block_text_left_column' => Yii::t('back/content_pages', 'Footer Seo Block Text Left Column') . ' [' . $this->language . ']',
            'footer_seo_block_text_left_right' => Yii::t('back/content_pages', 'Footer Seo Block Text Right Column') . ' [' . $this->language . ']',
            'footer_seo_block_button_label_hide' => Yii::t('back/content_pages', 'Footer Seo Block Button Hide Label') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['breadcrums_label', 'first_screen_label', 'first_screen_button_label', 'second_screen_label1', 'second_screen_label2', 'second_screen_description1', 'second_screen_description2', 'etap_block_label', 'inside_service_block_label', 'inside_service_block_description', 'etap_second_block_label'], 'string'],
            [['footer_seo_block_button_label_hide', 'footer_request_block_label', 'footer_request_block_button_label', 'footer_seo_block_label', 'footer_seo_block_button_label', 'footer_seo_block_text_left_column', 'footer_seo_block_text_left_right'], 'safe']
        ];
    }
}
