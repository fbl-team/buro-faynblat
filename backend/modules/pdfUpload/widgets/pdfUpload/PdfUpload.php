<?php
namespace backend\modules\pdfUpload\widgets\pdfUpload;

use backend\modules\pdfUpload\models\PdfUploadModel;
use common\components\model\ActiveRecord;
use common\models\EntityToFile;
use kartik\file\FileInput;
use metalguardian\fileProcessor\helpers\FPM;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class PdfUpload
 * @package backend\modules\imagesUpload\widgets\pdfUpload
 */
class PdfUpload extends Widget
{
    /**
     * @var ActiveRecord $model
     */
    public $model;

    /**
     * @var string $attribute
     */
    public $attribute;

    /**
     * @var string saveAttribute
     */
    public $saveAttribute = null;

    /**
     * @var string
     */
    public $uploadUrl;

    /**
     * @var bool
     */
    public $multiple = true;

    /**
     * @var float
     */
    public $aspectRatio = 0;


    public function run()
    {
        if (!$this->model || !$this->attribute) {
            return null;
        }

        $extraData = $this->model->isNewRecord
            ? ['sign' => $this->model->sign]
            : ['id' => $this->model->id];

        $previewImages = [];
        $previewImagesConfig = [];

        $existModelImages = EntityToFile::find()->where('entity_model_name = :emn', [':emn' => $this->model->formName()]);
        if ($this->saveAttribute !== null) {
            $existModelImages->andWhere('attribute = :attr', [':attr' => $this->saveAttribute]);
        }
        $existModelImages = $this->model->isNewRecord
            ? $existModelImages->andWhere('temp_sign = :ts', [':ts' => $this->model->sign])
            : $existModelImages->andWhere('entity_model_id = :id', [':id' => $this->model->id]);

        $existModelImages = $existModelImages->orderBy('position DESC')->all();

        /**
         * @var \common\models\EntityToFile $file
         */
        foreach ($existModelImages as $image) {
            $fileName = $image->file->base_name.'.'.$image->file->extension;
            $url = \Yii::$app->urlManager->createAbsoluteUrl(FPM::originalSrc($image->file_id));
            $previewImages[] = $this->render('_pdf_preview', compact('url'));
            $previewImagesConfig[] = [
                'caption' => $fileName,
                'width' => '120px',
                'url' => PdfUploadModel::deleteImageUrl(['id' => $image->id]),
                'key' => $image->id,
            ];
        }

        $multiple = $this->multiple ? 'true' : 'false';

        $output = Html::hiddenInput('urlForSorting', PdfUploadModel::sortImagesUrl(), ['id' => 'urlForSorting']);
        $output .= Html::hiddenInput('aspectRatio', $this->aspectRatio, ['class' => 'aspect-ratio']);

        $index = $this->model->relModelIndex;
        $attribute = $index === null ? $this->attribute : "[$index]$this->attribute";
        $uploadUrl = PdfUploadModel::uploadUrl([
            'model_name' => $this->model->className(),
            'attribute' => $attribute,
            'entity_attribute' => $this->saveAttribute,
        ]);

        $output .= FileInput::widget(
            [
                'language' => 'ru',
                'model' => $this->model,
                'attribute' => $attribute,
                'options' => [
                    'multiple' => $this->multiple,
                    'accept' => 'application/pdf'
                ],
                'pluginOptions' => [
                    'dropZoneEnabled' => false,
                    'browseClass' => 'btn btn-success',
                    'browseIcon' => '<i class="glyphicon glyphicon-picture"></i> ',
                    'removeClass' => "btn btn-danger",
                    'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> ',
                    'uploadClass' => "btn btn-info",
                    'uploadIcon' => '<i class="glyphicon glyphicon-upload"></i> ',
                    'uploadUrl' => $uploadUrl,
                    'allowedFileExtensions' => ['pdf'],
                    'maxFileCount' => 15,
                    'allowedPreviewTypes' => ['document'],
                    'uploadExtraData' => $extraData,
                    'initialPreview' => $previewImages,
                    'initialPreviewConfig' => $previewImagesConfig,
//                    'previewTemplates' => ['document'],
//                    'previewSettings' => [
//                        'video' =>  ['width' =>  "280px", 'height' => "170px"],
//                    ],
                    'overwriteInitial' => false,
                    'showRemove' => false,
                    'showPreview' => true,
                    'fileActionSettings' => [
                        'indicatorSuccess' => $this->render('_success_buttons_template')
                    ],
                ],
                'pluginEvents' => [
                    'filebrowse' => "function(file, previewId, index, reader) {
                        var multiple = $multiple;
                        if (!multiple) {
                            var buttonId = file.target.id;
                            var filePreview = $('#' + buttonId).parents('.file-input');
                            var isImage = filePreview.find('.file-preview-frame').length;
                            if (isImage) {
                                event.preventDefault();
                                alert('Можно загрузить только одно видео! Для загрузки нового, удалите первое.');
                            }
                        }
                    }",
                    'fileuploaded' => 'function(event, data, previewId, index) {
                       var elem = $("#"+previewId).find(".file-actions .file-upload-indicator .kv-file-remove");
                       var cropElem = $("#"+previewId).find(".file-actions .crop-link");
                       var img = $("#"+previewId).find("img");
                       //id for cropped image replace
                       img.attr("id", "preview-image-"+data.response.imgId);

                       elem.attr("data-url", data.response.deleteUrl);
                       elem.attr("data-key", data.response.id);
                       cropElem.attr("href", data.response.cropUrl);

                       //Resort images
                       saveSort();

                       //Fix crop url for old images
//                       fixMultiUploadImageCropUrl();
                    }',
//                    'fileloaded' => 'function(file, previewId, index, reader) {
//                        //Fix url for old images
//                        fixMultiUploadImageCropUrl();
//                    }'
                ]
            ]
        );

        $output .= '<br>';
        $output .= $this->render('_modal');

        return $output;
    }
}
