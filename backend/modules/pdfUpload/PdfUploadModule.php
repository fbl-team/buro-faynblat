<?php

namespace backend\modules\pdfUpload;

class PdfUploadModule extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\pdfUpload\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
