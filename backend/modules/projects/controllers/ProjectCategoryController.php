<?php

namespace backend\modules\projects\controllers;

use backend\components\BackendController;
use backend\modules\projects\models\ProjectCategory;

/**
 * ProjectCategoryController implements the CRUD actions for ProjectCategory model.
 */
class ProjectCategoryController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProjectCategory::className();
    }
}
