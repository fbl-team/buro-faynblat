<?php

namespace backend\modules\projects\controllers;

use backend\components\BackendController;
use backend\modules\projects\models\ProjectsStyles;

/**
 * ProjectsStylesController implements the CRUD actions for ProjectsStyles model.
 */
class ProjectsStylesController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return ProjectsStyles::className();
    }
}
