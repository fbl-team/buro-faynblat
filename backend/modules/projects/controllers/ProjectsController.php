<?php

namespace backend\modules\projects\controllers;

use backend\components\BackendController;
use backend\helpers\CheckboxHelper;
use backend\modules\projects\models\Projects;
use Yii;
use yii\helpers\Json;

/**
 * ProjectsController implements the CRUD actions for Projects model.
 */
class ProjectsController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Projects::className();
    }

    public function actionGetContentByType()
    {
        $typeId = Yii::$app->request->post('type');

        if ($typeId >= 0) {
            $articleConstructorHelper = Yii::$container->get('ProjectsConstructor');

            return Json::encode([
                'append' => [
                    [
                        'what' => '.template-list-constructor',
                        'data' => $this->renderAjax('content-entity', [
                            'constructor' => $articleConstructorHelper,
                            'type' => $typeId
                        ])
                    ],
                ],
            ]);
        }

        return false;
    }

    /**
     *
     */
    public function actionAjaxCheckbox()
    {
        $modelID = Yii::$app->request->post('modelId');
        $modelName = Yii::$app->request->post('modelName');
        $attribute = Yii::$app->request->post('attribute');

        if (Yii::$app->request->isAjax && $modelID && $modelName && $attribute) {
            $model = $modelName::findOne($modelID);
            if ($model) {
                $model->$attribute = $model->$attribute ? 0 : 1;
                $model->save(false);

                CheckboxHelper::unCheckAllExcept($modelName, $modelID, $attribute);
            }
        }
    }
}
