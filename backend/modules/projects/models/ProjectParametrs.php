<?php

namespace backend\modules\projects\models;

use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;

/**
 * This is the model class for table "{{%project_parametrs}}".
 *
 * @property integer $id
 * @property integer $project_id
 * @property string $label
 * @property string $description
 *
 * @property Projects $project
 * @property ProjectParametrsTranslation[] $translations
 */
class ProjectParametrs extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_parametrs}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['project_id'], 'required'],
            [['project_id'], 'integer'],
            [['label', 'description'], 'string'],
            [['project_id'], 'exist', 'targetClass' => Projects::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/projects', 'ID'),
            'project_id' => Yii::t('back/projects', 'Project ID'),
            'label' => Yii::t('back/projects', 'Label'),
            'description' => Yii::t('back/projects', 'Description'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProjectParametrsTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/projects', 'Project Parametrs');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'project_id',
                    // 'label:ntext',
                    // 'description:ntext',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'project_id',
                    [
                        'attribute' => 'label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                    ],
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new ProjectParametrsSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
//            'project_id' => [
//                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
//                'items' => Projects::getItems(),
//                'options' => [
//                    'prompt' => '',
//                ],
//            ],
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'description' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],

            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
//            'position' => [
//                'type' => ActiveFormBuilder::INPUT_TEXT,
//            ],
            
        ];
    }

}
