<?php

namespace backend\modules\projects\models;

use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;

/**
 * This is the model class for table "{{%project_category}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $published
 * @property integer $position
 *
 * @property ProjectCategoryTranslation[] $translations
 * @property Projects[] $projects
 */
class ProjectCategory extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [['label'], 'string'],
            [['published', 'position'], 'integer'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/project_category', 'ID'),
            'label' => Yii::t('back/project_category', 'Label'),
            'published' => Yii::t('back/project_category', 'Published'),
            'position' => Yii::t('back/project_category', 'Position'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProjectCategoryTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Projects::className(), ['category_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('project_category', 'Project Category');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    [
                        'class' => 'backend\components\SortingSerialColumn',
                    ],
                    // 'id',
                    'label',
                    'published:boolean',
//                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'label',
                    'published:boolean',
//                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new ProjectCategorySearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
//            'position' => [
//                'type' => ActiveFormBuilder::INPUT_TEXT,
//            ],
            
        ];
    }

}
