<?php

namespace backend\modules\projects\models;

use Yii;

/**
* This is the model class for table "{{%projects_styles_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
*/
class ProjectsStylesTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%projects_styles_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/projects_styles', 'Label') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [['label'], 'string', 'max' => 255],
         ];
    }
}
