<?php

namespace backend\modules\projects\models;

use backend\helpers\CheckboxHelper;
use backend\helpers\TypicalFunction;
use kartik\select2\Select2;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;
use backend\modules\imagesUpload\models\ImagesUploadModel;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%projects}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $city_and_square
 * @property integer $category_id
 * @property integer $show_on_main
 * @property integer $show_on_about
 * @property integer $show_on_services
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ProjectContent[] $projectContents
 * @property ProjectCategory $category
 * @property ProjectsTranslation[] $translations
 */
class Projects extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;
    public $bigImage;
    public $smallImage;

    private $constructor;

    public $content = [];
    public $relatedProjects = [];
    public $styles = [];

    /**
     * Temporary sign which used for saving images before model save
     * @var
     */
    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }

    /**
     * Projects constructor.
     * @param array $config
     * @param null $constructor
     */
    public function __construct(array $config = [], $constructor = null)
    {
        parent::__construct($config);
        $this->constructor = Yii::$container->get('ProjectsConstructor');
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%projects}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'category_id', 'alias'], 'required'],
            [['label', 'alias', 'city_and_square'], 'string'],
            [['category_id', 'show_on_main', 'show_on_about', 'show_on_services', 'published', 'position'], 'integer'],
            [['category_id'], 'exist', 'targetClass' => ProjectCategory::className(), 'targetAttribute' => 'id'],
            [['show_on_main'], 'default', 'value' => 1],
            [['show_on_about'], 'default', 'value' => 1],
            [['show_on_services'], 'default', 'value' => 1],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['sign'], 'string', 'max' => 255],
            [['project_parameters', 'content', 'relatedProjects', 'styles'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/projects', 'ID'),
            'bigImage' => Yii::t('back/projects', 'Big image'),
            'relatedProjects' => Yii::t('back/projects', 'Related projects'),
            'styles' => Yii::t('back/projects', 'Project Styles'),
            'smallImage' => Yii::t('back/projects', 'Small image'),
            'label' => Yii::t('back/projects', 'Label'),
            'city_and_square' => Yii::t('back/projects', 'City And Square'),
            'category_id' => Yii::t('back/projects', 'Category ID'),
            'show_on_main' => Yii::t('back/projects', 'Show on main'),
            'show_on_about' => Yii::t('back/projects', 'Show on about'),
            'show_on_services' => Yii::t('back/projects', 'Show on services'),
            'published' => Yii::t('back/projects', 'Published'),
            'position' => Yii::t('back/projects', 'Position'),
            'project_parameters' => Yii::t('back/projects', 'Project parameters'),

            'footer_request_block_label' => Yii::t('back/projects', 'Footer Request Block Label'),
            'footer_request_block_button_label' => Yii::t('back/projects', 'Footer Request Block Button Label'),
            'footer_seo_block_label' => Yii::t('back/projects', 'Footer Seo Block Label'),
            'footer_seo_block_button_label' => Yii::t('back/projects', 'Footer Seo Block Button Label'),
            'footer_seo_block_text_left_column' => Yii::t('back/projects', 'Footer Seo Block Text Left Column'),
            'footer_seo_block_text_left_right' => Yii::t('back/projects', 'Footer Seo Block Text Right Column'),
            'footer_seo_block_button_label_hide' => Yii::t('back/projects', 'Footer Seo Block Button Hide Label'),
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'city_and_square',
            'project_parameters',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
            'footer' => [
                'class' => \backend\components\FooterBehavior::className(),
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectContents()
    {
        return $this->hasMany(ProjectContent::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProjectCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(ProjectsTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/projects', 'Projects');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    // 'city_and_square:ntext',
                    [
                        'attribute' => 'category_id',
                        'filter' => ProjectCategory::getItems(),
                        'format' => 'raw',
                        'value' => function ($data) {

                            return ProjectCategory::getItems()[$data->category_id] ? ProjectCategory::getItems()[$data->category_id] : 'category not selected!';

                        }
                    ],

                    'show_on_main:boolean',
                    'show_on_about:boolean',
                    'show_on_services:boolean',
                    'published:boolean',
                    [
                        'attribute' => 'position',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::input('text', null, $data->position, [
                                'class' => 'form-control position_input',
                                'data-url' => '/site/position',
                                'data-id' => $data->id,
                                'data-model-name' => $data->className(),
                            ]);
                        }
                    ],
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'label',
                    'alias',
                    'city_and_square',
                    'project_parameters',
                    [
                        'attribute' => 'city_and_square',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'category_id',
                        'format' => 'raw',
                        'value' => ProjectCategory::getItems()[$this->category_id] ? ProjectCategory::getItems()[$this->category_id] : 'category not selected!'
                    ],
                    [
                        'attribute' => 'bigImage',
                        'format' => 'raw',
                        'value' => TypicalFunction::getImage($this, 'projectBigImage', ['project', 'back'])
                    ],
                    [
                        'attribute' => 'smallImage',
                        'format' => 'raw',
                        'value' => TypicalFunction::getImage($this, 'projectSmallImage', ['project', 'back'])
                    ],
                    'show_on_main:boolean',
                    'show_on_about:boolean',
                    'show_on_services:boolean',
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new ProjectsSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        return [
            'form-set' => [
                Yii::t('back/projects', 'Main content') => [
                    'bigImage' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'hint' => Yii::t('back/projects', 'For correct view images on main page downloaded files must be in 700*455(px) format, or fold last one'),
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'bigImage',
                            'saveAttribute' => EntityToFile::TYPE_PROJECT_BIG_IMAGE,
                            'aspectRatio' => 700 / 455, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'smallImage' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'hint' => Yii::t('back/projects', 'For correct view images on main page downloaded files must be in 400*360(px) format, or fold last one'),
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'smallImage',
                            'saveAttribute' => EntityToFile::TYPE_PROJECT_SMALL_IMAGE,
                            'aspectRatio' => 400 / 360, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'sign' => [
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => false,
                    ],
                    'label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                            'class' => 's_name form-control'
                        ],
                    ],
                    'alias' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                            'class' => 's_alias form-control'
                        ],
                    ],
                    'city_and_square' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'project_parameters' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'hint' => Yii::t('back/projects', 'To select the words that will appear bold styles use b - tag. For example: text without bold &ltb&gt BOLD &lt/b&gt text without bold'),
                    ],
                    'category_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => ProjectCategory::getItems(),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'styles' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => Select2::className(),
                        'options' => [
                            'data' => ArrayHelper::map(ProjectsStyles::find()->orderBy('position DESC')->asArray()->all(), 'id', 'label'),
                            'options' => ['multiple' => true, 'placeholder' => Yii::t('back/projects', 'Select styles for project')]
                        ]
                    ],
                    'relatedProjects' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => Select2::className(),
                        'options' => [
                            'data' => ArrayHelper::map(Projects::find()->orderBy('position DESC')->asArray()->all(), 'id', 'label'),
                            'options' => ['multiple' => true, 'placeholder' => Yii::t('back/projects', 'Select related projects')]
                        ]
                    ],
                    'show_on_main' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
//                        'options' => [
//                            'checked' => false,
//                        ]
                    ],
                    'show_on_about' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
//                        'options' => [
//                            'checked' => false,
//                        ]
                    ],
                    'show_on_services' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
//                        'options' => [
//                            'checked' => false,
//                        ]
                    ],
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'position' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                ],
                Yii::t('back/projects', 'Content constructor') => [
                    'content' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => $this->constructor->getContent($this),
                        'options' => [
                            'class' => 'article-constructor'
                        ]
                    ]
                ],
//                Yii::t('back/projects', 'Project parameters') => [
//                    $this->getRelatedFormConfig()['project_parameters']
//                ],
            ]

        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->constructor->saveConstructorData($this);

        EntityToFile::updateImages($this->id, $this->sign);

        $this->saveRelatedProjects();
        $this->saveStyles();
        $this->saveCheckboxValues();
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }

    /**
     *
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->constructor->loadContent($this);

        $this->loadRelatedProjects();
        $this->loadStyles();
    }

    public function getProjectBigImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_PROJECT_BIG_IMAGE])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

    public function getProjectSmallImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_PROJECT_SMALL_IMAGE])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

    public function loadRelatedProjects()
    {
        $this->relatedProjects = ArrayHelper::map(ProjectRelatedProject::find()->where('project_id = :tid', [':tid' => $this->id])->all(), 'id', 'related_project_id');
    }

    public function saveRelatedProjects()
    {
        ProjectRelatedProject::deleteAll('project_id = :tid', [':tid' => $this->id]);

        if (is_array($this->relatedProjects)) {
            foreach ($this->relatedProjects as $relatedProject) {
                $model = new ProjectRelatedProject();
                $model->project_id = $this->id;
                $model->related_project_id = (int)$relatedProject;
                $model->save(false);
            }
        }
    }

    public function loadStyles()
    {
        $this->styles = ArrayHelper::map(ProjectsStylesRelated::find()->where('project_id = :tid', [':tid' => $this->id])->all(), 'id', 'project_style_id');
    }

    public function saveStyles()
    {
        ProjectsStylesRelated::deleteAll('project_id = :tid', [':tid' => $this->id]);

        if (is_array($this->styles)) {
            foreach ($this->styles as $relatedStyles) {
                $model = new ProjectsStylesRelated();
                $model->project_id = $this->id;
                $model->project_style_id = (int)$relatedStyles;
                $model->save(false);
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectsParameters()
    {
        return $this->hasMany(ProjectParametrs::className(), ['project_id' => 'id'])
            ->orderBy('position');
    }

    public function saveCheckboxValues()
    {
//        if ($this->show_on_main) {
//            CheckboxHelper::unCheckAllExcept($this, $this->id, 'show_on_main');
//        }
        if ($this->show_on_about) {
            CheckboxHelper::unCheckAllExcept($this, $this->id, 'show_on_about');
        }
        if ($this->show_on_services) {
            CheckboxHelper::unCheckAllExcept($this, $this->id, 'show_on_services');
        }
    }

    /**
     * @return array
     */
//    public function getRelatedFormConfig()
//    {
//        return [
//            'project_parameters' => [
//                'relation' => 'projectsParameters', //имя реляции (всегда с маленькой буквы, от названия метода реляции убрать get)
//            ],
//        ];
//    }
}
