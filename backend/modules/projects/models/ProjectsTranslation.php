<?php

namespace backend\modules\projects\models;

use Yii;

/**
* This is the model class for table "{{%projects_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $city_and_square
*/
class ProjectsTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%projects_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/projects', 'Label') . ' [' . $this->language . ']',
            'city_and_square' => Yii::t('back/projects', 'City And Square') . ' [' . $this->language . ']',

            'footer_request_block_label' => Yii::t('back/projects', 'Footer Request Block Label') . ' [' . $this->language . ']',
            'footer_request_block_button_label' => Yii::t('back/projects', 'Footer Request Block Button Label') . ' [' . $this->language . ']',
            'footer_seo_block_label' => Yii::t('back/projects', 'Footer Seo Block Label') . ' [' . $this->language . ']',
            'footer_seo_block_button_label' => Yii::t('back/projects', 'Footer Seo Block Button Label') . ' [' . $this->language . ']',
            'footer_seo_block_text_left_column' => Yii::t('back/projects', 'Footer Seo Block Text Left Column') . ' [' . $this->language . ']',
            'footer_seo_block_text_left_right' => Yii::t('back/projects', 'Footer Seo Block Text Right Column') . ' [' . $this->language . ']',
            'footer_seo_block_button_label_hide' => Yii::t('back/projects', 'Footer Seo Block Button Hide Label') . ' [' . $this->language . ']',
            'project_parameters' => Yii::t('back/projects', 'Project parameters') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [['label', 'city_and_square'], 'string'],
            [['project_parameters', 'footer_seo_block_button_label_hide', 'footer_request_block_label', 'footer_request_block_button_label', 'footer_seo_block_label', 'footer_seo_block_button_label', 'footer_seo_block_text_left_column', 'footer_seo_block_text_left_right'], 'safe']
         ];
    }
}
