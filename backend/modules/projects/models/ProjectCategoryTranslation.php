<?php

namespace backend\modules\projects\models;

use Yii;

/**
* This is the model class for table "{{%project_category_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $content
*/
class ProjectCategoryTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%project_category_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/project_category', 'label') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [['label'], 'string'],
         ];
    }
}
