<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 7/13/16
 * Time: 4:19 PM
 */

namespace backend\modules\projects\models;


interface IConstructorStrategy
{
    public function getContent($model, $key = null);
    
    public function getLabel();
    
    public function saveEntity($contentArr, $model);
    
    public function loadData($model, $component);
}