<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 7/13/16
 * Time: 4:29 PM
 */

namespace backend\modules\article\models\articleConstructorStrategies;


use backend\modules\article\models\Article;
use backend\modules\article\models\ArticleConstructorData;
use backend\modules\article\models\ArticleConstructorDataTranslation;
use backend\modules\article\models\IConstructorStrategy;
use common\helpers\LanguageHelper;
use vova07\imperavi\Widget;
use Yii;
use yii\helpers\Html;
use yii\helpers\Url;

class TextStrategy implements IConstructorStrategy
{
    const TYPE = 1;
    
    public function getContent($model, $key = null)
    {
        $uniqueKey = \Yii::$app->security->generateRandomString(10);

        if ($key === null) {
            $key = $uniqueKey;
        }
        
        $content = '';

        $attributeContent = $this->createAttribute('content', $key);

        $content .= Html::label($this->getLabel(), $attributeContent, ['class' => 'form-control label-constructor-meta-data']);
        $content .= Widget::widget(
            [
                'model' => $model,
                'attribute' => $attributeContent,
                'settings' => [
                    'buttons' => [
                        'html',
                        'formatting',
                        'bold',
                        'italic',
                        'deleted',
                        'unorderedlist',
                        'orderedlist',
                        'outdent',
                        'indent',
                        'image',
                        'file',
                        'link',
                        'alignment',
                        'horizontalrule',
                        'table'
                    ],
                    'imageUpload' =>  Url::to(['image-upload']),
                    'plugins' => [
                        'table'
                    ],
                    'lang' => 'ru',
                    'minHeight' => 250,
                    'pastePlainText' => true,
                    'buttonSource' => true,
                    'replaceDivs' => true,
                    'paragraphize' => true,
                ],
            ]
        );
        foreach (LanguageHelper::getLanguageModels() as $languageModel) {
            if (LanguageHelper::getDefaultLanguage()->locale !== $languageModel->locale) {
                $attributeContent = $this->createAttribute('content', $key, $languageModel->locale);

                $content .= Html::label($this->getLabel() . '[' . $languageModel->locale . ']', $attributeContent, ['class' => 'form-control label-constructor-meta-data']);
                $content .= Widget::widget(
                    [
                        'model' => $model,
                        'attribute' => $attributeContent,
                        'settings' => [
                            'buttons' => [
                                'html',
                                'formatting',
                                'bold',
                                'italic',
                                'deleted',
                                'unorderedlist',
                                'orderedlist',
                                'outdent',
                                'indent',
                                'image',
                                'file',
                                'link',
                                'alignment',
                                'horizontalrule',
                                'table'
                            ],
                            'imageUpload' =>  Url::to(['image-upload']),
                            'plugins' => [
                                'table'
                            ],
                            'lang' => 'ru',
                            'minHeight' => 250,
                            'pastePlainText' => true,
                            'buttonSource' => true,
                            'replaceDivs' => true,
                            'paragraphize' => true,
                        ],
                    ]
                );
            }
        }
        
        return $content;
    }

    public function getLabel()
    {
        return Yii::t('back/articleConstructorHelper', 'Text');
    }

    /**
     * @param $contentArr
     * @param Article $model
     */
    public function saveEntity($contentArr, $model)
    {
        $constructorDataModel = new ArticleConstructorData();
        $data = $contentArr['default_locale'];
        
        $constructorDataModel->article_id = $model->id;
        $constructorDataModel->type = static::TYPE;
        $constructorDataModel->content = $data['content'];
        $constructorDataModel->save();

        foreach (LanguageHelper::getLanguageModels() as $languageModel) {
            if (LanguageHelper::getDefaultLanguage()->locale !== $languageModel->locale) {
                $constructorDataModelT = new ArticleConstructorDataTranslation();
                $data = $contentArr[$languageModel->locale];

                $constructorDataModelT->language = $languageModel->locale;
                $constructorDataModelT->model_id = $constructorDataModel->id;
                $constructorDataModelT->content = $data['content'];
                $constructorDataModelT->save();
            }
        }
    }

    /**
     * @param Article $model
     * @param ArticleConstructorData $component
     */
    public function loadData($model, $component)
    {
        $model->content[$component->id][static::TYPE]['default_locale']['content'] = $component->content;
        /* @var ArticleConstructorDataTranslation[] $translationComponents */
        $translationComponents = $component->translations;
        foreach ($translationComponents as $translationComponent) {
            if (LanguageHelper::getDefaultLanguage()->locale !== $translationComponent->language) {
                $model->content[$component->id][static::TYPE][$translationComponent->language]['content'] = $translationComponent->content;
            }
        }
    }

    public function createAttribute($name, $key, $locale = 'default_locale')
    {
        return 'content[' . $key . '][' . static::TYPE . '][' . $locale . '][' . $name . ']';
    }
}
