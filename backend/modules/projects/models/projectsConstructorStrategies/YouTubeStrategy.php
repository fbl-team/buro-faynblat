<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 7/13/16
 * Time: 4:29 PM
 */

namespace backend\modules\article\models\articleConstructorStrategies;


use backend\modules\article\models\Article;
use backend\modules\article\models\ArticleConstructorData;
use backend\modules\article\models\IConstructorStrategy;
use Yii;
use yii\helpers\Html;

class YouTubeStrategy implements IConstructorStrategy
{
    const TYPE = 3;
    
    public function getContent($model, $key = null)
    {
        $uniqueKey = \Yii::$app->security->generateRandomString(10);

        if ($key === null) {
            $key = $uniqueKey;
        }
        
        $content = '';

        $attribute = $this->createAttribute('you_tube_link', $key);

        $content .= Html::label($this->getLabel(), $attribute, ['class' => 'form-control label-constructor-meta-data']);
        $content .=  Html::activeInput('text', $model, $attribute, ['class' => 'form-control input-constructor-meta-data', 'placeholder' => Yii::t('back/articleConstructorHelper', 'Enter YouTube link')]);
        
        return $content;
    }

    public function getLabel()
    {
        return Yii::t('back/articleConstructorHelper', 'YouTube link');
    }

    /**
     * @param $contentArr
     * @param Article $model
     */
    public function saveEntity($contentArr, $model)
    {
        $constructorDataModel = new ArticleConstructorData();
        $data = $contentArr['default_locale'];
        
        $constructorDataModel->article_id = $model->id;
        $constructorDataModel->type = static::TYPE;
        $constructorDataModel->you_tube_link = $data['you_tube_link'];
        $constructorDataModel->save();
    }

    /**
     * @param Article $model
     * @param ArticleConstructorData $component
     */
    public function loadData($model, $component)
    {
        $model->content[$component->id][static::TYPE]['default_locale']['you_tube_link'] = $component->you_tube_link;
    }

    public function createAttribute($name, $key, $locale = 'default_locale')
    {
        return 'content[' . $key . '][' . static::TYPE . '][' . $locale . '][' . $name . ']';
    }
}