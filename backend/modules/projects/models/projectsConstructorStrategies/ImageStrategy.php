<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 7/14/16
 * Time: 12:32 PM
 */

namespace backend\modules\projects\models\projectsConstructorStrategies;


use backend\modules\article\models\Article;
use backend\modules\article\models\ArticleConstructorData;
use backend\modules\projects\models\IConstructorStrategy;
use backend\modules\imagesUpload\models\ImagesUploadModel;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use backend\modules\projects\models\ProjectContent;
use yii\helpers\Html;

abstract class ImageStrategy implements IConstructorStrategy
{
    /**
     * @return bool
     */
    abstract protected function isMultiUpload();
    abstract protected function getSaveAtributeConstant();

    public function getContent($model, $key = null)
    {
        $uniqueKey = \Yii::$app->security->generateRandomString(10);

        if ($key === null) {
            $key = $uniqueKey;
        }

        $content = '';

        $signForConstructorImage = \Yii::$app->security->generateRandomString();
        $uniqueI = \Yii::$app->security->generateRandomString(10);

        $attribute = $this->createAttribute('content', $key);
        $content .= Html::label($this->getLabel(), $attribute, ['class' => 'form-control label-constructor-meta-data']);

        if ($key) {
            $constructorModel  = ProjectContent::find()->where(['id' => $key])->one();
            if ($constructorModel) {
                $constructorImageModel = new ProjectContent();
                $constructorImageModel->setAttributes($constructorModel->getAttributes());
                $constructorImageModel->sign = $constructorModel->content;
            } else {
                $constructorImageModel = new ProjectContent();
                $constructorImageModel->sign = $signForConstructorImage;
            }
        } else {
            $constructorImageModel = new ProjectContent();
            $constructorImageModel->sign = $signForConstructorImage;
        }

        $content .= ImageUpload::widget([
            'model' => $constructorImageModel,
            'attribute' => "image[$uniqueI]",
            'saveAttribute' => $this->getSaveAtributeConstant(),
            'aspectRatio' => 483/644, //Пропорция для кропа
            'multiple' => $this->isMultiUpload(), //Вкл/выкл множественную загрузку
        ]);

        $content .=  Html::activeHiddenInput($model, $attribute, ['value' => $constructorImageModel->sign]);

        return $content;
    }

    abstract public function getLabel();

    /**
     * @param $contentArr
     * @param Article $model
     */
    public function saveEntity($contentArr, $model)
    {
        $constructorDataModel = new ProjectContent();
        $data = $contentArr['default_locale'];

        $constructorDataModel->project_id = $model->id;
        $constructorDataModel->type = static::TYPE;
        $constructorDataModel->content = $data['content'];
        $constructorDataModel->save();
    }

    /**
     * @param Article $model
     * @param ProjectContent $component
     */
    public function loadData($model, $component)
    {
        $model->content[$component->id][static::TYPE]['default_locale']['content'] = $component->content;
    }

    public function createAttribute($name, $key, $locale = 'default_locale')
    {
        return 'content[' . $key . '][' . static::TYPE . '][' . $locale . '][' . $name . ']';
    }
}