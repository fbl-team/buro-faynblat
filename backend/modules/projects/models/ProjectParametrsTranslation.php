<?php

namespace backend\modules\projects\models;

use Yii;

/**
* This is the model class for table "{{%project_parametrs_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $description
*/
class ProjectParametrsTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%project_parametrs_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/projects', 'Label') . ' [' . $this->language . ']',
            'description' => Yii::t('back/projects', 'Description') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label', 'description'], 'string'],
         ];
    }
}
