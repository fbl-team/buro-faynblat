<?php

namespace backend\modules\projects\models;

use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%projects_styles_related}}".
 *
 * @property integer $id
 * @property integer $project_id
 * @property integer $project_style_id
 *
 * @property Projects $project
 * @property ProjectsStyles $projectStyle
 */
class ProjectsStylesRelated extends \common\components\model\ActiveRecord implements BackendModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%projects_styles_related}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'project_style_id'], 'required'],
            [['project_id', 'project_style_id'], 'integer'],
            [['project_id'], 'exist', 'targetClass' => Projects::className(), 'targetAttribute' => 'id'],
            [['project_style_id'], 'exist', 'targetClass' => ProjectsStyles::className(), 'targetAttribute' => 'id'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/projects_styles', 'ID'),
            'project_id' => Yii::t('back/projects_styles', 'Project ID'),
            'project_style_id' => Yii::t('back/projects_styles', 'Project Style ID'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectStyle()
    {
        return $this->hasOne(ProjectsStyles::className(), ['id' => 'project_style_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Projects Styles Related');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'project_id',
                    'project_style_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'project_id',
                    'project_style_id',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new ProjectsStylesRelatedSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
            'project_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => Projects::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'project_style_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => ProjectsStyles::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            
        ];
    }

}
