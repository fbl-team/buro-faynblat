<?php

namespace backend\modules\projects\models;

use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%project_related_project}}".
 *
 * @property integer $id
 * @property integer $project_id
 * @property integer $related_project_id
 *
 * @property Projects $project
 * @property Projects $relatedProject
 */
class ProjectRelatedProject extends \common\components\model\ActiveRecord implements BackendModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_related_project}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'related_project_id'], 'required'],
            [['project_id', 'related_project_id'], 'integer'],
            [['project_id'], 'exist', 'targetClass' => Projects::className(), 'targetAttribute' => 'id'],
            [['related_project_id'], 'exist', 'targetClass' => Projects::className(), 'targetAttribute' => 'id'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/projects', 'ID'),
            'project_id' => Yii::t('back/projects', 'Project ID'),
            'related_project_id' => Yii::t('back/projects', 'Related Project ID'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'related_project_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Project Related Project');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'project_id',
                    'related_project_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'project_id',
                    'related_project_id',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new ProjectRelatedProjectSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
            'project_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => Projects::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'related_project_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => Projects::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            
        ];
    }

}
