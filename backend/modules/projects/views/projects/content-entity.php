<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 7/13/16
 * Time: 2:53 PM
 */

use backend\modules\article\helpers\ArticleConstructorHelper;
use backend\modules\article\models\Article;
use backend\modules\projects\models\Projects;

/* @var \backend\modules\projects\helpers\ProjectsConstructorHelper $constructor */
/* @var integer $type */

$model = new Projects();
echo $constructor->getContentByType($model, $type);