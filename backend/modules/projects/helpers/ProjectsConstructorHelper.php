<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 7/13/16
 * Time: 1:21 PM
 */

namespace backend\modules\projects\helpers;

use backend\modules\projects\models\IConstructorStrategy;
use backend\modules\projects\models\ProjectContent;
use backend\modules\projects\models\Projects;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class ProjectsConstructorHelper
{
    private $strategies = [];
    private $contentTypeList = [];
    
    public function addConstructorStrategy($type, IConstructorStrategy $constructorStrategy)
    {
        $this->strategies[$type] = $constructorStrategy;
        $this->contentTypeList[$type] = $constructorStrategy->getLabel();
    }

    public function removeConstructorStrategy($type)
    {
        if (isset($this->strategies[$type])) {
            unset($this->strategies[$type]);
            unset($this->contentTypeList[$type]);
        }
    }

    /**
     * @return IConstructorStrategy[]
     */
    public function getStrategies()
    {
        return $this->strategies;
    }

    /**
     * @return array
     */
    public function getContentTypeList()
    {
        return $this->contentTypeList;
    }
    
    public function loadContent($model)
    {
        /* @var ProjectContent[] $components*/
        $components = ProjectContent::find()->where(['project_id' => $model->id])->all();
        
        foreach ($components as $component) {
            $this->getStrategies()[$component->type]->loadData($model, $component);
        }
    }

    public function getContent(Projects $model)
    {
        $output = '';

        $output .= Html::beginTag('div', ['class' => 'form-group template-builder-constructor']);
        $output .= Html::beginTag('div', ['class' =>  'template-list-constructor']);
        $output .= Html::hiddenInput(Html::getInputName($model, 'content'), '');

        if (!empty ($model->content)) {
            foreach ($model->content as $key => $content) {
                foreach ($content as $typeId => $contentArr) {
                    $output .= $this->getContentByType($model, $typeId, $key);
                }
            }
        }
        $output .= Html::endTag('div');

        $output .= Html::beginTag('div', ['class' => 'form-group content-append-constructor']);
        $output .= Html::label(Yii::t('back/articleConstructorHelper', 'Add block'), 'component');
        $output .= Html::tag('br');
        $output .= Html::dropDownList(
            'component',
            [],
            static::getContentTypeList(),
            ['class' => 'form-control width-50 component-select']
        );
        $output .= Html::button(
            '<i class="glyphicon glyphicon-plus"></i>',
            [
                'class' => 'btn btn-success btn-template-builder-constructor',
                'data-url' => static::getContentByTypeUrl()
            ]
        );
        $output .= Html::endTag('div');

        $output .= Html::endTag('div');

        return $output;
    }

    public static function getContentByTypeUrl()
    {
        return static::createUrl('/projects/projects/get-content-by-type', []);
    }

    public static function createUrl($route, $params)
    {
        return Url::to(
            ArrayHelper::merge(
                [$route],
                $params
            )
        );
    }

    public function getContentByType($model, $typeId, $key = null)
    {
        $content = Html::beginTag('div', ['class' => 'form-group content-append-constructor filled']);
        $content .= Html::button(
            '<i class="glyphicon glyphicon-move"></i>',
            [
                'class' => 'btn btn-info btn-template-mover',
            ]
        );
        $content .= Html::button(
            '<i class="glyphicon glyphicon-trash"></i>',
            [
                'class' => 'btn btn-danger btn-template-delete-constructor',
            ]
        );
        
        $content .= $this->getStrategies()[$typeId]->getContent($model, $key);
        
        $content .= Html::endTag('div');

        return $content;
    }
    
    public function saveConstructorData($model)
    {
        $this->deleteConstructorData($model);

        if (!empty ($model->content)) {
            foreach ($model->content as $key => $content) {
                foreach ($content as $typeId => $contentArr) {
                    $this->getStrategies()[$typeId]->saveEntity($contentArr, $model);
                }
            }
        }
    }
    
    public function deleteConstructorData($model)
    {
        ProjectContent::deleteAll(['project_id' => $model->id]);
    }
}