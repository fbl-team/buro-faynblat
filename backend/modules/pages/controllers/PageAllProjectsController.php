<?php

namespace backend\modules\pages\controllers;

use backend\components\StaticController;
use backend\modules\pages\models\allProjects\PageAllProjects;

/**
 * PageAllProjectsController implements the CRUD actions for PageAllProjects model.
 */
class PageAllProjectsController extends StaticController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return PageAllProjects::className();
    }
}
