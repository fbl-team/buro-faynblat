<?php

namespace backend\modules\pages\controllers;

use backend\components\StaticController;
use backend\modules\pages\models\oneBlog\PageOneBlog;

/**
 * PageOneBlogController implements the CRUD actions for PageOneBlog model.
 */
class PageOneBlogController extends StaticController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return PageOneBlog::className();
    }
}
