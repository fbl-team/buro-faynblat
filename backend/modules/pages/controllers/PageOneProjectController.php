<?php

namespace backend\modules\pages\controllers;

use backend\components\StaticController;
use backend\modules\pages\models\oneProject\PageOneProject;

/**
 * PageOneProjectController implements the CRUD actions for PageOneProject model.
 */
class PageOneProjectController extends StaticController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return PageOneProject::className();
    }
}
