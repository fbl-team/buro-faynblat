<?php

namespace backend\modules\pages\controllers;

use backend\components\StaticController;
use backend\modules\pages\models\allBlogs\PageAllBlogs;

/**
 * PageAllBlogsController implements the CRUD actions for PageAllBlogs model.
 */
class PageAllBlogsController extends StaticController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return PageAllBlogs::className();
    }
}
