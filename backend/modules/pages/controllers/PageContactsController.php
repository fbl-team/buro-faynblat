<?php

namespace backend\modules\pages\controllers;

use backend\components\StaticController;
use backend\modules\pages\models\contacts\PageContacts;

/**
 * PageContactsController implements the CRUD actions for PageContacts model.
 */
class PageContactsController extends StaticController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return PageContacts::className();
    }
}
