<?php

namespace backend\modules\pages\controllers;

use backend\components\StaticController;
use backend\modules\pages\models\media\PageMedia;

/**
 * PageMediaController implements the CRUD actions for PageMedia model.
 */
class PageMediaController extends StaticController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return PageMedia::className();
    }
}
