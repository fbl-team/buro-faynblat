<?php

namespace backend\modules\pages\controllers;

use backend\components\BackendController;
use backend\components\StaticController;
use backend\modules\pages\models\careers\PageCareers;

/**
 * PageAboutController implements the CRUD actions for PageAbout model.
 */
class PageCareersController extends StaticController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return PageCareers::className();
    }
}
