<?php

namespace backend\modules\pages\controllers;

use backend\components\StaticController;
use backend\modules\pages\models\home\PageMain;

/**
 * PageMainController implements the CRUD actions for PageMain model.
 */
class PageMainController extends StaticController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return PageMain::className();
    }
}
