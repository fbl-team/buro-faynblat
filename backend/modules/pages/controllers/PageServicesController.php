<?php

namespace backend\modules\pages\controllers;

use backend\components\StaticController;
use backend\modules\pages\models\services\PageServices;

/**
 * PageServicesController implements the CRUD actions for PageServices model.
 */
class PageServicesController extends StaticController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return PageServices::className();
    }
}
