<?php

namespace backend\modules\pages\controllers;

use backend\components\BackendController;
use backend\components\StaticController;
use backend\modules\pages\models\about\PageAbout;

/**
 * PageAboutController implements the CRUD actions for PageAbout model.
 */
class PageAboutController extends StaticController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return PageAbout::className();
    }
}
