<?php

namespace backend\modules\pages\models\careers;

use backend\components\ImperaviContent;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;
use common\models\EntityToFile;


class PageCareers extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_careers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description','label'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'description' => Yii::t('back/careers', 'Description'),
            'label' => Yii::t('back/careers', 'Label'),

            'footer_request_block_label' => Yii::t('back/about', 'Footer Request Block Label'),
            'footer_request_block_button_label' => Yii::t('back/about', 'Footer Request Block Button Label'),
            'footer_seo_block_label' => Yii::t('back/about', 'Footer Seo Block Label'),
            'footer_seo_block_button_label' => Yii::t('back/about', 'Footer Seo Block Button Label'),
            'footer_seo_block_text_left_column' => Yii::t('back/about', 'Footer Seo Block Text Left Column'),
            'footer_seo_block_text_left_right' => Yii::t('back/about', 'Footer Seo Block Text Right Column'),
            'footer_seo_block_button_label_hide' => Yii::t('back/about', 'Footer Seo Block Button Hide Label'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'description',
            'label'
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
            'footer' => [
                'class' => \backend\components\FooterBehavior::className(),
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageCareersTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Page Careers');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    [
                        'attribute' => 'label',
                        'format' => 'html',
                    ],
                    'id',
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                    ]
                ];
            break;
        }

        return [];
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
            'form-set' => [
                Yii::t('back/careers', 'Description') => [
                    'label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'description' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => ImperaviContent::className(),
                        'options' => [
                            'customSettings' => [
                                'buttons' => [
                                    'html',
                                    'formatting',
                                    'bold',
                                    'italic',
                                    'unorderedlist',
                                    'orderedlist',
                                    'link',
                                    'alignment',
                                    'horizontalrule',
                                ],
                            ],
                        ],
                    ]
                ]
            ]

        ];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new PageCareersSearch();
    }
}
