<?php

namespace backend\modules\pages\models\careers;

use Yii;

class PageCareersTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_careers_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'description' => Yii::t('back/careers', 'Description') . ' [' . $this->language . ']',
            'label' => Yii::t('back/careers', 'Label') . ' [' . $this->language . ']',

            'footer_request_block_label' => Yii::t('back/about', 'Footer Request Block Label') . ' [' . $this->language . ']',
            'footer_request_block_button_label' => Yii::t('back/about', 'Footer Request Block Button Label') . ' [' . $this->language . ']',
            'footer_seo_block_label' => Yii::t('back/about', 'Footer Seo Block Label') . ' [' . $this->language . ']',
            'footer_seo_block_button_label' => Yii::t('back/about', 'Footer Seo Block Button Label') . ' [' . $this->language . ']',
            'footer_seo_block_text_left_column' => Yii::t('back/about', 'Footer Seo Block Text Left Column') . ' [' . $this->language . ']',
            'footer_seo_block_text_left_right' => Yii::t('back/about', 'Footer Seo Block Text Right Column') . ' [' . $this->language . ']',
            'footer_seo_block_button_label_hide' => Yii::t('back/about', 'Footer Seo Block Button Hide Label') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['footer_request_block_label', 'description','label'], 'string'],
            [['footer_seo_block_button_label_hide', 'footer_request_block_label', 'footer_request_block_button_label', 'footer_seo_block_label', 'footer_seo_block_button_label', 'footer_seo_block_text_left_column', 'footer_seo_block_text_left_right'], 'safe']
         ];
    }
}
