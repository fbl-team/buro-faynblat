<?php

namespace backend\modules\pages\models\media;

use Yii;

/**
* This is the model class for table "{{%page_media_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $write_block_label
* @property string $write_block_button_label
* @property string $speak_block_label
* @property string $speak_block_button_label
*/
class PageMediaTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_media_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/page_media', 'Label') . ' [' . $this->language . ']',
            'write_block_label' => Yii::t('back/page_media', 'Write Block Label') . ' [' . $this->language . ']',
            'write_block_button_label' => Yii::t('back/page_media', 'Write Block Button Label') . ' [' . $this->language . ']',
            'speak_block_label' => Yii::t('back/page_media', 'Speak Block Label') . ' [' . $this->language . ']',
            'speak_block_button_label' => Yii::t('back/page_media', 'Speak Block Button Label') . ' [' . $this->language . ']',

            'footer_request_block_label' => Yii::t('back/page_media', 'Footer Request Block Label') . ' [' . $this->language . ']',
            'footer_request_block_button_label' => Yii::t('back/page_media', 'Footer Request Block Button Label') . ' [' . $this->language . ']',
            'footer_seo_block_label' => Yii::t('back/page_media', 'Footer Seo Block Label') . ' [' . $this->language . ']',
            'footer_seo_block_button_label' => Yii::t('back/page_media', 'Footer Seo Block Button Label') . ' [' . $this->language . ']',
            'footer_seo_block_text_left_column' => Yii::t('back/page_media', 'Footer Seo Block Text Left Column') . ' [' . $this->language . ']',
            'footer_seo_block_text_left_right' => Yii::t('back/page_media', 'Footer Seo Block Text Right Column') . ' [' . $this->language . ']',
            'footer_seo_block_button_label_hide' => Yii::t('back/page_main', 'Footer Seo Block Button Hide Label') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label', 'write_block_label', 'write_block_button_label', 'speak_block_label', 'speak_block_button_label'], 'string'],
            [['footer_seo_block_button_label_hide', 'footer_request_block_label', 'footer_request_block_button_label', 'footer_seo_block_label', 'footer_seo_block_button_label', 'footer_seo_block_text_left_column', 'footer_seo_block_text_left_right'], 'safe']
         ];
    }
}
