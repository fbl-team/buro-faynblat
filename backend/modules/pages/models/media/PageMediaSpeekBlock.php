<?php

namespace backend\modules\pages\models\media;

use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;

/**
 * This is the model class for table "{{%page_media_speek_block}}".
 *
 * @property integer $id
 * @property integer $page_media_id
 * @property string $label
 * @property integer $published
 * @property integer $position
 *
 * @property PageMedia $pageMedia
 * @property PageMediaSpeekBlockTranslation[] $translations
 */
class PageMediaSpeekBlock extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;

    public $elementImage;

    /**
     * Temporary sign which used for saving images before model save
     * @var
     */
    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_media_speek_block}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['page_media_id'], 'required'],
            [['page_media_id', 'published', 'position'], 'integer'],
            [['you_tube_video_code', 'label'], 'string'],
            [['page_media_id'], 'exist', 'targetClass' => PageMedia::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['sign'], 'string', 'max' => 255],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/page_media/speek_block', 'ID'),
            'page_media_id' => Yii::t('back/page_media/speek_block', 'Page Media ID'),
            'label' => Yii::t('back/page_media/speek_block', 'Label'),
            'you_tube_video_code' => Yii::t('back/page_media/speek_block', 'YouTube Video Link'),
            'elementImage' => Yii::t('back/page_media/speek_block', 'Element Image'),
            'published' => Yii::t('back/page_media/speek_block', 'Published'),
            'position' => Yii::t('back/page_media/speek_block', 'Position'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageMedia()
    {
        return $this->hasOne(PageMedia::className(), ['id' => 'page_media_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageMediaSpeekBlockTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Page Media Speek Block');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'page_media_id',
                    // 'label:ntext',
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'page_media_id',
                    [
                        'attribute' => 'label',
                        'format' => 'html',
                    ],
                    'published:boolean',
                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new PageMediaSpeekBlockSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
//            'page_media_id' => [
//                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
//                'items' => PageMedia::getItems(),
//                'options' => [
//                    'prompt' => '',
//                ],
//            ],
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT
            ],
            'you_tube_video_code' => [
                'type' => ActiveFormBuilder::INPUT_TEXT
            ],
            'elementImage' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'hint' => Yii::t('back/projects', 'For correct view images on main page downloaded files must be in 485*320(px) format, or fold last one'),
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => 'elementImage',
                    'saveAttribute' => EntityToFile::TYPE_PAGE_MEDIA_SPEEK_BLOCK_IMAGE,
                    'aspectRatio' => 485/320, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ])
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
//            'position' => [
//                'type' => ActiveFormBuilder::INPUT_TEXT,
//            ],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }

}
