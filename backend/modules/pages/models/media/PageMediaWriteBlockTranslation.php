<?php

namespace backend\modules\pages\models\media;

use Yii;

/**
* This is the model class for table "{{%page_media_write_block_translation}}".
*
* @property integer $model_id
* @property string $language
*/
class PageMediaWriteBlockTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_media_write_block_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/page_media/write_block', 'Label') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'string'],
         ];
    }
}
