<?php

namespace backend\modules\pages\models\media;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PageMediaSearch represents the model behind the search form about `PageMedia`.
 */
class PageMediaSearch extends PageMedia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['label', 'write_block_label', 'write_block_button_label', 'speak_block_label', 'speak_block_button_label'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PageMedia::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'write_block_label', $this->write_block_label])
            ->andFilterWhere(['like', 'write_block_button_label', $this->write_block_button_label])
            ->andFilterWhere(['like', 'speak_block_label', $this->speak_block_label])
            ->andFilterWhere(['like', 'speak_block_button_label', $this->speak_block_button_label]);

        return $dataProvider;
    }
}
