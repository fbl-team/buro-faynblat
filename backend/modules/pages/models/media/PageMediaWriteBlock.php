<?php

namespace backend\modules\pages\models\media;

use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use backend\modules\pdfUpload\widgets\pdfUpload\PdfUpload;
use common\models\EntityToFile;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;

/**
 * This is the model class for table "{{%page_media_write_block}}".
 *
 * @property integer $id
 * @property integer $page_media_id
 * @property string $label
 * @property integer $published
 * @property integer $position
 *
 * @property PageMedia $pageMedia
 * @property PageMediaWriteBlockTranslation[] $translations
 */
class PageMediaWriteBlock extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;

    public $elementImage;
    public $elementPdf;

    /**
     * Temporary sign which used for saving images before model save
     * @var
     */
    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_media_write_block}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['page_media_id'], 'required'],
            [['page_media_id', 'published', 'position'], 'integer'],
            [['label'], 'string'],
            [['page_media_id'], 'exist', 'targetClass' => PageMedia::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['sign'], 'string', 'max' => 255],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/page_media/write_block', 'ID'),
            'page_media_id' => Yii::t('back/page_media/write_block', 'Page Media ID'),
            'label' => Yii::t('back/page_media/write_block', 'Label'),
            'published' => Yii::t('back/page_media/write_block', 'Published'),
            'position' => Yii::t('back/page_media/write_block', 'Position'),
            'elementImage' => Yii::t('back/page_media/write_block', 'Element Image'),
            'elementPdf' => Yii::t('back/page_media/write_block', 'Element Pdf'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label'
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageMedia()
    {
        return $this->hasOne(PageMedia::className(), ['id' => 'page_media_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageMediaWriteBlockTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Page Media Write Block');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'page_media_id',
                    // 'label:ntext',
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'page_media_id',
                    [
                        'attribute' => 'label',
                        'format' => 'html',
                    ],
                    'published:boolean',
                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new PageMediaWriteBlockSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
//            'page_media_id' => [
//                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
//                'items' => PageMedia::getItems(),
//                'options' => [
//                    'prompt' => '',
//                ],
//            ],
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT
            ],
            'elementImage' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'hint' => Yii::t('back/projects', 'For correct view images on main page downloaded files must be in 310*400(px) format, or fold last one'),
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => 'elementImage',
                    'saveAttribute' => EntityToFile::TYPE_PAGE_MEDIA_WRITE_BLOCK_IMAGE,
                    'aspectRatio' => 310/400, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ])
            ],
            'elementPdf' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'hint' => Yii::t('back/projects', 'Download pdf file'),
                'value' => PdfUpload::widget([
                    'model' => $this,
                    'attribute' => 'elementPdf',
                    'saveAttribute' => EntityToFile::TYPE_PAGE_MEDIA_WRITE_BLOCK_PDF,
//                    'aspectRatio' => 310/400, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ])
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
//            'position' => [
//                'type' => ActiveFormBuilder::INPUT_TEXT,
//            ],

        ];

    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }

}
