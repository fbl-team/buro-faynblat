<?php

namespace backend\modules\pages\models\media;

use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;

/**
 * This is the model class for table "{{%page_media}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $write_block_label
 * @property string $write_block_button_label
 * @property string $speak_block_label
 * @property string $speak_block_button_label
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property PageMediaSpeekBlock[] $pageMediaSpeekBlocks
 * @property PageMediaTranslation[] $translations
 * @property PageMediaWriteBlock[] $pageMediaWriteBlocks
 */
class PageMedia extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_media}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'write_block_label', 'write_block_button_label', 'speak_block_label', 'speak_block_button_label'], 'string'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/page_media', 'ID'),
            'label' => Yii::t('back/page_media', 'Label'),
            'write_block_label' => Yii::t('back/page_media', 'Write Block Label'),
            'write_block_button_label' => Yii::t('back/page_media', 'Write Block Button Label'),
            'speak_block_label' => Yii::t('back/page_media', 'Speak Block Label'),
            'speak_block_button_label' => Yii::t('back/page_media', 'Speak Block Button Label'),

            'footer_request_block_label' => Yii::t('back/page_media', 'Footer Request Block Label'),
            'footer_request_block_button_label' => Yii::t('back/page_media', 'Footer Request Block Button Label'),
            'footer_seo_block_label' => Yii::t('back/page_media', 'Footer Seo Block Label'),
            'footer_seo_block_button_label' => Yii::t('back/page_media', 'Footer Seo Block Button Label'),
            'footer_seo_block_text_left_column' => Yii::t('back/page_media', 'Footer Seo Block Text Left Column'),
            'footer_seo_block_text_left_right' => Yii::t('back/page_media', 'Footer Seo Block Text Right Column'),
            'footer_seo_block_button_label_hide' => Yii::t('back/page_media', 'Footer Seo Block Button Hide Label'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'write_block_label',
            'write_block_button_label',
            'speak_block_label',
            'speak_block_button_label',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
            'footer' => [
                'class' => \backend\components\FooterBehavior::className(),
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageMediaSpeekBlocks()
    {
        return $this->hasMany(PageMediaSpeekBlock::className(), ['page_media_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageMediaWriteBlocks()
    {
        return $this->hasMany(PageMediaWriteBlock::className(), ['page_media_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageMediaTranslation::className(), ['model_id' => 'id']);
    }

    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Page Media');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    // 'label:ntext',
                    // 'write_block_label:ntext',
                    // 'write_block_button_label:ntext',
                    // 'speak_block_label:ntext',
                    // 'speak_block_button_label:ntext',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    [
                        'attribute' => 'label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'write_block_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'write_block_button_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'speak_block_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'speak_block_button_label',
                        'format' => 'html',
                    ],
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new PageMediaSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
            'form-set' => [
                Yii::t('back/page_media', 'Main info') => [
                    'label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT
                    ],
                ],
                Yii::t('back/page_media', 'Write Block Label') => [
                    'write_block_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT
                    ],
                    'write_block_button_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT
                    ],
                    $this->getRelatedFormConfig()['write_block']
                ],
                Yii::t('back/page_media', 'Speak Block Label') => [
                    'speak_block_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT
                    ],
                    'speak_block_button_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT
                    ],
                    $this->getRelatedFormConfig()['speek_block']
                ]
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpeekBlocks()
    {
        return $this->hasMany(PageMediaSpeekBlock::className(), ['page_media_id' => 'id'])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWriteBlocks()
    {
        return $this->hasMany(PageMediaWriteBlock::className(), ['page_media_id' => 'id'])
            ->orderBy('position');
    }

    /**
     * @return array
     */
    public function getRelatedFormConfig()
    {
        return [
            'speek_block' => [
                'relation' => 'speekBlocks', //имя реляции (всегда с маленькой буквы, от названия метода реляции убрать get)
            ],
            'write_block' => [
                'relation' => 'writeBlocks', //имя реляции (всегда с маленькой буквы, от названия метода реляции убрать get)
            ],
        ];
    }

}
