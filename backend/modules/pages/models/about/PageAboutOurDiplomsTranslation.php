<?php

namespace backend\modules\pages\models\about;

use Yii;

/**
* This is the model class for table "{{%page_about_our_diploms_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $description
*/
class PageAboutOurDiplomsTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_about_our_diploms_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/about/our_diploms', 'Label') . ' [' . $this->language . ']',
            'description' => Yii::t('back/about/our_diploms', 'Description') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label', 'description'], 'string'],
         ];
    }
}
