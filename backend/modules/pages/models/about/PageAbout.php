<?php

namespace backend\modules\pages\models\about;

use backend\components\FooterBehavior;
use backend\modules\videoUpload\models\VideoUploadModel;
use backend\modules\videoUpload\widgets\videoUpload\VideoUpload;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;
use backend\modules\imagesUpload\models\ImagesUploadModel;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%page_about}}".
 *
 * @property integer $id
 * @property string $first_screen_label
 * @property string $first_screen_description
 * @property string $first_screen_image
 * @property string $short_about_block_label1
 * @property string $short_about_block_label2
 * @property string $short_about_block_description1
 * @property string $short_about_block_description2
 * @property string $work_block_label
 * @property string $work_block_description
 * @property string $about_our_work_label
 * @property string $our_diplom_block_label
 * @property string $our_diplom_block_button_label
 * @property string $canal_block_label
 * @property string $canal_url
 * @property string $canal_block_button_label
 * @property string $projects_block_label
 * @property string $projects_block_description
 * @property string $projects_block_button_label
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property PageAboutAboutOurWork[] $pageAboutAboutOurWorks
 * @property PageAboutCanalDisain[] $pageAboutCanalDisains
 * @property PageAboutOurDiploms[] $pageAboutOurDiploms
 * @property PageAboutTranslation[] $translations
 * @property PageAboutWorkPrinciple[] $pageAboutWorkPrinciples
 */
class PageAbout extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;

    public $titleImage;

    public $firstScreenVideo;

    /**
    * Temporary sign which used for saving images before model save
    * @var
    */
    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_about}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['canal_url', 'first_screen_label', 'first_screen_description', 'first_screen_image', 'short_about_block_label1', 'short_about_block_label2', 'short_about_block_description1', 'short_about_block_description2', 'work_block_label', 'work_block_description', 'about_our_work_label', 'our_diplom_block_label', 'our_diplom_block_button_label', 'canal_block_label', 'canal_block_button_label', 'projects_block_label', 'projects_block_description', 'projects_block_button_label'], 'string'],
            [['sign'], 'string', 'max' => 255],
    
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/about', 'ID'),
            'first_screen_label' => Yii::t('back/about', 'Label'),
            'first_screen_description' => Yii::t('back/about', 'First Screen Description'),
            'first_screen_image' => Yii::t('back/about', 'First Screen Image'),
            'firstScreenVideo' => Yii::t('back/about', 'First Screen Video'),
            'short_about_block_label1' => Yii::t('back/about', 'Short About Block Label1'),
            'short_about_block_label2' => Yii::t('back/about', 'Short About Block Label2'),
            'short_about_block_description1' => Yii::t('back/about', 'Short About Block Description1'),
            'short_about_block_description2' => Yii::t('back/about', 'Short About Block Description2'),
            'work_block_label' => Yii::t('back/about', 'Work Block Label'),
            'work_block_description' => Yii::t('back/about', 'Work Block Description'),
            'about_our_work_label' => Yii::t('back/about', 'About Our Work Label'),
            'our_diplom_block_label' => Yii::t('back/about', 'Our Diplom Block Label'),
            'our_diplom_block_button_label' => Yii::t('back/about', 'Our Diplom Block Button Label'),
            'canal_block_label' => Yii::t('back/about', 'Canal Block Label'),
            'canal_url' => Yii::t('back/about', 'Canal Block Link'),
            'canal_block_button_label' => Yii::t('back/about', 'Canal Block Button Label'),
            'projects_block_label' => Yii::t('back/about', 'Projects Block Label'),
            'projects_block_description' => Yii::t('back/about', 'Projects Block Description'),
            'projects_block_button_label' => Yii::t('back/about', 'Projects Block Button Label'),

            'footer_request_block_label' => Yii::t('back/about', 'Footer Request Block Label'),
            'footer_request_block_button_label' => Yii::t('back/about', 'Footer Request Block Button Label'),
            'footer_seo_block_label' => Yii::t('back/about', 'Footer Seo Block Label'),
            'footer_seo_block_button_label' => Yii::t('back/about', 'Footer Seo Block Button Label'),
            'footer_seo_block_text_left_column' => Yii::t('back/about', 'Footer Seo Block Text Left Column'),
            'footer_seo_block_text_left_right' => Yii::t('back/about', 'Footer Seo Block Text Right Column'),
            'footer_seo_block_button_label_hide' => Yii::t('back/about', 'Footer Seo Block Button Hide Label'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'first_screen_label',
            'first_screen_description',
            'first_screen_image',
            'short_about_block_label1',
            'short_about_block_label2',
            'short_about_block_description1',
            'short_about_block_description2',
            'work_block_label',
            'work_block_description',
            'about_our_work_label',
            'our_diplom_block_label',
            'our_diplom_block_button_label',
            'canal_block_label',
            'canal_block_button_label',
            'projects_block_label',
            'projects_block_description',
            'projects_block_button_label',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
            'footer' => [
                'class' => \backend\components\FooterBehavior::className(),
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageAboutTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Page About');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    // 'first_screen_label:ntext',
                    // 'first_screen_description:ntext',
                    // 'first_screen_image:ntext',
                    // 'short_about_block_label1:ntext',
                    // 'short_about_block_label2:ntext',
                    // 'short_about_block_description1:ntext',
                    // 'short_about_block_description2:ntext',
                    // 'work_block_label:ntext',
                    // 'work_block_description:ntext',
                    // 'about_our_work_label:ntext',
                    // 'our_diplom_block_label:ntext',
                    // 'our_diplom_block_button_label:ntext',
                    // 'canal_block_label:ntext',
                    // 'canal_block_button_label:ntext',
                    // 'projects_block_label:ntext',
                    // 'projects_block_description:ntext',
                    // 'projects_block_button_label:ntext',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    [
                        'attribute' => 'first_screen_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'first_screen_description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'first_screen_image',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'short_about_block_label1',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'short_about_block_label2',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'short_about_block_description1',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'short_about_block_description2',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'work_block_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'work_block_description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'about_our_work_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'our_diplom_block_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'our_diplom_block_button_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'canal_block_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'canal_block_button_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'projects_block_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'projects_block_description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'projects_block_button_label',
                        'format' => 'html',
                    ],
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new PageAboutSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
            'form-set' => [
                Yii::t('back/about', 'First screen') => [
                    'first_screen_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'first_screen_description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
                    'first_screen_image' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'titleImage' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'hint' => Yii::t('back/projects', 'For correct view images on main page downloaded files must be in 1350*550(px) format, or fold last one'),
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'titleImage',
                            'saveAttribute' => EntityToFile::TYPE_PAGE_ABOUT_FIRST_SCREEN_IMAGE,
                            'aspectRatio' => 1350/550, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'firstScreenVideo' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'hint' => Yii::t('back/page_main', 'For correctly display video on web-site we recommend download 2 same video files with MP4 and WEBM formats'),
                        'value' => VideoUpload::widget([
                            'model' => $this,
                            'attribute' => 'firstScreenVideo',
                            'saveAttribute' => EntityToFile::TYPE_PAGE_ABOUT_FIRST_SCREEN_VIDEO,
                            //'aspectRatio' => 300/200, //Пропорция для кропа
                            'multiple' => true, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'sign' => [
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => false,
                    ],
                ],
                Yii::t('back/about', 'Work principles block') => [
                    'work_block_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'work_block_description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
                    'about_our_work_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    $this->getRelatedFormConfig()['work_principles']
                ],
                Yii::t('back/about', 'Info in digits') => [
                    'short_about_block_label1' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'short_about_block_label2' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'short_about_block_description1' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
                    'short_about_block_description2' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
                    $this->getRelatedFormConfig()['about_our_works']
                ],
                Yii::t('back/about', 'Ouw diploms block') => [
                    'our_diplom_block_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'our_diplom_block_button_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    $this->getRelatedFormConfig()['our_diploms']
                ],
                Yii::t('back/about', 'Canal design block') => [
                    'canal_url' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'canal_block_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'canal_block_button_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    $this->getRelatedFormConfig()['canal_disain']
                ],
                Yii::t('back/about', 'Projects block') => [
                    'projects_block_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'hint' => Yii::t('back/about', 'To select the words that will appear with an animation use b - tag. For example: text without animation &ltb&gt ANIMATED &lt/b&gt text without animation'),
                    ],
                    'projects_block_description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
                    'projects_block_button_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                ]
            ]

        ];
    }

    /**
    * @inheritdoc
    */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
    * @inheritdoc
    */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }

    /**
     * @return array
     */
    public function getRelatedFormConfig()
    {
        return [
            'work_principles' => [
                'relation' => 'pageAboutWorkPrinciples', //имя реляции (всегда с маленькой буквы, от названия метода реляции убрать get)
            ],
            'about_our_works' => [
                'relation' => 'pageAboutAboutOurWorks', //имя реляции (всегда с маленькой буквы, от названия метода реляции убрать get)
            ],
            'our_diploms' => [
                'relation' => 'pageAboutOurDiploms', //имя реляции (всегда с маленькой буквы, от названия метода реляции убрать get)
            ],
            'canal_disain' => [
                'relation' => 'pageAboutCanalDisains', //имя реляции (всегда с маленькой буквы, от названия метода реляции убрать get)
            ],

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageAboutWorkPrinciples()
    {
        return $this->hasMany(PageAboutWorkPrinciple::className(), ['page_about_id' => 'id'])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageAboutAboutOurWorks()
    {
        return $this->hasMany(PageAboutAboutOurWork::className(), ['page_about_id' => 'id'])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageAboutCanalDisains()
    {
        return $this->hasMany(PageAboutCanalDisain::className(), ['page_about_id' => 'id'])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageAboutOurDiploms()
    {
        return $this->hasMany(PageAboutOurDiploms::className(), ['page_about_id' => 'id'])
            ->orderBy('position');
    }

    /**
     * @return $this
     */
    public function getFirstScreenImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->alias('t2')
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_PAGE_ABOUT_FIRST_SCREEN_IMAGE])
            ->orderBy('t2.position DESC');
    }

    /**
     * @return $this
     */
    public function getFirstScreenVideo()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->alias('t2')
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_PAGE_ABOUT_FIRST_SCREEN_VIDEO])
            ->orderBy('t2.position DESC');
    }
}
