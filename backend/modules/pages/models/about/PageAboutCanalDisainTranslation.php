<?php

namespace backend\modules\pages\models\about;

use Yii;

/**
* This is the model class for table "{{%page_about_canal_disain_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $description
*/
class PageAboutCanalDisainTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_about_canal_disain_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/about/canal_disain', 'Label under image') . ' [' . $this->language . ']',
//            'description' => Yii::t('back/about/canal_disain', 'YouTube video link') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'string'],
//            [['label', 'description'], 'string'],
         ];
    }
}
