<?php

namespace backend\modules\pages\models\about;

use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;

/**
 * This is the model class for table "{{%page_about_about_our_work}}".
 *
 * @property integer $id
 * @property integer $page_about_id
 * @property string $label
 * @property string $description
 * @property string $label_under_description
 * @property integer $published
 * @property integer $position
 *
 * @property PageAbout $pageAbout
 * @property PageAboutAboutOurWorkTranslation[] $translations
 */
class PageAboutAboutOurWork extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_about_about_our_work}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['page_about_id'], 'required'],
            [['page_about_id', 'published', 'position'], 'integer'],
            [['label', 'description', 'label_under_description'], 'string'],
            [['page_about_id'], 'exist', 'targetClass' => PageAbout::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/about/about_our_work', 'ID'),
            'page_about_id' => Yii::t('back/about/about_our_work', 'Page About ID'),
            'label' => Yii::t('back/about/about_our_work', 'Label (digits)'),
            'description' => Yii::t('back/about/about_our_work', 'Label Under Digits'),
            'label_under_description' => Yii::t('back/about/about_our_work', 'Label Above Digits'),
            'published' => Yii::t('back/about/about_our_work', 'Published'),
            'position' => Yii::t('back/about/about_our_work', 'Position'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
            'label_under_description',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageAbout()
    {
        return $this->hasOne(PageAbout::className(), ['id' => 'page_about_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageAboutAboutOurWorkTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Page About About Our Work');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'page_about_id',
                    // 'label:ntext',
                    // 'description:ntext',
                    // 'label_under_description:ntext',
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'page_about_id',
                    [
                        'attribute' => 'label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'label_under_description',
                        'format' => 'html',
                    ],
                    'published:boolean',
                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new PageAboutAboutOurWorkSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
//            'page_about_id' => [
//                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
//                'items' => PageAbout::getItems(),
//                'options' => [
//                    'prompt' => '',
//                ],
//            ],
            'label_under_description' => [
                'type' => ActiveFormBuilder::INPUT_TEXT
            ],
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT
            ],
            'description' => [
                'type' => ActiveFormBuilder::INPUT_TEXTAREA
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
//            'position' => [
//                'type' => ActiveFormBuilder::INPUT_TEXT,
//            ],
            
        ];
    }

}
