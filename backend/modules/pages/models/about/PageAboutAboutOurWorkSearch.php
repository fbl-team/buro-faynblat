<?php

namespace backend\modules\pages\models\about;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PageAboutAboutOurWorkSearch represents the model behind the search form about `PageAboutAboutOurWork`.
 */
class PageAboutAboutOurWorkSearch extends PageAboutAboutOurWork
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'page_about_id', 'published', 'position'], 'integer'],
            [['label', 'description', 'label_under_description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PageAboutAboutOurWork::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'page_about_id' => $this->page_about_id,
            'published' => $this->published,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'label_under_description', $this->label_under_description]);

        return $dataProvider;
    }
}
