<?php

namespace backend\modules\pages\models\about;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PageAboutSearch represents the model behind the search form about `PageAbout`.
 */
class PageAboutSearch extends PageAbout
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['first_screen_label', 'first_screen_description', 'first_screen_image', 'short_about_block_label1', 'short_about_block_label2', 'short_about_block_description1', 'short_about_block_description2', 'work_block_label', 'work_block_description', 'about_our_work_label', 'our_diplom_block_label', 'our_diplom_block_button_label', 'canal_block_label', 'canal_block_button_label', 'projects_block_label', 'projects_block_description', 'projects_block_button_label'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PageAbout::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'first_screen_label', $this->first_screen_label])
            ->andFilterWhere(['like', 'first_screen_description', $this->first_screen_description])
            ->andFilterWhere(['like', 'first_screen_image', $this->first_screen_image])
            ->andFilterWhere(['like', 'short_about_block_label1', $this->short_about_block_label1])
            ->andFilterWhere(['like', 'short_about_block_label2', $this->short_about_block_label2])
            ->andFilterWhere(['like', 'short_about_block_description1', $this->short_about_block_description1])
            ->andFilterWhere(['like', 'short_about_block_description2', $this->short_about_block_description2])
            ->andFilterWhere(['like', 'work_block_label', $this->work_block_label])
            ->andFilterWhere(['like', 'work_block_description', $this->work_block_description])
            ->andFilterWhere(['like', 'about_our_work_label', $this->about_our_work_label])
            ->andFilterWhere(['like', 'our_diplom_block_label', $this->our_diplom_block_label])
            ->andFilterWhere(['like', 'our_diplom_block_button_label', $this->our_diplom_block_button_label])
            ->andFilterWhere(['like', 'canal_block_label', $this->canal_block_label])
            ->andFilterWhere(['like', 'canal_block_button_label', $this->canal_block_button_label])
            ->andFilterWhere(['like', 'projects_block_label', $this->projects_block_label])
            ->andFilterWhere(['like', 'projects_block_description', $this->projects_block_description])
            ->andFilterWhere(['like', 'projects_block_button_label', $this->projects_block_button_label]);

        return $dataProvider;
    }
}
