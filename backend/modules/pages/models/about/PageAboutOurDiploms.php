<?php

namespace backend\modules\pages\models\about;

use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;

/**
 * This is the model class for table "{{%page_about_our_diploms}}".
 *
 * @property integer $id
 * @property integer $page_about_id
 * @property string $label
 * @property string $description
 * @property integer $published
 * @property integer $position
 *
 * @property PageAbout $pageAbout
 * @property PageAboutOurDiplomsTranslation[] $translations
 */
class PageAboutOurDiploms extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;

    public $titleImage;

    /**
     * Temporary sign which used for saving images before model save
     * @var
     */
    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_about_our_diploms}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['page_about_id'], 'required'],
            [['page_about_id', 'published', 'position'], 'integer'],
            [['label', 'description'], 'string'],
            [['page_about_id'], 'exist', 'targetClass' => PageAbout::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['sign'], 'string', 'max' => 255],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/about/our_diploms', 'ID'),
            'page_about_id' => Yii::t('back/about/our_diploms', 'Page About ID'),
            'label' => Yii::t('back/about/our_diploms', 'Label'),
            'description' => Yii::t('back/about/our_diploms', 'Description'),
            'titleImage' => Yii::t('back/about/our_diploms', 'Diploms image'),
            'published' => Yii::t('back/about/our_diploms', 'Published'),
            'position' => Yii::t('back/about/our_diploms', 'Position'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageAbout()
    {
        return $this->hasOne(PageAbout::className(), ['id' => 'page_about_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageAboutOurDiplomsTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Page About Our Diploms');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'page_about_id',
                    // 'label:ntext',
                    // 'description:ntext',
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'page_about_id',
                    [
                        'attribute' => 'label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                    ],
                    'published:boolean',
                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new PageAboutOurDiplomsSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
//            'page_about_id' => [
//                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
//                'items' => PageAbout::getItems(),
//                'options' => [
//                    'prompt' => '',
//                ],
//            ],
            'titleImage' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'hint' => Yii::t('back/projects', 'For correct view images on main page downloaded files must be in 165*200(px) format, or fold last one'),
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => 'titleImage',
                    'saveAttribute' => EntityToFile::TYPE_PAGE_ABOUT_DIPLOMS_IMAGE,
                    'aspectRatio' => 165/200, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ])
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT
            ],
            'description' => [
                'type' => ActiveFormBuilder::INPUT_TEXTAREA
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
//            'position' => [
//                'type' => ActiveFormBuilder::INPUT_TEXT,
//            ],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }

}
