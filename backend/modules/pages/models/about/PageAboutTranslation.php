<?php

namespace backend\modules\pages\models\about;

use Yii;

/**
* This is the model class for table "{{%page_about_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $first_screen_label
* @property string $first_screen_description
* @property string $first_screen_image
* @property string $short_about_block_label1
* @property string $short_about_block_label2
* @property string $short_about_block_description1
* @property string $short_about_block_description2
* @property string $work_block_label
* @property string $work_block_description
* @property string $about_our_work_label
* @property string $our_diplom_block_label
* @property string $our_diplom_block_button_label
* @property string $canal_block_label
* @property string $canal_block_button_label
* @property string $projects_block_label
* @property string $projects_block_description
* @property string $projects_block_button_label
*/
class PageAboutTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_about_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'first_screen_label' => Yii::t('back/about', 'Label') . ' [' . $this->language . ']',
            'first_screen_description' => Yii::t('back/about', 'First Screen Description') . ' [' . $this->language . ']',
            'first_screen_image' => Yii::t('back/about', 'First Screen Image') . ' [' . $this->language . ']',
            'short_about_block_label1' => Yii::t('back/about', 'Short About Block Label1') . ' [' . $this->language . ']',
            'short_about_block_label2' => Yii::t('back/about', 'Short About Block Label2') . ' [' . $this->language . ']',
            'short_about_block_description1' => Yii::t('back/about', 'Short About Block Description1') . ' [' . $this->language . ']',
            'short_about_block_description2' => Yii::t('back/about', 'Short About Block Description2') . ' [' . $this->language . ']',
            'work_block_label' => Yii::t('back/about', 'Work Block Label') . ' [' . $this->language . ']',
            'work_block_description' => Yii::t('back/about', 'Work Block Description') . ' [' . $this->language . ']',
            'about_our_work_label' => Yii::t('back/about', 'About Our Work Label') . ' [' . $this->language . ']',
            'our_diplom_block_label' => Yii::t('back/about', 'Our Diplom Block Label') . ' [' . $this->language . ']',
            'our_diplom_block_button_label' => Yii::t('back/about', 'Our Diplom Block Button Label') . ' [' . $this->language . ']',
            'canal_block_label' => Yii::t('back/about', 'Canal Block Label') . ' [' . $this->language . ']',
            'canal_block_button_label' => Yii::t('back/about', 'Canal Block Button Label') . ' [' . $this->language . ']',
            'projects_block_label' => Yii::t('back/about', 'Projects Block Label') . ' [' . $this->language . ']',
            'projects_block_description' => Yii::t('back/about', 'Projects Block Description') . ' [' . $this->language . ']',
            'projects_block_button_label' => Yii::t('back/about', 'Projects Block Button Label') . ' [' . $this->language . ']',

            'footer_request_block_label' => Yii::t('back/about', 'Footer Request Block Label') . ' [' . $this->language . ']',
            'footer_request_block_button_label' => Yii::t('back/about', 'Footer Request Block Button Label') . ' [' . $this->language . ']',
            'footer_seo_block_label' => Yii::t('back/about', 'Footer Seo Block Label') . ' [' . $this->language . ']',
            'footer_seo_block_button_label' => Yii::t('back/about', 'Footer Seo Block Button Label') . ' [' . $this->language . ']',
            'footer_seo_block_text_left_column' => Yii::t('back/about', 'Footer Seo Block Text Left Column') . ' [' . $this->language . ']',
            'footer_seo_block_text_left_right' => Yii::t('back/about', 'Footer Seo Block Text Right Column') . ' [' . $this->language . ']',
            'footer_seo_block_button_label_hide' => Yii::t('back/about', 'Footer Seo Block Button Hide Label') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['footer_request_block_label', 'first_screen_label', 'first_screen_description', 'first_screen_image', 'short_about_block_label1', 'short_about_block_label2', 'short_about_block_description1', 'short_about_block_description2', 'work_block_label', 'work_block_description', 'about_our_work_label', 'our_diplom_block_label', 'our_diplom_block_button_label', 'canal_block_label', 'canal_block_button_label', 'projects_block_label', 'projects_block_description', 'projects_block_button_label'], 'string'],
            [['footer_seo_block_button_label_hide', 'footer_request_block_label', 'footer_request_block_button_label', 'footer_seo_block_label', 'footer_seo_block_button_label', 'footer_seo_block_text_left_column', 'footer_seo_block_text_left_right'], 'safe']
        ];
    }
}
