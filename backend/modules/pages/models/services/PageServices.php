<?php

namespace backend\modules\pages\models\services;

use backend\modules\videoUpload\models\VideoUploadModel;
use backend\modules\videoUpload\widgets\videoUpload\VideoUpload;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;
use backend\modules\imagesUpload\models\ImagesUploadModel;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%page_services}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $description
 * @property string $what_we_do_block_label
 * @property string $what_we_do_block_description
 * @property string $how_we_do_that_block_label
 * @property string $how_we_do_that_block_description
 * @property string $project_block_label
 * @property string $project_block_description
 * @property string $project_block_button_label
 *
 * @property PageServicesHowWeDoBlock[] $pageServicesHowWeDoBlocks
 * @property PageServicesTranslation[] $translations
 * @property PageServicesWhatWeDoBlock[] $pageServicesWhatWeDoBlocks
 */
class PageServices extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;
    public $firstScreenImage;
    public $firstScreenVideo;

    /**
    * Temporary sign which used for saving images before model save
    * @var
    */
    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_services}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'description', 'what_we_do_block_label', 'what_we_do_block_description', 'how_we_do_that_block_label', 'how_we_do_that_block_description', 'project_block_label', 'project_block_description', 'project_block_button_label'], 'string'],
            [['sign'], 'string', 'max' => 255],
    
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/page_services', 'ID'),
            'firstScreenImage' => Yii::t('back/page_services', 'First Screen Image'),
            'firstScreenVideo' => Yii::t('back/page_services', 'First Screen Video'),
            'label' => Yii::t('back/page_services', 'Label'),
            'description' => Yii::t('back/page_services', 'Description'),
            'what_we_do_block_label' => Yii::t('back/page_services', 'What We Do Block Label'),
            'what_we_do_block_description' => Yii::t('back/page_services', 'What We Do Block Description'),
            'how_we_do_that_block_label' => Yii::t('back/page_services', 'How We Do That Block Label'),
            'how_we_do_that_block_description' => Yii::t('back/page_services', 'How We Do That Block Description'),
            'project_block_label' => Yii::t('back/page_services', 'Project Block Label'),
            'project_block_description' => Yii::t('back/page_services', 'Project Block Description'),
            'project_block_button_label' => Yii::t('back/page_services', 'Project Block Button Label'),

            'footer_request_block_label' => Yii::t('back/page_services', 'Footer Request Block Label'),
            'footer_request_block_button_label' => Yii::t('back/page_services', 'Footer Request Block Button Label'),
            'footer_seo_block_label' => Yii::t('back/page_services', 'Footer Seo Block Label'),
            'footer_seo_block_button_label' => Yii::t('back/page_services', 'Footer Seo Block Button Label'),
            'footer_seo_block_text_left_column' => Yii::t('back/page_services', 'Footer Seo Block Text Left Column'),
            'footer_seo_block_text_left_right' => Yii::t('back/page_services', 'Footer Seo Block Text Right Column'),
            'footer_seo_block_button_label_hide' => Yii::t('back/page_services', 'Footer Seo Block Button Hide Label'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
            'what_we_do_block_label',
            'what_we_do_block_description',
            'how_we_do_that_block_label',
            'how_we_do_that_block_description',
            'project_block_label',
            'project_block_description',
            'project_block_button_label',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
            'footer' => [
                'class' => \backend\components\FooterBehavior::className(),
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageServicesHowWeDoBlocks()
    {
        return $this->hasMany(PageServicesHowWeDoBlock::className(), ['page_services_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageServicesTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageServicesWhatWeDoBlocks()
    {
        return $this->hasMany(PageServicesWhatWeDoBlock::className(), ['page_services_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return Yii::t('back/page_services', 'Project Services');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    // 'label:ntext',
                    // 'description:ntext',
                    // 'what_we_do_block_label:ntext',
                    // 'what_we_do_block_description:ntext',
                    // 'how_we_do_that_block_label:ntext',
                    // 'how_we_do_that_block_description:ntext',
                    // 'project_block_label:ntext',
                    // 'project_block_description:ntext',
                    // 'project_block_button_label:ntext',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    [
                        'attribute' => 'label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'what_we_do_block_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'what_we_do_block_description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'how_we_do_that_block_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'how_we_do_that_block_description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'project_block_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'project_block_description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'project_block_button_label',
                        'format' => 'html',
                    ],
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new PageServicesSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
            'form-set' => [
                Yii::t('back/page_services', 'First screen') => [
                    'firstScreenImage' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'hint' => Yii::t('back/page_services', 'For correct view images on main page downloaded files must be in 1350*500(px) format, or fold last one'),
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'firstScreenImage',
                            'saveAttribute' => EntityToFile::TYPE_SERVICES_FIRST_SCREEN_IMAGE, //TODO Создать контанту и раскомментировать
                            'aspectRatio' => 1350/500, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'firstScreenVideo' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'hint' => Yii::t('back/page_services', 'For correctly display video on web-site we recommend download 2 same video files with MP4 and WEBM formats'),
                        'value' => VideoUpload::widget([
                            'model' => $this,
                            'attribute' => 'firstScreenVideo',
                            'saveAttribute' => EntityToFile::TYPE_SERVICES_FIRST_SCREEN_VIDEO,
                            //'aspectRatio' => 300/200, //Пропорция для кропа
                            'multiple' => true, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
                    'sign' => [
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => false,
                    ],
                ],
                Yii::t('back/page_services', 'What we do') => [
                    'what_we_do_block_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'what_we_do_block_description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
                    $this->getRelatedFormConfig()['what_we_do_block']
                ],
                Yii::t('back/page_services', 'How we do') => [
                    'how_we_do_that_block_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'how_we_do_that_block_description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,

                    ],
                    $this->getRelatedFormConfig()['how_we_do_block']
                ],
                Yii::t('back/page_services', 'Projects block') => [
                    'project_block_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'project_block_description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
                    'project_block_button_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                ]
            ]
        ];
    }

    /**
    * @inheritdoc
    */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
    * @inheritdoc
    */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWhatWeDoBlock()
    {
        return $this->hasMany(PageServicesWhatWeDoBlock::className(), ['page_services_id' => 'id'])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHowWeDoBlock()
    {
        return $this->hasMany(PageServicesHowWeDoBlock::className(), ['page_services_id' => 'id'])
            ->orderBy('position');
    }

    /**
     * @return array
     */
    public function getRelatedFormConfig()
    {
        return [
            'what_we_do_block' => [
                'relation' => 'whatWeDoBlock', //имя реляции (всегда с маленькой буквы, от названия метода реляции убрать get)
            ],
            'how_we_do_block' => [
                'relation' => 'howWeDoBlock', //имя реляции (всегда с маленькой буквы, от названия метода реляции убрать get)
            ],
        ];
    }
}
