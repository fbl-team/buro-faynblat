<?php

namespace backend\modules\pages\models\services;

use Yii;

/**
* This is the model class for table "{{%page_services_how_we_do_block_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $description
*/
class PageServicesHowWeDoBlockTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_services_how_we_do_block_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/page_services', 'Label') . ' [' . $this->language . ']',
            'description' => Yii::t('back/page_services', 'Description') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label', 'description'], 'string'],
         ];
    }
}
