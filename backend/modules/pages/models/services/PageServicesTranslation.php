<?php

namespace backend\modules\pages\models\services;

use Yii;

/**
* This is the model class for table "{{%page_services_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $description
* @property string $what_we_do_block_label
* @property string $what_we_do_block_description
* @property string $how_we_do_that_block_label
* @property string $how_we_do_that_block_description
* @property string $project_block_label
* @property string $project_block_description
* @property string $project_block_button_label
*/
class PageServicesTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_services_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/page_services', 'Label') . ' [' . $this->language . ']',
            'description' => Yii::t('back/page_services', 'Description') . ' [' . $this->language . ']',
            'what_we_do_block_label' => Yii::t('back/page_services', 'What We Do Block Label') . ' [' . $this->language . ']',
            'what_we_do_block_description' => Yii::t('back/page_services', 'What We Do Block Description') . ' [' . $this->language . ']',
            'how_we_do_that_block_label' => Yii::t('back/page_services', 'How We Do That Block Label') . ' [' . $this->language . ']',
            'how_we_do_that_block_description' => Yii::t('back/page_services', 'How We Do That Block Description') . ' [' . $this->language . ']',
            'project_block_label' => Yii::t('back/page_services', 'Project Block Label') . ' [' . $this->language . ']',
            'project_block_description' => Yii::t('back/page_services', 'Project Block Description') . ' [' . $this->language . ']',
            'project_block_button_label' => Yii::t('back/page_services', 'Project Block Button Label') . ' [' . $this->language . ']',

            'footer_request_block_label' => Yii::t('back/page_services', 'Footer Request Block Label') . ' [' . $this->language . ']',
            'footer_request_block_button_label' => Yii::t('back/page_services', 'Footer Request Block Button Label') . ' [' . $this->language . ']',
            'footer_seo_block_label' => Yii::t('back/page_services', 'Footer Seo Block Label') . ' [' . $this->language . ']',
            'footer_seo_block_button_label' => Yii::t('back/page_services', 'Footer Seo Block Button Label') . ' [' . $this->language . ']',
            'footer_seo_block_text_left_column' => Yii::t('back/page_services', 'Footer Seo Block Text Left Column') . ' [' . $this->language . ']',
            'footer_seo_block_text_left_right' => Yii::t('back/page_services', 'Footer Seo Block Text Right Column') . ' [' . $this->language . ']',
            'footer_seo_block_button_label_hide' => Yii::t('back/page_main', 'Footer Seo Block Button Hide Label') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label', 'description', 'what_we_do_block_label', 'what_we_do_block_description', 'how_we_do_that_block_label', 'how_we_do_that_block_description', 'project_block_label', 'project_block_description', 'project_block_button_label'], 'string'],
            [['footer_seo_block_button_label_hide', 'footer_request_block_label', 'footer_request_block_button_label', 'footer_seo_block_label', 'footer_seo_block_button_label', 'footer_seo_block_text_left_column', 'footer_seo_block_text_left_right'], 'safe']
        ];
    }
}
