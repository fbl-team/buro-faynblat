<?php

namespace backend\modules\pages\models\services;

use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use backend\modules\videoUpload\models\VideoUploadModel;
use backend\modules\videoUpload\widgets\videoUpload\VideoUpload;
use common\models\EntityToFile;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;

/**
 * This is the model class for table "{{%page_services_what_we_do_block}}".
 *
 * @property integer $id
 * @property integer $page_services_id
 * @property string $label
 * @property string $description
 * @property integer $published
 * @property integer $position
 *
 * @property PageServices $pageServices
 * @property PageServicesWhatWeDoBlockTranslation[] $translations
 */
class PageServicesWhatWeDoBlock extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;
    public $titleImage;
    public $titleVideo;

    /**
     * Temporary sign which used for saving images before model save
     * @var
     */
    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_services_what_we_do_block}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['page_services_id'], 'required'],
            [['page_services_id', 'published', 'position'], 'integer'],
            [['label', 'description'], 'string'],
            [['element_url'], 'safe'],
            [['page_services_id'], 'exist', 'targetClass' => PageServices::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['sign'], 'string', 'max' => 255],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/page_services', 'ID'),
            'page_services_id' => Yii::t('back/page_services', 'Page Services ID'),
            'label' => Yii::t('back/page_services', 'Label'),
            'element_url' => Yii::t('back/page_services', 'Element link'),
            'titleImage' => Yii::t('back/page_services', 'Element Image'),
            'titleVideo' => Yii::t('back/page_services', 'Element Video'),
            'description' => Yii::t('back/page_services', 'Description'),
            'published' => Yii::t('back/page_services', 'Published'),
            'position' => Yii::t('back/page_services', 'Position'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageServices()
    {
        return $this->hasOne(PageServices::className(), ['id' => 'page_services_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageServicesWhatWeDoBlockTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/page_services', 'Page Services What We Do Block Elements');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'page_services_id',
                    // 'label:ntext',
                    // 'description:ntext',
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'page_services_id',
                    [
                        'attribute' => 'label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                    ],
                    'published:boolean',
                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new PageServicesWhatWeDoBlockSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
//            'page_services_id' => [
//                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
//                'items' => PageServices::getItems(),
//                'options' => [
//                    'prompt' => '',
//                ],
//            ],
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'description' => [
                'type' => ActiveFormBuilder::INPUT_TEXTAREA,
            ],
            'element_url' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
//                'hint' => Yii::t('back/content_pages', 'enter relative link, like - /about'),
            ],
            'titleImage' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'hint' => Yii::t('back/page_services', 'For correct view images on main page downloaded files must be in 700*450(px) format, or fold last one'),
                'value' => ImageUpload::widget([
                    'model' => $this,
                    'attribute' => 'titleImage',
                    'saveAttribute' => EntityToFile::TYPE_SERVICES_WHAT_WE_DO_IMAGE,
                    'aspectRatio' => 700/450, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ])
            ],
            'titleVideo' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'hint' => Yii::t('back/page_services', 'For correctly display video on web-site we recommend download 2 same video files with MP4 and WEBM formats'),
                'value' => VideoUpload::widget([
                    'model' => $this,
                    'attribute' => 'titleVideo',
                    'saveAttribute' => EntityToFile::TYPE_SERVICES_WHAT_WE_DO_VIDEO,
                    //'aspectRatio' => 300/200, //Пропорция для кропа
                    'multiple' => true, //Вкл/выкл множественную загрузку
                ])
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'sign' => [
                'type' => ActiveFormBuilder::INPUT_HIDDEN,
                'label' => false,
            ],
//            'position' => [
//                'type' => ActiveFormBuilder::INPUT_TEXT,
//            ],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }


}
