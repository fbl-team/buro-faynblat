<?php

namespace backend\modules\pages\models\services;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PageServicesSearch represents the model behind the search form about `PageServices`.
 */
class PageServicesSearch extends PageServices
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['label', 'description', 'what_we_do_block_label', 'what_we_do_block_description', 'how_we_do_that_block_label', 'how_we_do_that_block_description', 'project_block_label', 'project_block_description', 'project_block_button_label'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PageServices::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'what_we_do_block_label', $this->what_we_do_block_label])
            ->andFilterWhere(['like', 'what_we_do_block_description', $this->what_we_do_block_description])
            ->andFilterWhere(['like', 'how_we_do_that_block_label', $this->how_we_do_that_block_label])
            ->andFilterWhere(['like', 'how_we_do_that_block_description', $this->how_we_do_that_block_description])
            ->andFilterWhere(['like', 'project_block_label', $this->project_block_label])
            ->andFilterWhere(['like', 'project_block_description', $this->project_block_description])
            ->andFilterWhere(['like', 'project_block_button_label', $this->project_block_button_label]);

        return $dataProvider;
    }
}
