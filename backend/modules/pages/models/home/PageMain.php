<?php

namespace backend\modules\pages\models\home;

use backend\modules\videoUpload\models\VideoUploadModel;
use backend\modules\videoUpload\widgets\videoUpload\VideoUpload;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;
use backend\modules\imagesUpload\models\ImagesUploadModel;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%page_main}}".
 *
 * @property integer $id
 * @property string $first_screen_label
 * @property string $filosophy_block_label
 * @property string $filosophy_block_short_description
 * @property string $filosophy_block_full_description
 * @property string $filosophy_block_button_label
 * @property string $project_block_label
 * @property string $project_block_short_description
 * @property string $project_block_button_label
 * @property string $blog_block_label
 * @property string $blog_block_short_description
 * @property string $blog_block_button_label
 *
 * @property PageMainTranslation[] $translations
 */
class PageMain extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;
    public $titleImage;
    public $filosophyBlockImage;
    public $firstScreenVideo;

    /**
    * Temporary sign which used for saving images before model save
    * @var
    */
    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_main}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_screen_vimeo_video', 'first_screen_under_play_button_text', 'first_screen_label', 'filosophy_block_label', 'filosophy_block_short_description', 'filosophy_block_full_description', 'filosophy_block_button_label', 'project_block_label', 'project_block_short_description', 'project_block_button_label', 'blog_block_label', 'blog_block_short_description', 'blog_block_button_label'], 'string'],
            [['sign'], 'string', 'max' => 255],
    
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/page_main', 'ID'),
            'firstScreenVideo' => Yii::t('back/page_main', 'First Screen Video'),
            'first_screen_label' => Yii::t('back/page_main', 'First Screen Label'),
            'first_screen_vimeo_video' => Yii::t('back/page_main', 'First Screen Vimeo Video Code'),
            'first_screen_under_play_button_text' => Yii::t('back/page_main', 'First Screen Under Play Button Text'),
            'filosophyBlockImage' => Yii::t('back/page_main', 'Filosophy Block Image'),
            'filosophy_block_label' => Yii::t('back/page_main', 'Filosophy Block Label'),
            'filosophy_block_short_description' => Yii::t('back/page_main', 'Filosophy Block Short Description'),
            'filosophy_block_full_description' => Yii::t('back/page_main', 'Filosophy Block Full Description'),
            'filosophy_block_button_label' => Yii::t('back/page_main', 'Filosophy Block Button Label'),
            'project_block_label' => Yii::t('back/page_main', 'Project Block Label'),
            'project_block_short_description' => Yii::t('back/page_main', 'Project Block Short Description'),
            'project_block_button_label' => Yii::t('back/page_main', 'Project Block Button Label'),
            'blog_block_label' => Yii::t('back/page_main', 'Project Block Label'),
            'blog_block_short_description' => Yii::t('back/page_main', 'Project Block Short Description'),
            'blog_block_button_label' => Yii::t('back/page_main', 'Project Block Button Label'),

            'footer_request_block_label' => Yii::t('back/page_main', 'Footer Request Block Label'),
            'footer_request_block_button_label' => Yii::t('back/page_main', 'Footer Request Block Button Label'),
            'footer_seo_block_label' => Yii::t('back/page_main', 'Footer Seo Block Label'),
            'footer_seo_block_button_label' => Yii::t('back/page_main', 'Footer Seo Block Button Label'),
            'footer_seo_block_text_left_column' => Yii::t('back/page_main', 'Footer Seo Block Text Left Column'),
            'footer_seo_block_text_left_right' => Yii::t('back/page_main', 'Footer Seo Block Text Right Column'),
            'footer_seo_block_button_label_hide' => Yii::t('back/page_main', 'Footer Seo Block Button Hide Label'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'first_screen_label',
            'filosophy_block_label',
            'filosophy_block_short_description',
            'filosophy_block_full_description',
            'filosophy_block_button_label',
            'project_block_label',
            'project_block_short_description',
            'project_block_button_label',
            'blog_block_label',
            'blog_block_short_description',
            'blog_block_button_label',
            'first_screen_under_play_button_text',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
            'footer' => [
                'class' => \backend\components\FooterBehavior::className(),
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageMainTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Page Main');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    // 'first_screen_label:ntext',
                    // 'filosophy_block_label:ntext',
                    // 'filosophy_block_short_description:ntext',
                    // 'filosophy_block_full_description:ntext',
                    // 'filosophy_block_button_label:ntext',
                    // 'project_block_label:ntext',
                    // 'project_block_short_description:ntext',
                    // 'project_block_button_label:ntext',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    [
                        'attribute' => 'first_screen_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'filosophy_block_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'filosophy_block_short_description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'filosophy_block_full_description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'filosophy_block_button_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'project_block_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'project_block_short_description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'project_block_button_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'blog_block_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'blog_block_short_description',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'blog_block_button_label',
                        'format' => 'html',
                    ],
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new PageMainSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
            'form-set' => [
                Yii::t('back/page_main', 'First screen content') => [
                    'firstScreenVideo' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'hint' => Yii::t('back/page_main', 'For correctly display video on web-site we recommend download 2 same video files with MP4 and WEBM formats'),
                        'value' => VideoUpload::widget([
                            'model' => $this,
                            'attribute' => 'firstScreenVideo',
                            'saveAttribute' => EntityToFile::TYPE_PAGE_HOME_FIRST_SCREEN_VIDEO,
                            //'aspectRatio' => 300/200, //Пропорция для кропа
                            'multiple' => true, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'firstScreenImage' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'hint' => Yii::t('back/page_main', 'For correct view images on main page downloaded files must be in 1350*550(px) format, or fold last one'),
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'titleImage',
                            'saveAttribute' => EntityToFile::TYPE_PAGE_HOME_FIRST_SCREEN_IMAGE,
                            'aspectRatio' => 1350/550,
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'first_screen_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'hint' => Yii::t('back/page_main', 'For split text on new row use span - tag. For example: &ltspan&gt first row &lt/span&gt &ltspan&gt second row &lt/span&gt &ltspan&gt third row &lt/span&gt'),
                    ],
                    'first_screen_vimeo_video' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'hint' => Yii::t('back/page_main', 'Enter vimeo video code (id)'),
                    ],
                    'first_screen_under_play_button_text' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'sign' => [
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => false,
                    ],
                ],
                Yii::t('back/page_main', 'Filosophy screen content') => [
                    'filosophyBlockImage' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'hint' => Yii::t('back/projects', 'For correct view images on main page downloaded files must be in 500*600(px) format, or fold last one'),
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'filosophyBlockImage',
                            'saveAttribute' => EntityToFile::TYPE_PAGE_HOME_FILOSOPHY_BLOCK_IMAGE,
                            'aspectRatio' => 500/600, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'filosophy_block_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'filosophy_block_short_description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                        'hint' => Yii::t('back/about', 'To select the words that will appear with an animation use b - tag. For example: text without animation &ltb&gt ANIMATED &lt/b&gt text without animation'),
                    ],
                    'filosophy_block_full_description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
                    'filosophy_block_button_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                ],
                Yii::t('back/page_main', 'Projects block') => [
                    'project_block_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'project_block_short_description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                        'hint' => Yii::t('back/page_main', 'To select the words that will appear with an animation use b - tag. For example: text without animation &ltb&gt ANIMATED &lt/b&gt text without animation'),
                    ],
                    'project_block_button_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                ],
                Yii::t('back/page_main', 'Blog block') => [
                    'blog_block_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                    'blog_block_short_description' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                        'hint' => Yii::t('back/page_main', 'To select the words that will appear with an animation use b - tag. For example: text without animation &ltb&gt ANIMATED &lt/b&gt text without animation'),
                    ],
                    'blog_block_button_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                ]
            ]
        ];
    }

    /**
    * @inheritdoc
    */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        EntityToFile::updateImages($this->id, $this->sign);
    }

    /**
    * @inheritdoc
    */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }

    /**
     * @return $this
     */
    public function getFilosophyBlockImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->alias('t2')
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_PAGE_HOME_FILOSOPHY_BLOCK_IMAGE])
            ->orderBy('t2.position DESC');
    }

    /**
     * @return $this
     */
    public function getFirstScreenVideos()
    {
        return $this->hasMany(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->alias('t2')
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_PAGE_HOME_FIRST_SCREEN_VIDEO])
            ->orderBy('t2.position DESC');
    }
}
