<?php

namespace backend\modules\pages\models\home;

use Yii;

/**
* This is the model class for table "{{%page_main_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $first_screen_label
* @property string $filosophy_block_label
* @property string $filosophy_block_short_description
* @property string $filosophy_block_full_description
* @property string $filosophy_block_button_label
* @property string $project_block_label
* @property string $project_block_short_description
* @property string $project_block_button_label
* @property string $blog_block_label
* @property string $blog_block_short_description
* @property string $blog_block_button_label
*/
class PageMainTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_main_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'first_screen_label' => Yii::t('back/page_main', 'First Screen Label') . ' [' . $this->language . ']',
            'first_screen_under_play_button_text' => Yii::t('back/page_main', 'First Screen Under Play Button Text') . ' [' . $this->language . ']',
            'filosophy_block_label' => Yii::t('back/page_main', 'Filosophy Block Label') . ' [' . $this->language . ']',
            'filosophy_block_short_description' => Yii::t('back/page_main', 'Filosophy Block Short Description') . ' [' . $this->language . ']',
            'filosophy_block_full_description' => Yii::t('back/page_main', 'Filosophy Block Full Description') . ' [' . $this->language . ']',
            'filosophy_block_button_label' => Yii::t('back/page_main', 'Filosophy Block Button Label') . ' [' . $this->language . ']',
            'project_block_label' => Yii::t('back/page_main', 'Project Block Label') . ' [' . $this->language . ']',
            'project_block_short_description' => Yii::t('back/page_main', 'Project Block Short Description') . ' [' . $this->language . ']',
            'project_block_button_label' => Yii::t('back/page_main', 'Project Block Button Label') . ' [' . $this->language . ']',
            'blog_block_label' => Yii::t('back/page_main', 'Project Block Label') . ' [' . $this->language . ']',
            'blog_block_short_description' => Yii::t('back/page_main', 'Project Block Short Description') . ' [' . $this->language . ']',
            'blog_block_button_label' => Yii::t('back/page_main', 'Project Block Button Label') . ' [' . $this->language . ']',

            'footer_request_block_label' => Yii::t('back/page_main', 'Footer Request Block Label') . ' [' . $this->language . ']',
            'footer_request_block_button_label' => Yii::t('back/page_main', 'Footer Request Block Button Label') . ' [' . $this->language . ']',
            'footer_seo_block_label' => Yii::t('back/page_main', 'Footer Seo Block Label') . ' [' . $this->language . ']',
            'footer_seo_block_button_label' => Yii::t('back/page_main', 'Footer Seo Block Button Label') . ' [' . $this->language . ']',
            'footer_seo_block_text_left_column' => Yii::t('back/page_main', 'Footer Seo Block Text Left Column') . ' [' . $this->language . ']',
            'footer_seo_block_text_left_right' => Yii::t('back/page_main', 'Footer Seo Block Text Right Column') . ' [' . $this->language . ']',
            'footer_seo_block_button_label_hide' => Yii::t('back/page_main', 'Footer Seo Block Button Hide Label') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['first_screen_under_play_button_text', 'first_screen_label', 'filosophy_block_label', 'filosophy_block_short_description', 'filosophy_block_full_description', 'filosophy_block_button_label', 'project_block_label', 'project_block_short_description', 'project_block_button_label', 'blog_block_label', 'blog_block_short_description', 'blog_block_button_label'], 'string'],
            [['footer_seo_block_button_label_hide', 'footer_request_block_label', 'footer_request_block_button_label', 'footer_seo_block_label', 'footer_seo_block_button_label', 'footer_seo_block_text_left_column', 'footer_seo_block_text_left_right'], 'safe']
        ];
    }
}
