<?php

namespace backend\modules\pages\models\home;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PageMainSearch represents the model behind the search form about `PageMain`.
 */
class PageMainSearch extends PageMain
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['first_screen_label', 'filosophy_block_label', 'filosophy_block_short_description', 'filosophy_block_full_description', 'filosophy_block_button_label', 'project_block_label', 'project_block_short_description', 'project_block_button_label', 'blog_block_label', 'blog_block_short_description', 'blog_block_button_label'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PageMain::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'first_screen_label', $this->first_screen_label])
            ->andFilterWhere(['like', 'filosophy_block_label', $this->filosophy_block_label])
            ->andFilterWhere(['like', 'filosophy_block_short_description', $this->filosophy_block_short_description])
            ->andFilterWhere(['like', 'filosophy_block_full_description', $this->filosophy_block_full_description])
            ->andFilterWhere(['like', 'filosophy_block_button_label', $this->filosophy_block_button_label])
            ->andFilterWhere(['like', 'project_block_label', $this->project_block_label])
            ->andFilterWhere(['like', 'project_block_short_description', $this->project_block_short_description])
            ->andFilterWhere(['like', 'project_block_button_label', $this->project_block_button_label])
            ->andFilterWhere(['like', 'blog_block_label', $this->project_block_label])
            ->andFilterWhere(['like', 'blog_block_short_description', $this->project_block_short_description])
            ->andFilterWhere(['like', 'blog_block_button_label', $this->project_block_button_label]);

        return $dataProvider;
    }
}
