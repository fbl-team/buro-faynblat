<?php

namespace backend\modules\pages\models\allBlogs;

use backend\components\FooterBehavior;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;

/**
 * This is the model class for table "{{%page_all_blogs}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $description
 *
 * @property PageAllBlogsTranslation[] $translations
 */
class PageAllBlogs extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_all_blogs}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'description'], 'string'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/all_blogs', 'ID'),
            'label' => Yii::t('back/all_blogs', 'Label'),
            'description' => Yii::t('back/all_blogs', 'Description'),

            'footer_request_block_label' => Yii::t('back/all_blogs', 'Footer Request Block Label'),
            'footer_request_block_button_label' => Yii::t('back/all_blogs', 'Footer Request Block Button Label'),
            'footer_seo_block_label' => Yii::t('back/all_blogs', 'Footer Seo Block Label'),
            'footer_seo_block_button_label' => Yii::t('back/all_blogs', 'Footer Seo Block Button Label'),
            'footer_seo_block_text_left_column' => Yii::t('back/all_blogs', 'Footer Seo Block Text Left Column'),
            'footer_seo_block_text_left_right' => Yii::t('back/all_blogs', 'Footer Seo Block Text Right Column'),
            'footer_seo_block_button_label_hide' => Yii::t('back/all_blogs', 'Footer Seo Block Button Hide Label'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
            'footer' => [
                'class' => \backend\components\FooterBehavior::className(),
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageAllBlogsTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Page All Blogs');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    // 'label:ntext',
                    // 'description:ntext',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    [
                        'attribute' => 'label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                    ],
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new PageAllBlogsSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'description' => [
                'type' => ActiveFormBuilder::INPUT_TEXTAREA,
            ],
        ];
    }

}
