<?php

namespace backend\modules\pages\models\oneBlog;

use Yii;

/**
* This is the model class for table "{{%page_one_blog_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $breadcrums_label
* @property string $breadcrums_url
* @property string $another_blog_block_label
* @property string $another_blog_block_published
* @property string $another_blog_button_label
*/
class PageOneBlogTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_one_blog_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'breadcrums_label' => Yii::t('back/page_one_blog', 'Breadcrums Label') . ' [' . $this->language . ']',
            'breadcrums_url' => Yii::t('back/page_one_blog', 'Breadcrums Url') . ' [' . $this->language . ']',
            'add_content_button_label' => Yii::t('back/page_one_blog', 'Another Blog Block Label') . ' [' . $this->language . ']',
            'another_blog_block_label' => Yii::t('back/page_one_blog', 'Another Blog Block Label') . ' [' . $this->language . ']',
            'another_blog_block_published' => Yii::t('back/page_one_blog', 'Another Blog Block Published') . ' [' . $this->language . ']',
            'another_blog_button_label' => Yii::t('back/page_one_blog', 'Another Blog Button Label') . ' [' . $this->language . ']',
            'footer_request_block_label' => Yii::t('back/page_one_blog', 'Footer Request Block Label') . ' [' . $this->language . ']',
            'footer_request_block_button_label' => Yii::t('back/page_one_blog', 'Footer Request Block Button Label') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['footer_request_block_label', 'footer_request_block_button_label', 'add_content_button_label', 'breadcrums_label', 'breadcrums_url', 'another_blog_block_label', 'another_blog_block_published', 'another_blog_button_label'], 'string'],
         ];
    }
}
