<?php

namespace backend\modules\pages\models\contacts;

use common\helpers\ContactsHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PageContactsSearch represents the model behind the search form about `PageContacts`.
 */
class PageContactsElementsColumn2Search extends PageContactsElementsColumn1
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['column_type', 'label', 'first_column_label', 'second_column_label', 'third_column_label'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PageContacts::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'column_type' => ContactsHelper::CONTACTS_COLUMN_2
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'first_column_label', $this->first_column_label])
            ->andFilterWhere(['like', 'second_column_label', $this->second_column_label])
            ->andFilterWhere(['like', 'third_column_label', $this->third_column_label]);

        return $dataProvider;
    }
}
