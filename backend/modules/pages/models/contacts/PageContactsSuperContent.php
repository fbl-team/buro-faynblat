<?php

namespace backend\modules\pages\models\contacts;

use common\helpers\ContactsHelper;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;

/**
 * This is the model class for table "{{%page_contacts_super_content}}".
 *
 * @property integer $id
 * @property integer $page_contacts_id
 * @property integer $content_type
 * @property string $content
 * @property integer $published
 * @property integer $position
 *
 * @property PageContacts $pageContacts
 * @property PageContactsSuperContentTranslation[] $translations
 */
class PageContactsSuperContent extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_contacts_super_content}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['column_type', 'content_type'], 'required'],
            [['page_contacts_id', 'content_type', 'published', 'position'], 'integer'],
            [['content'], 'string'],
            [['page_contacts_id'], 'exist', 'targetClass' => PageContacts::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/page_contacts', 'ID'),
            'page_contacts_id' => Yii::t('back/page_contacts', 'Page Contacts ID'),
            'content_type' => Yii::t('back/page_contacts', 'Content Type'),
            'content' => Yii::t('back/page_contacts', 'Content'),
            'published' => Yii::t('back/page_contacts', 'Published'),
            'position' => Yii::t('back/page_contacts', 'Position'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'content',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageContacts()
    {
        return $this->hasOne(PageContacts::className(), ['id' => 'page_contacts_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageContactsSuperContentTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Page Contacts Super Content');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'page_contacts_id',
                    'content_type',
                    // 'content:ntext',
                    'published:boolean',
                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'page_contacts_id',
                    'content_type',
                    [
                        'attribute' => 'content',
                        'format' => 'html',
                    ],
                    'published:boolean',
                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new PageContactsSuperContentSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
//            'page_contacts_id' => [
//                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
//                'items' => PageContacts::getItems(),
//                'options' => [
//                    'prompt' => '',
//                ],
//            ],
            'content_type' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => ContactsHelper::getTypeArrayForContacts(),
            ],
            'content' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
//            'position' => [
//                'type' => ActiveFormBuilder::INPUT_TEXT,
//            ],
            
        ];
    }

}
