<?php

namespace backend\modules\pages\models\contacts;

use common\helpers\ContactsHelper;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;

/**
 * This is the model class for table "{{%page_contacts_super_content}}".
 *
 * @property integer $id
 * @property integer $page_contacts_id
 * @property integer $content_type
 * @property string $content
 * @property integer $published
 * @property integer $position
 *
 * @property PageContacts $pageContacts
 * @property PageContactsSuperContentTranslation[] $translations
 */
class PageContactsElementsColumn1 extends PageContactsSuperContent
{
    public function init()
    {
        parent::init();
        $this->column_type = ContactsHelper::CONTACTS_COLUMN_1;
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new PageContactsElementsColumn1Search();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageContactsElementsColumn1Translation::className(), ['model_id' => 'id']);
    }
}
