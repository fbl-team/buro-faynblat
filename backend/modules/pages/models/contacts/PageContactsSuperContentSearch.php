<?php

namespace backend\modules\pages\models\contacts;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * PageContactsSuperContentSearch represents the model behind the search form about `PageContactsSuperContent`.
 */
class PageContactsSuperContentSearch extends PageContactsSuperContent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'page_contacts_id', 'content_type', 'published', 'position'], 'integer'],
            [['content'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PageContactsSuperContent::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'page_contacts_id' => $this->page_contacts_id,
            'content_type' => $this->content_type,
            'published' => $this->published,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'content', $this->content]);

        return $dataProvider;
    }
}
