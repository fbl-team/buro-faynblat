<?php
namespace backend\modules\pages\models\contacts;

/**
* This is the model class for table "{{%PageContactsElementsColumn2}}".
*
* @property integer $model_id
* @property string $language
* @property string $content
*/
class PageContactsElementsColumn2Translation extends PageContactsSuperContentTranslation
{

}
