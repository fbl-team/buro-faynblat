<?php

namespace backend\modules\pages\models\contacts;

use Yii;

/**
* This is the model class for table "{{%page_contacts_super_content_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $content
*/
class PageContactsSuperContentTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_contacts_super_content_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'content' => Yii::t('back/page_contacts', 'Content') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['content'], 'string'],
         ];
    }
}
