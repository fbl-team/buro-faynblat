<?php
namespace backend\modules\pages\models\contacts;

/**
* This is the model class for table "{{%PageContactsElementsColumn1}}".
*
* @property integer $model_id
* @property string $language
* @property string $content
*/
class PageContactsElementsColumn1Translation extends PageContactsSuperContentTranslation
{

}
