<?php

namespace backend\modules\pages\models\contacts;

use Yii;

/**
* This is the model class for table "{{%page_contacts_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $first_column_label
* @property string $second_column_label
* @property string $third_column_label
* @property string $four_column_label
*/
class PageContactsTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%page_contacts_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/page_contacts', 'Label') . ' [' . $this->language . ']',
            'first_column_label' => Yii::t('back/page_contacts', 'Content') . ' [' . $this->language . ']',
            'second_column_label' => Yii::t('back/page_contacts', 'Content') . ' [' . $this->language . ']',
            'third_column_label' => Yii::t('back/page_contacts', 'Content') . ' [' . $this->language . ']',
            'four_column_label' => Yii::t('back/page_contacts', 'Content') . ' [' . $this->language . ']',

            'footer_request_block_label' => Yii::t('back/page_contacts', 'Footer Request Block Label') . ' [' . $this->language . ']',
            'footer_request_block_button_label' => Yii::t('back/page_contacts', 'Footer Request Block Button Label') . ' [' . $this->language . ']',
            'footer_seo_block_label' => Yii::t('back/page_contacts', 'Footer Seo Block Label') . ' [' . $this->language . ']',
            'footer_seo_block_button_label' => Yii::t('back/page_contacts', 'Footer Seo Block Button Label') . ' [' . $this->language . ']',
            'footer_seo_block_text_left_column' => Yii::t('back/page_contacts', 'Footer Seo Block Text Left Column') . ' [' . $this->language . ']',
            'footer_seo_block_text_left_right' => Yii::t('back/page_contacts', 'Footer Seo Block Text Right Column') . ' [' . $this->language . ']',
            'footer_seo_block_button_label_hide' => Yii::t('back/page_contacts', 'Footer Seo Block Button Hide Label') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [['first_column_label', 'second_column_label', 'third_column_label', 'four_column_label'], 'string'],
            [['label'], 'string', 'max' => 255],
            [['footer_seo_block_button_label_hide', 'footer_request_block_label', 'footer_request_block_button_label', 'footer_seo_block_label', 'footer_seo_block_button_label', 'footer_seo_block_text_left_column', 'footer_seo_block_text_left_right'], 'safe']
         ];
    }
}
