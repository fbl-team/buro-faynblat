<?php

namespace backend\modules\pages\models\contacts;

use common\helpers\ContactsHelper;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;

/**
 * This is the model class for table "{{%page_contacts}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $first_column_label
 * @property string $second_column_label
 * @property string $third_column_label
 * @property string $four_column_label
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property PageContactsSuperContent[] $pageContactsSuperContents
 * @property PageContactsTranslation[] $translations
 */
class PageContacts extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_contacts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [
                [
                    'first_column_label',
                    'second_column_label',
                    'third_column_label',
                    'four_column_label',
                ],
                'string'
            ],
            [['label'], 'string', 'max' => 255],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/page_contacts', 'ID'),
            'label' => Yii::t('back/page_contacts', 'Label'),
            'first_column_label' => Yii::t('back/page_contacts', 'Content'),
            'second_column_label' => Yii::t('back/page_contacts', 'Content'),
            'third_column_label' => Yii::t('back/page_contacts', 'Content'),
            'four_column_label' => Yii::t('back/page_contacts', 'Content'),

            'footer_request_block_label' => Yii::t('back/page_contacts', 'Footer Request Block Label'),
            'footer_request_block_button_label' => Yii::t('back/page_contacts', 'Footer Request Block Button Label'),
            'footer_seo_block_label' => Yii::t('back/page_contacts', 'Footer Seo Block Label'),
            'footer_seo_block_button_label' => Yii::t('back/page_contacts', 'Footer Seo Block Button Label'),
            'footer_seo_block_text_left_column' => Yii::t('back/page_contacts', 'Footer Seo Block Text Left Column'),
            'footer_seo_block_text_left_right' => Yii::t('back/page_contacts', 'Footer Seo Block Text Right Column'),
            'footer_seo_block_button_label_hide' => Yii::t('back/page_contacts', 'Footer Seo Block Button Hide Label'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'first_column_label',
            'second_column_label',
            'third_column_label',
            'four_column_label',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
            'footer' => [
                'class' => \backend\components\FooterBehavior::className(),
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PageContactsTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Page Contacts');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    // 'first_column_label:ntext',
                    // 'second_column_label:ntext',
                    // 'third_column_label:ntext',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'label',
                    [
                        'attribute' => 'first_column_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'second_column_label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'third_column_label',
                        'format' => 'html',
                    ],
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new PageContactsSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
            'form-set' => [
                Yii::t('back/page_contacts', 'Main content') => [
                    'label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                ],
                Yii::t('back/page_contacts', 'First column') => [
                    'first_column_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT
                    ],
                    $this->getRelatedFormConfig()['first_elements_column']
                ],
                Yii::t('back/page_contacts', 'Second column') => [
                    'second_column_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT
                    ],
                    $this->getRelatedFormConfig()['second_elements_column']
                ],
                Yii::t('back/page_contacts', 'Third column') => [
                    'third_column_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT
                    ],
                    $this->getRelatedFormConfig()['third_elements_column']
                ],
                Yii::t('back/page_contacts', 'Address') => [
                    'four_column_label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    ],
                ],
            ]

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageContactsElementsColumn1()
    {
        return $this->hasMany(PageContactsElementsColumn1::className(), ['page_contacts_id' => 'id'])
            ->andWhere(['column_type' => ContactsHelper::CONTACTS_COLUMN_1])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageContactsElementsColumn2()
    {
        return $this->hasMany(PageContactsElementsColumn2::className(), ['page_contacts_id' => 'id'])
            ->andWhere(['column_type' => ContactsHelper::CONTACTS_COLUMN_2])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageContactsElementsColumn3()
    {
        return $this->hasMany(PageContactsElementsColumn3::className(), ['page_contacts_id' => 'id'])
            ->andWhere(['column_type' => ContactsHelper::CONTACTS_COLUMN_3])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageContactsElementsColumn4()
    {
        return $this->hasMany(PageContactsElementsColumn4::className(), ['page_contacts_id' => 'id'])
            ->andWhere(['column_type' => ContactsHelper::CONTACTS_COLUMN_4])
            ->orderBy('position');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPageContactsSuperContents()
    {
        return $this->hasMany(PageContactsSuperContent::className(), ['page_contacts_id' => 'id']);
    }

    /**
     * @return array
     */
    public function getRelatedFormConfig()
    {
        return [
            'first_elements_column' => [
                'relation' => 'pageContactsElementsColumn1', //имя реляции (всегда с маленькой буквы, от названия метода реляции убрать get)
            ],
            'second_elements_column' => [
                'relation' => 'pageContactsElementsColumn2', //имя реляции (всегда с маленькой буквы, от названия метода реляции убрать get)
            ],
            'third_elements_column' => [
                'relation' => 'pageContactsElementsColumn3', //имя реляции (всегда с маленькой буквы, от названия метода реляции убрать get)
            ],
        ];
    }

}
