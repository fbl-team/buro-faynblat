<?php

namespace backend\modules\imagesUpload\controllers;

use backend\components\BackendController;
use backend\modules\imagesUpload\models\AltImages;

/**
 * AltImagesController implements the CRUD actions for AltImages model.
 */
class AltImagesController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return AltImages::className();
    }
}
