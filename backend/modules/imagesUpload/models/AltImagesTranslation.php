<?php

namespace backend\modules\imagesUpload\models;

use Yii;

/**
* This is the model class for table "{{%alt_images_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
*/
class AltImagesTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%alt_images_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back\alt-images', 'Label') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'string', 'max' => 145],
         ];
    }
}
