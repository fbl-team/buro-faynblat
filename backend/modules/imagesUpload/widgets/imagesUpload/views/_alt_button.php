<?php
use \backend\modules\imagesUpload\models\ImagesUploadModel;

?>
<a class="alt-link btn btn-xs btn-default pull-right" data-toggle="modal" href="<?= ImagesUploadModel::getAltUrl(['id' => '']) ?>" {dataKey} data-target=".modal-hidden"  data-dismiss="modal">
    <i class="glyphicon glyphicon-tag file-icon-large text-success"></i>
</a>
