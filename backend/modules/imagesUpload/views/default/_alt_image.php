<?php
use \backend\modules\imagesUpload\models\ImagesUploadModel;
use \common\components\model\Translateable;
use yii\helpers\Html;
$langModels = $model->getTranslationModels();
?>
<div class="container form-group">
    <?=Html::tag('label', $model->attributeLabels()['label'], ['class' => 'control-label']); ?>
    <?=Html::hiddenInput($model->formName().'[file_id]', $file, ['id' => 'file_id']); ?>
    <?=Html::activeTextInput($model,'label', ['class' => 'form-control'])?>
    <?php foreach ($langModels as $key => $value) : ?>
        <div class="language_alt">
            <?=Html::activeHiddenInput($value, 'language' , []) ?>
            <?=Html::tag('label', $value->attributeLabels()['label'] , ['class' => 'control-label']); ?>
            <?=Html::activeTextInput($value,'label', ['class' => 'form-control'])?>
        </div>
    <?php endforeach; ?>
    <div class="col-sm-8 margined centered" style="text-align: center">
        <?= Html::a(Yii::t('back\buttons', 'Save'), ImagesUploadModel::saveAltUrl(), ['class' => 'btn btn-info save-alt']); ?>
        <?= Html::a(Yii::t('back\buttons', 'Cancel'), '#', ['class' => 'btn btn-warning cancel-crop']); ?>
    </div>
</div>




