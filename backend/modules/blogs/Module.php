<?php

namespace backend\modules\blogs;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\blogs\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
