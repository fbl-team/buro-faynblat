<?php

namespace backend\modules\blogs\models;

use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;

/**
 * This is the model class for table "{{%blog_parametrs}}".
 *
 * @property integer $id
 * @property integer $blog_id
 * @property string $label
 * @property string $description
 *
 * @property Blogs $blog
 * @property BlogParametrsTranslation[] $translations
 */
class BlogParametrs extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_parametrs}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['blog_id'], 'required'],
            [['blog_id'], 'integer'],
            [['label', 'description'], 'string'],
            [['blog_id'], 'exist', 'targetClass' => Blogs::className(), 'targetAttribute' => 'id'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/blogs', 'ID'),
            'blog_id' => Yii::t('back/blogs', 'Blog ID'),
            'label' => Yii::t('back/blogs', 'Label'),
            'description' => Yii::t('back/blogs', 'Description'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'description',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlog()
    {
        return $this->hasOne(Blogs::className(), ['id' => 'blog_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(BlogParametrsTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/blogs', 'Blog Parametrs');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'blog_id',
                    // 'label:ntext',
                    // 'description:ntext',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'blog_id',
                    [
                        'attribute' => 'label',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'description',
                        'format' => 'html',
                    ],
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new BlogParametrsSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
//            'blog_id' => [
//                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
//                'items' => Blogs::getItems(),
//                'options' => [
//                    'prompt' => '',
//                ],
//            ],
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'description' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],

            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
//            'position' => [
//                'type' => ActiveFormBuilder::INPUT_TEXT,
//            ],
            
        ];
    }

}
