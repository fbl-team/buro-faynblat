<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 7/13/16
 * Time: 4:29 PM
 */

namespace backend\modules\article\models\articleConstructorStrategies;


use backend\modules\article\models\Article;
use backend\modules\article\models\ArticleConstructorData;
use backend\modules\article\models\ArticleCTA;
use backend\modules\article\models\IConstructorStrategy;
use Yii;
use yii\helpers\Html;

class CTAStrategy implements IConstructorStrategy
{
    const TYPE = 2;
    
    public function getContent($model, $key = null)
    {
        $uniqueKey = \Yii::$app->security->generateRandomString(10);

        if ($key === null) {
            $key = $uniqueKey;
        }
        
        $content = '';

        $attribute = $this->createAttribute('cta_id', $key);

        $content .= Html::label($this->getLabel(), $attribute, ['class' => 'form-control label-constructor-meta-data']);
        $ctaItems = ArticleCTA::getItems('id', 'internal_name');
        $content .= Html::activeDropDownList($model, $attribute, $ctaItems, ['class' => 'form-control input-constructor-meta-data', 'placeholder' => Yii::t('back/articleConstructorHelper', 'Select CTA')]);
        
        return $content;
    }

    public function getLabel()
    {
        return Yii::t('back/articleConstructorHelper', 'CTA');
    }

    /**
     * @param $contentArr
     * @param Article $model
     */
    public function saveEntity($contentArr, $model)
    {
        $constructorDataModel = new ArticleConstructorData();
        $data = $contentArr['default_locale'];
        
        $constructorDataModel->article_id = $model->id;
        $constructorDataModel->type = static::TYPE;
        $constructorDataModel->cta_id = $data['cta_id'];
        $constructorDataModel->save();
    }

    /**
     * @param Article $model
     * @param ArticleConstructorData $component
     */
    public function loadData($model, $component)
    {
        $model->content[$component->id][static::TYPE]['default_locale']['cta_id'] = $component->cta_id;
    }

    public function createAttribute($name, $key, $locale = 'default_locale')
    {
        return 'content[' . $key . '][' . static::TYPE . '][' . $locale . '][' . $name . ']';
    }
}