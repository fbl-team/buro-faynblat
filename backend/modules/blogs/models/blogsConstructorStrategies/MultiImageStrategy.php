<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 7/13/16
 * Time: 4:29 PM
 */

namespace backend\modules\blogs\models\blogsConstructorStrategies;


use common\models\EntityToFile;
use Yii;

class MultiImageStrategy extends ImageStrategy
{
    const TYPE = 2;

    /**
     * @inheritdoc
     */
    protected function isMultiUpload()
    {
        return true;
    }

    protected function getSaveAtributeConstant()
    {
        return EntityToFile::TYPE_BLOG_CONSTRUCTOR_TWO_IMAGES;
    }

    public function getLabel()
    {
        return Yii::t('back/articleConstructorHelper', 'Two Images');
    }
}