<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 7/13/16
 * Time: 4:29 PM
 */

namespace backend\modules\blogs\models\blogsConstructorStrategies;

use common\models\EntityToFile;

class SingleImageStrategy extends ImageStrategy
{
    const TYPE = 1;

    /**
     * @return bool
     */
    protected function isMultiUpload()
    {
        return false;
    }

    protected function getSaveAtributeConstant()
    {
        return EntityToFile::TYPE_BLOG_CONSTRUCTOR_BIG_IMAGE;
    }

    public function getLabel()
    {
        return \Yii::t('back/blogs', 'Single Big Image');
    }
}