<?php
/**
 * Created by PhpStorm.
 * User: vadim
 * Date: 7/13/16
 * Time: 4:29 PM
 */

namespace backend\modules\blogs\models\blogsConstructorStrategies;

use backend\modules\blogs\models\IConstructorStrategy;
use backend\modules\blogs\models\BlogContent;
use backend\modules\blogs\models\BlogContentTranslation;
use common\helpers\LanguageHelper;
use Yii;
use yii\helpers\Html;

class TwoColumnsStrategy implements IConstructorStrategy
{
    const TYPE = 0;
    
    public function getContent($model, $key = null)
    {
        $uniqueKey = \Yii::$app->security->generateRandomString(10);

        if ($key === null) {
            $key = $uniqueKey;
        }
        
        $content = '';

        $attributeLeftColumn = $this->createAttribute('label', $key);
        $attributeRightColumn = $this->createAttribute('content', $key);

        $content .= Html::label($this->getLabel(), $attributeLeftColumn, ['class' => 'form-control label-constructor-meta-data']);
        $content .=  Html::activeTextarea($model, $attributeLeftColumn, ['class' => 'form-control input-constructor-meta-data blog-constructor-left-column', 'placeholder' => Yii::t('back/blogs', 'Enter left column text')]);
        $content .=  Html::activeTextarea($model, $attributeRightColumn, ['class' => 'form-control input-constructor-meta-data blog-constructor-right-column', 'placeholder' => Yii::t('back/blogs', 'Enter right column text')]);

        foreach (LanguageHelper::getLanguageModels() as $languageModel) {
            if (LanguageHelper::getDefaultLanguage()->locale !== $languageModel->locale) {
                $attributeLeftColumn = $this->createAttribute('label', $key, $languageModel->locale);
                $attributeRightColumn = $this->createAttribute('content', $key, $languageModel->locale);

                $content .= Html::label($this->getLabel() . '[' . $languageModel->locale . ']', $attributeLeftColumn, ['class' => 'form-control label-constructor-meta-data']);
                $content .=  Html::activeTextarea($model, $attributeLeftColumn, ['class' => 'form-control input-constructor-meta-data blog-constructor-left-column', 'placeholder' => Yii::t('back/blogs', 'Enter left column text')]);
                $content .=  Html::activeTextarea($model, $attributeRightColumn, ['class' => 'form-control input-constructor-meta-data blog-constructor-right-column', 'placeholder' => Yii::t('back/blogs', 'Enter right column text')]);
            }
        }
        
        return $content;
    }

    public function getLabel()
    {
        return Yii::t('back/blogs', 'Two text columns');
    }

    /**
     * @param $contentArr
     * @param Article $model
     */
    public function saveEntity($contentArr, $model)
    {
        $constructorDataModel = new BlogContent();
        $data = $contentArr['default_locale'];

        $constructorDataModel->blog_id = $model->id;
        $constructorDataModel->type = static::TYPE;
        $constructorDataModel->label = $data['label'];
        $constructorDataModel->content = $data['content'];
        $constructorDataModel->save(false);

        foreach (LanguageHelper::getLanguageModels() as $languageModel) {
            if (LanguageHelper::getDefaultLanguage()->locale !== $languageModel->locale) {
                $constructorDataModelT = new BlogContentTranslation();
                $data = $contentArr[$languageModel->locale];

                $constructorDataModelT->language = $languageModel->locale;
                $constructorDataModelT->model_id = $constructorDataModel->id;
                $constructorDataModelT->label = $data['label'];
                $constructorDataModelT->content = $data['content'];
                $constructorDataModelT->save(false);
            }
        }
    }

    /**
     * @param Article $model
     * @param ArticleConstructorData $component
     */
    public function loadData($model, $component)
    {
        $model->content[$component->id][static::TYPE]['default_locale']['label'] = $component->label;
        $model->content[$component->id][static::TYPE]['default_locale']['content'] = $component->content;
        /* @var ArticleConstructorDataTranslation[] $translationComponents */
        $translationComponents = $component->translations;
        foreach ($translationComponents as $translationComponent) {
            if (LanguageHelper::getDefaultLanguage()->locale !== $translationComponent->language) {
                $model->content[$component->id][static::TYPE][$translationComponent->language]['label'] = $translationComponent->label;
                $model->content[$component->id][static::TYPE][$translationComponent->language]['content'] = $translationComponent->content;
            }
        }
    }

    public function createAttribute($name, $key, $locale = 'default_locale')
    {
        return 'content[' . $key . '][' . static::TYPE . '][' . $locale . '][' . $name . ']';
    }
}