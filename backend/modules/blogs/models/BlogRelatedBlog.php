<?php

namespace backend\modules\blogs\models;

use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%blog_related_blog}}".
 *
 * @property integer $id
 * @property integer $blog_id
 * @property integer $related_blog_id
 *
 * @property Blogs $blog
 * @property Blogs $relatedBlog
 */
class BlogRelatedBlog extends \common\components\model\ActiveRecord implements BackendModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_related_blog}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['blog_id', 'related_blog_id'], 'required'],
            [['blog_id', 'related_blog_id'], 'integer'],
            [['blog_id'], 'exist', 'targetClass' => Blogs::className(), 'targetAttribute' => 'id'],
            [['related_blog_id'], 'exist', 'targetClass' => Blogs::className(), 'targetAttribute' => 'id'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/blogs', 'ID'),
            'blog_id' => Yii::t('back/blogs', 'Blog ID'),
            'related_blog_id' => Yii::t('back/blogs', 'Related Blog ID'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlog()
    {
        return $this->hasOne(Blogs::className(), ['id' => 'blog_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelatedBlog()
    {
        return $this->hasOne(Blogs::className(), ['id' => 'related_blog_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Blog Related Blog');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'blog_id',
                    'related_blog_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'blog_id',
                    'related_blog_id',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new BlogRelatedBlogSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
            'blog_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => Blogs::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'related_blog_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => Blogs::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            
        ];
    }

}
