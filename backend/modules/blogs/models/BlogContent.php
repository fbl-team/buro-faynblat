<?php

namespace backend\modules\blogs\models;

use common\models\EntityToFile;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;

/**
 * This is the model class for table "{{%blog_content}}".
 *
 * @property integer $id
 * @property integer $blog_id
 * @property string $type
 * @property string $label
 * @property string $content
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Blogs $blog
 * @property BlogContentTranslation[] $translations
 */
class BlogContent extends \common\components\model\ActiveRecord implements Translateable
{
    use \backend\components\TranslateableTrait;

    public $sign;
    public $image;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blog_content}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/blogs', 'ID'),
            'blog_id' => Yii::t('back/blogs', 'Blog ID'),
            'type' => Yii::t('back/blogs', 'Content'),
            'label' => Yii::t('back/blogs', 'Content'),
            'content' => Yii::t('back/blogs', 'Content'),
            'published' => Yii::t('back/blogs', 'Published'),
            'position' => Yii::t('back/blogs', 'Position'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'content',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlog()
    {
        return $this->hasOne(Blogs::className(), ['id' => 'blog_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(BlogContentTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Blog Content');
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new BlogContentSearch();
    }

    public function getSingleImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_BLOG_CONSTRUCTOR_BIG_IMAGE])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

    public function getMultiImage()
    {
        return $this->hasMany(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_CONSTRUCTOR_MULTI_IMAGE_BLOCK])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->content) {
            \Yii::$app->db->createCommand()
                ->update(
                    EntityToFile::tableName(),
                    [
                        'entity_model_id' => $this->id,
                    ],
                    'temp_sign = :ts',
                    [':ts' => $this->content]
                )
                ->execute();
        }
    }

}
