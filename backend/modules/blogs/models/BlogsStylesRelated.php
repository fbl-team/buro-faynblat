<?php

namespace backend\modules\blogs\models;

use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%blogs_styles_related}}".
 *
 * @property integer $id
 * @property integer $blog_id
 * @property integer $blog_style_id
 *
 * @property Blogs $blog
 * @property BlogsStyles $blogStyle
 */
class BlogsStylesRelated extends \common\components\model\ActiveRecord implements BackendModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blogs_styles_related}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['blog_id', 'blog_style_id'], 'required'],
            [['blog_id', 'blog_style_id'], 'integer'],
            [['blog_id'], 'exist', 'targetClass' => Blogs::className(), 'targetAttribute' => 'id'],
            [['blog_style_id'], 'exist', 'targetClass' => BlogsStyles::className(), 'targetAttribute' => 'id'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/blogs_styles', 'ID'),
            'blog_id' => Yii::t('back/blogs_styles', 'Blog ID'),
            'blog_style_id' => Yii::t('back/blogs_styles', 'Blog Style ID'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlog()
    {
        return $this->hasOne(Blogs::className(), ['id' => 'blog_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogStyle()
    {
        return $this->hasOne(BlogsStyles::className(), ['id' => 'blog_style_id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Blogs Styles Related');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'blog_id',
                    'blog_style_id',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'blog_id',
                    'blog_style_id',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new BlogsStylesRelatedSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
            'blog_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => Blogs::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            'blog_style_id' => [
                'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                'items' => BlogsStyles::getItems(),
                'options' => [
                    'prompt' => '',
                ],
            ],
            
        ];
    }

}
