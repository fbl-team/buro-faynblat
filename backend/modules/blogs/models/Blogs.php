<?php

namespace backend\modules\blogs\models;

use backend\components\ImperaviContent;
use backend\helpers\CheckboxHelper;
use backend\helpers\TypicalFunction;
use kartik\datecontrol\DateControl;
use kartik\select2\Select2;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;
use backend\modules\imagesUpload\models\ImagesUploadModel;
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use common\models\EntityToFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\DateTimePicker;

/**
 * This is the model class for table "{{%blogs}}".
 *
 * @property integer $id
 * @property string $label
 * @property string $announcement
 * @property string $city_and_square
 * @property integer $category_id
 * @property integer $show_on_main
 * @property integer $show_on_about
 * @property integer $show_on_services
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property BlogContent[] $blogContents
 * @property BlogCategory $category
 * @property BlogsTranslation[] $translations
 */
class Blogs extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    use \backend\components\TranslateableTrait;
    public $smallImage;

    private $constructor;

    public $content = [];
    public $relatedBlogs = [];
    public $styles = [];

    /**
     * Temporary sign which used for saving images before model save
     * @var
     */
    public $sign;

    public function init()
    {
        parent::init();

        if (!$this->sign) {
            $this->sign = \Yii::$app->security->generateRandomString();
        }
    }

    /**
     * Blogs constructor.
     * @param array $config
     * @param null $constructor
     */
    public function __construct(array $config = [], $constructor = null)
    {
        parent::__construct($config);
        $this->constructor = Yii::$container->get('BlogsConstructor');
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blogs}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'category_id', 'alias'], 'required'],
            [['label', 'alias', 'city_and_square'], 'string'],
            [['category_id', 'show_on_main', 'show_on_about', 'show_on_services', 'published', 'position','created_at'], 'integer'],
            [['category_id'], 'exist', 'targetClass' => BlogCategory::className(), 'targetAttribute' => 'id'],
            [['show_on_main'], 'default', 'value' => 1],
            [['show_on_about'], 'default', 'value' => 1],
            [['show_on_services'], 'default', 'value' => 1],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            [['sign'], 'string', 'max' => 255],
            [['blog_parameters', 'content', 'relatedBlogs', 'styles','announcement'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/blogs', 'ID'),
            'relatedBlogs' => Yii::t('back/blogs', 'Related blogs'),
            'styles' => Yii::t('back/blogs', 'Post Styles'),
            'smallImage' => Yii::t('back/blogs', 'Small image'),
            'label' => Yii::t('back/blogs', 'Label'),
            'city_and_square' => Yii::t('back/blogs', 'City And Square'),
            'category_id' => Yii::t('back/blogs', 'Category ID'),
            'show_on_main' => Yii::t('back/blogs', 'Show on main'),
            'show_on_about' => Yii::t('back/blogs', 'Show on about'),
            'show_on_services' => Yii::t('back/blogs', 'Show on services'),
            'published' => Yii::t('back/blogs', 'Published'),
            'position' => Yii::t('back/blogs', 'Position'),
            'blog_parameters' => Yii::t('back/blogs', 'Post parameters'),
            'announcement' => Yii::t('back/blogs', 'Post announcement'),
            'created_at' => Yii::t('back/blogs', 'Created At'),

            'footer_request_block_label' => Yii::t('back/blogs', 'Footer Request Block Label'),
            'footer_request_block_button_label' => Yii::t('back/blogs', 'Footer Request Block Button Label'),
            'footer_seo_block_label' => Yii::t('back/blogs', 'Footer Seo Block Label'),
            'footer_seo_block_button_label' => Yii::t('back/blogs', 'Footer Seo Block Button Label'),
            'footer_seo_block_text_left_column' => Yii::t('back/blogs', 'Footer Seo Block Text Left Column'),
            'footer_seo_block_text_left_right' => Yii::t('back/blogs', 'Footer Seo Block Text Right Column'),
            'footer_seo_block_button_label_hide' => Yii::t('back/blogs', 'Footer Seo Block Button Hide Label'),
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label',
            'announcement',
            'city_and_square',
            'blog_parameters',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
            'seo' => [
                'class' => \notgosu\yii2\modules\metaTag\components\MetaTagBehavior::className(),
            ],
            'footer' => [
                'class' => \backend\components\FooterBehavior::className(),
            ]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogContents()
    {
        return $this->hasMany(BlogContent::className(), ['blog_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(BlogCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(BlogsTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * Get title for the template page
     *
     * @return string
     */
    public function getTitle()
    {
        return \Yii::t('back/blogs', 'Blogs');
    }

    /**
     * Get attribute columns for index and view page
     *
     * @param $page
     *
     * @return array
     */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    'label',
                    // 'city_and_square:ntext',
                    [
                        'attribute' => 'category_id',
                        'filter' => BlogCategory::getItems(),
                        'format' => 'raw',
                        'value' => function ($data) {

                            return BlogCategory::getItems()[$data->category_id] ? BlogCategory::getItems()[$data->category_id] : 'category not selected!';

                        }
                    ],

                    'show_on_main:boolean',
                    'show_on_about:boolean',
                    'show_on_services:boolean',
                    'published:boolean',
                    [
                        'attribute' => 'position',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::input('text', null, $data->position, [
                                'class' => 'form-control position_input',
                                'data-url' => '/site/position',
                                'data-id' => $data->id,
                                'data-model-name' => $data->className(),
                            ]);
                        }
                    ],
                    ['class' => 'yii\grid\ActionColumn'],
                ];
                break;
            case 'view':
                return [
                    'id',
                    'label',
                    'alias',
                    'city_and_square',
                    'blog_parameters',
                    [
                        'attribute' => 'city_and_square',
                        'format' => 'html',
                    ],
                    [
                        'attribute' => 'category_id',
                        'format' => 'raw',
                        'value' => BlogCategory::getItems()[$this->category_id] ? BlogCategory::getItems()[$this->category_id] : 'category not selected!'
                    ],
                    'announcement:html',
                    [
                        'attribute' => 'smallImage',
                        'format' => 'raw',
                        'value' => TypicalFunction::getImage($this, 'blogSmallImage', ['blog', 'back'])
                    ],
                    'show_on_main:boolean',
                    'show_on_about:boolean',
                    'show_on_services:boolean',
                    'position',
                ];
                break;
        }

        return [];
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getSearchModel()
    {
        return new BlogsSearch();
    }

    /**
     * @return array
     */
    public function getFormConfig()
    {
        return [
            'form-set' => [
                Yii::t('back/blogs', 'Main content') => [
                    'announcement' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => ImperaviContent::className(),
                        'options' => [
                            'customSettings' => [
                                'buttons' => [
                                    'html',
                                    'formatting',
                                    'bold',
                                    'italic',
                                    'unorderedlist',
                                    'orderedlist',
                                    'link',
                                    'alignment',
                                    'horizontalrule',
                                ],
                            ],
                        ],
                    ],
                    'smallImage' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'hint' => Yii::t('back/blogs', 'For correct view images on main page downloaded files must be in 400*360(px) format, or fold last one'),
                        'value' => ImageUpload::widget([
                            'model' => $this,
                            'attribute' => 'smallImage',
                            'saveAttribute' => EntityToFile::TYPE_BLOG_SMALL_IMAGE,
                            'aspectRatio' => 400 / 360, //Пропорция для кропа
                            'multiple' => false, //Вкл/выкл множественную загрузку
                        ])
                    ],
                    'sign' => [
                        'type' => ActiveFormBuilder::INPUT_HIDDEN,
                        'label' => false,
                    ],
                    'label' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                            'class' => 's_name form-control'
                        ],
                    ],
                    'alias' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'options' => [
                            'maxlength' => true,
                            'class' => 's_alias form-control'
                        ],
                    ],
                    'created_at' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => DateControl::className(),
                        'options' => [
                            'type' => DateControl::FORMAT_DATETIME,
                            'displayFormat'=>'php:d-m-Y H:i',
                            'saveFormat'=>'php:U',
                            'displayTimezone' => 'Europe/Kiev',
                            'widgetOptions' => [
                                'pluginOptions' => [
                                    'autoclose' => true
                                ]
                            ]
                        ]
                    ],
                    'blog_parameters' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                        'hint' => Yii::t('back/blogs', 'To select the words that will appear bold styles use b - tag. For example: text without bold &ltb&gt BOLD &lt/b&gt text without bold'),
                    ],
                    'category_id' => [
                        'type' => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
                        'items' => BlogCategory::getItems(),
                        'options' => [
                            'prompt' => '',
                        ],
                    ],
                    'styles' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => Select2::className(),
                        'options' => [
                            'data' => ArrayHelper::map(BlogsStyles::find()->orderBy('position DESC')->asArray()->all(), 'id', 'label'),
                            'options' => ['multiple' => true, 'placeholder' => Yii::t('back/blogs', 'Select styles for blog')]
                        ]
                    ],
                    'relatedBlogs' => [
                        'type' => ActiveFormBuilder::INPUT_WIDGET,
                        'widgetClass' => Select2::className(),
                        'options' => [
                            'data' => ArrayHelper::map(Blogs::find()->orderBy('position DESC')->asArray()->all(), 'id', 'label'),
                            'options' => ['multiple' => true, 'placeholder' => Yii::t('back/blogs', 'Select related blogs')]
                        ]
                    ],
                    'show_on_main' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
//                        'options' => [
//                            'checked' => false,
//                        ]
                    ],
                    'show_on_about' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
//                        'options' => [
//                            'checked' => false,
//                        ]
                    ],
                    'show_on_services' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
//                        'options' => [
//                            'checked' => false,
//                        ]
                    ],
                    'published' => [
                        'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    ],
                    'position' => [
                        'type' => ActiveFormBuilder::INPUT_TEXT,
                    ],
                ],
                Yii::t('back/blogs', 'Content constructor') => [
                    'content' => [
                        'type' => ActiveFormBuilder::INPUT_RAW,
                        'value' => $this->constructor->getContent($this),
                        'options' => [
                            'class' => 'article-constructor'
                        ]
                    ]
                ],
//                Yii::t('back/blogs', 'Blog parameters') => [
//                    $this->getRelatedFormConfig()['blog_parameters']
//                ],
            ]

        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->constructor->saveConstructorData($this);

        EntityToFile::updateImages($this->id, $this->sign);

        $this->saveRelatedBlogs();
        $this->saveStyles();
        $this->saveCheckboxValues();
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        EntityToFile::deleteImages($this->formName(), $this->id);
    }

    /**
     *
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->constructor->loadContent($this);

        $this->loadRelatedBlogs();
        $this->loadStyles();
    }

    public function getBlogSmallImage()
    {
        return $this->hasOne(EntityToFile::className(), ['entity_model_id' => 'id'])
            ->andOnCondition(['t2.entity_model_name' => static::formName(), 't2.attribute' => EntityToFile::TYPE_BLOG_SMALL_IMAGE])
            ->from(['t2' => EntityToFile::tableName()])
            ->orderBy('t2.position DESC');
    }

    public function loadRelatedBlogs()
    {
        $this->relatedBlogs = ArrayHelper::map(BlogRelatedBlog::find()->where('blog_id = :tid', [':tid' => $this->id])->all(), 'id', 'related_blog_id');
    }

    public function saveRelatedBlogs()
    {
        BlogRelatedBlog::deleteAll('blog_id = :tid', [':tid' => $this->id]);

        if (is_array($this->relatedBlogs)) {
            foreach ($this->relatedBlogs as $relatedBlog) {
                $model = new BlogRelatedBlog();
                $model->blog_id = $this->id;
                $model->related_blog_id = (int)$relatedBlog;
                $model->save(false);
            }
        }
    }

    public function loadStyles()
    {
        $this->styles = ArrayHelper::map(BlogsStylesRelated::find()->where('blog_id = :tid', [':tid' => $this->id])->all(), 'id', 'blog_style_id');
    }

    public function saveStyles()
    {
        BlogsStylesRelated::deleteAll('blog_id = :tid', [':tid' => $this->id]);

        if (is_array($this->styles)) {
            foreach ($this->styles as $relatedStyles) {
                $model = new BlogsStylesRelated();
                $model->blog_id = $this->id;
                $model->blog_style_id = (int)$relatedStyles;
                $model->save(false);
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlogsParameters()
    {
        return $this->hasMany(BlogParametrs::className(), ['blog_id' => 'id'])
            ->orderBy('position');
    }

    public function saveCheckboxValues()
    {
//        if ($this->show_on_main) {
//            CheckboxHelper::unCheckAllExcept($this, $this->id, 'show_on_main');
//        }
        if ($this->show_on_about) {
            CheckboxHelper::unCheckAllExcept($this, $this->id, 'show_on_about');
        }
        if ($this->show_on_services) {
            CheckboxHelper::unCheckAllExcept($this, $this->id, 'show_on_services');
        }
    }

    /**
     * @return array
     */
//    public function getRelatedFormConfig()
//    {
//        return [
//            'blog_parameters' => [
//                'relation' => 'blogsParameters', //имя реляции (всегда с маленькой буквы, от названия метода реляции убрать get)
//            ],
//        ];
//    }
}
