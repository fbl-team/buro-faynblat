<?php

namespace backend\modules\blogs\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * BlogsSearch represents the model behind the search form about `Blogs`.
 */
class BlogsSearch extends Blogs
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'show_on_main', 'show_on_about', 'show_on_services', 'published', 'position'], 'integer'],
            [['label', 'city_and_square'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Blogs::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'show_on_main' => $this->show_on_main,
            'show_on_about' => $this->show_on_about,
            'show_on_services' => $this->show_on_services,
            'published' => $this->published,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'city_and_square', $this->city_and_square]);

        return $dataProvider;
    }
}
