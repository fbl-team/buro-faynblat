<?php

namespace backend\modules\blogs\models;

use Yii;

/**
* This is the model class for table "{{%blogs_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
* @property string $city_and_square
*/
class BlogsTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%blogs_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/blogs', 'Label') . ' [' . $this->language . ']',
            'announcement' => Yii::t('back/blogs', 'Announcement') . ' [' . $this->language . ']',
            'city_and_square' => Yii::t('back/blogs', 'City And Square') . ' [' . $this->language . ']',

            'footer_request_block_label' => Yii::t('back/blogs', 'Footer Request Block Label') . ' [' . $this->language . ']',
            'footer_request_block_button_label' => Yii::t('back/blogs', 'Footer Request Block Button Label') . ' [' . $this->language . ']',
            'footer_seo_block_label' => Yii::t('back/blogs', 'Footer Seo Block Label') . ' [' . $this->language . ']',
            'footer_seo_block_button_label' => Yii::t('back/blogs', 'Footer Seo Block Button Label') . ' [' . $this->language . ']',
            'footer_seo_block_text_left_column' => Yii::t('back/blogs', 'Footer Seo Block Text Left Column') . ' [' . $this->language . ']',
            'footer_seo_block_text_left_right' => Yii::t('back/blogs', 'Footer Seo Block Text Right Column') . ' [' . $this->language . ']',
            'footer_seo_block_button_label_hide' => Yii::t('back/blogs', 'Footer Seo Block Button Hide Label') . ' [' . $this->language . ']',
            'blog_parameters' => Yii::t('back/blogs', 'Blog parameters') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [['label', 'city_and_square'], 'string'],
            [['blog_parameters', 'footer_seo_block_button_label_hide', 'footer_request_block_label', 'footer_request_block_button_label', 'footer_seo_block_label', 'footer_seo_block_button_label', 'footer_seo_block_text_left_column', 'footer_seo_block_text_left_right','announcement'], 'safe']
         ];
    }
}
