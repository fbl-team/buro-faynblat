<?php

namespace backend\modules\blogs\controllers;

use backend\components\BackendController;
use backend\modules\blogs\models\BlogsStyles;

/**
 * BlogsStylesController implements the CRUD actions for BlogsStyles model.
 */
class BlogsStylesController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return BlogsStyles::className();
    }
}
