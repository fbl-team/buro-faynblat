<?php

namespace backend\modules\blogs\controllers;

use backend\components\BackendController;
use backend\modules\blogs\models\BlogCategory;

/**
 * BlogCategoryController implements the CRUD actions for BlogCategory model.
 */
class BlogCategoryController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return BlogCategory::className();
    }
}
