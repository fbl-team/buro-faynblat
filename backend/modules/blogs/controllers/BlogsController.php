<?php

namespace backend\modules\blogs\controllers;

use backend\components\BackendController;
use backend\helpers\CheckboxHelper;
use backend\modules\blogs\models\Blogs;
use Yii;
use yii\helpers\Json;

/**
 * BlogsController implements the CRUD actions for Blogs model.
 */
class BlogsController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Blogs::className();
    }

    public function actionGetContentByType()
    {
        $typeId = Yii::$app->request->post('type');

        if ($typeId >= 0) {
            $articleConstructorHelper = Yii::$container->get('BlogsConstructor');

            return Json::encode([
                'append' => [
                    [
                        'what' => '.template-list-constructor',
                        'data' => $this->renderAjax('content-entity', [
                            'constructor' => $articleConstructorHelper,
                            'type' => $typeId
                        ])
                    ],
                ],
            ]);
        }

        return false;
    }

    /**
     *
     */
    public function actionAjaxCheckbox()
    {
        $modelID = Yii::$app->request->post('modelId');
        $modelName = Yii::$app->request->post('modelName');
        $attribute = Yii::$app->request->post('attribute');

        if (Yii::$app->request->isAjax && $modelID && $modelName && $attribute) {
            $model = $modelName::findOne($modelID);
            if ($model) {
                $model->$attribute = $model->$attribute ? 0 : 1;
                $model->save(false);

                CheckboxHelper::unCheckAllExcept($modelName, $modelID, $attribute);
            }
        }
    }
}
