<?php

namespace backend\modules\request\models;

use kartik\date\DatePicker;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;

/**
 * This is the model class for table "{{%request}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property integer $published
 * @property integer $created_at
 */
class Request extends \common\components\model\ActiveRecord implements BackendModel
{
    public $showCreateButton = false;
    public $showUpdateButton = false;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%request}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'email'], 'string'],
            [['published'], 'integer'],
            [['published'], 'default', 'value' => 1],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/request', 'ID'),
            'name' => Yii::t('back/request', 'Name'),
            'phone' => Yii::t('back/request', 'Phone'),
            'email' => Yii::t('back/request', 'Email'),
            'published' => Yii::t('back/request', 'Published'),
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Request');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'created_at',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return \Yii::$app->formatter->asDatetime($data->created_at, 'medium');
                        },
                        'filter' => DatePicker::widget(
                            [
                                'model' => $this,
                                'attribute' => 'created_at',
                                'options' => [
                                    'class' => 'form-control form-control-filters'
                                ],
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'dd-mm-yyyy'
                                ]
                            ]
                        ),
                        'headerOptions' => ['class' => 'col-md-3'],
                    ],
                    'name:ntext',
                    'phone:ntext',
                    'email:ntext',
                    'published:boolean',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'name',
                    'phone',
                    'email',
                    'published:boolean',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new RequestSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
            'name' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'phone' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'email' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            
        ];
    }

}
