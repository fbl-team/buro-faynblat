<?php
/**
 * @author walter
 */

namespace backend\modules\configuration\models;


use backend\components\ImperaviContent;
use kartik\datecontrol\DateControl;
use metalguardian\formBuilder\ActiveFormBuilder;

class Configurator extends Configuration
{
    public $hint = false;
    /**
     * @return array
     */
    public function getValueFieldConfig()
    {
        $hint = $this->hint ? $this->hint : null;
        switch ($this->type) {
            case static::TYPE_STRING:
                return [
                    'type' => ActiveFormBuilder::INPUT_TEXT,
                    'hint' => $hint,
                ];
            case static::TYPE_TEXT:
                return [
                    'type' => ActiveFormBuilder::INPUT_TEXTAREA,
                    'hint' => $hint,
                ];
            case static::TYPE_HTML:
                return [
                    'type' => ActiveFormBuilder::INPUT_WIDGET,
                    'widgetClass' => ImperaviContent::className(),
                    'hint' => $hint,
                ];
            case static::TYPE_INTEGER:
                return [
                    'type' => ActiveFormBuilder::INPUT_TEXT,
                    'hint' => $hint,
                ];
            case static::TYPE_DOUBLE:
                return [
                    'type' => ActiveFormBuilder::INPUT_TEXT,
                    'hint' => $hint,
                ];
            case static::TYPE_BOOLEAN:
                return [
                    'type' => ActiveFormBuilder::INPUT_CHECKBOX,
                    'hint' => $hint,
                    'options' => [
                        'label' => $this->description
                    ]
                ];
            case static::TYPE_FILE:
                return [
                    'type' => ActiveFormBuilder::INPUT_FILE,
                    //'hint' => $description . '<p>' . Html::a(FPM::originalSrc($this->value), FPM::originalSrc($this->value)) . '</p>',
                ];
            case static::TYPE_DATE:
                return [
                    'type' => ActiveFormBuilder::INPUT_WIDGET,
                    'widgetClass' => DateControl::className(),
                    'options' => [
                        'type'=>DateControl::FORMAT_DATE,
                    ],
                ];
            case static::TYPE_DATE_TIME:
                return [
                    'type' => ActiveFormBuilder::INPUT_WIDGET,
                    'widgetClass' => DateControl::className(),
                    'options' => [
                        'type'=>DateControl::FORMAT_DATETIME,
                    ],
                ];
        }
        return [
            'type' => ActiveFormBuilder::INPUT_TEXT,
        ];
    }

}