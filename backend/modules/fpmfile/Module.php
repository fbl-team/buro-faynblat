<?php

namespace backend\modules\fpmfile;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\fpmfile\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
