<?php

namespace backend\modules\fpmfile\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * EntityToFileSearch represents the model behind the search form about `EntityToFile`.
 */
class EntityToFileSearch extends EntityToFile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'entity_model_id', 'file_id', 'position'], 'integer'],
            [['entity_model_name', 'temp_sign', 'attribute', 'alt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EntityToFile::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'entity_model_id' => $this->entity_model_id,
            'file_id' => $this->file_id,
            'position' => $this->position,
        ]);

        $query->andFilterWhere(['in', 'entity_model_name', $this->getModulesArray()]);

        $query->andFilterWhere(['like', 'entity_model_name', $this->entity_model_name])
            ->andFilterWhere(['like', 'temp_sign', $this->temp_sign])
            ->andFilterWhere(['like', 'attribute', $this->attribute])
            ->andFilterWhere(['like', 'alt', $this->alt]);

        return $dataProvider;
    }
}
