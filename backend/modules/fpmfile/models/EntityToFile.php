<?php

namespace backend\modules\fpmfile\models;

use backend\helpers\TypicalFunction;
use metalguardian\fileProcessor\helpers\FPM;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%entity_to_file}}".
 *
 * @property integer $id
 * @property string $entity_model_name
 * @property integer $entity_model_id
 * @property integer $file_id
 * @property string $temp_sign
 * @property integer $position
 * @property string $attribute
 * @property string $alt
 *
 * @property FpmFile $file
 */
class EntityToFile extends \common\components\model\ActiveRecord implements BackendModel
{


    public $showCreateButton = false;
    public $showDeleteButton = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%entity_to_file}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alt'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'entity_model_name' => Yii::t('app', 'Entity Model Name'),
            'entity_model_id' => Yii::t('app', 'Entity Model ID'),
            'file_id' => Yii::t('app', 'File ID'),
            'temp_sign' => Yii::t('app', 'Temp Sign'),
            'position' => Yii::t('app', 'Position'),
            'attribute' => Yii::t('app', 'Attribute'),
            'alt' => Yii::t('app', 'Alt'),
        ];
    }

    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('app', 'Entity To File');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    ['class' => 'yii\grid\SerialColumn'],
                    // 'id',
                    [
                        'attribute' => 'entity_model_name',
                        'filter' => $this->getModulesArray(),
                        'format' => 'raw',
                        'value' => function ($data) {

                            return $data->entity_model_name;

                        }
                    ],
                    [
                        'attribute' => 'file_id',
                        'format' => 'raw',
                        'value' => function ($data) {

                            return TypicalFunction::getImageById($data, 'file_id', ['direction', 'adminPreview']);

                        }
                    ],
                     'alt:ntext',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}{update}'
                    ],
                ];
            break;
            case 'view':
                return [
                    'id',
                    'entity_model_name',
                    'file_id:file',
                    [
                        'attribute' => 'alt',
                        'format' => 'html',
                    ],
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new EntityToFileSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
            'imagePreview' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => function () {
                    return $this->isNewRecord
                        ? null
                        : Html::img(
                            FPM::src(
                                $this->file_id,
                                'direction',
                                'adminPreview'
                            )
                        );
                },
                'label' => ''
            ],
            'alt' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
                'options' => [
                    'maxlength' => true,

                ],
            ],

        ];
    }

    public function getModulesArray()
    {
        return [
//            'Products' => 'Products',
            'ContentPages' => 'Content Pages',
            'PageMain' => 'Page Main',
            'Projects' => 'Projects',
            'ProjectContent' => 'Project Content',
            'PageAboutCanalDisain' => 'Page About Canal Disain',
            'PageMediaSpeekBlock' => 'Page Media Speek Block',
            'PageMediaWriteBlock' => 'Page Media Write Block',
            'PageServicesWhatWeDoBlock' => 'Page Services What We Do Block',
            'ContentPageInsideBlock' => 'Content Page Inside Block',
            'PageAbout' => 'Page About',
            'PageServices' => 'Page Services',
            'PageAboutOurDiploms' => 'Page About Our Diploms',
        ];
    }

}
