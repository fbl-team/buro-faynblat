<?php

namespace backend\modules\fpmfile\controllers;

use backend\components\BackendController;
use backend\modules\fpmfile\models\EntityToFile;

/**
 * EntityToFileController implements the CRUD actions for EntityToFile model.
 */
class EntityToFileController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return EntityToFile::className();
    }
}
