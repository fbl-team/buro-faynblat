<?php

namespace backend\modules\menus\models;

use Yii;

/**
* This is the model class for table "{{%menus_element_translation}}".
*
* @property integer $model_id
* @property string $language
* @property string $label
*/
class FooterMenuTranslation extends \common\components\model\ActiveRecord
{
    /**
    * @inheritdoc
    */
    public static function tableName()
    {
        return '{{%menus_element_translation}}';
    }

    /**
    * @inheritdoc
    */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back/footer_menu', 'Label') . ' [' . $this->language . ']',
        ];
    }

    /**
    * @inheritdoc
    */
    public function rules()
    {
        return [
            [['label'], 'string'],
         ];
    }
}
