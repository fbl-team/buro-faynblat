<?php

namespace backend\modules\menus\models;

use common\helpers\MenusHelper;
use Yii;
use \backend\components\BackendModel;
use metalguardian\formBuilder\ActiveFormBuilder;
use \common\components\model\Translateable;

/**
 * This is the model class for table "{{%menus_element}}".
 *
 * @property integer $id
 * @property integer $type_id
 * @property string $label
 * @property string $url
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property MenusElementTranslation[] $menusElementTranslations
 */
class HeaderMenu extends \common\components\model\ActiveRecord implements BackendModel, Translateable
{
    public $showCreateButton = false;
    public $showDeleteButton = false;
    public $type_id = MenusHelper::HEADER_ELEMENTS;

    use \backend\components\TranslateableTrait;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%menus_element}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'published', 'position'], 'integer'],
            [['label', 'url'], 'string'],
            [['url'], 'required'],
            [['published'], 'default', 'value' => 1],
            [['position'], 'default', 'value' => 0],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back/header_menu', 'ID'),
            'type_id' => Yii::t('back/header_menu', '1-header; 2-footer; 3-right;'),
            'label' => Yii::t('back/header_menu', 'Label'),
            'url' => Yii::t('back/header_menu', 'Url'),
            'published' => Yii::t('back/header_menu', 'Published'),
            'position' => Yii::t('back/header_menu', 'Position'),
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
            'label',
        ];
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],
            'timestamp' => [
                'class' => \yii\behaviors\TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(HeaderMenuTranslation::className(), ['model_id' => 'id']);
    }
    /**
    * Get title for the template page
    *
    * @return string
    */
    public function getTitle()
    {
        return \Yii::t('back/header_menu', 'Header Menu');
    }

    /**
    * Get attribute columns for index and view page
    *
    * @param $page
    *
    * @return array
    */
    public function getColumns($page)
    {
        switch ($page) {
            case 'index':
                return [
                    [
                        'class' => 'backend\components\SortingSerialColumn',
                    ],
                    // 'id',
//                    'type_id',
                    'label',
                    'url',
                    'published:boolean',
//                    'position',
                    ['class' => 'yii\grid\ActionColumn'],
                ];
            break;
            case 'view':
                return [
                    'id',
//                    'type_id',
                    'label',
                    'url',
                    'published:boolean',
                    'position',
                ];
            break;
        }

        return [];
    }

    /**
    * @return \yii\db\ActiveRecord
    */
    public function getSearchModel()
    {
        return new HeaderMenuSearch();
    }

    /**
    * @return array
    */
    public function getFormConfig()
    {
        return [
//            'type_id' => [
//                'type' => ActiveFormBuilder::INPUT_TEXT,
//            ],
            'label' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'url' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            'published' => [
                'type' => ActiveFormBuilder::INPUT_CHECKBOX,
            ],
            'position' => [
                'type' => ActiveFormBuilder::INPUT_TEXT,
            ],
            
        ];
    }

}
