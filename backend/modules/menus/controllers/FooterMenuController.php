<?php

namespace backend\modules\menus\controllers;

use backend\components\BackendController;
use backend\modules\menus\models\FooterMenu;

/**
 * HeaderMenuController implements the CRUD actions for HeaderMenu model.
 */
class FooterMenuController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return FooterMenu::className();
    }
}
