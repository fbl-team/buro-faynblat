<?php

namespace backend\modules\menus\controllers;

use backend\components\BackendController;
use backend\modules\menus\models\HeaderMenu;
use backend\modules\menus\models\RightMenu;

/**
 * HeaderMenuController implements the CRUD actions for HeaderMenu model.
 */
class RightMenuController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return RightMenu::className();
    }
}
