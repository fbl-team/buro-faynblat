<?php

namespace backend\modules\menus\controllers;

use backend\components\BackendController;
use backend\modules\menus\models\HeaderMenu;

/**
 * HeaderMenuController implements the CRUD actions for HeaderMenu model.
 */
class HeaderMenuController extends BackendController
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return HeaderMenu::className();
    }
}
