<?php

namespace backend\modules\menus;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\menus\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
