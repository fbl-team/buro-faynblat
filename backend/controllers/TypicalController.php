<?php
namespace backend\controllers;

use backend\modules\services\models\Services;
use common\components\model\ActiveRecord;
use metalguardian\fileProcessor\helpers\FPM;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;

/**
 * Site controller
 */
class TypicalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['test', 'save-sort'],
                        'allow' => true,
                        'roles' => [\common\models\User::ROLE_ADMIN],
                    ],
                ],
            ],
        ];
    }

    public function actionTest()
    {
        return $this->render('index');
    }


    public function actionSaveSort()
    {
        $sort = \Yii::$app->request->post('sort');
        $className = \Yii::$app->request->post('className');

        if (Yii::$app->request->isAjax && $sort && $className) {
            $sortArray = explode(',', $sort);
            $headerModels = $className::find()->all();
            foreach ($headerModels as $model) {
                foreach ($sortArray as $key => $value) {
                    if ($model->id == $value) {
                        $model->position = $key;
                        $model->save(false);
                        break;
                    }
                }
            }
        }
//        echo Json::encode(null);
    }

    public function actionAjaxCheckbox()
    {
        $modelID = Yii::$app->request->post('modelId');
        $modelName = Yii::$app->request->post('modelName');
        $attribute = Yii::$app->request->post('attribute');

        if (Yii::$app->request->isAjax && $modelID && $modelName && $attribute) {
            $model = $modelName::findOne($modelID);
            if ($model) {
                $model->$attribute = $model->$attribute ? 0 : 1;
                $model->save(false);
            }
        }
    }

}
