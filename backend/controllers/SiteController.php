<?php
namespace backend\controllers;

use backend\helpers\CheckboxHelper;
use backend\modules\projects\models\Projects;
use common\components\model\ActiveRecord;
use metalguardian\fileProcessor\helpers\FPM;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\web\Controller;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'ajax-checkbox', 'delete-file', 'position'],
                        'allow' => true,
                        'roles' => [\common\models\User::ROLE_ADMIN],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionPosition()
    {
        $modelID = Yii::$app->request->post('modelId');
        $modelName = Yii::$app->request->post('modelName');
        $positionValue = Yii::$app->request->post('positionValue');

        if (Yii::$app->request->isAjax && $modelID && $modelName && $positionValue) {
            $model = $modelName::findOne($modelID);
            if ($model) {
                $model->position = $positionValue;
                $model->save(false);
            }
        }
    }

    public function actionAjaxCheckbox()
    {
        $data = null;

        $modelID = Yii::$app->request->post('modelId');
        $modelName = Yii::$app->request->post('modelName');
        $attribute = Yii::$app->request->post('attribute');

        if (Yii::$app->request->isAjax && $modelID && $modelName && $attribute) {
            $model = $modelName::findOne($modelID);
            if ($model) {
                $model->$attribute = $model->$attribute ? 0 : 1;
                $model->save(false);

                $checkboxWithOneOptionValue = Yii::$app->params['ajaxCheckboxWithOnlyOneExistValue'];
                if (isset($checkboxWithOneOptionValue[$modelName]) && in_array($attribute, $checkboxWithOneOptionValue[$modelName])) {
                    $data = CheckboxHelper::unCheckAllExcept($modelName, $modelID, $attribute);
                }

            }
        }
        echo Json::encode($data);
    }

    public function actionDeleteFile()
    {
        $modelID = Yii::$app->request->post('modelId');
        $modelName = Yii::$app->request->post('modelName');
        $attribute = Yii::$app->request->post('attribute');
        $language = Yii::$app->request->post('language');

        if (Yii::$app->request->isAjax && $modelID && $modelName && $attribute) {
            $error = true;
            /** @var $model ActiveRecord */
            if ($language) {
                $model = $modelName::find()->where(['model_id' => $modelID, 'language' => $language])->one();
            } else {
                $model = $modelName::findOne($modelID);
            }
            if ($model) {
                $fileId = $model->$attribute;
                $model->$attribute = null;
                if ($model->save(false)) {
                    FPM::deleteFile($fileId);
                    $error = false;
                }
            }

            return Json::encode(['error' => $error]);
        }

        return false;
    }
}
