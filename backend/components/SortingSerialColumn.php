<?php
/**
 * 
 */

namespace backend\components;
use backend\modules\pages\models\header\PageHeader;
use Yii;
use yii\grid\SerialColumn;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class ActionColumn
 */
class SortingSerialColumn extends SerialColumn
{
    /**
     * @inheritdoc
     */
    public $header = '#';
    public $addId = false;


    /**
     * @inheritdoc
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $pagination = $this->grid->dataProvider->getPagination();
        $params = [
            'class' => 'badge sortable-grid',
            'data-id' => $model->id
        ];
        if (!$index) {
            $params['data-url'] = Url::base(true) . '/typical/save-sort';
            $params['data-class-name'] = $model->className();
            $params['id'] = 'sort-url';
        }

        if ($pagination !== false) {

            return Html::tag('span', '<i class="glyphicon glyphicon-move"></i>', $params).($this->addId ? $model->id-1 : '');
        } else {
            return Html::tag('span', '<i class="glyphicon glyphicon-move"></i>', $params).($this->addId ? $model->id-1 : '');
        }
    }
}
