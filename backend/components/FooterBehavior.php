<?php
/**
 * Created by PhpStorm.
 * User: metal
 * Date: 02.10.15
 * Time: 17:02
 */

namespace backend\components;

use backend\modules\pages\models\ButtonToPage;
use common\components\model\ActiveRecord;
use Yii;
use yii\base\Behavior;
use yii\helpers\ArrayHelper;
use yii\validators\DefaultValueValidator;
use yii\validators\SafeValidator;


/**
 * Class UploadAction
 */
class FooterBehavior extends Behavior
{
//    public $buttons = [];

    public function events()
    {
        return [
//            ActiveRecord::EVENT_BEFORE_INSERT => 'saveFooterAttributes',
//            ActiveRecord::EVENT_BEFORE_UPDATE => 'saveFooterAttributes',
//            ActiveRecord::EVENT_AFTER_FIND => 'loadButtons',
            ActiveRecord::EVENT_INIT => 'attachValidator',
        ];
    }

    public function attachValidator()
    {
        /** @var ActiveRecord $model */
        $model = $this->owner;
        $model->validators[] = new SafeValidator([
            'attributes' => [
                'footer_request_block_label',
                'footer_request_block_button_label',
                'footer_seo_block_label',
                'footer_seo_block_button_label',
                'footer_seo_block_text_left_column',
                'footer_seo_block_text_left_right',
                'footer_seo_block_button_label_hide',
            ],

        ]);
        $model->validators[] = new DefaultValueValidator([
            'attributes' => [
                'footer_request_block_label',
            ],
            'value' => 'Назначить встречу с викторией'
        ]);

        $model->validators[] = new DefaultValueValidator([
            'attributes' => [
                'footer_request_block_button_label',
            ],
            'value' => 'Отправить заявку'
        ]);
    }

//    public function loadFooterAttributes()
//    {
//        $this->owner->buttons = ArrayHelper::map(ButtonToPage::find()->where(['model_name' => $this->owner->formName(), 'page_id' => $this->owner->id])->all(), 'id', 'button_id');
//    }
//
//    public function saveFooterAttributes()
//    {
//        $model->model_name = $this->owner->formName();
//        $model->page_id = $this->owner->id;
//        $model->button_id = (int)$buttons;
//    }

}
