<?php
/**
 * Author: metal
 * Email: metal
 */

namespace backend\components;

use common\helpers\LanguageHelper;
use vova07\imperavi\actions\GetAction;
use vova07\imperavi\actions\UploadAction;
use Yii;
use yii\base\Model;
use yii\base\NotSupportedException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class BackendController
 * @package backend\components
 */
abstract class StaticController extends BackendController
{
    public function actionIndex()
    {
        $model =  $this->getModelClass();
        $page = $model::find()->one();

        if (!$page) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $this->redirect(['update', 'id' => $page->id]);
    }

    /**
     * Displays a single Menu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        \Yii::$app->getSession()->setFlash('info', \Yii::t('app', 'Record successfully updated!'));

        $this->redirect(['update', 'id' => $id]);
    }
}
