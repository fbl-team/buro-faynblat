<?php
/**
 * This is the template for generating the model class of a specified table.
 */

/* @var $this yii\web\View */
/* @var $generator \backend\components\gii\staticPageModel\Generator */
/* @var $behaviors string[] list of behaviors */

echo "<?php\n";
?>

namespace <?= $generator->ns ?>;

use backend\modules\configuration\components\ConfigurationModel;
use common\models\Configuration;
use common\models\<?= $generator->modelClassName ?> as Common<?= $generator->modelClassName ?>;
<?php if ($generator->isImage): ?>
use backend\modules\imagesUpload\widgets\imagesUpload\ImageUpload;
use metalguardian\formBuilder\ActiveFormBuilder;
<?php endif; ?>
<?php if ($generator->isRelatedFormWidget): ?>
use yii\db\ActiveQueryInterface;
<?php endif; ?>
/**
* Class <?= $generator->modelClassName ?>
*/
class <?= $generator->modelClassName ?> extends ConfigurationModel
{
<?php if ($generator->isImage) : ?>
    public $titleImage;
<?php endif; ?>
<?php if ($generator->showAsConfig) : ?>
    public $showAsConfig = true;
<?php else: ?>
    public $showAsConfig = false;
<?php endif; ?>
    /**
    * Title of the form
    *
    * @return string
    */
    public function getTitle()
    {
        return '<?= $generator->title ?>';
    }

    /**
    * @return array
    */
    public function getFormTypes()
    {
        return [
<?php foreach ($generator->keys as $key):
        if ($constant = $generator->getConstantName($key['type'])): ?>
            Common<?= $generator->modelClassName ?>::<?= $generator->formatToConstant($key['id']) ?> => Configuration::<?= $constant ?>,
<?php endif;
    endforeach; ?>
<?php if ($generator->isImage) : ?>
            'titleImage' => [
                'type' => ActiveFormBuilder::INPUT_RAW,
                'value' => ImageUpload::widget([
                'model' => $this,
                'attribute' => 'titleImage',
                    //'saveAttribute' => EntityToFile::TYPE_ARTICLE_TITLE_IMAGE, //TODO Создать контанту и раскомментировать
                    //'aspectRatio' => 300/200, //Пропорция для кропа
                    'multiple' => false, //Вкл/выкл множественную загрузку
                ])
            ]
<?php endif; ?>
        ];
    }

    /**
    * @return array
    */
    public function getFormDescriptions()
    {
        return [
<?php foreach ($generator->keys as $key): ?>
            Common<?= $generator->modelClassName ?>::<?= $generator->formatToConstant($key['id']) ?> => '<?= $key['description'] ?>',
<?php endforeach; ?>
<?php if ($generator->isImage) : ?>
            'titleImage' => 'Image'
<?php endif; ?>
        ];
    }

    /**
    * @return array
    */
    public function getFormHints()
    {
        return [
<?php foreach ($generator->keys as $key): ?>
            Common<?= $generator->modelClassName ?>::<?= $generator->formatToConstant($key['id']) ?> => '<?= $key['hint'] ?>',
<?php endforeach; ?>
        ];
    }

    /**
    * @return array
    */
    public static function getTranslationAttributes()
    {
        return [
<?php foreach ($generator->keys as $key): ?>
<?php if($key['isTranslatable']) : ?>
            Common<?= $generator->modelClassName ?>::<?= $generator->formatToConstant($key['id']) ?>,
<?php endif; ?>
<?php endforeach; ?>
        ];
    }

<?php if ($behaviors) : ?>
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            <?= implode(",\n            ", $behaviors) . ",\n" ?>
        ]);
    }
<?php endif; ?>

<?php if ($generator->isRelatedFormWidget): ?>
    //TODO Set proper configuration for Related Form Widget
    /**
    * @return array
    */
    public function getFormConfig()
    {
        $config = [
            'form-set' => [
                'Tab name' => [
                    $this->getRelatedFormConfig()['relationName'] ?? null
                ]
            ]
        ];

        return $config;
    }

    /**
    * @return array
    */
    public function getRelatedFormConfig()
    {
        $config = [
            'relationName' => [
                'relation' => 'relationName',
            ],
        ];

        return $config;
    }

    /**
    * @return ActiveQueryInterface
    */
    public function getRelationName()
    {
        return $this->hasMany(ModelName::className(), ['foreign_key' => 'id'])->orderBy('position');
    }
<?php endif; ?>
}
