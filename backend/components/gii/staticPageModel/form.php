<?php
/* @var $this yii\web\View */
use backend\modules\configuration\models\Configuration;
use metalguardian\formBuilder\ActiveFormBuilder;
use unclead\widgets\MultipleInput;
use yii\gii\GiiAsset;

/* @var $form yii\widgets\ActiveForm */
/* @var $generator yii\gii\generators\form\Generator */

$asset = GiiAsset::register($this);

echo $form->field($generator, 'ns');
echo $form->field($generator, 'modelClassName');
echo $form->field($generator, 'controllerClass');
echo $form->field($generator, 'title');
echo $form->field($generator, 'keys')->widget(MultipleInput::className(), [
    'min'               => 1, // should be at least 1 row
    'addButtonPosition' => MultipleInput::POS_HEADER, // show add button in the header
    'columns' => [
        [
            'name'  => 'id',
            'type'  => ActiveFormBuilder::INPUT_TEXT,
            'title' => 'Key',
        ],
        [
            'name'  => 'type',
            'type'  => ActiveFormBuilder::INPUT_DROPDOWN_LIST,
            'items' => Configuration::getList('type'),
            'title' => 'Field type',
        ],
        [
            'name'  => 'description',
            'type'  => ActiveFormBuilder::INPUT_TEXT,
            'title' => 'Field description',
        ],
        [
            'name'  => 'hint',
            'type'  => ActiveFormBuilder::INPUT_TEXT,
            'title' => 'Field hint',
        ],
        [
            'name'  => 'isTranslatable',
            'type'  => ActiveFormBuilder::INPUT_CHECKBOX,
            'title' => 'Is Translatable',
        ],
    ]
]);
echo $form->field($generator, 'isSeo')->checkbox();
echo $form->field($generator, 'isImage')->checkbox();
echo $form->field($generator, 'isRelatedFormWidget')->checkbox();
echo $form->field($generator, 'showAsConfig')->checkbox();
