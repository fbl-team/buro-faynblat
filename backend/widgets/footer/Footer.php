<?php
/**
 * Author: Pavel Naumenko
 */

namespace backend\widgets\footer;

/**
 * Class Widget
 *
 * @package app\modules\banner\widgets\headBannerSlider
 */
class Footer extends \yii\base\Widget
{

    public $model;
    public $form;
    public $translationModels;

    public function run()
    {
        $behavior = $this->model->getBehavior('footer');
        if (!$behavior) {
            return null;
        }

        return $this->render('footer', [
            'model' => $this->model,
            'form' => $this->form,
            'translationModels' => $this->translationModels
        ]);
    }
}
