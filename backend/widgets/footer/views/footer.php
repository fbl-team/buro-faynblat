<?php
/**
 * Author: Myha
 */
use backend\components\ImperaviContent;
use backend\modules\pages\models\PageButtons;
use metalguardian\formBuilder\ActiveFormBuilder;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

$redactorConfig = [
    'customSettings' => [
        'buttons' => [
            'html',
            'formatting',
            'bold',
            'italic',
            'unorderedlist',
            'orderedlist',
            'link',
            'alignment',
            'horizontalrule',
        ],
    ],
];
$buttonGetFormConfig = [
    'footer_request_block_label' => [
        'type' => ActiveFormBuilder::INPUT_TEXT,
        'hint' => Yii::t('back/footer_block', 'To select the words that will appear with an animation use b - tag. For example: text without animation &ltb&gt ANIMATED &lt/b&gt text without animation'),
    ],
    'footer_request_block_button_label' => [
        'type' => ActiveFormBuilder::INPUT_TEXT
    ],
    'footer_seo_block_label' => [
        'type' => ActiveFormBuilder::INPUT_WIDGET,
        'widgetClass' => ImperaviContent::className(),
        'options' => $redactorConfig,
    ],
    'footer_seo_block_button_label' => [
        'type' => ActiveFormBuilder::INPUT_TEXT
    ],
    'footer_seo_block_button_label_hide' => [
        'type' => ActiveFormBuilder::INPUT_TEXT
    ],
    'footer_seo_block_text_left_column' => [
        'type' => ActiveFormBuilder::INPUT_WIDGET,
        'widgetClass' => ImperaviContent::className(),
        'options' => $redactorConfig,
    ],
    'footer_seo_block_text_left_right' => [
        'type' => ActiveFormBuilder::INPUT_WIDGET,
        'widgetClass' => ImperaviContent::className(),
        'options' => $redactorConfig,
    ],
];

if ($model->formName() == 'Projects' || $model->formName() == 'Blogs') {
    unset($buttonGetFormConfig['footer_request_block_label'], $buttonGetFormConfig['footer_request_block_button_label']);
}

echo $form->prepareRowsForMultilangBehavior($model, $buttonGetFormConfig, $translationModels);