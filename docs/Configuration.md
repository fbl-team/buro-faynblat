Конфигуратор
===================================

Позволяет создавать отдельные формы для конфига, т.е. теперь не надо создавать отдельную таблицу на каждую страницу, 
достаточно сгенерировать модель и контроллер через gii в разделе Static Page Model Generator. 
Для этого нужно заполнить обязательные поля:

Model namespace - namespace модели для бэкэнда;

Model Class Name - название генерируемой модели;

Controller Class - полное имя контроллера для бэкэнда;

Title - заголовок модели для отображения на странице редактирования в бэкэнде;

Keys - ключи с типами и описанием. Названия ключей (Key) должны быть уникальны в пределах одной модели. Типы (Field type)
определяют формат полей для ввода. Описания (Field description) подписывают поля на странице редактирования в бэкэнде.

Кроме этого, есть возоможность сгенерировать seo behavior, imageUploadWidget с кроппером и заготовку для подключения
RelatedFormWidget. Соответственные галочки необходимо поставить в самом генераторе.

##Особенности использования

Для backend

Подключение imageUploadWidget работает по стандартному [прниципу множественной загрузки](docs/File_upload.md).
Однако его можно подключать не только через getFormConfig(), но и через getFormTypes(), это позволяет размеситть виджет
в удобном месте между ключами. В случае с getFormConfig(), виджет можно разместить либо под всеми ключами, либо в другой
вкладке.
```php
/**
* @return array
*/
public function getFormTypes()
{
    return [
        CommonStaticPage::KEY_1 => Configuration::TYPE_STRING,
        CommonStaticPage::KEY_2 => Configuration::TYPE_STRING,
        'titleImage' => [
            'type' => ActiveFormBuilder::INPUT_RAW,
            'value' => ImageUpload::widget([
            'model' => $this,
            'attribute' => 'titleImage',
                //'saveAttribute' => EntityToFile::TYPE_ARTICLE_TITLE_IMAGE, //TODO Создать контанту и раскомментировать
                //'aspectRatio' => 300/200, //Пропорция для кропа
                'multiple' => false, //Вкл/выкл множественную загрузку
            ])
        ]
    ];
}
```
Подключение RelatedFormWidget работает также по стандартному [прниципу](docs/RelatedFormWidget.md). Определяется метод
getFormConfig(), в котором можно добавить данный виджет в новуй вкладку или вниз основной страницы.

```php
/**
* @return array
*/
public function getFormConfig()
{
    $config = [
        'form-set' => [
            'Tab name' => [
                $this->getRelatedFormConfig()['relationName'] ?? null
            ]
        ]
    ];

    return $config;
}

/**
* @return array
*/
public function getRelatedFormConfig()
{
    $config = [
        'relationName' => [
            'relation' => 'relationName',
        ],
    ];

    return $config;
}

/**
* @return ActiveQueryInterface
*/
public function getRelationName()
{
    return $this->hasMany(ModelName::className(), ['foreign_key' => 'id'])->orderBy('position');
}
```
Контроллер и необходимая коммоновская модель генерируются автоматически и не требуют ручного вмешательства.


Для frontend

В экшене статической страницы небоходимо создать экземпляр коммоновской модели и вызвать метод get() (так модель
наполняется данными). Для подключения SEO небоходимо вызвать метод ConfigurationMetaTagRegister::register($model)
```php
public function actionIndex()
{
    $model = (new StaticPageSample())->get();
    ConfigurationMetaTagRegister::register($model);

    return $this->render('index', ['model' => $model]);
}
```
Во вьюхе для вывода данных из модели, достаточно обратиться к конкретному атрибуту модели, который автоматически
сгенерировался на этапе gii.
```php
$model->key2
$model->key3
```
