<?php

namespace common\models;

use Yii;
use common\components\{
    Translate, model\Translateable
};

/**
 * This is the model class for table "{{%alt_images}}".
 *
 * @property integer $id
 * @property string $label
 * @property integer $file_id
 *
 * @property AltImagesTranslation[] $translations
 */
class AltImages extends \common\components\model\ActiveRecord implements Translateable
{

    use \backend\components\TranslateableTrait;
    use Translate;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%alt_images}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('back\alt-images', 'ID'),
            'label' => Yii::t('back\alt-images', 'Label'),
            'file_id' => Yii::t('back\alt-images', 'File id'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'translateable' => [
                'class' => \creocoder\translateable\TranslateableBehavior::className(),
                'translationAttributes' => static::getTranslationAttributes(),
            ],

        ];
    }

    /**
     * @return array
     */
    public static function getTranslationAttributes()
    {
        return [
            'label'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(AltImagesTranslation::className(), ['model_id' => 'id']);
    }

    /**
     * Get alt tag for image
     * @param $id - FPM id
     * @return mixed|string
     */
    public static function getAltTag($id)
    {
        $model = self::find()->where(['file_id' => $id])->one();

        if ($model) {
            return $model->label;
        }
        return 'image';
    }
}
