<?php

namespace common\models;

use metalguardian\fileProcessor\models\File;
use Yii;

/**
 * @inheritdoc
 *
 * @property FpmFile $file
 */
class EntityToFile extends \common\models\base\EntityToFile
{

    const CONTENT_PAGE_SLIDER = 'content_page_slider';
    const CONTENT_PAGE_INSIDE_WORK_BLOCK = 'content_page_inside_work_block';
    const TYPE_CONTENT_PAGE_INSIDE_BLOCK_IMAGE = 'content_page_inside_block_image';
    const TYPE_CONTENT_PAGE_INSIDE_BLOCK_VIDEO = 'content_page_inside_block_video';
    const TYPE_PROJECT_CONSTRUCTOR_BIG_IMAGE = 'project_constructor_big_image';
    const TYPE_PROJECT_CONSTRUCTOR_TWO_IMAGES = 'project_constructor_two_images';
    const TYPE_PROJECT_BIG_IMAGE = 'project_big_image';
    const TYPE_PROJECT_SMALL_IMAGE = 'project_small_image';
    const TYPE_BLOG_CONSTRUCTOR_BIG_IMAGE = 'blog_constructor_big_image';
    const TYPE_BLOG_CONSTRUCTOR_TWO_IMAGES = 'blog_constructor_two_images';
    const TYPE_BLOG_BIG_IMAGE = 'blog_big_image';
    const TYPE_BLOG_SMALL_IMAGE = 'blog_small_image';
    const TYPE_PAGE_HOME_FIRST_SCREEN_VIDEO = 'page_home_first_screen_video';
    const TYPE_PAGE_HOME_FIRST_SCREEN_IMAGE = 'page_home_first_screen_image';
    const TYPE_PAGE_HOME_FILOSOPHY_BLOCK_IMAGE = 'page_home_filosophy_block_image';
    const TYPE_PAGE_ABOUT_FIRST_SCREEN_IMAGE = 'page_about_first_screen_image';
    const TYPE_PAGE_ABOUT_FIRST_SCREEN_VIDEO = 'page_about_first_screen_video';
    const TYPE_PAGE_ABOUT_DIPLOMS_IMAGE = 'page_about_diploms_image';
    const TYPE_PAGE_CANAL_PREVIEW_IMAGE = 'page_about_canal_previeq_image';
    const TYPE_SERVICES_FIRST_SCREEN_IMAGE = 'page_services_first_screen_image';
    const TYPE_SERVICES_FIRST_SCREEN_VIDEO = 'page_services_first_screen_video';
    const TYPE_SERVICES_WHAT_WE_DO_IMAGE = 'page_services_what_we_do_image';
    const TYPE_SERVICES_WHAT_WE_DO_VIDEO = 'page_services_what_we_do_video';
    const TYPE_PAGE_MEDIA_WRITE_BLOCK_IMAGE = 'page_media_write_block_image';
    const TYPE_PAGE_MEDIA_WRITE_BLOCK_PDF = 'page_media_write_block_pdf';
    const TYPE_PAGE_MEDIA_SPEEK_BLOCK_IMAGE = 'page_media_speek_block_pdf';

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['id' => 'file_id']);
    }

}
