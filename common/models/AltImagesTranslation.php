<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%alt_images_translation}}".
 *
 * @property integer $model_id
 * @property string $language
 * @property string $label
 *
 * @property AltImages $model
 */
class AltImagesTranslation extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%alt_images_translation}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'label' => Yii::t('back\alt-images', 'Alt image') . ' [' . $this->language . ']',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(AltImages::className(), ['id' => 'model_id']);
    }
}
