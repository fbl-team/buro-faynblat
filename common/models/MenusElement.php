<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%menus_element}}".
 *
 * @property integer $id
 * @property integer $type_id
 * @property string $label
 * @property string $url
 * @property integer $published
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property MenusElementTranslation[] $translations
 */
class MenusElement extends \common\components\model\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%menus_element}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type_id' => Yii::t('app', '1-header; 2-footer; 3-right;'),
            'label' => Yii::t('app', 'Label'),
            'url' => Yii::t('app', 'Url'),
            'published' => Yii::t('app', 'Published'),
            'position' => Yii::t('app', 'Position'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(MenusElementTranslation::className(), ['model_id' => 'id']);
    }
}
