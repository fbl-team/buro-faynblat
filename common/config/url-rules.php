<?php
return [
    '' => 'home/index',

    'projects' => 'project/all-projects',
    'projects/<category>' => 'project/all-projects',
    'projects/<category>/<styles>' => 'project/all-projects',
    'project/<alias>' => 'project/one-project',

    'blog' => 'blog/all-blogs',
    'blog/<category>' => 'blog/all-blogs',
    'blog/<category>/<styles>' => 'blog/all-blogs',
    'post/<alias>' => 'blog/one-blog',

    'about' => 'about/index',
    'careers' => 'careers/index',
    'services' => 'services/index',
    'media' => 'media/index',
    'contacts' => 'contacts/index',
    'robots.txt' => 'site/robots',
    'sitemap.xml' => 'sitemap/default/index',
    'test' => 'test/index',

    'load-more-elements/<type>/<parentModelId>/<iteration>/<buttonLabel>' => 'site/load-more-elements',
    'load-more-elements/<type>/<parentModelId>/<iteration>' => 'site/load-more-elements',

    'faynblat-call-you' => 'request/request/faynblat-call-you',
    'faynblat-careers-request' => 'careersRequest/careers-request/send',

    //content pages
    '<alias>' => 'content-pages/index',
];
