<?php
use \metalguardian\fileProcessor\helpers\FPM;

return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'modules' => [
        'fileProcessor' => [
            'class' => '\metalguardian\fileProcessor\Module',
            'imageSections' => [
                'direction' => [
                    'adminPreview' => [
                        'action' => 'thumbnail',
                        'width' => 255,
                        'height' => 290,
                    ],
                    'smallPreview' => [
                        'action' => 'adaptiveThumbnail',
                        'width' => 84,
                        'height' => 84,
                    ],
                ],

                'share' => [
                    'share' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 1200,
                        'height' => 630,
                    ],
                ],
                'project' => [
                    'back' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 250,
                        'height' => 170,
                    ],
                    'bigimage' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 700,
                        'height' => 455,
                    ],
                    'smallimage' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 400,
                        'height' => 360,
                    ],
                    'anotherproject' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 375,
                        'height' => 340,
                    ],
                    'contentsingleimage' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 1200,
                        'height' => 750,
                    ],
                    'contenttwoimage' => [
                        'action' => FPM::ACTION_THUMBNAIL,
                        'width' => 1200,
                        'height' => 750,
                    ],
                ],
                'blog' => [
                    'back' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 250,
                        'height' => 170,
                    ],
                    'bigimage' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 700,
                        'height' => 455,
                    ],
                    'smallimage' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 400,
                        'height' => 360,
                    ],
                    'anotherblog' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 375,
                        'height' => 340,
                    ],
                    'contentsingleimage' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 1200,
                        'height' => 750,
                    ],
                    'contenttwoimage' => [
                        'action' => FPM::ACTION_THUMBNAIL,
                        'width' => 1200,
                        'height' => 750,
                    ],
                ],
                'pageabout' => [
                    'back' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 250,
                        'height' => 170,
                    ],
                    'firstscreen' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 1350,
                        'height' => 550,
                    ],
                    'diploms' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 165,
                        'height' => 200,
                    ],
                    'canal' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 490,
                        'height' => 320,
                    ],
                ],
                'pagemain' => [
                    'back' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 250,
                        'height' => 170,
                    ],
                    'filosophy' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 500,
                        'height' => 600,
                    ],
                    'firstscreen' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 1350,
                        'height' => 550,
                    ],
                ],
                'pageservices' => [
                    'back' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 250,
                        'height' => 170,
                    ],
                    'firstscreen' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 1350,
                        'height' => 500,
                    ],
                    'whatwedo' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 700,
                        'height' => 450,
                    ],
                ],
                'contentpage' => [
                    'back' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 1350,
                        'height' => 500,
                    ],
                    'slider' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 1350,
                        'height' => 500,
                    ],
                    'insideimage' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 700,
                        'height' => 450,
                    ],
                ],
                'pagemedia' => [
                    'back' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 1350,
                        'height' => 500,
                    ],
                    'writeblock' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 310,
                        'height' => 400,
                    ],
                    'speekblock' => [
                        'action' => FPM::ACTION_ADAPTIVE_THUMBNAIL,
                        'width' => 485,
                        'height' => 320,
                    ],
                ],
                /*'module' => [
                    'size' => [
                        'action' => 'frame',
                        'width' => 400,
                        'height' => 200,
                        'startX' => 100,
                        'startY' => 100,
                    ],
                ],*/
                'admin' => [
                    'file' => [
                        'action' => FPM::ACTION_THUMBNAIL,
                        'width' => 100,
                        'height' => 100,
                    ]
                ]
            ],
        ],
    ],
    'components' => [
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'formatter' => [
            'timeFormat' => 'H:i:s',
            'dateFormat' => 'Y-m-d',
        ],
        'i18n' => [
            'class' => 'Zelenin\yii\modules\I18n\components\I18N',
            'languages' => [
                'ru',
                'en',
                'uk',
            ]
        ],
        'urlManager' => [
            'class' => '\common\components\UrlManager',
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'enableLanguageDetection' => false,
            'rules' => [],
        ],
    ],
    'sourceLanguage' => 'xx',
    'language' => 'ru',
];
