<?php

namespace common\components\model;

/**
 * Class ActiveRecord
 * @package common\components\model
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    use Helper;
    use RelatedFormHelpTrait;

    public $showCreateButton = true;
    public $showUpdateButton = true;
    public $showDeleteButton = true;

    /**
     * @inheritdoc
     * @return DefaultQuery
     */
    public static function find()
    {
        return new DefaultQuery(get_called_class());
    }

    /**
     * @return mixed
     */
    public function getLabelForUpdateAction()
    {
        switch (true) {
            case $this->hasAttribute('label') && $this->label:
                return $this->label;
            case $this->hasAttribute('name') && $this->name:
                return $this->name;
        }

        return $this->id;
    }
}
