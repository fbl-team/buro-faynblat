<?php
/**
 * Author: metal
 * Email: metal
 */

namespace common\helpers;

use common\models\Language;
use Yii;

/**
 * Class LanguageHelper
 * @package common\helpers
 */
class BlogsHelper
{
    const PAGE_ALL_BLOG_PAGINATION_COUNT = 4;
    const SHOW_ON_MAIN = 'show_on_main';
    const SHOW_ON_ABOUT = 'show_on_about';
    const SHOW_ON_SERVICES = 'show_on_services';
    const SHOW_LAST = 'show_last';

    const CONTENT_TYPE_TWO_TEXT_COLUMN = 0;
    const CONTENT_TYPE_SINGLE_IMAGE = 1;
    const CONTENT_TYPE_TWO_IMAGE = 2;

    public static function allBlogsWhatViewRender($index)
    {
        $result = '_item_1_3';
        switch (++$index) {
            case 1:
                $result = '_item_1_3';
                break;
            case 2:
                $result = '_item_2_4';
                break;
            case 3:
                $result = '_item_1_3';
                break;
            case 4:
                $result = '_item_2_4';
                break;
            default:
                $result = '_item_1_3';
                break;
        }
        return $result;
    }

    public static function displaySvgOrNot($count)
    {
        return $count >= 3 ? 'show-svg' : 'hide-svg';
    }

}
