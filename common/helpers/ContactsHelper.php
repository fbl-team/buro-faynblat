<?php
/**
 * Author: metal
 * Email: metal
 */

namespace common\helpers;

use common\models\Language;
use Yii;

/**
 * Class LanguageHelper
 * @package common\helpers
 */
class ContactsHelper extends AbsoluteLocaleUrlTextHelper
{
    const CONTACTS_COLUMN_1 = 1;
    const CONTACTS_COLUMN_2 = 2;
    const CONTACTS_COLUMN_3 = 3;
    const CONTACTS_COLUMN_4 = 4;
}
