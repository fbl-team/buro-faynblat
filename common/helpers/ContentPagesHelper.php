<?php
/**
 * Author: metal
 * Email: metal
 */

namespace common\helpers;

use common\models\Language;
use frontend\helpers\SiteUrlHelper;
use Yii;

/**
 * Class LanguageHelper
 * @package common\helpers
 */
class ContentPagesHelper
{
    const ELEMENT_DESIGN = 'design';
    const ELEMENT_BUILDINGS = 'buildings';
    const ELEMENT_LANDSCAPING = 'landscaping';
    const ELEMENT_ARCHITECTURE = 'architecture';
    const ELEMENT_PLANNING = 'planning';

    public static function getPageLabelList()
    {
        return [
            self::ELEMENT_DESIGN => Yii::t('back/menu', 'Design'),
            self::ELEMENT_BUILDINGS => Yii::t('back/menu', 'Buildings'),
            self::ELEMENT_LANDSCAPING => Yii::t('back/menu', 'Landscaping'),
            self::ELEMENT_ARCHITECTURE => Yii::t('back/menu', 'Architecture'),
            self::ELEMENT_PLANNING => Yii::t('back/menu', 'Planning')
        ];
    }

    public static function getPageLabel($alias)
    {
        $itemsArray = self::getPageLabelList();
        return $itemsArray[$alias] ? $itemsArray[$alias] : '';
    }
}
