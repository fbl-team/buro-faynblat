<?php
/**
 * Author: metal
 * Email: metal
 */

namespace common\helpers;

use common\models\Language;
use Yii;
use yii\helpers\Html;

/**
 * Class LanguageHelper
 * @package common\helpers
 */
class AbsoluteLocaleUrlTextHelper
{
    const TYPE_LOCALE_LINK = 0;
    const TYPE_ABSOLUTE_LINK = 1;
    const TYPE_JUST_TEXT = 2;
    const TYPE_EMAIL = 3;
    const TYPE_PHONE = 4;

    /**
     * @param bool $forFront
     * @return array
     */
    public static function getTypeArray()
    {

        $result = [
            self::TYPE_LOCALE_LINK => \Yii::t('footer', 'Locale link'),
            self::TYPE_ABSOLUTE_LINK => \Yii::t('footer', 'Absolute link'),
            self::TYPE_JUST_TEXT => \Yii::t('footer', 'Just text'),
            self::TYPE_EMAIL => \Yii::t('footer', 'Email (mail-to link)'),
        ];

        return $result;

    }

    /**
     * @param bool $forFront
     * @return array
     */
    public static function getTypeArrayForContacts()
    {

        $result = [
//            self::TYPE_JUST_TEXT => \Yii::t('footer', 'Just text'),
            self::TYPE_EMAIL => \Yii::t('footer', 'Email (mail-to link)'),
            self::TYPE_PHONE => \Yii::t('footer', 'Phone (call-to link)'),
        ];

        return $result;

    }

    /**
     * @param bool $forFront
     * @return array
     */
    public static function getTypeArrayForAbout()
    {

        $result = [
            self::TYPE_JUST_TEXT => \Yii::t('footer', 'Just text'),
            self::TYPE_EMAIL => \Yii::t('footer', 'Email (mail-to link)'),
            self::TYPE_PHONE => \Yii::t('footer', 'Phone (call-to link)'),
        ];

        return $result;

    }

    /**
     * @param $elementId
     * @param bool $forFront
     * @return string
     */
    public static function getTypeValue($elementId)
    {

        return isset(self::getTypeArray()[$elementId]) ? self::getTypeArray()[$elementId] : 'type for select type id does not exist!';

    }

    public static function renderHtmlForElementType($element)
    {
        $result = '';
        switch ($element->content_type) {
            case self::TYPE_JUST_TEXT:
                $result .= Html::beginTag('tr');
                $result .= Html::tag('td', $element->label);
                $result .= Html::tag('td', $element->content);
                $result .= Html::endTag('tr');
                break;
            case self::TYPE_EMAIL:
                $result .= Html::beginTag('tr');
                $result .= Html::tag('td', $element->label);
                $result .= Html::tag('td', $element->content);
                $result .= Html::endTag('tr');
                break;
            case self::TYPE_PHONE:
                $result .= Html::beginTag('tr');
                $result .= Html::tag('td', $element->label);
                $result .= Html::tag('td', $element->content);
                $result .= Html::endTag('tr');
                break;
            default:
                break;
        }
        return $result;
    }

    public static function renderHtmlForElementTypeOnContacts($element)
    {
        $result = '';
        switch ($element->content_type) {
            case self::TYPE_JUST_TEXT:
                $result .= Html::beginTag('div', ['class' => 'contact__info contact__info__last']);
                $result .= Html::tag('span', $element->label, ['class' => 'contact__label']);
                $result .= Html::tag('p', $element->content, ['class' => 'contact__text']);
                $result .= Html::endTag('div');
                break;
            case self::TYPE_EMAIL:
                $result .= Html::beginTag('div', ['class' => 'contact__info']);
                $result .= Html::tag('span', $element->label, ['class' => 'contact__label']);
                $result .= Html::tag('a', $element->content, ['class' => 'contact__link', 'href' => 'mailto:'.$element->content]);
                $result .= Html::endTag('div');
                break;
            case self::TYPE_PHONE:
                $result .= Html::beginTag('div', ['class' => 'contact__info']);
                $result .= Html::tag('span', $element->label, ['class' => 'contact__label']);
                $result .= Html::tag('a', $element->content, ['class' => 'contact__link', 'href' => 'callto:'.$element->content]);
                $result .= Html::endTag('div');
                break;
            default:
                break;
        }
        return $result;
    }

}
