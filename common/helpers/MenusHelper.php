<?php
/**
 * Author: metal
 * Email: metal
 */

namespace common\helpers;

use common\models\Language;
use frontend\helpers\SiteUrlHelper;
use frontend\modules\contentPages\models\ContentPages;
use Yii;

/**
 * Class LanguageHelper
 * @package common\helpers
 */
class MenusHelper
{
    const HEADER_ELEMENTS = 1;
    const FOOTER_ELEMENTS = 2;
    const RIGHT_ELEMENTS = 3;

    public static function getMenusItemList()
    {
        return $itemsArray = [
            '/home' => SiteUrlHelper::createHomeUrl(),
            '/projects' => SiteUrlHelper::createAllProjectsUrl(),
            '/blog' => SiteUrlHelper::createAllBlogsUrl(),
            '/about' => SiteUrlHelper::createAboutUrl(),
            '/services' => SiteUrlHelper::createServicesUrl(),
            '/media' => SiteUrlHelper::createMediaUrl(),
            '/contacts' => SiteUrlHelper::createContactsUrl(),
            '/careers' => SiteUrlHelper::createCareersPagesUrl(),
            '/design' => SiteUrlHelper::createContentPageUrl(['alias' => ContentPagesHelper::ELEMENT_DESIGN]),
            '/buildings' => SiteUrlHelper::createContentPageUrl(['alias' => ContentPagesHelper::ELEMENT_BUILDINGS]),
            '/landscaping' => SiteUrlHelper::createContentPageUrl(['alias' => ContentPagesHelper::ELEMENT_LANDSCAPING]),
            '/architecture' => SiteUrlHelper::createContentPageUrl(['alias' => ContentPagesHelper::ELEMENT_ARCHITECTURE]),
            '/planning' => SiteUrlHelper::createContentPageUrl(['alias' => ContentPagesHelper::ELEMENT_PLANNING]),
        ];
    }

    public static function getMenusItemUrl($urlFromBase)
    {
        $itemsArray = self::getMenusItemList();
        return $itemsArray[$urlFromBase] ? $itemsArray[$urlFromBase] : '';
    }
}
