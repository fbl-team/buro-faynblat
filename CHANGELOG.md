Melon.ng Change Log
===================

0.6.2 Sep 21, 2016
---------------------
- Add: "Configurator" module for easy creating entities backend forms
 without creating tables for each entity(tolik505)
- Add: ajax redirect handler in common.js(dvixi)
- Add: possibility to add custom seo text and noindex tag in seo-behavior(hanterrian)
- Fix: application is now PosgtreSQL ready(dvixi)
- Chg: update bower-asset/cropper package to latest version(dvixi)

0.6.1 May 23, 2016
---------------------
- Add: backend related form widget(tolik505)
- Add: backend form config tabs(tolik505)
- Add: Docs for new Gii(notgosu)
- Fix: small html bugs fixed(dvixi)
- Fix: change cropper viewMode to prevent crop outside of image(notgosu)
- Fix: clear modal window after close(notgosu)
- Chg: updated to latest Yii version
- Chg: switch to another main page tree widget(tolik505)

0.6.0beta April 19, 2016
------------------------
- Chg: Updated backend Gii CRUD, Model generators. Backend models now 
separated from common(notgosu)
- Enh: Use `common\components\Schema` to support `comment()` method inside
 migrations code(metal, notgosu)
